# To Run Test
npm run test

# To run application in DEV mode
npm run start:dev

# To make the build
npm run build

# To build and start
npm run start 


# After running the application please hit below url to get the view Data
# GET
http://localhost:8000/getViewData


# After running the application please hit below url to get the view Data
# GET
http://localhost:8000/verifyCourse?id={courseId}

# POST Call to save result in DB:
http://localhost:8000/saveCourse

# format

{
  "fname":"Tilax",
  "lname":"Yoza",
  "street_address_1":"sdcsd sds",
  "city":"Gurugram",
  "email":"errahulsh@gmail.com",
  "course_id":3,
  "payment_status":"SUCCESS",
  "order_total":130,
  "course_amount_paid":120
  
  }




