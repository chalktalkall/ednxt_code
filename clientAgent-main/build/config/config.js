"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
var MSSQL_HOST = process.env.MSSQL_HOST || 'localhost';
var MSSQL_DATABASE = process.env.MSSQL_DATABASE || '';
var MSSQL_USER = process.env.MSSQL_HOST || 'SA';
var MSSQL_PASS = process.env.MSSQL_HOST || 'Root@12345';
var MSSQL = {
    user: MSSQL_USER,
    password: MSSQL_PASS,
    server: MSSQL_HOST,
    database: MSSQL_DATABASE,
    connectionTimeout: 10000,
    options: {
        encrypt: true,
        enableArithAbort: true
    },
    pool: {
        autostart: true
    },
    beforeConnect: function (conn) {
        conn.on('debug', function (message) { return console.info(message); });
        conn.on('error', function (err) { return console.error(err); });
        //  conn.removeAllListeners();
    }
};
var SERVER_HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
var SERVER_PORT = process.env.SERVER_PORT || 8000;
var SERVER = {
    hostname: SERVER_HOSTNAME,
    port: SERVER_PORT
};
var config = {
    mssql: MSSQL,
    server: SERVER
};
exports.default = config;
