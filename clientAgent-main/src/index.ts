import express from 'express';
import sql from 'mssql';
import config from './config/config';

const app = express();
const PORT = config.server.port;
app.use(express.json());


app.get('/getViewData', async (req, res) => {

    var connection: sql.ConnectionPool = new sql.ConnectionPool(config.mssql, function (err: any) {
       
        if (err != null) {
            console.warn("Issue with connecting to SQL Server!");
            return res.send("Issue with connecting to SQL Server"+err);
        }
        else {
            var requestQuery = new sql.Request(connection);
            requestQuery.query`SELECT * FROM LMS_Data`.then(res1 => { 
                return res.send(res1.recordsets[0]);
            });
        }
    });  
   
});

app.get('/verifyCourse', async (req, res) => {
   
    var connection: sql.ConnectionPool = new sql.ConnectionPool(config.mssql, function (err: any) {
       
        if (err != null) {
            console.warn("Issue with connecting to SQL Server!");
            return res.send("Issue with connecting to SQL Server"+err);
        }
        else {
            var requestQuery = new sql.Request(connection);
            requestQuery.query("SELECT * FROM LMS_Data where id="+req.query.id).then(data => { 
                return res.send(data.recordsets[0]);
            });
        }
    }); 
});

app.post('/saveCourse', async (req, res) => {
    var jsonData=req.body;
    var connection: sql.ConnectionPool = new sql.ConnectionPool(config.mssql, function (err: any) {

        if (err != null) {
            console.warn("Issue with connecting to SQL Server!");
            return res.send("Issue with connecting to SQL Server"+err);
        } else {
            var requestQuery = new sql.Request(connection);
            requestQuery.query("INSERT INTO Receipt (fname, lname, street_address_1, city, email, course_id,payment_status,order_total,course_amount_paid) VALUES ('"+jsonData.fname
            +"','"
            +jsonData.lname+"', '"
            +jsonData.street_address_1+"', '"
            +jsonData.city+"', '"
            +jsonData.email+"','"
            +jsonData.course_id+"','"
            +jsonData.payment_status+"','"
            +jsonData.order_total+"','"
            +jsonData.course_amount_paid+"')").then((data) => {
               if(Number(data.rowsAffected)>0){
                res.status(200).json({ status: "success"});
                res.send();
               }
             });
           
     }
    }
    );
   

});

app.get('/report', async (req, res) => {
   
   

});

app.listen(PORT, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`);
});

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next();
}
);
export default app;