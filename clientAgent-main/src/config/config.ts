
import sql from 'mssql';

require('dotenv').config()

const MSSQL_HOST = process.env.MSSQL_HOST || 'localhost';
const MSSQL_DATABASE = process.env.MSSQL_DATABASE || '';
const MSSQL_USER = process.env.MSSQL_HOST || 'SA';
const MSSQL_PASS = process.env.MSSQL_HOST || 'Root@12345';

var MSSQL: sql.config = {
    user:MSSQL_USER,
    password: MSSQL_PASS,
    server: MSSQL_HOST,
    database: MSSQL_DATABASE,
    connectionTimeout: 10000,
    options: {
        encrypt: true,
        enableArithAbort:true 
    },
    pool: {
        autostart: true
    },
    beforeConnect: conn => {
        conn.on('debug', message => console.info(message));
        conn.on('error', err => console.error(err));
     //  conn.removeAllListeners();
    }
}

const SERVER_HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
const SERVER_PORT = process.env.SERVER_PORT || 8000;

const SERVER = {
    hostname: SERVER_HOSTNAME,
    port: SERVER_PORT
};

const config = {
    mssql: MSSQL,
    server: SERVER
};

export default config;