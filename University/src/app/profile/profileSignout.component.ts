import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivateGuardService } from '../shared/services/activate-guard.service';
import { Router} from '@angular/router';
import { CartService } from '../shared/services/cart.service';
import { MainService } from '../shared/services/main.service';



@Component({
      selector: 'app-profile',
      templateUrl: './profile.component.html',
      styleUrls: ['./profile.component.scss']
})


export class ProfileSignoutComponent implements OnInit {

public loading:boolean=false;

        errorMsz:string;
        successMsz:string;
        attentionMsz:string;
        //phone:any;
 constructor( public cartService:CartService,  private activateGuard: ActivateGuardService,private router:Router, public service: MainService) {

  }

  ngOnInit() {
       this.cartService.logout().then((val) =>this.callInit());
  }
  callInit(){
      this.activateGuard.setCanActivate(false);
      this.router.navigate(['/home']);
  }
  getBadgeCount(){
    return this.cartService.getBadgeCount(  );
  }

}
