import { Component, OnInit } from '@angular/core';

import { CustomerLogin } from '../profile/profile.component'
import { MainService } from '../shared/services/main.service';
import { Http } from '@angular/http'


@Component({
  selector: 'app-profile-user-list',
  templateUrl: './profile-user-list.component.html',
  styles: []
})
export class ProfileUserListComponent implements OnInit {

  billInfos: Array<any> = [];
  customerData: Array<any> = [];
  //User INfo on profile -- this is data fom login API
  cl_customerId = (this.service.customerInfo["customerId"]);
  cl_firstName = (this.service.customerInfo["firstName"]);

  /* cl_restaurantId=(this.service.customerInfo["restaurantId"]);*/
  cl_phone = (this.service.customerInfo["phone"]);
  cl_email = (this.service.customerInfo["email"]);
  cl_rewardPoints = (this.service.customerInfo["rewardPoints"]);

  //customer info from get Customer API(ACCOUNT BALANCE)
  credit_balance = (this.service.customerInfo.credit["creditBalance"]);
  credit_type = (this.service.customerInfo.credit.creditType["banner"]);
  statement_billing_cycle = (this.service.customerInfo.credit.creditType["billingCycle"]);
  status = (this.service.customerInfo.credit["status"]);
  //avaliable credit
  Avil_credit = (this.service.customerInfo.credit["maxLimit"] - this.service.customerInfo.credit["creditBalance"]);
  //getuserBill Info here
  //abc_amount = (this.service.getCustomerCreditBillInfo["availableCredit"]);

  //variables to display in user profile
  constructor(public service: MainService, private http: Http) { }
  //this WorkSpace newGenApp
  ngOnInit() {
    let custData = this.service.customerInfo;
  }


}
