import { Component, OnInit } from '@angular/core';
import { MainService } from '../shared/services/main.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
 title:string;
  constructor(public service:MainService) { }

  ngOnInit() {
    this.title=this.service.headerTitle;
  }

}
