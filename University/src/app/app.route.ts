import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule,ExtraOptions } from '@angular/router';
import { ActivateGuardService,CanDeactivateGuard } from './shared/services/activate-guard.service';

import { LocationComponent } from './home/location.component';
import { MenuComponent } from './menu/menu.component';
import { HistoryComponent } from './history/history.component';
import { CartComponent } from './cart/cart.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileSignoutComponent} from './profile/profileSignout.component';
import { FavouriteComponent } from './favourite/favourite.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { CuisinesComponent } from './courses/cuisines.component';
import {ProfileUserListComponent} from './profile-user-list/profile-user-list.component';
import { PrivacyPoliciesComponent } from './policies/privacy-policies/privacy-policies.component';
import { TermsConditionsComponent } from './policies/terms-conditions/terms-conditions.component';
import { ContactUsComponent } from './policies/contact-us/contact-us.component';
import { AboutUsComponent } from './policies/about-us/about-us.component';
import {InstituteCoursesComponent} from './institute-courses/institute-courses.component';

const routerOptions: ExtraOptions = {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    // scrollOffset: [0, 64],
    useHash: true ,
    enableTracing: false
  };

const MAINMENU_ROUTES: Routes = [
    // full : makes sure the path is absolute path
    {
         path: '',
         redirectTo: '/home',
         pathMatch: 'full'
    },
    {
        path: 'policies',
        component: PrivacyPoliciesComponent
    },
    {
        path: 'terms',
        component: TermsConditionsComponent
    },
    {
        path: 'contact',
        component: ContactUsComponent
    },
    {
        path: 'about',
        component: AboutUsComponent
    },
    {
         path: 'home',
         component: LocationComponent
     },
    {
        path: 'menu',
        component: MenuComponent,
        canActivate: [ActivateGuardService],
        canDeactivate: [CanDeactivateGuard]
     },
    {
         path: 'history',
         component: HistoryComponent ,
         canActivate: [ActivateGuardService]
    },
    {
        path: 'cart',
        component: CartComponent,
        canActivate: [ActivateGuardService],
        canDeactivate: [CanDeactivateGuard]
    },
    {
        path: 'courses',
        component: CuisinesComponent,
        canActivate: [ActivateGuardService]
    },
    {
         path: 'profile',
         component: ProfileComponent,
          canActivate: [ActivateGuardService]
    },
    {
         path: 'profileSignout',
         component: ProfileSignoutComponent,
          canActivate: [ActivateGuardService]
    },
     {
         path: 'profile-user-list',
         component: ProfileUserListComponent,
         canActivate: [ActivateGuardService]
    },
    {
        path: 'favourite',
        component: FavouriteComponent,
        canActivate: [ActivateGuardService]
    },

    {
        path: 'institute',
        component: InstituteCoursesComponent,
    },
    {
        path: '**',
        component: NotFoundComponent
    }

]
@NgModule({
    imports: [CommonModule, RouterModule.forRoot(MAINMENU_ROUTES, routerOptions)],
    exports: [RouterModule]
  })
export class CONST_ROUTING{

}
