import { Component, OnInit, Injectable, Input, ViewChild,TemplateRef} from '@angular/core';
import { MainService } from '../shared/services/main.service';
import { CartService } from '../shared/services/cart.service';
import { PlaceOrder } from '../shared/models/placeOrder';
import { Router } from '@angular/router';
import { Observable, timer } from 'rxjs';
import { map } from "rxjs/operators";
import { FormGroup, FormControl } from '@angular/forms';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { formatDate } from '@angular/common';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';
import { NgxSpinnerService } from "ngx-spinner";
 

export class CartCustomerLogin {
  appId: any;
  device: any;
  orgId: number;
  phoneNumber: any;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

@Injectable()
export class CartComponent implements OnInit {

  @ViewChild('otpCatcher') public otpCatcher;
  @ViewChild('onlyErrorMsz') public onlyErrorMsz;
  @ViewChild('onlySuccessMsz') public onlySuccessMsz;
  @ViewChild('onlyAttentionMsz') public onlyAttentionMsz;
  @ViewChild('fbNumberCatcher') public fbNumberCatcher;
  @ViewChild('otpSent') public otpSent;
  //@ViewChild('tt', {static: false}) cartTooltip: NgbTooltip;
  cartItemForToolTip:any;
  otpMessage:string;
  @ViewChild('customLoadingTemplate', { static: false }) customLoadingTemplate: TemplateRef<any>;
  public loadingTemplate: TemplateRef<any>;
 // tooltipCart: any;
  successMsz: string;
  errorMsz: string;
  attentionMsz: string;
  selected: boolean;
  @Input() restaurantId: any;
  title: string;
  restaurantTax: Array<any>;
  taxValue: number;
  defaultTax: any;
  overRiddenTax: any;
  cartCustomerLogin: CartCustomerLogin;
  customerData: any;
  countryCode: string="+91";
  otpmodi: any;
  @Input() isLogin = false;
  addresses: any = [];
  deliveryTimeToday: any;
  deliveryTimeTomorrow: any;
  deliverydays = ['Select Day', 'Today', 'Tommorow'];
  defaultDeliveryDay = 'deliveryToday';
  payments: string;
   paymentMode = ['Pay online'];
 // paymentMode = ['COD'];
  instructionTo = '';
  defaultDeliveryTime = '';
  customerFromInfo: FormGroup;
  customerBasicInfo: any;
  deliveryAreaList: any = [];
  addNewAddress: boolean;
  isAddressExist: boolean;
  isCustomerCredit: boolean = false;
  placeOrderNow: PlaceOrder;
  public loading: boolean = true;
  public displayCheckData: any;
  customerPhoneNo: any;
  otp: number;
  fbUserPhoneNo: any;
  cacheCustEmail: any;
  currency: string;
  //for pop up true/false
  isPopUp = false;
  @Input() initialStart: boolean = true;
  timer: Observable<number>;
  minDate;
  maxDate;
  selectedCourse;
  //@ViewChild('giftCardError')  giftCardError : Popup;
 
 constructor(public service: MainService,
    public analyticsService :GoogleAnalyticsService,
    public cartService: CartService, public router: Router,public storage: LocalStorage,public spinner: NgxSpinnerService) {
    for (const connectorsKey of Object.keys(this.cartService.socialConnector)) {
      if (this.cartService.socialConnector[connectorsKey].name == "Facebook") {
         this.initFbLogin(this.cartService.socialConnector[connectorsKey].appId);
      }
    }
  }

  ngOnInit() {

    this.spinner.show();
    if(this.cartService.timerSubscription!=undefined){
      this.cartService.timerSubscription.unsubscribe();
    }
    this.selected = true;
    this.cartService.getRestaurantFromCacheCart().then(()=>{
    this.cartService.getcartItems().then(()=>{
    this.cartService.restoreFromCache().then(() => this.callInit());
    })
    });
    if(this.cartService.selectedCourse==undefined){
     this.storage.getItem('selectedCourse').subscribe((selectedCourse:string) => {
      if (selectedCourse != null && selectedCourse != undefined) {
        this.cartService.selectedCourse= JSON.parse(selectedCourse);
      } 
    }, () => { });
  }
  }

  callInit() {
    if(this.service.selectPlatter!=undefined){
      this.analyticsService.eventEmitter(this.service.selectPlatter.name,"Cart", "/cart")
    }
    this.minDate=new Date();
    var date = new Date();
    var temp = date.setDate(date.getDate() + 30);
    if(this.cartService.validBy!=null){
      this.maxDate = new Date(this.cartService.validBy);
    }else{
    this.maxDate = new Date(temp);
    }
    if (this.cartService.getBadgeCount() == 0) {
      this.initialStart = false;
      if (this.service.restaurant == undefined &&    this.onlyAttentionMsz!=undefined  ) {
        this.attentionMsz = "Please select the location to be able to select course and place the order. Thank you";
        this.onlyAttentionMsz.show();
        this.timer = timer(3000); // 5000 millisecond means 5 seconds
        this.timer.subscribe(() => {
          this.spinner.hide();
          this.onlyAttentionMsz.hide();
          this.router.navigate(['/home']);
        });
      } else {
        // this.attentionMsz = "Your cart is empty please opt for courses from the menu. Thank you";
        // this.onlyAttentionMsz.show();
        // this.timer = timer(3000); // 5000 millisecond means 5 seconds
        // this.timer.subscribe(() => {
        //   this.onlyAttentionMsz.hide();
        //   this.router.navigate(['/menu']);
        // });
      }
    }
    if(this.service.restaurant!=undefined){
    this.service.selectedRestaurantId = this.service.restaurant.restaurantId;
    this.title = this.service.restaurant.restaurantName;
    this.restaurantTax = this.service.restaurant.taxList;
    if(this.service.restaurant !=undefined ||this.service.restaurant.countryCode!=undefined){
      this.countryCode = this.service.restaurant.countryCode;
    }
    this.currency = this.service.restaurant.currency;
  
    this.cartService.updateGrandTotal()
    this.getEmailFromCache();
    this.spinner.show();
    if (this.cacheCustEmail != undefined || this.cacheCustEmail != null) {
      this.getCustomerInformation(this.cacheCustEmail);
    } else if (this.service.customerInfo == undefined) {
      this.initializeForm();
    } else {
      this.getCustomerInformation(this.service.customerInfo.customers[0].phone);
    }

    this.cartService.updateGrandTotal()
  }
    this.cartCustomerLogin = {
      appId: null,
      device: null,
      orgId: 3,
      phoneNumber: '',
    };
    this.spinner.hide();
  }

  initFbLogin(appId) {
  }

  loginCustomer(email) {
    this.otpMessage="Grab a cup of coffee while we upadate your inbox with the Verification Code";
    if (!this.validateEmail(email)) {
      return false;
    }
    
    if(this.service.customerInfo==undefined){
       this.spinner.show();
      this.service.loginCustomer(email)
        .subscribe(
              data => {
                this.spinner.hide();
                this.otpMessage="";
                this.openOTPDialog(email);
                           },
        err => {//show error msg
          }
        );
        }else{
         // this.otpCatcher.hide();
         if(this.service.customerInfo.email!=null){
          this.getCustomerInformation(this.service.customerInfo.email);
         }

        }
  }

  cancelOTPBox() {
    this.otpSent.hide();
  }

  setDeliveryAddress(deliveryAddress) {
    this.addNewCustomerAddress(false);
    let areaFound = false;
    this.addresses.forEach((addressFromList) => {
      if (deliveryAddress === addressFromList.customerAddress) {
        this.deliveryAreaList.forEach(area => {
          if (area.name == addressFromList.deliveryArea) {
            this.cartService.orderDeliveryArea = area;
            areaFound = true;
          }
        });
      }
    });
    this.cartService.updateGrandTotal();
  }

  addNewCustomerAddress(value) {
    if (value) {
      this.addNewAddress = true;
      this.cartService.orderDeliveryArea = this.deliveryAreaList[0];
    } else {
      this.addNewAddress = false;
    }
  }

  applyGiftCard(code) {
    this.spinner.show();
    this.cartService.applyGiftCard(code).pipe(
      map((data: any) => {
        if (data.result == "SUCCESS") {
          this.successMsz = "Gift card applied";
          this.onlySuccessMsz.show();
          this.service.customerInfo = this.getCustomerInformation(this.service.customerInfo.customers[0].phone);
        } else if (data.result == "ERROR") {
          this.spinner.hide();
          this.errorMsz = "Invalid gift card code!";
          this.onlyErrorMsz.show();//new added popup
          setTimeout(() => {
            this.onlyErrorMsz.hide();
           this.spinner.hide();
          }, 3000);
        }
      })).subscribe();

  }

  getCouponAndGiftCardVerified(code, formData) {
    if (Number.isInteger(parseInt(code))) {
      if (code.length == 16) {
        var isValidEmail = this.validateEmail(formData.email);
        var isValidDeliveryArea = this.validateDeliveryArea(this.cartService.orderDeliveryArea.name);
        var isValidAddress = this.validateAddress(formData.address);
        var isValidName = this.validateName(formData.username);
        this.customerBasicInfo.orgId = this.service.restaurant.parentRestaurantId;
        if (isValidEmail && isValidDeliveryArea && isValidAddress && isValidName) {
          this.cartService.setCustomerInfo(this.customerBasicInfo).pipe(
            map((data: any) => {
              if (data.status = "success") {
                this.applyGiftCard(code);
              } else {
                return false;
              }
            })).subscribe();

        } else {
          return false;
        }
      } else {

        this.errorMsz = "Invalid Gift card redemption code. Please enter 16 digit numeric code!";
        this.onlyErrorMsz.show();; //new added popup
        setTimeout(() => {
          this.onlyErrorMsz.hide();
        }, 3000);
      }
    } else {
      if (this.cartService.couponApplied.length > 0) {
        for (const couponkey of Object.keys(this.cartService.couponApplied)) {
          if (this.cartService.couponApplied[couponkey].couponCode.toUpperCase( ) == code.toUpperCase( )) {
            this.errorMsz = code + " code has already been applied!"
            this.onlyErrorMsz.show();
            setTimeout(() => {
              this.onlyErrorMsz.hide();
            }, 3000);
            return false;
          }
        }
      }
      if(code!=undefined && code.length>0){
      this.cartService.applyCoupon(code).pipe(
        map((data: any) => {
          if (data.isValid) {
            if (data.isCouponApplicable) {
              if (this.cartService.grandTotal >= data.rules.minOrderPayment) {
                if (data.rules.isAbsoluteDiscount) {

                } else {
                  let couponData = {
                    name: data.couponName,
                    couponCode: data.couponCode,
                    isAbsoluteDiscount: data.rules.isAbsoluteDiscount,
                    discountValue: data.rules.discountValue,
                    couponSum: 0
                  };
                  this.cartService.couponApplied.push(couponData);
                  this.successMsz = "Coupon applied";
                  this.onlySuccessMsz.show();
                  this.cartService.updateGrandTotal();
                }
                this.storage.setItem('couponApplied', this.cartService.couponApplied).subscribe(() => { }, () => { });
              }
            } else {
              this.errorMsz = code + " code has already been applied!"
              this.onlyErrorMsz.show();
              setTimeout(() => {
                this.onlyErrorMsz.hide();
              }, 3000);
              return false;
            }
          } else {
            if(data.error.length>0){
              this.errorMsz=data.error;
            }else{
            this.errorMsz = code + " is invalid Coupon code! "
            }
            this.onlyErrorMsz.show();
            setTimeout(() => {
              this.onlyErrorMsz.hide();
            }, 3000);
          }

        })).subscribe();
      }else{
      
      }
    }
  }


  verifyOauthOtp(value: any,email:string) {
   this.service.verifyOtp(value,email).subscribe(
      data => {
        this.spinner.hide();
        if(data==true){
          this.service.getCustomerInfo(email, this.service.restaurant.restaurantId).subscribe(
            data => {
              this.service.customerInfo = data;
              this.isLogin = true;
              this.service.isLogin = true;
             this.spinner.hide();
              this.storage.setItem('email',email).subscribe(() => { }, () => { });
              this.initForm();
            });
        }else{
          this.openOTPDialog(email);
          this.errorMsz = "Wrong OTP. Please try again";
          this.onlyErrorMsz.show();
          setTimeout(() => {
            this.onlyErrorMsz.hide();
          }, 3000);
        }
      }, err => {
        this.errorMsz = "Wrong OTP. Please try again";
      this.onlyErrorMsz.show();
      setTimeout(() => {
        this.onlyErrorMsz.hide();
      }, 3000);
      this.openOTPDialog(email);
      });
  }
 
  
  openOTPDialog(email){
    this.service.openOtpBox();
    this.service.confirmed().subscribe(confirmed => {
      if (confirmed) {
        this.spinner.show();
        this.verifyOauthOtp(this.service.otp,email);
        }
      });
  }

  
  fbUserPhoneSubmit(value: any) {

    this.fbUserPhoneNo = value.fbUserPhoneNo;
    var phone = new String(this.fbUserPhoneNo);
    this.spinner.show();
    // if (phone.length == 10) {
    //   phone = this.service.restaurant.countryCode + "" + phone;
    // }

    this.service.getCustomerInfo(phone, this.service.restaurant.restaurantId).subscribe(
      data => {
        this.service.customerInfo = data;
        this.isLogin = true;
        this.spinner.hide();
        this.service.isLogin = true;
        // this.fbPhoneNumber.hide();
        this.fbNumberCatcher.hide();
        this.initForm();
      })
  }

  public initForm() {
    this.customerBasicInfo = this.service.customerInfo.customers[0];
    this.addresses = this.service.customerInfo.customerAddress;
    if (this.addresses != null && this.addresses.length == 0) {
      this.addNewAddress = true;
      this.isAddressExist = false;
    } else {
      this.addNewAddress = false;
      this.isAddressExist = true;
    }
    let defaultAddress = this.customerBasicInfo.address;

    if (defaultAddress == null && this.addresses.length > 0) {
      this.customerBasicInfo.addressId = this.addresses[0].id;
      this.customerBasicInfo.address = this.addresses[0].customerAddress;
      this.customerBasicInfo.deliveryArea = this.addresses[0].deliveryArea;
    } else {
      //  this.addresses.forEach((addressFromList) => {
      //         if(addressFromList.customerAddress==defaultAddress){
      //            this.customerBasicInfo.addressId=addressFromList.id;
      //           this.customerBasicInfo.address= addressFromList.customerAddress;
      //         }
      //      });

    }
    if ((this.customerBasicInfo != undefined) && (this.customerBasicInfo.firstName != null || this.customerBasicInfo.lastName != null)) {
      var username = this.customerBasicInfo.firstName + " " + this.customerBasicInfo.lastName;
      this.customerFromInfo = new FormGroup({
        'username': new FormControl(username.trim()),
        'email': new FormControl(this.customerBasicInfo.email),
        'classMode':new FormControl(this.getClassMode()),
        'address': new FormControl(this.cartService.locationAddress),
        'deliveryDateTime': new FormControl(this.cartService.dateTimeSelected),
        'giftcard': new FormControl(),
        'pay': new FormControl("Pay online"),
        'message': new FormControl("")

      });
    } else {
      this.initializeForm();
    }
  }

  validateName(name) {
    if (name == undefined || name.trim() == '') {
      this.attentionMsz = "Please enter your name to be able to place your order.";
      this.onlyAttentionMsz.show();
      setTimeout(() => {
        this.onlyAttentionMsz.hide();
      }, 3000);
      return false;
    } else {
      this.customerBasicInfo.firstName = name.split(' ').slice(0, -1).join(' ');
      this.customerBasicInfo.lastName = name.split(' ').slice(-1).join(' ');
      return true;
    }
  }

  
  validateClassMode(classMode:string) {
    if (classMode == undefined || classMode.trim() == '') {
      this.attentionMsz = "Please select the class mode.";
      this.onlyAttentionMsz.show();
      setTimeout(() => {
        this.onlyAttentionMsz.hide();
      }, 3000);
      return false;
    } else {
      return true;
    }
  }


  validateAddress(customerAddress) {
    if (customerAddress == null || customerAddress == '' || customerAddress == undefined) {
      this.attentionMsz = "Please enter an address to be able to place your order";
      this.onlyAttentionMsz.show();
      setTimeout(() => {
        this.onlyAttentionMsz.hide();
      }, 3000);

      return false;
    } else {
      if (Number.isInteger(parseInt(customerAddress))) {
        this.addresses.forEach((addressFromList) => {
          if (addressFromList.id == customerAddress) {
            this.customerBasicInfo.address = addressFromList.customerAddress;
          }
        });
      } else {
        this.customerBasicInfo.address = customerAddress;
      }

      return true;
    }
  }

  validateDeliveryTime(customerDeliveryTime) {

    if (customerDeliveryTime == null || customerDeliveryTime == '' || customerDeliveryTime == undefined) {
      this.attentionMsz = "Please enter a valid Estimated Time.";
      this.onlyAttentionMsz.show();;
      setTimeout(() => {
        this.onlyAttentionMsz.hide();
      }, 3000);
      return false;
    } else {
      this.customerBasicInfo.deliveryDateTime = formatDate(new Date(customerDeliveryTime), 'yyyy-MM-dd HH:mm', 'en-US', '+0530');
      if (customerDeliveryTime != "" || customerDeliveryTime != undefined) {
        this.cartService.dateTimeSelected = formatDate(new Date(customerDeliveryTime), 'yyyy-MM-dd HH:mm', 'en-US', '+0530');
      }else{
        return false;
      }
      return true;
    }
  }

  validateDeliveryArea(customerDeliveryArea) {

    if (customerDeliveryArea == null || customerDeliveryArea == '' || customerDeliveryArea == undefined) {
      this.attentionMsz = "Please enter a valid pick-up area to be able to place your order.";
      this.onlyAttentionMsz.show();;
      setTimeout(() => {
        this.onlyAttentionMsz.hide();
      }, 3000);
      return false;
    } else {
      this.customerBasicInfo.deliveryArea = customerDeliveryArea;
      return true;
    }
  }

  validateEmail(email) {
    var emailMatch = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    if (email != undefined && email != '' && email != null) {
      if (emailMatch.test(email)) {
        if(this.customerBasicInfo!=undefined)
          this.customerBasicInfo.email = email;
        return true;
      } else {
        this.attentionMsz = "Please enter a valid email address to get the invoice";
        this.onlyAttentionMsz.show();
        setTimeout(() => {
          this.onlyAttentionMsz.hide();
        }, 3000);
        return false;
      }
    } else {
      this.attentionMsz = "Please enter a valid email address to get the invoice";
      this.onlyAttentionMsz.show();
      setTimeout(() => {
        this.onlyAttentionMsz.hide();
      }, 3000);
      return false;
    }
  }
  showMenuPage() {
    this.router.navigate(['/menu'])
  }
  setDefaultDeliveryArea() {
    let isDeliveryAreaInValid = true;
    if (this.deliveryAreaList != undefined) {
      this.addresses.forEach((addressFromList) => {
        if (this.customerBasicInfo.address == addressFromList.customerAddress) {
          this.cartService.orderDeliveryArea = this.deliveryAreaList.filter(
            area => area.name === addressFromList.deliveryArea);
          if (this.cartService.orderDeliveryArea[0] != undefined) {
            this.cartService.orderDeliveryArea = this.cartService.orderDeliveryArea[0];
            isDeliveryAreaInValid = false;

          } else {
            this.cartService.orderDeliveryArea = this.deliveryAreaList[0];
            isDeliveryAreaInValid = false;
          }
        }
      });

      if (isDeliveryAreaInValid) {
        this.cartService.orderDeliveryArea = this.deliveryAreaList[0];
      }
    }
    this.cartService.updateGrandTotal();
  }

  initializeForm() {

    this.addNewAddress = true;
    this.customerFromInfo = new FormGroup({
      'username': new FormControl(),
      'email': new FormControl(),
      'classMode': new FormControl(),  
      'address': new FormControl(this.cartService.locationAddress),
      'deliveryDay': new FormControl("Today"),
      'deliveryDateTime': new FormControl(this.cartService.dateTimeSelected),
      // 'deliveryArea':new FormControl("Select Day"),
      'giftcard': new FormControl(),
      'pay': new FormControl("COD"),
      'message': new FormControl(""),
    });
    this.cartService.updateGrandTotal();
  }

  getCustomerInformation(email) {
    this.spinner.show();
    if(email!=null){
    this.service.getCustomerInfo(email, this.service.restaurant.restaurantId).subscribe(
      data => {
        this.service.customerInfo = data;
        this.isLogin = true;
        this.service.isLogin = true;
        this.storage.setItem('email', email).subscribe(() => { }, () => { });
        this.cartService.verifyCoupon();
        this.spinner.hide();
        this.initForm();
      });
    }
  }

  validateFields(formData:any) {
    if (this.validateName(formData.username) &&
      this.validateEmail(formData.email)) {
      this.spinner.hide();
      return true;
    } else {
      this.spinner.hide();
      return false;
    }
  }

 openBottomCartToolTip(section:any,tt:any){
  this.cartItemForToolTip = section[0].items;
  if (tt.isOpen()) {
      tt.close()
  } else {
      tt.placement="right";
      tt.triggers="manual";
      tt.container="body";
      tt.autoClose=true;
      tt.open()
  }
 }

 getClassMode(){
  for (const key of Object.keys(this.cartService.itmes)) {
    if(this.cartService.itmes[key].classMode!=null || this.cartService.itmes[key].classMode!=undefined ){
      if(this.cartService.itmes[key].classMode=="Both"){
        return "Online";
      }else{
        return this.cartService.itmes[key].classMode;
      }
    }
  }

 }
  placeOrderButton(formData) {

    this.placeOrderNow = new PlaceOrder();
    var isValid = this.validateFields(formData);
    if (!(isValid)) {
      return isValid;
    }
    if (Object.keys(this.cartService.itmes).length < 1) {
      this.attentionMsz = "Please add item to the cart, to be able to place the order";
      this.onlyAttentionMsz.show();
      return false;
    }

    this.placeOrderNow.placeOrder.order.orderSource = "Website";
    this.spinner.show();
   
    this.placeOrderNow.placeOrder.order.deliveryDateTime = this.cartService.dateTimeSelected;
    if (this.cartService.grandTotal == 0) {
      this.placeOrderNow.placeOrder.order.paymentMethod = "CUSTOMER CREDIT";
    } else {
      if (formData.pay == "Pay online") {
        this.placeOrderNow.placeOrder.order.paymentMethod = "Online";
      } else if (formData.pay == "Mobikwik") {
        this.placeOrderNow.placeOrder.order.paymentMethod = "MobiKwikWallet";
      } else if (formData.pay == "PayTM") {
        this.placeOrderNow.placeOrder.order.paymentMethod = "payTm";
      } else if (formData.pay == "COD") {
        this.placeOrderNow.placeOrder.order.paymentMethod = "COD";
      } else {
        this.placeOrderNow.placeOrder.order.paymentMethod = formData.pay;
      }
    }

    this.placeOrderNow.placeOrder.order.noOfPeople = this.cartService.noOfPlates;
    this.placeOrderNow.placeOrder.order.instructions = formData.message; //deliveryInstruction
    this.placeOrderNow.Order.classMode=formData.classMode;
        for (const key of Object.keys(this.cartService.couponApplied)) {
      this.placeOrderNow.Order.couponCode.push(this.cartService.couponApplied[key].couponCode);
    }
    for (const key of Object.keys(this.cartService.itmes)) {
      this.placeOrderNow.placeOrder.order.items.push(this.cartService.itmes[key]);
    }

    this.placeOrderNow.placeOrder.customer.address = this.customerBasicInfo.address;
    this.placeOrderNow.placeOrder.customer.city = this.service.restaurant.city;
    this.placeOrderNow.placeOrder.customer.email = formData.email;
    this.placeOrderNow.placeOrder.customer.name = formData.username;
    this.placeOrderNow.placeOrder.customer.phone = this.customerBasicInfo.phone;
    this.placeOrderNow.placeOrder.customer.latitude = this.service.lat;
    this.placeOrderNow.placeOrder.customer.longitude = this.service.lng;
    this.placeOrderNow.customer.id = this.customerBasicInfo.customerId;
    this.placeOrderNow.placeOrder.countryId = this.service.restaurant.restaurantId;
    
    this.cartService.placeOrder(this.placeOrderNow.placeOrder).pipe(
      map((data: any) => {
        if (data.status == "success") {
          if (data.paymentType == "Online") {
            this.cartService.confirmOrderAndPayment(data.paymentType, data.Id);
          } 
         
        } else {
          if(data.error.length>0){
            this.errorMsz=data.error;
          }else{
            this.errorMsz = "Error has happned. Please try to place an order again.";
          }
          this.spinner.hide();
          this.onlyErrorMsz.show();
          setTimeout(() => {
            this.onlyErrorMsz.hide();
          }, 3000);
        }
      })).subscribe();
    this.cartService.displayInvoice = false;

  }

  removeCoupon(coupon) {
    this.cartService.couponApplied.forEach((couponFromList) => {
      if (couponFromList.couponCode == coupon) {
        this.cartService.couponApplied.splice(this.cartService.couponApplied.indexOf(couponFromList), 1);
        this.storage.setItem('couponApplied', this.cartService.couponApplied).subscribe(() => { }, () => { });
        this.cartService.updateGrandTotal()
      }

    });
  }

  ngAfterContentInit() {
   this.spinner.hide();
  }

  private handleError(error) {
    console.error('Error processing action', error);
  }

  // method for displaying the dishDetails
  AddOnDisplay() {
    this.isPopUp = true;
  }

  getEmailFromCache() {
    this.storage.getItem('email').subscribe((phoneNumber) => {
      if (phoneNumber != null) {
        this.cacheCustEmail = phoneNumber;
        this.getCustomerInformation(this.cacheCustEmail);
      }
    }, () => { });
  }

  

  showErrorMessage() {
    this.onlyErrorMsz.show();
  }

  showSuccessMessage() {
    this.onlySuccessMsz.show();
  }

  showAttentionMessage() {
    this.onlyAttentionMsz.show();
  }

}
