export class TaxInCart{
    constructor(public taxTypeId:number,
                public restaurentId:number,
                public name:string,
                public taxValue:number){}
}