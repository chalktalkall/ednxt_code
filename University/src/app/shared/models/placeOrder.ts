
export class PlaceOrder{

public discountList = {
    id:Number,
   name:String,
   category:String,
   type:String,
   value:Number
}

public customer={
   name:"",
   phone:"",
   email: "",
   address:"",
   deliveryArea:"",
   latitude:0.0,
   longitude:0.0,
   city:"",
   id: Number
}

 public  Order={
   instructions:"",
   paymentMethod:"",
   orderSource:"",
   noOfPeople:0,
   deliveryCharges:Number,
   deliveryDateTime:"", // format MM-DD-YYYY HH:MM
   couponCode:[],
   items:[],
   classMode:""
}
public countryId:number;

public placeOrder ={order:this.Order, customer:this.customer, countryId:this.countryId }

}
