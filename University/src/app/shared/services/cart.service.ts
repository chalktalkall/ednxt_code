import { Injectable } from '@angular/core';
import { OrderItem, AddOn, Section, Items } from '../models/orderItem';
import { RequestOptions, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { MainService } from '../services/main.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { takeUntil, take } from 'rxjs/operators';
import { Subject, timer, Subscription, Observable } from 'rxjs';

export const PLATTER_ADDED = "PLATTER_ADDED";
export const ADDON_ADDED = "ADDON_ADDED";
export const ADDON_PLATTER = "ADDON_PLATTER";
export const CANT_CLUB = "CANT_CLUB";
export const OVERRIDE_PLATTER = "OVERRIDE_PLATTER";

@Injectable()
export class CartService {

  itmes: Map<number, OrderItem> = new Map<number, OrderItem>();
 // totalItemCount = 0;
  selectedDynCuisine;
  subTotal: number = 0;
  tokenAmount:number=0;
  grandTotal: number = 0;
  dishTaxDefined: Array<any>;
  tax: Map<string, number> = new Map<string, number>();
  orderDeliveryArea: any;
  deliveryTime: any = [];
  applyTaxToDeliveryCharge: boolean = false;
  customerCreditBalance: number = 0;
  grandTotalWithoutCC: number;
  couponDiscount = 0;
  couponApplied: any = [];
  couponList: any = [];
  isCustomerCredit: boolean = false;
  displayInvoice: boolean = false;
  socialConnector = [];
  validBy=null;
  isDeliveryChargeApplicable: boolean = false;
  constructor(private router:Router,public mainService: MainService,private http: HttpClient, public storage: LocalStorage) { }

  locationAddress:string;
  foodType:string;
  dateTimeSelected:string;
  noOfPlates:number=1;
  selectedCourse;
  isOpen:boolean=false;

  _payOnline = "restaurant/stripeCheckout?";
  _payByMobikwikWallet = "restaurant/redirectToMobiKwik?";
  _payBypayTm = "restaurant/redirectToPayTm?";
  _setCustInfo = 'customer/setCustomerInfo.json';

  removePlatters(){
    this.itmes = new Map<number, OrderItem>();
    this.updateGrandTotal();
    this.storage.setItem('items', JSON.stringify(this.itmes)).subscribe(() => { }, () => { });

  }

  
  addToCart(item, menu) {
    let returnStatus;
    if(Object.keys(this.itmes).length==0 || item.courseId!=undefined){
      if (this.itmes[item.itemId] == undefined || this.itmes[item.courseId] == undefined) {
        if (item.menuId != null || item.courseId != undefined) {
          var platterFound = false;
          for (const checkKey of Object.keys(this.itmes)) {
            if (this.itmes[checkKey].menuId == item.courseId) {
                this.itmes[checkKey] = new OrderItem(
                this.itmes[checkKey].itemName,
                this.itmes[checkKey].itemPrice,
                this.itmes[checkKey].imageUrl,
                this.itmes[checkKey].menuId,
                (Number(this.itmes[checkKey].quantity) + 1),
                this.itmes[checkKey].sections,
                this.itmes[checkKey].addOn,
                this.itmes[checkKey].displayprice,
                this.itmes[checkKey].instructions,
                this.itmes[checkKey].cuisineType,
                this.itmes[checkKey].lms_course_id);
              platterFound = true;
            }
          }
  
          if (platterFound == false) {
            var addOn = [];
            let sectionList = new Array<Section>();
            this.itmes[item.courseId] = new OrderItem(item.name, item.price, item.imageUrl, item.courseId, this.noOfPlates, sectionList, addOn,item.displayprice,item.course_format,item.id);
         //   this.totalItemCount++;
          }
          returnStatus= PLATTER_ADDED;
        } else {
          if(Object.keys(this.itmes).length==0){
            this.addToCard(menu,menu);
            this.addAddOns(item,menu);
            returnStatus=ADDON_PLATTER;
          }else{
           let isAddOnAdded =  this.addAddOns(item,menu);
           if(isAddOnAdded==true){
             returnStatus=ADDON_ADDED;
           }else{
             returnStatus=OVERRIDE_PLATTER;
           }
           
        }
      }
        this.storage.setItem('items', JSON.stringify(this.itmes)).subscribe(() => { }, () => { });
        this.storage.setItem('selectedCourse', JSON.stringify(item)).subscribe(() => { }, () => { });
        this.selectedCourse=item;

      } else {
        if (item.menuId != null || item.menuId != undefined) {
          this.incermentItemCount(item.menuId, false, 0);
        }
      }
      this.subTotal += item.price;
      this.updateGrandTotal();
       return returnStatus;
     }else{
       return OVERRIDE_PLATTER;
   }

  }

   addToCard(item, menu) {
    let returnStatus;
    if(Object.keys(this.itmes).length==0 || item.itemId!=undefined){
      if (this.itmes[item.itemId] == undefined || this.itmes[item.menuId] == undefined) {
        if (item.menuId != null || item.menuId != undefined) {
          var platterFound = false;
          for (const checkKey of Object.keys(this.itmes)) {
            if (this.itmes[checkKey].menuId == item.menuId) {
                this.itmes[checkKey] = new OrderItem(
                this.itmes[checkKey].itemName,
                this.itmes[checkKey].itemPrice,
                this.itmes[checkKey].imageUrl,
                this.itmes[checkKey].menuId,
                (Number(this.itmes[checkKey].quantity) + 1),
                this.itmes[checkKey].sections,
                this.itmes[checkKey].addOn,
                this.itmes[checkKey].displayprice,
                this.itmes[checkKey].instructions,
                this.itmes[checkKey].cuisineType);
              platterFound = true;
            }
          }
  
          if (platterFound == false) {
            var addOn = [];
            let sectionList = new Array<Section>();
            for (const key of Object.keys(item.sections)) {
              let itemList = new Array<Items>();
              for (const itemKey of Object.keys(item.sections[key].items)) {
                  itemList.push(new Items(
                  item.sections[key].items[itemKey].itemId,
                  item.sections[key].items[itemKey].countryId,
                  item.sections[key].items[itemKey].vendorId,
                  item.sections[key].items[itemKey].name,
                  item.sections[key].items[itemKey].price,
                  item.sections[key].items[itemKey].quantityInG,
                  item.sections[key].items[itemKey].itemType,
                  item.sections[key].items[itemKey].isDishReplaceable,
                  item.sections[key].items[itemKey].displayPrice,
                  item.sections[key].items[itemKey].imageUrl

                ));
              }
  
              if (item.sections[key].platterSectionType != 'ADD_ON') {
                sectionList.push(new Section(
                  item.sections[key].sectionId,
                  item.sections[key].name,
                  item.sections[key].shortName,
                  item.sections[key].price,
                  item.sections[key].platterSectionType,
                  itemList));
              }
            }
            this.itmes[item.menuId] = new OrderItem(item.name, item.price, item.imageUrl, item.menuId, this.noOfPlates, sectionList, addOn,item.displayprice,item.classMode,item.id);
         //   this.totalItemCount++;
          }
          returnStatus= PLATTER_ADDED;
        } else {
          if(Object.keys(this.itmes).length==0){
            this.addToCard(menu,menu);
            this.addAddOns(item,menu);
            returnStatus=ADDON_PLATTER;
          }else{
           let isAddOnAdded =  this.addAddOns(item,menu);
           if(isAddOnAdded==true){
             returnStatus=ADDON_ADDED;
           }else{
             returnStatus=OVERRIDE_PLATTER;
           }
           
        }
      }
        this.storage.setItem('items', JSON.stringify(this.itmes)).subscribe(() => { }, () => { });
        
        this.storage.setItem('tempAddedItem', JSON.stringify(this.mainService.tempAddedItem)).subscribe(() => { }, () => { });

      } else {
        if (item.menuId != null || item.menuId != undefined) {
          this.incermentItemCount(item.menuId, false, 0);
        }
      }
      this.subTotal += item.price;
      this.updateGrandTotal();
       return returnStatus;
     }else{
       return OVERRIDE_PLATTER;
   }

  }

  addAddOns(item,menu){
    for (const key of Object.keys(this.itmes)) {
      if (this.itmes[key].menuId == menu.menuId) {
        if (this.itmes[key].addOn != null) {
          var addOnFound = false;
          for (var i = 0; i < this.itmes[key].addOn.length; i++) {
            if (this.itmes[key].addOn[i].itemId == item.itemId) {
              this.itmes[key].addOn[i].quantity = Number(this.itmes[key].addOn[i].quantity) + 1;
              addOnFound = true;
            }
          }
          if (!addOnFound) {
            this.itmes[key].addOn.push(new AddOn(item.name, item.price, item.itemId, this.noOfPlates,item.displayPrice,item.imageUrl));
          }

        } else {
          this.itmes[key].addOn.push(new AddOn(item.name, item.price, item.itemId, this.noOfPlates,item.displayPrice,item.imageUrl));
        }
          return true;
      }else{
           return false; 
     }
    }
  }


  
  getBadgeCount() {
    var addOnCount=0;
    for (const key of Object.keys(this.itmes)) {
      if(this.itmes[key].addOn.length>0){
        addOnCount= this.itmes[key].addOn.length;
      }
    }
    return Object.keys(this.itmes).length + addOnCount;
  }

  removePlatterFromCard(item) {
    if (this.itmes[item.menuId] != null) {
      delete this.itmes[item.menuId];
      this.subTotal -= item.price;
      // this.storage.removeItem('tempAddedItem').subscribe(() => {
      // }, () => { });
    }
    //this.itmes[item.menuId].quantity--;
   // this.totalItemCount--;
    this.storage.setItem('items', JSON.stringify(this.itmes)).subscribe(() => { this.updateGrandTotal();}, () => { });
    //this.storage.setItem('totalItemCount', this.totalItemCount).subscribe(() => { }, () => { });
    if( this.getBadgeCount()==0 ){
      this.isOpen=false;
    }
  }


  removeAddOnFromCard(item,addOn){
    var menuId=item.menuId;
    var addOnId=addOn.itemId;
    if (this.itmes[menuId].addOn != undefined && this.itmes[menuId].addOn != null) {

      for (var i = 0; i < this.itmes[menuId].addOn.length; i++) {
        if (this.itmes[menuId].addOn[i].itemId == addOnId) {
             this.itmes[menuId].addOn.splice(i, 1);
        }
      }
    }
    this.updateGrandTotal();
    this.storage.setItem('items', JSON.stringify(this.itmes)).subscribe(() => { }, () => { });
  }

  decrementItemCount(menuId: number, isAddon: boolean, addOnId: number) {
    if (isAddon) {
      if (this.itmes[menuId].addOn != undefined && this.itmes[menuId].addOn != null) {

        for (var i = 0; i < this.itmes[menuId].addOn.length; i++) {
          if (this.itmes[menuId].addOn[i].itemId == addOnId) {
              if(this.itmes[menuId].addOn[i].quantity>1){
                this.itmes[menuId].addOn[i].quantity--;
              }else{
               this.itmes[menuId].addOn.splice(i, 1);
              }
           // this.itmes[menuId].addOn.delete(i);
            // if (this.itmes[menuId].addOn[i].quantity > 0) {
            //   //this.itmes[menuId].addOn[i].quantity--;
            //   // if (this.itmes[menuId].addOn[i].quantity === 0) {
            //   //   this.itmes[menuId].addOn.delete(menuId);
            //   // }
            // }
          }
        }
      }
    } 
    
    this.updateGrandTotal();
    this.storage.setItem('items', JSON.stringify(this.itmes)).subscribe(() => { }, () => { });
    //this.storage.setItem('totalItemCount', this.totalItemCount).subscribe(() => { }, () => { });
  }

  incermentItemCount(menuId: number, isAddon: boolean, addOnId: number) {
    if (isAddon) {
      if (this.itmes[menuId].addOn != undefined && this.itmes[menuId].addOn != null) {
        for (var i = 0; i < this.itmes[menuId].addOn.length; i++) {

          if (this.itmes[menuId].addOn[i].itemId == addOnId) {
            this.itmes[menuId].addOn[i].quantity++;
          }
        }
      }
    } else if (!isAddon) {
      this.itmes[menuId].itemCount++;
      this.itmes[menuId].quantity++;
     // this.totalItemCount++;
    }
    this.updateGrandTotal();
    this.storage.setItem('items', JSON.stringify(this.itmes)).subscribe(() => { }, () => { });
   // this.storage.setItem('totalItemCount', this.totalItemCount).subscribe(() => { }, () => { });
  }

  calculateTax(definedtax: Array<any>, orderItem: Map<number, OrderItem>) {
    this.dishTaxDefined = definedtax;
    for (const key of Object.keys(orderItem)) {
      this.calculateItemTax(orderItem[key], true);
    }
  }


  calculateItemTax(item: OrderItem, add: boolean) {
    for (let i = 0; i < this.dishTaxDefined.length; i++) {
        if(this.dishTaxDefined[i].minValue<this.subTotal && this.dishTaxDefined[i].maxValue>=this.subTotal){
          this.addToTax(this.dishTaxDefined[i], item.itemPrice*item.quantity ,this.dishTaxDefined[i].taxValue,this.dishTaxDefined[i].chargeType, add);
          if (item.addOn != undefined && item.addOn != null) {
            for (var j = 0; j < item.addOn.length; j++) {
              this.addToTax(this.dishTaxDefined[i], item.addOn[j].itemPrice*item.addOn[j].quantity , this.dishTaxDefined[i].taxValue,this.dishTaxDefined[i].chargeType,add);
            }
          }
      }
    }
  }

  addToTax(dishTaxDefined: any, orderAmount: number,taxValue:any,chargeType:any, add: boolean) {
    var amount=0;
    if(chargeType=="PERCENTAGE"){
      amount=(orderAmount * taxValue / 100);
    }else{
      amount=taxValue;
    }
    //(item.itemPrice * this.dishTaxDefined[i].taxValue / 100) * item.quantity

    if (add) {
      if (this.tax[dishTaxDefined.name]) {
        this.tax[dishTaxDefined.name] += amount;
      } else {
        this.tax[dishTaxDefined.name] = amount;
      }
    } else {
      if (this.tax[dishTaxDefined.name]) {
        this.tax[dishTaxDefined.name] -= amount;
      }
    }
  }

  applyCoupon(code) {
    const body = {
      'restaurantID': this.mainService.selectedRestaurantId,
      'customerId': this.mainService.customerInfo.customers[0].customerId,
      'couponCode': code,
      'orderSource': "Website",
      'paymentMode': "any"
    };
    const bodyString = JSON.stringify(body);
    // const headers = new Headers({ 'Content-Type': 'application/json' });
    // const option = new RequestOptions({ headers: headers });
    const Url = this.mainService.baseUrl + 'coupon/getCouponDef.json?';
    return this.http.post(Url, body);

  }
  applyGiftCard(code) {
    const body = {
      'giftCardId': code,
      'customerId': this.mainService.customerInfo.customers[0].customerId,
    };
    const bodyString = JSON.stringify(body);
    // const headers = new Headers({ 'Content-Type': 'application/json ; charset=UTF-8' });
    // const option = new RequestOptions({ headers: headers });
    const Url = this.mainService.baseUrl + 'giftCard/redeemGiftCard';
    return this.http.post(Url, body);
  }

  setCustomerInfo(customerInfo) {
    const customers = {
      'firstName': customerInfo.firstName,
      'lastName': customerInfo.lastName,
      'customerId': customerInfo.customerId,
      'email': customerInfo.email,
      'phono': customerInfo.phone,
      'deliveryArea': customerInfo.deliveryArea,
      'address': customerInfo.address,
      'orgId': customerInfo.orgId,
      'restaurantId': customerInfo.restaurantId,
    };
    const bodyString = JSON.stringify(customers);

    // const headers = new Headers({ 'Content-Type': 'application/json ; charset=UTF-8' });
    // const option = new RequestOptions({ headers: headers });
    const Url = this.mainService.baseUrl + this._setCustInfo;
    return this.http.post(Url, customers);

  }

  displayCheck(checkId) {
    const headers = new HttpHeaders({ 'Accept': 'text/html, application/xhtml+xml, */*', 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.get(this.mainService.baseUrl + 'order/generateCheckForPrint?checkId=' + checkId, { headers, responseType: 'text' });
  }

  emailCheck(checkId) {
    return this.http.get(this.mainService.baseUrl + 'order/sendEmailUsingAPI?&checkId=' + checkId);
  }
  redirectToRazorPay(checkId,restaurantId) {
    return this.http.get(this.mainService.baseUrl + 'restaurant/redirectToRazorPay?&checkId=' + checkId+"&restaurantId="+restaurantId);
  }

  
  getCouponDiscount(item: OrderItem, couponValue) {
    return ((item.itemPrice * couponValue) / 100);
  }


  updateGrandTotal() {
    this.subTotal = 0;
    this.couponDiscount = 0;

    // let couponValue = 0;
    this.tax = new Map<string, number>();
    this.applyTaxToDeliveryCharge = false;
    let orderItems = new Map<number, OrderItem>();
    for (const key of Object.keys(this.itmes)) {
      if (this.itmes[key].menuId == 0) {

      } else {
        orderItems[this.itmes[key].menuId] = new OrderItem(
          this.itmes[key].itemName,
          this.itmes[key].itemPrice,
          this.itmes[key].imageUrl,
          this.itmes[key].menuId,
          this.itmes[key].quantity,
          this.itmes[key].sections,
          this.itmes[key].addOn,
          this.itmes[key].displayPrice,
          this.itmes[key].instructions,
          this.itmes[key].cuisineType
        )
      }
    }
    /* reseting coupon sum */

    for (const couponkey of Object.keys(this.couponApplied)) {
      this.couponApplied[couponkey].couponSum = 0;
    }
    /* ends here */
    for (const key of Object.keys(orderItems)) {
      var addOnPriceTemp = 0;
      if (this.couponApplied.length > 0) {
        for (const couponkey of Object.keys(this.couponApplied)) {
          let localVal = 0;
          if (!this.couponApplied[couponkey].isAbsoluteDiscount) {
            localVal = this.getCouponDiscount(orderItems[key], this.couponApplied[couponkey].discountValue);
            let localAddOnSum = 0;
            if (orderItems[key].addOn != undefined || orderItems[key].addOn != null) {
              for (var i = 0; i < orderItems[key].addOn.length; i++) {
                localAddOnSum += this.getCouponDiscount(orderItems[key].addOn[i], this.couponApplied[couponkey].discountValue) * orderItems[key].addOn[i].quantity;
              }
            }
            this.couponApplied[couponkey].couponSum += localVal * orderItems[key].quantity + localAddOnSum;
            localVal=this.couponApplied[couponkey].couponSum;

          } else if (this.couponApplied[couponkey].isAbsoluteDiscount) {
            localVal = this.couponApplied[couponkey].discountValue;
          }
          this.couponDiscount += localVal * orderItems[key].quantity;
          orderItems[key].itemPrice -= localVal;
        }
      }

      for (var i = 0; i < orderItems[key].addOn.length; i++) {
        addOnPriceTemp += (orderItems[key].addOn[i].itemPrice * orderItems[key].addOn[i].quantity);
      }
      this.subTotal += orderItems[key].itemPrice * orderItems[key].quantity + addOnPriceTemp;
    }

    if (this.mainService.restaurant.taxList != null) {
      this.calculateTax(this.mainService.restaurant.taxList, orderItems);
    }

    this.grandTotal = this.subTotal;
    for (const key of Object.keys(this.tax)) {
      this.grandTotal += this.tax[key];
    }
    this.grandTotalWithoutCC = this.grandTotal;
    if (this.mainService.customerInfo != undefined) {
      if (this.mainService.customerInfo.customers[0].credit != null) {
        if (this.mainService.customerInfo.customers[0].credit.creditBalance > 0) {
          this.grandTotal += this.mainService.customerInfo.customers[0].credit.creditBalance;
          this.customerCreditBalance = this.mainService.customerInfo.customers[0].credit.creditBalance;
        } else if (this.mainService.customerInfo.customers[0].credit.creditBalance < 0) {
          if (this.grandTotal > Math.abs(this.mainService.customerInfo.customers[0].credit.creditBalance)) {
            this.grandTotal += this.mainService.customerInfo.customers[0].credit.creditBalance;
            this.customerCreditBalance = this.mainService.customerInfo.customers[0].credit.creditBalance;
          } else if (this.grandTotal < Math.abs(this.mainService.customerInfo.customers[0].credit.creditBalance)) {
            this.grandTotal = this.mainService.customerInfo.customers[0].credit.creditBalance + this.grandTotal;
            this.customerCreditBalance = this.mainService.customerInfo.customers[0].credit.creditBalance;
          }

          if (this.grandTotal < 0) {
            this.grandTotal = 0;
            this.isCustomerCredit = true;
          } else {
            this.isCustomerCredit = false;
          }
        }
      }
    }
    //this.getAddTokenAmount();
  }


  placeOrder(placeOrderJson) {
  //  placeOrderJson.countryId = this.selectedRestaurantId;
    const bodyString = JSON.stringify(placeOrderJson);
    //const headers = new HttpHeaders({'Content-Type': 'application/json'});
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    // const option = new RequestOptions({ headers: headers });
    const Url = this.mainService.baseUrl + 'order/placeOrder';
    return this.http.post(Url, bodyString, { headers });
  }

  public clearCart() {
    var promise = new Promise((resolve, reject) => {
      this.storage.removeItem('items').subscribe(() => { }, () => { });
    // this.storage.removeItem('totalItemCount').subscribe(() => { }, () => { });
      this.storage.removeItem('couponApplied').subscribe(() => { }, () => { });
      this.storage.removeItem('navigation').subscribe(() => { }, () => { });
      this.clearCache().then((val) => resolve(0));
      this.itmes = new Map<number, OrderItem>();
     // this.totalItemCount = 0;
      this.subTotal = 0;
      this.grandTotal = 0;
      this.dishTaxDefined = [];
      this.tax = new Map<string, number>();
      this.orderDeliveryArea;
      //this.deliveryTime = [];
      this.grandTotalWithoutCC=0;
      this.applyTaxToDeliveryCharge = false;
      //this.selectedRestaurantId = restaurant.restaurantId;
      //this.mainService.restaurant = restaurant;
      this.couponApplied = [];
      resolve(0);
    });
    return promise;
  }

  replaceWithSelectedItemTemp(item, selectedSection, itemId) {

    let correctPlatter=false;
    for(const key of Object.keys(this.itmes)){
            for(const key1 of Object.keys(this.itmes[key].sections)){
              if(this.itmes[key].sections[key1].sectionId==selectedSection.sectionId){
                correctPlatter=true;
              }
            }
      }
    
    for (const key of Object.keys(item.repListTemp)) {
      
      if (item.repListTemp[key].itemId == itemId) {
        
        this.deepCopyItemToRepList(item, item.repListTemp,itemId)

        item.itemId = item.repListTemp[key].itemId;
        item.dishId = item.repListTemp[key].dishId;
        item.name = item.repListTemp[key].name;
        item.imageUrl = item.repListTemp[key].imageUrl;
        item.description = item.repListTemp[key].description;
        item.shortDescription=item.repListTemp[key].shortDescription;
        item.courseDetialsURL=item.repListTemp[key].courseDetialsURL;
        item.courseDuration=item.repListTemp[key].courseDuration;
        this.replaceWithSelectedItem(selectedSection);
      }
    }
   return correctPlatter;
    }

  deepCopyItemToRepList(item, repListTemp,itemId) {
    var isCopied = false;
    for (const key of Object.keys(repListTemp)) {
      if (repListTemp[key].itemId == item.itemId) {
        isCopied = true;
      }
    }
    if (!isCopied) {
      item.repListTemp.push(JSON.parse(JSON.stringify(item)));
    }
  }

  replaceWithSelectedItem(selectedSection) {
    if (selectedSection.platterSectionType == "MAIN_PLATTER") {
      for (const key of Object.keys(this.itmes)) {
        if (this.itmes[key] != null && this.itmes[key] != undefined) {

          for (const sectionKey of Object.keys(this.itmes[key].sections)) {

            if (this.itmes[key].sections[sectionKey].sectionId == selectedSection.sectionId) {
              for (const sectionItem2Key of Object.keys(this.itmes[key].sections[sectionKey].items)) {
                this.itmes[key].sections[sectionKey]= JSON.parse(JSON.stringify(selectedSection))
               return true;
              }
            }
          }
        }
      }
    }
    
  }

  confirmOrderAndPayment(status:string, checkId:number) {
    var myinterval = 1*60*1000; // 15 min interval
    setInterval(function(){ this.clearCart(); }, myinterval );
    if (status == "Online") {
      window.location.href = this.mainService.baseUrl + "" + this._payOnline + "restaurantId=" + this.mainService.selectedRestaurantId + "&checkId=" + checkId;
    } 
  }

  removeAfter5Min() {
   
  }
  getRestaurantFromCache(){
    var promise = new Promise((resolve, reject) => {
      if(this.mainService.restaurant==null || this.mainService.restaurant==undefined){
        this.storage.getItem('restaurant').subscribe((restaurant) => {
          if (restaurant != null) {
            let jsonObj: any = restaurant;
            let restInfo = JSON.parse(jsonObj);
            this.mainService.restaurant = restInfo;
           
          }resolve(0);
        }, () => { });
      }else{
        resolve(0);
      }
    });
    return promise;
  }

  getRestaurantFromCacheCart(){
    var promise = new Promise((resolve, reject) => {
      // if(this.mainService.restaurant==null || this.mainService.restaurant==undefined){
        this.storage.getItem('restaurant').subscribe((restaurant) => {
          if (restaurant != null) {
            let jsonObj: any = restaurant;
            let restInfo = JSON.parse(jsonObj);
            this.mainService.restaurant = restInfo;
            this.mainService.getRestaurantInfo(this.mainService.restaurant.restaurantId).subscribe((data)=>{
              this.mainService.selectRestaurant(data);
              resolve(0);
            })
          }resolve(0);
        }, () => { });
      // }else{
      //   resolve(0);
      // }
    });
    return promise;
  }
  getcartItems(){
      var promise = new Promise((resolve, reject) => {
        if(true){
          this.storage.getItem('items').subscribe((items) => {
            if (items != null) {
              let jsonObj: any = items;
              let itemJson = JSON.parse(jsonObj);
              this.itmes = itemJson;
            } resolve(0);
          }, () => {
    
          });
        }else{
          resolve(0);
        }
      });
      return promise;
  }

  getCartFromCache(){
    var promise = new Promise((resolve, reject) => {
    if(Object.keys(this.itmes).length==0){
      this.storage.getItem('items').subscribe((items) => {
        if (items != null) {
          let jsonObj: any = items;
          let itemJson = JSON.parse(jsonObj);
          this.itmes = itemJson;
        } resolve(0);
      }, () => {

      });
    }else{
      resolve(0);
    }
  });
  return promise;
  }

  getMiscFromCache(){
    var promise = new Promise((resolve, reject) => {
    this.storage.getItem('couponApplied').subscribe((couponApplied) => {
      if (couponApplied != null) {
        this.couponApplied = couponApplied;
      } resolve(0);
    }, () => { });

    // this.storage.getItem('locationAddress').subscribe((locationAddress:any) => {
    //   if (locationAddress != null) {
    //     this.locationAddress = JSON.parse(locationAddress);
    //   } resolve(0);
    // }, () => {});
  });
  
  return promise;
  }

  verifyCoupon(){
    for(var i=0;i<this.couponApplied.length;i++){
      this.applyCoupon(this.couponApplied[i].couponCode).subscribe((data:any)=>{
        if(!data.isCouponApplicable){
          this.couponApplied.splice(this.couponApplied.indexOf(this.couponApplied[i]), 1);
          this.storage.setItem('couponApplied', this.couponApplied).subscribe(() => { }, () => { });
          this.updateGrandTotal();
        }
      })
    }
  }

  restoreFromCacheForInst() {
    var promise = new Promise((resolve, reject) => {
      this.getRestaurantFromCache().then(()=>{
          this.getCartFromCache().then(()=>{
                  this.getMiscFromCache().then(()=>{
                    this.restoreValidByFromCache().then(()=>{
                      resolve(0);
                    })
                    });
          });
      })
    });
    return promise;
  }
  
  restoreFromCache() {
    var promise = new Promise((resolve, reject) => {
      this.getRestaurantFromCache().then(()=>{
          this.getCartFromCache().then(()=>{
                this.allCuisined().then(()=>{
                  this.getMiscFromCache().then(()=>{
                    this.restoreValidByFromCache().then(()=>{
                      resolve(0);
                    })
                    });
                });
          });
      })
    });
    return promise;
  }

restoreSelectedCityIds() {
  var promise = new Promise((resolve, reject) => {
  this.storage.getItem('selectedCityIds').subscribe((selectedCityIds:string[]) => {
    if (selectedCityIds != null && selectedCityIds != undefined) {
      this.mainService.selectedCityIds=selectedCityIds;
    } resolve(0);
  }, () => { });

});
return promise;
}

  restoreCusinesFromCache() {
    var promise = new Promise((resolve, reject) => {
    this.storage.getItem('allCuisine').subscribe((cuisines) => {
      if (cuisines != null && cuisines != undefined) {
        let jsonObj: any = cuisines;
        let cuisine = JSON.parse(jsonObj);
        this.mainService.cuisines = cuisine;
        this.mainService.selectedCuisine=undefined;
      } resolve(0);
    }, () => { });
    this.restoreClassMode();
  });
  return promise;
}

restoreInstCusinesFromCache() {
  var promise = new Promise((resolve, reject) => {
  this.storage.getItem('allInstCuisine').subscribe((cuisines) => {
    if (cuisines != null && cuisines != undefined) {
      let jsonObj: any = cuisines;
      let cuisine = JSON.parse(jsonObj);
      this.mainService.cuisines = cuisine;
    } resolve(0);
    }, () => { });
    this.restoreClassMode();
   
  });
return promise;
}

restoreClassMode(){
  var promise = new Promise((resolve, reject) => {
  this.storage.getItem('classMode').subscribe((classMode) => {
    if (classMode != null && classMode != undefined) {
      let jsonObj: any = classMode;
      let cuisine = JSON.parse(jsonObj);
      this.mainService.classModes = cuisine;
    } resolve(0);
    }, () => { });
  });
  return promise;
}

restoreValidByFromCache() {
  var promise = new Promise((resolve, reject) => {
  this.storage.getItem('validBy').subscribe((validBy) => {
    if (validBy != null && validBy != undefined) {
      this.validBy=validBy;
    } resolve(0);
  }, () => { });

});
return promise;
}

  restorePlatterFromCache() {
    var promise = new Promise((resolve, reject) => {
    this.storage.getItem('selectedPlatter').subscribe((selectedPlatter) => {
      if (selectedPlatter != null && selectedPlatter != undefined) {
        let jsonObj: any = selectedPlatter;
        let platter = JSON.parse(jsonObj);
        this.mainService.selectPlatter = platter;
        this.allCuisined();
      } resolve(0);
    }, () => { });
  });
   return promise;
  }

  restoreNavigationFromCache() {
    var promise = new Promise((resolve, reject) => {
    this.storage.getItem('navigation').subscribe((navigation) => {
      if (navigation != null && navigation != undefined) {
        let jsonObj: any = navigation;
        let navigationA = JSON.parse(jsonObj);
        this.mainService.navigationArray = navigationA;
      } resolve(0);
    }, () => { });
  });
   return promise;
  }

  // restoreAlreadyAddedPlatterFromCache(){
  //   var promise = new Promise((resolve, reject) => {
  //   this.storage.getItem('tempAddedItem').subscribe((tempAddedItem) => {
  //     if (tempAddedItem != null) {
  //       let jsonObj: any = tempAddedItem;
  //       let itemJson = JSON.parse(jsonObj);
  //       this.mainService.tempAddedItem = itemJson;
  //       if(this.mainService.tempAddedItem  !=undefined && this.mainService.tempAddedItem!=""){
  //         this.mainService.selectPlatter = this.mainService.tempAddedItem;
  //         resolve(0);
  //       }

  //     }
  //   }, () => {

  //   });
  // });

  // return promise;
  // }

  allCuisined() {
    var promise = new Promise((resolve, reject) => {
    this.storage.getItem('allCuisine').subscribe((allCuisines) => {
      if (allCuisines != null && allCuisines != undefined) {
        let jsonObj: any = allCuisines;
        let cuisine = JSON.parse(jsonObj);
        this.mainService.cuisines = cuisine;
      }resolve(0);
    }, () => { });
  });
  return  promise;
  }
  
  clearCache() {
    var promise = new Promise((resolve, reject) => {
      this.couponApplied = [];
      this.storage.removeItem('items').subscribe(() => {
        // this.storage.removeItem('totalItemCount').subscribe(() => {
        //   this.storage.removeItem('couponApplied').subscribe(() => { resolve(); }, () => { });
        // }, () => { });
      }, () => { });

      this.storage.removeItem('tempAddedItem').subscribe(() => {
        // this.storage.removeItem('totalItemCount').subscribe(() => {
        //   this.storage.removeItem('couponApplied').subscribe(() => { resolve(); }, () => { });
        // }, () => { });
      }, () => { });
      
    })
    return promise;
  }

  logout() {
    var promise = new Promise((resolve, reject) => {
      this.isOpen=false;
      this.mainService.customerInfo = null;
      this.mainService.isLogin = false;
      this.couponApplied = [];
      this.clearCart();
      this.storage.removeItem('email').subscribe(() => {
        // this.storage.removeItem('totalItemCount').subscribe(() => {
        //   this.storage.removeItem('couponApplied').subscribe(() => { resolve(); }, () => { });
        // }, () => { });
        resolve(0);
      }, () => { });
      // this.storage.removeItem('custPhoneNo').subscribe(() => {
      //   // this.storage.removeItem('totalItemCount').subscribe(() => {
      //   //   this.storage.removeItem('couponApplied').subscribe(() => { resolve(); }, () => { });
      //   // }, () => { });
      //   resolve();
      // }, () => { });
      //this.storage.clear().subscribe(() => { }, () => { });
     
    });
    return promise;
  }

  minutesDisplay = 0;
  secondsDisplay = 0;

  endTime = 1;
  interval = 15000;
  //interval = 300;
  unsubscribe$: Subject<void> = new Subject();
  timerSubscription: Subscription;
  __userActionOccured: Subject<void> = new Subject();
  get userActionOccured(): Observable<void> { return this.__userActionOccured.asObservable() };

  notifyUserAction() {
    this.__userActionOccured.next();
  }
  watchMan() {
    this.resetTimer();
    this.userActionOccured.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      if (this.timerSubscription) {
        this.timerSubscription.unsubscribe();
      }
      this.resetTimer();
    });

  }

  resetTimer(endTime: number = this.endTime) {
    //const interval = 300;
    const duration = endTime * 60;
    this.timerSubscription = timer(0, this.interval).pipe(
      take(duration)
    ).subscribe(value =>
      this.render((duration - +value) * this.interval),
      err => { },
      () => {
        this.logOutUser();
      }
    )
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private render(count) {
    this.secondsDisplay = this.getSeconds(count);
    this.minutesDisplay = this.getMinutes(count);
  }

  private getSeconds(ticks: number) {
    const seconds = ((ticks % 60000) / 1000).toFixed(0);
    return this.pad(seconds);
  }

  private getMinutes(ticks: number) {
    const minutes = Math.floor(ticks / 60000);
    return this.pad(minutes);
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }

  logOutUser() {
   this.clearCart().then(()=> {this.router.navigate(['/home']);});
  }

  getAddTokenAmount(){
     if(this.mainService.selectPlatter!=undefined){
       this.tokenAmount=(this.mainService.selectPlatter.vendorMargin *this.subTotal)/100;
     }
     return this.subTotal;
   }

}
