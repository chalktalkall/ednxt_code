import { CanActivate } from '@angular/router';
import {Component, Injectable } from '@angular/core';
// import {Popup} from 'ng2-opd-popup';
import { CanDeactivate } from '@angular/router';
import { Observable }    from 'rxjs';
 

@Injectable()
export  class ActivateGuardService implements CanActivate {

  private can: boolean = false;
 // @ViewChild('popup1') popup1: Popup;
  
  canActivate() {
    return true;
  }

  setCanActivate(can) {
    this.can = can;
  }
}
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
 }
  
@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  canDeactivate(component: CanComponentDeactivate) {
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}