import { Injectable } from '@angular/core';
import { Headers, } from '@angular/http';
import { Router } from '@angular/router';
import { Location, Time } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { CustomerInfo, CustomerLogin } from '../../profile/profile.component';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { DemoRegd } from '../models/Demo_Regd.model';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { OTPdialogComponent } from '../components/tools/OTPdialog.component';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  customerLogin: CustomerLogin;
  // Global varibles
  public initialCuisineData: any;
  public lat: any;
  public lng: any;
  public date: Date;
  public time: Time;
  public cuisines: any;
  public static organizationId: number = 11;
  public static countryId=12;
  public headerTitle: string;
  public customerInfo: any;
  public menus: Array<any> = [];
  public mEmenu: Array<any> = [];
  public customerId: any;
  public lastActiveRestaurant: any;
  public currencyCode: any;
  public selectedCuisine: any;
  public selectedInstData: any;
  public classModes:any=[];
  
  public selectPlatter: any;
  public locationToMenuFlag: boolean = false;
  public tempAddedItem: any;
  public untouchedCuisineData: any;
  public foodType: any;
  public courseTypeList: any = [];
  public vendorList;
  
  public noOfattendees:number;
  public selectedRestaurantId:number;
  public  isCorporate:boolean=false;
  sortByArray:any={};
  

  //Constants : 

  facebook="#";
  insta="#";
  linkedIn="#";
  youtube="";

  // for menu item
  public cartItem: any;
  public serverFixedURL: any;
  public isLogin = false;
  // For qa baseUrl

  // declares global
  textFromURL: string;
  paramLabSelected: any;
  restaurant: any;
  /** Dev URL */
  public otp: number;
  public baseUrl = '';
  public baseImageUrl: string;
  public serverUrl = '';
  dialogRef: MatDialogRef<OTPdialogComponent>;
  public selectedClassMode: string;
  public selectedCityIds: string[];

  public navigationArray:any=[];
  constructor( private OTPdialog: MatDialog, 
    private storage: LocalStorage, private router: Router, private http: HttpClient, location: Location) {
    this.serverFixedURL = MainService.getAbsoluteDomainUrl();
    this.baseUrl = this.serverFixedURL + '/path/';
    this.baseImageUrl = "http://www.coursedge.org";
    this.serverUrl = this.serverFixedURL;

    if ("http://localhost:4200" == this.serverFixedURL || "http://localhost:4300" == this.serverFixedURL || this.serverFixedURL == undefined) {
      // this.baseUrl='http://localhost:8081/hopprz/';
     this.baseUrl = 'http://localhost:8080/ednxt/';
    //this.baseUrl = 'http://www.coursedge.org/ednxt/';
    } else {
      //this.baseUrl = 'https://letschalktalk.com/ct/';
      this.baseUrl='http://www.coursedge.org/ednxt/';
    }
  }
  public openOtpBox() {
    this.dialogRef = this.OTPdialog.open(OTPdialogComponent, {
      disableClose: true,
      data: {}
    });
  }

  public setNavigation(nav:any){
  //  var elmFound=false;
    for(var i=0;i<this.navigationArray.length;i++){
      if(this.navigationArray[i]==nav){
      //  elmFound=true;
        const index = this.navigationArray.indexOf(nav, 0);
        if (index > -1) {
          this.navigationArray.splice(index, 1);
         // this.navigationArray.push(nav);
       }
      }
    }
    this.navigationArray.push(nav);
    this.storage.setItem('navigation', JSON.stringify(this.navigationArray)).subscribe(() => { }, () => { });
  }
  public confirmed(): Observable<any> {
    return this.dialogRef.afterClosed().pipe(take(1), map(res => {
      return res;
    }
    ));
  }
  public verifyOtp(otp: any, customerPhoneNo) {
    var restaurantId = 0;
    if (this.restaurant == undefined) {
      restaurantId = MainService.organizationId;
    } else {
      restaurantId = this.restaurant.restaurantId
    }
    return this.verifyOauthOTP(customerPhoneNo, otp, restaurantId);
  }

  calculateHrDuration(duration: number) {
    if(duration==null){
      return "n/a"
    }
    return duration + " hr";
  }

  getAllModes(data:any){
    this.classModes.length=0
    for (const key of Object.keys(data)) {
      if(data[key]!=undefined){
        if(data[key].course_format!="" &&data[key].course_format!=null){
        if(this.classModes!="" && this.classModes.some((item) => item.mode == data[key].course_format?(item.count+=1):false)){
      
          }else  if(this.classModes.length>0 && data[key].course_format!=null){
            var cM={
              mode:data[key].course_format,
              count:1
            }
            this.classModes.push(cM);
          }else  if(this.classModes=="" &&data[key].course_format!=null){
            var mc={
              mode:data[key].course_format,
              count:1
            }
            this.classModes.push(mc);
           
          }
        }
      }
    }
    this.storage.setItem('classMode', JSON.stringify(this.classModes)).subscribe(() => { }, () => { });
  }

  calculateDuration(duration: number,courseDurationInHr:number) {
    if(duration==null && (courseDurationInHr==null || courseDurationInHr<=0)){
      return "n/a"
    }
    if(courseDurationInHr>0){
      return courseDurationInHr+" hr";
    }
    if (duration < 7 && duration > 0) {

      return duration + " days";
    }
    else if (duration >= 30) {
      var month = duration / 30;
      if (month == 1) {
        return month + " month"
      } else {
        var totalMonthDays = this.float2int(month) * 30;
        var daysLeft = duration - totalMonthDays;
        if (daysLeft == 0) {
          return month + " months";
        } else if (this.float2int(month) == 1) {
          return this.float2int(month) + " month " + daysLeft + " days";
        } else {
          return this.float2int(month) + " months " + daysLeft + " days";
        }
      }
    } else if (duration < 30 && duration > 0) {
      var week = duration / 7;
      if (week == 1) {
        return week + " week"
      } else {
        var totalWeekDays = this.float2int(week) * 7;
        var weekDaysLeft = duration - totalWeekDays;
        if (weekDaysLeft == 0) {
          return week + " week"
        } else if (this.float2int(week) == 1) {
          return this.float2int(week) + " week " + weekDaysLeft + " days";
        }
        else {
          return this.float2int(week) + " weeks " + weekDaysLeft + " days";
        }
      }
    } else if (duration > 0) {
      return duration + " days";
    }
  }

  float2int(value: number) {
    return value | 0;
  }


  calculateOffPrice(actualPrice, displayPrice) {
    return Math.round(((displayPrice - actualPrice) / displayPrice) * 100);
  }

  demoRegistration(demo: DemoRegd) {
    var body;
    if(demo.date==null){
      body = {
        'fullName': demo.name,
        'phone': demo.phone,
        'email': demo.email,
        'countryId': demo.countryId,
        'courseId': demo.courseId,
        'vendorId': demo.vendorId,
        'platterId': demo.platterId,
        'attendeesCount':demo.attendeesCount
      };
    }else{
     body = {
      'fullName': demo.name,
      'phone': demo.phone,
      'demoDate': demo.date,
      'email': demo.email,
      'countryId': demo.countryId,
      'courseId': demo.courseId,
      'vendorId': demo.vendorId,
      'platterId': demo.platterId, 
      'attendeesCount':demo.attendeesCount
    };
  }
    const Url = this.baseUrl + 'order/demoRegd';
    return this.http.post(Url, body);
  }

  // Grab Organization Info
  getOrganizationInfo() {
    // this.organizationId = 11;
    return this.http.get(this.baseUrl + 'organization/getOrganizationInfo?orgId=' + MainService.organizationId).subscribe();
  }

  // Grab Restaurant Info
  getRestaurantInfo(univId:any) {
    return this.http.get(this.baseUrl + 'restaurant/getrestaurantinfo?restaurantId=' + univId);
  }

  getVendorList(universityId:any) {
    const Url = this.baseUrl +  'vendor/listVendorsById';
    if(universityId!=0 && universityId!=null &&  universityId!=undefined){
      const body = {
        'universityId': universityId,
      };
      return this.http.post(Url, body);
    }else{
      const body = {
        'orgId':  MainService.organizationId
      };
      return this.http.post(Url, body);
    }
    //return this.http.get(this.baseUrl + 'vendor/listVendorsById/' + MainService.organizationId);
  }
  // getTaxINfo
  // getTaxInfo(restaurant) {
  //   return this.http.get(this.baseUrl + 'restaurant/getrestaurantinfo?restaurantId=' + restaurant.restaurantId);
  // }

  getCourseType() {
    return this.http.get(this.baseUrl + 'cuisineDishType/getCourseTypes/0');
  }
  // User registration
  userData: any;
  registerUser(customer: CustomerInfo) {
    const body = {
      'deviceNotificationRegId': null,
      'mobileNo': customer.phonenumber,
      'device': null,
      'orgID': 3,
      'emailId': customer.email,
      'userName': customer.fullname

    };
    const Url = this.baseUrl + 'customer/registerUser ';
    return this.http.post(Url, body);
  }

  deleteAddress(id: any, customerId: any) {
    const body = {
      'id': id,
      'customerId': customerId
    };
    const Url = this.baseUrl + 'customer/removeDeliveryAddress';
    return this.http.post(Url, body);
  }

  updateAddress(id: any, customerId: any, customerAddres: String, deliveryArea: String, city: String, state: String) {
    const body = {
      "customerAddress": customerAddres,
      "customerId": customerId,
      "deliveryArea": deliveryArea,
      "city": city,
      "state": state,
      "id": id
    };

    const Url = this.baseUrl + 'customer/updateDeliveryAddress';
    return this.http.post(Url, body);
  }

  // Verify OTP
  verifyOTP(phone, otp, orgId) {
    const body = {
      'mobileNo': phone,
      'otp': otp,
      'orgID': orgId
    };
    //const bodyString = JSON.stringify(body);
    //const headers = new Headers({ 'Content-Type': 'application/json' });
    //const option = new RequestOptions({ headers: headers });
    const Url = this.baseUrl + 'customer/verifyCustomerOTP';
    return this.http.post(Url, body);
  }

  // verify Auth OTP used for login otp verification
  verifyOauthOTP(email, otp, restaurantId) {
    const bodyString = "";
    const headers = new Headers({ 'Content-Type': 'application/json ; charset=UTF-8' });
    //const option = new RequestOptions({ headers: headers });
    const Url = this.baseUrl + 'customer/verifyOauthOTP?email=' + encodeURIComponent(email) +
      '&restaurantId=' + restaurantId +
      '&OTP=' + otp;
    return this.http.post(Url, bodyString);
  }

  getLatestOrderHistory(phone: any, orgId: number, limit: number, inDetail: any) {
    return this.http.get(this.baseUrl + 'customer/getLatestOrderHistory?phone=' + encodeURIComponent(phone) +
      '&countryId=' + MainService.countryId +
      '&orderLimit=' + limit +
      '&inDetail=' + inDetail);
  }

  // Login customer
  loginCustomer(email) {

    const body = {
      'countryId': MainService.countryId,
      'email': email,
      'orgId':-1
      // 'countryId':MainService.countryId
    };
    // const bodyString = JSON.stringify(body);
    //const headers = new Headers({ 'Content-Type': 'application/json ; charset=UTF-8' });
    // const option = new RequestOptions({ headers: headers });
    const Url = this.baseUrl + 'customer/fetchNewOTP';
    return this.http.post(Url, body);

  }

  requestTutor(name: string, phone: string, email: string, course: string, address: string, instruction: string) {

    if (phone.length == 10) {
      phone = "+91" + phone;
    }

    const body = {
      'name': name,
      'phone': phone,
      'email': email,
      'course': course,
      'address': address,
      'instruction': instruction
    };

    const Url = this.baseUrl + 'tutor/requestTutor';
    return this.http.post(Url, body);

  }

  registerInstructor(data:any) {

    const body = {
      "city":data.city,
      "countryId": MainService.countryId,
      "email": data.email,
      "experience": data.experience,
      "highestEducation": data.highestEducation,
      "linkedInProfile": data.linkedInProfile,
      "name": data.name,
      "phone": data.phone,
      "shortDescription":data.shortDescription,
      "speciality": data.speciality,
      "website": data.website
    }

    const Url = this.baseUrl + '/trainer/savePresn';
    return this.http.post(Url, body);

  }

  submitOTP(otp) {
    var promise = new Promise((resolve, reject) => {
      resolve(0);
    });
    return promise;
  }
  // Call User/customer profile services
  getCustomerData(phone, orgId) {
    return this.http.get(this.baseUrl + 'customer/getCustomerData.json?phone=' + encodeURIComponent(phone) +
      '&orgId=' + orgId);
  }

  // getCustomerDemoRegd(customerId) {
  //   return this.http.get(this.baseUrl + 'vendorPlatterMenu/getUserDemoRegd?customerId='+customerId);
  // }

  getReferedCourse(courseId:number,eventId:number) {  

   const bodyString = {
        "courseId":courseId, 
        "eventId":eventId
      };
    const Url = this.baseUrl + 'vendorPlatterMenu/getPlatterById';
    return this.http.post(Url, bodyString);
  }

  selectRestaurant(restaurant:any) {
    if (restaurant != undefined) {
      this.selectedRestaurantId = restaurant.restaurantId;
      this.restaurant = restaurant;
      this.restaurant.taxList = restaurant.taxList;
     
      this.storage.setItem('restaurant', JSON.stringify(restaurant)).subscribe(() => { }, () => { });
    }else{
     
       this.storage.setItem('restaurant', JSON.stringify(this.restaurant)).subscribe(() => { }, () => { });
    }
  }

  
  getCuisineByLocation(latitude, longitude, courseTypeList, classMode: string) {
    var bodyString = {};
    // if (latitude == 0 || longitude == 0) {
    //   bodyString = {
    //     "latitude": 28.4772,
    //     "longnitude": 77.0592
    //   };
    // } else {
    //   this.lat = latitude;
    //   this.lng = longitude;
    //   bodyString = {
    //     "latitude": latitude,
    //     "longnitude": longitude,
    //     "courseType": courseTypeList,
    //     "classMode": classMode
    //   };
    // }
    // const headers = new Headers({ 'Content-Type': 'application/json ; charset=UTF-8' });
    //  const option = new RequestOptions({ headers: headers });

    bodyString = {
          "courseType":courseTypeList,   
          "universityId":MainService.countryId
    };
    const Url = this.baseUrl + 'vendorPlatterMenu/allCoursesByCriteria';
    return this.http.post(Url, bodyString);

  }

  getCustomerInfo(phoneNumber, restaurantId) {
    return this.http.get(this.baseUrl + 'customer/getCustomerInfo.json?email=' + encodeURIComponent(phoneNumber) +
      '&restaurantId=' + restaurantId);

  }

  isCustomerFacebookIdExist(fbId) {
    const bodyString = "";
    const Url = this.baseUrl + 'customer/isCustomerFacebookIdExist?facebookId=' + fbId;
    return this.http.post(Url, bodyString);

  }

  getAllFeatureCourses() {  
    return this.http.get(this.baseUrl + 'vendorPlatterMenu/featureCourses/0');
  }

  public requestCallBack(phoneNumber, enquiryText) {
    return this.http.get(this.baseUrl + 'restaurant/emailCallBackRequest?phoneNumber=' + phoneNumber + "&enquiryText=" + enquiryText);
  }

  getVedorRaiting(rating:number){
    var star="";
    if(rating==0.5){
       star = '<i class="fa fa-star-half-o startColor" aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==1){
       star = '<i class="fa fa-star startColor" aria-hidden="true"></i> <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==1.5){
       star = '<i class="fa fa-star-half-o startColor"  aria-hidden="true">'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==2){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor" aria-hidden="true"></i>  <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==2.5){
       star = '<i class="fa fa-star startColor"   aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==3){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"> <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==3.5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==4){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==4.5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor" aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating" >' +rating+".0</i>";
      return star;
    }
  }

  vedorMenuRaiting(rating:number){
    var star="";
    if(rating==0.5){
       star = '<i class="fa fa-star-half-o startColor" aria-hidden="true"> <i class="startRating courseMenuRating">' +rating+"</i>";
      return star;
    }else if(rating==1){
       star = '<i class="fa fa-star startColor" aria-hidden="true"></i> <i class="startRating courseMenuRating">' +rating+".0</i>";
      return star;
    }else if(rating==1.5){
       star = '<i class="fa fa-star-half-o startColor"  aria-hidden="true">'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating courseMenuRating">' +rating+"</i>";
      return star;
    }else if(rating==2){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor" aria-hidden="true"></i>  <i class="startRating courseMenuRating">' +rating+".0</i>";
      return star;
    }else if(rating==2.5){
       star = '<i class="fa fa-star startColor"   aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating courseMenuRating">' +rating+"</i>";
      return star;
    }else if(rating==3){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"> <i class="startRating courseMenuRating">' +rating+".0</i>";
      return star;
    }else if(rating==3.5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating courseMenuRating">' +rating+"</i>";
      return star;
    }else if(rating==4){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating courseMenuRating">' +rating+".0</i>";
      return star;
    }else if(rating==4.5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating courseMenuRating">' +rating+"</i>";
      return star;
    }else if(rating==5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor" aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating courseMenuRating">' +rating+".0/5</i>";
      return star;
    }
  }
  public static getSiteCollectionUrl(): string {
    if (window
      && "location" in window
      && "protocol" in window.location
      && "pathname" in window.location
      && "host" in window.location) {
      let baseUrl = window.location.protocol + "//" + window.location.host;
      const pathname = window.location.pathname;
      const siteCollectionDetector = "/sites/";
      if (pathname.indexOf(siteCollectionDetector) >= 0) {
        baseUrl += pathname.substring(0, pathname.indexOf("/", siteCollectionDetector.length));
      }
      return baseUrl;
    }
    return null;
  }

  /*
   * Function to get Current Site Url
   * Samples:
   *      "https://domain.sharepoint.com/sites/intranet/subsite/Pages/Home.aspx"
   */
  public static getCurrentAbsoluteSiteUrl(): string {
    if (window
      && "location" in window
      && "protocol" in window.location
      && "pathname" in window.location
      && "host" in window.location) {
      return window.location.protocol + "//" + window.location.host + window.location.pathname;
    }
    return null;
  }

  /*
   * Function to get Current Site Url
   * Samples:
   *      "/sites/intranet"
   */
  public static getWebServerRelativeUrl(): string {
    if (window
      && "location" in window
      && "pathname" in window.location) {
      return window.location.pathname.replace(/\/$/, "");
    }
    return null;
  }

  /*
   * Function to get Layout Page Url
   * Replacement in SPFx for SP.Utilities.Utility.getLayoutsPageUrl('sp.js')
   * Samples:
   *      getLayoutsPageUrl('sp.js')
   *      "/sites/intranet/_layouts/15/sp.js"
   */
  public static getLayoutsPageUrl(libraryName: string): string {
    if (window
      && "location" in window
      && "pathname" in window.location
      && libraryName !== "") {
      return window.location.pathname.replace(/\/$/, "") + "/_layouts/15/" + libraryName;
    }
    return null;
  }

  /*
   * Function to get Current Domain Url
   * Samples:
   *      "https://domain.sharepoint.com"
   */
  public static getAbsoluteDomainUrl(): string {
    if (window
      && "location" in window
      && "protocol" in window.location
      && "host" in window.location) {
      return window.location.protocol + "//" + window.location.host;
    }
    return null;
  }



  /*---------WATCH MAN--------*/



  clearCache() {
    this.selectPlatter = [];
    this.tempAddedItem = [];
    var promise = new Promise((resolve, reject) => {
      this.storage.removeItem('items').subscribe(() => {
        this.storage.removeItem('totalItemCount').subscribe(() => {
          this.storage.removeItem('couponApplied').subscribe(() => { resolve(0); }, () => {
            this.storage.removeItem('tempAddedItem').subscribe(() => {
              resolve(0);
            }, () => { });
          });
        }, () => { });
      }, () => { });
    })
    return promise;
  }

}
