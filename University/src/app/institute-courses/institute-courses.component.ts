import { Component, OnInit,Input, ChangeDetectorRef,ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { ActivateGuardService } from '../shared/services/activate-guard.service';
import { MainService } from '../shared/services/main.service';
import { CartService } from '../shared/services/cart.service';
import { InstituteProfileService } from '../shared/services/institute-profile.service';
import { AppComponent } from '../app.component'
import {PlatformLocation} from '@angular/common';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { map } from 'rxjs/operators';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';
import { NgxSpinnerService } from "ngx-spinner";
 

@Component({
  selector: 'app-institute-courses',
  templateUrl: './institute-courses.component.html',
  styleUrls: ['./institute-courses.component.scss']
})
export class InstituteCoursesComponent implements OnInit {

  intro:string="Welocme to Let's Chalk Talk. Please text us you queries. We'll help you as soon as possible.";
  selectedSort:string="selected";
  selectedMode:string="selected";
  selectedLevel:string="selected"
  term:any;
  @ViewChild('changeLabDialog') public changeLabDialog;
  @ViewChild('onlyAttentionMsz') public onlyAttentionMsz;
  @ViewChild('onlySuccessMsz') public onlySuccessMsz;
  @ViewChild('onlyErrorMsz') public onlyErrorMsz; 
  successMsz: string;
  attentionMsz: string;
  errorMsz:string;
  loading=false;
  instDescription;
  instAddress;
  instPhone;
  instImage;
  instLogo;
  constructor(
    private instService:InstituteProfileService,
    public cd: ChangeDetectorRef,
    public appComponent: AppComponent,
    public service: MainService,
    private router: Router,
    private activateGuard: ActivateGuardService,
    public cartService: CartService,
    public analyticsService :GoogleAnalyticsService,
    protected storage: LocalStorage, private activatedRoute: ActivatedRoute,
    private elementRef: ElementRef,location: PlatformLocation,public spinner: NgxSpinnerService) {
  }
  
  customOptions1: OwlOptions = {
    stagePadding: 50,
    // merge:true,
    autoWidth:false,
    margin:10,
    center:true,
    dots:false,
    navSpeed: 700,
    items:1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  }

  customOptionsBar: OwlOptions = {
    stagePadding: 50,
    autoWidth:false,
    margin:10,
    dots:false,
    touchDrag:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
   
  }

  ngOnInit() {
    //this.loading=true;
    this.spinner.show();
    this.service.setNavigation("Institute")
    this.activatedRoute.queryParams.subscribe(params => {
      const id = params['id'];
      if(id!=undefined){
        this.getCoursesByIstID(id);
      }else{
        this.cartService.restoreInstCusinesFromCache().then((val) => {
          if(this.service.restaurant==undefined || this.service.cuisines==undefined){
              this.cartService.restoreFromCacheForInst().then((val) =>  this.selectDefaultCuisine());
          }else{
            this.selectDefaultCuisine();
          }
       })
      }
  });
  }

  getCoursesByIstID(inst:number){
    this.instService.getCourseByInstituteId(inst).pipe(map((data: any) => {
     // this.loading=false;
      this.spinner.hide();
      if(data.length==0){
        this.errorMsz = "We're Sorry, currently we don't have course for selected location";
        this.onlyErrorMsz.show();
        this.timeOutErrorMsz();
        return false;
      }
      this.service.initialCuisineData= data.sort((a,b) => a.name.localeCompare(b.name));
      this.service.cuisines = data.sort((a,b) => a.name.localeCompare(b.name));
      this.storage.setItem('allInstCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
      this.service.selectedInstData=undefined;
      var countryId=null;
     
      for(var i=0;i<Object.keys( this.service.cuisines).length;i++){
        if(Object.keys( this.service.cuisines[i]).length>0){
          if(this.service.cuisines[i].courses[0]!=undefined &&  Object.keys(this.service.cuisines[i].courses[0]).length>0){
            this.instDescription=this.service.cuisines[i].courses[0].vendor.description;
            this.instAddress=this.service.cuisines[i].courses[0].vendor.address;
            this.instPhone=this.service.cuisines[i].courses[0].vendor.businessContact;
            this.instImage=this.service.cuisines[i].courses[0].vendor.businessLandscapeImageUrl;
            this.instLogo=this.service.cuisines[i].courses[0].vendor.businessLogoImageUrl;
            this.analyticsService.eventEmitter("Insitute Profile",this.service.cuisines[i].courses[0].instituteName, "/institute")
            break;
          }
        }
      }
      this.selectDefaultCuisine();
      this.service.getRestaurantInfo(data[0].countryId).pipe(
            map((data:any) => {
              this.service.selectRestaurant(data);
              this.cuisineDiv();
      })).subscribe();
    }
    )).subscribe();
  }

  cuisineDiv(){
    // this.router.navigate(['/courses']);
    if(this.service.cuisines==undefined){
        this.attentionMsz="Currently we don't serve in the location you have selected";
        this.onlyAttentionMsz.show();
        this.timeOutAttentionMsz();
        this.router.navigate(['/menu'])
    }else if(this.service.cuisines.length==0){
        this.errorMsz = "Currently we don't serve in the location you have selected";
        this.onlyErrorMsz.show();
        this.timeOutErrorMsz();
        this.router.navigate(['/menu'])
        return false;
    }else{
      if(this.service.selectedInstData !=undefined){
        for (let key of Object.keys(this.service.cuisines)) {
          if(this.service.cuisines[key].name==this.service.selectedInstData.name){
            this.service.cuisines[key]["flag"]='true';
          }else{
            this.service.cuisines[key]["flag"]='false';
          }
        }
        this.storage.setItem('allInstCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.selectCuisine(this.service.selectedInstData)
      }else{
        this.service.cuisines[0]["flag"] ='true';
        this.storage.setItem('allInstCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.selectCuisine(this.service.cuisines[0])
      }
 
    }
  }

  getVedorRaiting(rating:number){
    var star="";
    if(rating==0.5){
       star = '<i class="fa fa-star-half-o startColor" aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==1){
       star = '<i class="fa fa-star startColor" aria-hidden="true"></i> <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==1.5){
       star = '<i class="fa fa-star-half-o startColor"  aria-hidden="true">'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==2){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor" aria-hidden="true"></i>  <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==2.5){
       star = '<i class="fa fa-star startColor"   aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==3){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"> <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==3.5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==4){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating">' +rating+".0</i>";
      return star;
    }else if(rating==4.5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star-half-o startColor"  aria-hidden="true"> <i class="startRating">' +rating+"</i>";
      return star;
    }else if(rating==5){
       star = '<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor" aria-hidden="true"></i>'
                +'<i class="fa fa-star startColor"  aria-hidden="true"></i> <i class="startRating">' +rating+".0</i>";
      return star;
    }
    //return '<i class="fa fa-star" style="color:#f3be5f !important" aria-hidden="true"></i>';
  }

  selectCuisine(data: any) {
    this.service.locationToMenuFlag=true;
    this.service.selectedInstData = data;
    this.service.getAllModes(this.service.selectedInstData.courses);
  }
  
  selectDefaultCuisine(){
    if(this.service.selectedInstData !=undefined){
      for (let key of Object.keys(this.service.cuisines)) {
        if(this.service.cuisines[key].name==this.service.selectedInstData.name){
          this.service.cuisines[key]["flag"]='true';
        }else{
          this.service.cuisines[key]["flag"]='false';
        }
      }
    }else{
      if(this.service.cuisines!=undefined){
        this.service.cuisines[0]["flag"] ='true';
        this.selectCuisine(this.service.cuisines[0])
      }else{
        this.router.navigate(['/home']);
      }
     
    }
   // this.loading=false;
    this.spinner.hide();
  }
  activeHomeDivSection(){
    this.router.navigate(['/home']);
  }

  activeCartSection(menu:any){
    var duplicateObject = JSON.parse(JSON.stringify( menu ));
    this.service.selectPlatter=duplicateObject;
    this.storage.setItem('selectedPlatter', JSON.stringify(menu)).subscribe(() => { }, () => { });
    this.activateGuard.setCanActivate(true);
    this.router.navigate(['/menu']);
  }

  // calcluateCourseDuration(menu:any){
  //  var courseDuration=0;
  //  var courseDurationInHr=0;
  //   for (let key of Object.keys(menu.sections)) {
  //     if(menu.sections[key].platterSectionType=="MAIN_PLATTER"){
  //         for (let index of Object.keys(menu.sections[key].items)) {

  //           var durHr=menu.sections[key].items[index].courseDurationInHr;
  //           if(durHr!=null&& durHr>0){
  //             if(!menu.sections[key].items[index].isDishReplaceable){
  //               courseDurationInHr+=durHr;
  //             }
  //           }else{
  //           if(menu.sections[key].items[index].courseDuration!=null){
  //             if(!menu.sections[key].items[index].isDishReplaceable){
  //               courseDuration+=menu.sections[key].items[index].courseDuration;
  //             }
  //           }else{
  //             courseDuration+=0;
  //           }
  //         }
  //         }

  //     }
  //     }
  //     if(courseDuration>0 ||courseDurationInHr>0){
  //      return this.service.calculateDuration(courseDuration,courseDurationInHr);
  //     }else{
  //       return "N/A";
  //     }
  //   }


    sortByOptions(){  

      this.storage.getItem('allInstCuisine').subscribe((cuisines) => {
        if (cuisines != null && cuisines != undefined) {
          let jsonObj: any = cuisines;
          let cuisine = JSON.parse(jsonObj);
          this.service.cuisines = cuisine;
          if(this.selectedMode!="selected"){
          this.service.selectedInstData.courses=   this.service.cuisines[0].courses.filter(a => a.course_format==null?"":a.course_format.toLowerCase()
            .match(this.selectedMode.toLowerCase())); 
        }else{
          this.service.selectedInstData= this.service.cuisines[0];
        }
        } 
        }, () => { });
        
  }
    
    sortByLowtoHighPrice(){
      this.service.selectedInstData.menus.sort((a,b) => b.course_format>a.price?-1:1);
    }

    sortByHightoLowPrice(){
      this.service.selectedInstData.menus.sort((a,b) => a.price>b.price?-1:1);
    }

    sortByLowtoHighRating(){
      this.service.selectedInstData.menus.sort((a,b) => b.rating>a.rating?-1:1);
    }

    sortByHightoLowRating(){
      this.service.selectedInstData.menus.sort((a,b) => a.rating>b.rating?-1:1);
    }
  
  
  timeOutErrorMsz(){
    setTimeout(() => {
      this.onlyErrorMsz.hide();
    }, 3000);
  }

  timeOutAttentionMsz(){
    setTimeout(() => {
      this.onlyAttentionMsz.hide();
    }, 3000);
  }

  timeOutSuccessMsz(){
    setTimeout(() => {
      this.onlySuccessMsz.hide();
    }, 3000);
  }


}
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'custFilterInst'})
export class custInstFilter implements PipeTransform {
  transform(value: any[], term: string): any[] {
   if(term!=undefined && term!="" && term !="undefined"){
    var val = value.filter((courses:any) => courses.name.toLowerCase().match(term.toLowerCase()))
    if(val.length==0){
      return [-1];
    }else{
    return val;
    }
   }else{
     return value;
   }
  }
}
