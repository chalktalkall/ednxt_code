import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstituteCoursesComponent } from './institute-courses.component';

describe('InstituteCoursesComponent', () => {
  let component: InstituteCoursesComponent;
  let fixture: ComponentFixture<InstituteCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstituteCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstituteCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
