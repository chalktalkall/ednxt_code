import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MainService } from '../../shared/services/main.service';
import { CartService } from '../../shared/services/cart.service';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();

  constructor(public service: MainService, public cartService: CartService) { }

  ngOnInit() {
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }

}