import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MainService } from '../../shared/services/main.service';
import { CartService } from '../../shared/services/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();

  constructor(public service: MainService, public cartService: CartService) { }

  ngOnInit() {
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }
  public _toggleOpened(): void {
    this.cartService.isOpen = !this.cartService.isOpen;
  }
}
