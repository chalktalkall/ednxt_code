import { Injectable } from '@angular/core';

declare let gtag:Function;
@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  constructor() { }

  public eventEmitter( 
    eventName: string, 
    page_title: string, 
    page_path: string){ 
         gtag('event', eventName, { 
                 page_title: page_title, 
                 page_path: page_path, 
                 send_to: 'G-Q3YF0159YV'
               })
    }
}
