import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { MatDialog } from '@angular/material/dialog';
import {MainService} from './main.service';

@Injectable({
  providedIn: 'root'
})
export class InstituteProfileService {

  constructor(public mainService:MainService, private http: HttpClient) {
    
    }

    getCourseByInstituteId(instId:number) {  
      return this.http.get(this.mainService.baseUrl + 'vendorPlatterMenu/getCoursesByInstituteId/'+instId);
    }
  
}
