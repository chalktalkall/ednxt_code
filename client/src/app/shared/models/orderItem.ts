export class OrderItem {
    itemCount: number;
    constructor(
        public itemName: string,
        public itemPrice: number,
        public imageUrl: string,
        public menuId: number,
        public quantity:number,
        public sections: any,
        public addOn: any,
        public displayPrice:any,
        public classMode:string,
        public lms_course_id:string,
        public instructions = "",
        public cuisineType = "Default"
     
    ) {
        this.itemCount = quantity;
    }



}

export class AddOn {
    constructor(
        public itemName: string,
        public itemPrice: number,
        public itemId: number,
        public quantity: number,
        public displayPrice:number,
        public imageUrl:string
    ) {
    }
}

export class Section {
    constructor(
        public sectionId: number,
        public name: string,
        public shortName: string,
        public price: number,
        public platterSectionType: string,
        public items: Array<Items>,
    ) {

    }
}

export class Items {

    constructor(
        public itemId: number,
        public countryId: number,
        public vendorId: number,
        public name: string,
        public price: number,
        public quantityInG: number,
        public itemType: string,
        public isDishReplaceable: boolean,
        public displayPrice:number,
        public imageUrl:string,
    ) {

    }
}
