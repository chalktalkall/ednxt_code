import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorRegComponent } from './instructor-reg.component';

describe('InstructorRegComponent', () => {
  let component: InstructorRegComponent;
  let fixture: ComponentFixture<InstructorRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
