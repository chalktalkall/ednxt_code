import {  Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MainService } from '../../services/main.service';

export interface DialogData {
  course: string;
  name: string;
  address:string;
  email: string;
  phone: number;
  instruction:string;
}

@Component({
  selector: 'app-trainer-model',
  templateUrl: './trainer-model.component.html',
  styleUrls: ['./trainer-model.component.scss']
})

export class TrainerModelComponent implements OnInit {

  successMsz:string;
  resultVal: any;
  public loading;

  constructor(public dialogRef: MatDialogRef<TrainerModelComponent>,@Inject(MAT_DIALOG_DATA) public data: DialogData, private mainSerivce: MainService) {

  }

  ngOnInit(): void {
    
  }
 
  onNoClick(): void {
    this.dialogRef.close();
  }

  onOKClick(result: any) {
   
    var phone = new String(result.phone);
    if (result.name == "" || result.name == undefined) {
      result.location.errorMsz = "Please enter correct Name";
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz()
      return false;
    }
    if (result.course == "" || result.course == undefined) {
      result.location.errorMsz = "Please enter correct Subject/Course name";
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz()
      return false;
    }
    // if (result.email == "" || result.email == undefined || !emailMatch.test(result.email)) {
    //   result.location.errorMsz = "Please enter correct email address";
    //   result.location.onlyErrorMsz.show();
    //   result.location.timeOutErrorMsz()
    //   return false;
    // }
    if (result.phone == "" || result.phone == undefined || phone.length !=10) {
      result.location.errorMsz = "Please enter correct 10 digit mobile number";
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz()
      return false;
    }else if(phone.length ==10){
      phone="+91"+phone;
      result.phone=phone;
    }

    if (result.address == "" || result.address == undefined) {
      result.location.errorMsz = "Please enter correct address";
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz()
      return false;
    }
    
    if (result.name != "" && result.phone != "" && result.date != "")
      this.dialogRef.close();

    result.location.loading = true;
   
    this.mainSerivce.loginCustomer(result.phone)
      .subscribe(
        data => {
          this.loading = false;
          this.mainSerivce.openOtpBox();
          this.mainSerivce.confirmed().subscribe(confirmed => {
            if (confirmed) {
              this.loading = true;
              this.verifyOauthOtp(this.mainSerivce.otp, result.phone,result);
            }
          }
          );

        },err => {
          //show error msg
        }
      );
  }
  verifyOauthOtp(value: any, phone: string,result) {
    this.mainSerivce.verifyOtp(value, phone).subscribe(
      data => {
        // result.loading=false;
        if(data){
          this.mainSerivce.requestTutor(result.name,result.phone,result.email,result.course,result.address,result.nstruction).subscribe(
            data=>{
              result.location.loading=false;
              this.responseCheck(data,result);
            });
        }else{
          result.location.errorMsz = "Wrong OTP. Please try again";
          result.location.onlyErrorMsz.show();
          setTimeout(() => {
            result.location.onlyErrorMsz.hide();
          }, 3000);
          this.openOTPDialog(phone,result);
        }
        
      },err => {
        result.location.errorMsz = "Wrong OTP. Please try again";
        result.location.onlyErrorMsz.show();
        setTimeout(() => {
          result.location.onlyErrorMsz.hide();
        }, 3000);
         this.openOTPDialog(phone,result);
      })
  }

  
  responseCheck(data:any,result:any){
    
    if(data.result=="SUCCESS"){
      result.location.successMsz=data.message;
      result.location.onlySuccessMsz.show();
      result.location.timeOutErrorMsz();
    }else{
      result.location.errorMsz=data.message;
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz();
    }
}

  openOTPDialog(phoneNumber,result){
    this.mainSerivce.openOtpBox();
    this.mainSerivce.confirmed().subscribe(confirmed => {
      if (confirmed) {
        this.loading=true;
        this.verifyOauthOtp(this.mainSerivce.otp,phoneNumber,result);
        }
      });
  }

}
