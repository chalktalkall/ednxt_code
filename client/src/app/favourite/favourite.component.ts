import { Component, OnInit } from '@angular/core';
import { MainService } from '../shared/services/main.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
 title:string;
  constructor(public service:MainService) { }

  ngOnInit() {
    this.title=this.service.headerTitle;
  }

}
