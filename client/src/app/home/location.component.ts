import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ActivateGuardService } from '../shared/services/activate-guard.service';
import { MainService } from '../shared/services/main.service';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';
import { CartService } from '../shared/services/cart.service';
import { AppComponent } from '../app.component'
import { map } from 'rxjs/operators';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { MatDialog } from '@angular/material/dialog';
import { TrainerModelComponent } from '../shared/components/trainer-model/trainer-model.component';
import { InstructorRegComponent } from '../shared/components/instructor-reg/instructor-reg.component';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgxSpinnerService } from "ngx-spinner";
 

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})

export class LocationComponent implements OnInit {
  @ViewChild('changeLabDialog') public changeLabDialog;
  @ViewChild('onlyAttentionMsz') public onlyAttentionMsz;
  @ViewChild('onlySuccessMsz') public onlySuccessMsz;
  @ViewChild('onlyErrorMsz') public onlyErrorMsz;
  @ViewChild('nav') ds: DragScrollComponent;
  @Output() updateAddressValue: EventEmitter<any> = new EventEmitter();

  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    autoWidth: true,
    animateIn: true,
    animateOut: true,
    startPosition: 0,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 2
      },
      940: {
        items: 2
      }
    }
    //nav: true,
  }
  public _positionNum: number = 1;
  selectedFromDropDown = false;
  @Input() tempAddress: string;
  tempDate: any;
  tempCount: number;
  tempTime: any = "20:00";

  showDefaultMessage: boolean = false;
  marker: any;
  isTracking: any;

  minDate = new Date();
  maxDate = new Date(2050, 0, 1);
  homeSection: boolean = true;
  term: any;
  cuisineSection: boolean = false;
  cartSection: boolean = false;
  successMsz: string;
  attentionMsz: string;
  errorMsz: string;
  lat: any;
  lng: any;
  public changedCuisine;
  loading: boolean = true;
  orgData: any;
  title: string;
  callBackNumber: any;
  callBackOnly: any;
  enquiryText: any;
  location: any;
  newSelectedRestaurant: any;
  restaurants: Array<Object> = [];
  textFromURL: string;
  locationArray: Array<any> = [];
  labsArrayByLocation: Array<any> = [];
  defaultLocationSelected: any;
  cuisineLength: number;
  localParamValue: any;
  defaultLoc = "home";
  featureCourses: any = [];


  constructor(public cd: ChangeDetectorRef,
    public appComponent: AppComponent,
    public service: MainService,
    private router: Router,
    private activateGuard: ActivateGuardService,
    public cartService: CartService,
    protected storage: LocalStorage, private activatedRoute: ActivatedRoute,
    private elementRef: ElementRef,
    public analyticsService: GoogleAnalyticsService, public dialog: MatDialog,public spinner: NgxSpinnerService) {
    //this.activeHomeDivSection();
  }


  ngOnInit() {
    this.service.isCorporate = false;
    this.service.navigationArray=[];
    this.service.setNavigation("Home");
    this.analyticsService.eventEmitter("Home page", "home", "/home")
    this.getFeatureCourses();
    this.getCourseType();
    this.getVendorList();
    this.activatedRoute.queryParams.subscribe(params => {
      const id = params['id'];
      if (id == 'aC_d') {
        this.term = this.cartService.foodType;
        this.cuisineDiv();
      } else {
        this.tempAddress = this.cartService.locationAddress;
        this.tempCount = this.cartService.noOfPlates;
        this.tempDate = new Date(this.service.date);
        if (this.service.time != undefined) {
          this.tempTime = this.service.time;
        }

      }
    });
    if (this.service.courseTypeList == undefined) {
      this.getCourseType();
    }
  // this.loading = true;
    this.spinner.show();
    this.cartService.restoreFromCache().then(() => {
     // this.loading = false;
      this.spinner.hide();
    });

  }

  moveLeft() {
    this.ds.moveLeft();
  }


  moveRight() {
    this.ds.moveRight();
  }


  activeCuisineDivSection(address) {
    if (true) {
      this.getCuisineByLocation(this.service.lat, this.service.lng);
    }
  }
  myPlaceHolder = "Search Course";
  onSearch($event) {
    this.myPlaceHolder = $event.term == '' ? 'Search Course' : '';
  }

  onPlaceHolderKeyUp($event) {

    if (this.service.selectedCityIds == undefined || this.service.selectedCityIds.length == 0) {
      this.myPlaceHolder = 'Search Course';
    }
  }

  cuisineDiv() {
    this.router.navigate(['/courses']);
    if (this.service.cuisines == undefined) {
      this.attentionMsz = "Currently we don't have any courses for the selected criteria";
      this.onlyAttentionMsz.show();
      this.timeOutAttentionMsz();
    } else if (this.service.cuisines.length == 0) {
      this.errorMsz = "Currently we don't have any courses for the the selected criteria";
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
      return false;
    } else {
      if (this.service.selectedCuisine != undefined) {
        for (let key of Object.keys(this.service.cuisines)) {
          if (this.service.cuisines[key].name == this.service.selectedCuisine.name) {
            this.service.cuisines[key]["flag"] = 'true';
          } else {
            this.service.cuisines[key]["flag"] = 'false';
          }
        }
        this.storage.setItem('allCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.selectCuisine(this.service.selectedCuisine)
      } else {
        this.service.cuisines[0]["flag"] = 'true';
        this.storage.setItem('allCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.selectCuisine(this.service.cuisines[0])
      }
      this.homeSection = false;
      this.cuisineSection = true;
      this.cartSection = false;
    }
  }

  validateLocationDetails(address) {
    let allValid = true;

    if (this.service.selectedCityIds == undefined || this.service.selectedCityIds == null) {
      this.attentionMsz = "Please select the valid course";
      this.onlyAttentionMsz.show();
      this.timeOutAttentionMsz();
      allValid = false;
    }
    return allValid;
  }


  activeCartSection(menu) {
    this.homeSection = false;
    this.cuisineSection = false;
    this.cartSection = true;
    var duplicateObject = JSON.parse(JSON.stringify(menu));
    this.service.selectPlatter = duplicateObject;
    this.storage.setItem('selectedPlatter', JSON.stringify(menu)).subscribe(() => { }, () => { });
    this.activateGuard.setCanActivate(true);
    this.router.navigate(['/menu']);
  }


  findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  getVendorList() {
    this.service.getVendorList(0).subscribe(
      data => {
        this.service.vendorList = data;
      });
  }
  getCourseType() {
    this.service.getCourseType().subscribe(
      data => {
        this.service.courseTypeList = data;
      });

  }

  onAddressChange() {
    this.lat = undefined;
    this.lng = undefined;
    this.service.lat = undefined;
    this.service.lng = undefined;
  }

  setLatNLng(lat, lng) {
    this.lat = lat;
    this.lng = lng;
    this.service.lat = lat;
    this.service.lng = lng;
  }

  activateInstitue(id: number) {
    this.storage.removeItem('classMode').subscribe(() => {
    }, () => { });
    this.router.navigate(['/institute'], { queryParams: { id: id } });
  }



  getCuisineByLocation(lat, lng) {
    //this.loading = true;
    this.spinner.show();

    if (true) {
      this.service.getCuisineByLocation(lat, lng, this.service.selectedCityIds, this.service.selectedClassMode).pipe(map((data: any) => {

        //this.loading = false;
        this.spinner.hide();
        if (data.length == 0) {
          this.errorMsz = "We're Sorry, Currently We Don't Have Course For Selected Criteria";
          this.onlyErrorMsz.show();
          this.timeOutErrorMsz();
          return false;
        }

        this.service.initialCuisineData = data.sort((a, b) => a.name.localeCompare(b.name));
        this.service.cuisines = data.sort((a, b) => a.name.localeCompare(b.name));
        this.storage.setItem('allCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.service.selectedCuisine = undefined;
        this.cuisineLength = Object.keys(this.service.cuisines).length;
        var countryId = null;

        // this.service.getRestaurantInfo(MainService.countryId).pipe(
        //   map((data: any) => {
        //     this.service.selectRestaurant(data);
        //     this.cuisineDiv();
        //   })).subscribe();
        this.cuisineDiv();
      }
      )).subscribe();
    }
  }

  getFeatureCourses() {
    this.service.getAllFeatureCourses().subscribe(data => {
      this.featureCourses = data;
      if (this.featureCourses[0] != undefined && Object.keys(this.featureCourses[0].courses).length > 0) {
        this.featureCourses = this.featureCourses[0].courses;
        // this.service.getRestaurantInfo(MainService.countryId).pipe(
        //   map((data) => {
        //     this.service.selectRestaurant(data);
        //   })).subscribe();


      } else {
        console.log("No feature coursees", data)
      }
    })
  }


  submitTutorRequest(course, name, phone, email, address, instruction) {
    if (course.value == "") {
      this.errorMsz = "Please provide course name."
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
      return false;
    } else if (name.value == "") {
      this.errorMsz = "Please provide your name."
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
      return false;
    } else if (phone.value == "" && phone.length != 10) {
      this.errorMsz = "Please provide valid phone number."
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
      return false;
    } else if (address.value == "") {
      this.errorMsz = "Please provide valid address"
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
      return false;
    }

   // this.loading = true;
    this.spinner.show();
    this.service.requestTutor(name.value, phone.value, email.value, course.value, address.value, instruction.value).subscribe(
      data => {
        //this.loading = false;
        this.spinner.hide();
        this.responseCheck(data);

      });
  }

  openTutorModel(): void {
    var phone;
    var email;
    var course;
    var instruction;
    var address;
    this.dialog.open(TrainerModelComponent, {
      width: '450px',
      disableClose: true,
      autoFocus: true,
      hasBackdrop: false,
      data: {
        course,
        name,
        phone,
        email,
        address,
        instruction,
        location: this,
      }
    });
  }

  openInstructorModel(): void {
    this.dialog.open(InstructorRegComponent, {
      width: '550px',
      disableClose: true,
      autoFocus: true,
      hasBackdrop: false,
      data: {
        location: this,
      }
    });

  }
  responseCheck(data: any) {
    if (data.result == "SUCCESS") {
      this.successMsz = data.message;
      this.onlySuccessMsz.show();
      this.timeOutErrorMsz();
    } else {
      this.errorMsz = data.message;
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
    }
  }
  showPackage(pack: any) {
    if (pack != "" && pack != undefined && pack != null) {
      this.service.textFromURL = pack;
      this.redirectMenu();
    }
  }

  redirectMenu() {
    this.redirectRestaurantMenu();
  }

  selectCuisine(data: any) {
    this.service.locationToMenuFlag = true;
    this.goWithCuisine(data);
  }

  goWithCuisine(datta) {
    this.service.selectedCuisine = datta;
    this.service.getAllModes(this.service.selectedCuisine.courses);
  }

  changeCuisine() {
    this.cartService.clearCart().then((val) => this.goWithCuisine(this.changedCuisine));
  }

  sendCallRequest() {
  //  this.loading = true;
    this.spinner.show();
    var pattern = /(7|8|9)\d{9}/;
    if (this.enquiryText != null && this.enquiryText != "" && this.enquiryText != undefined) {
      if (this.enquiryText.length < 501) {
        if (pattern.test(this.callBackNumber) && this.callBackNumber != "" && this.callBackNumber != undefined && this.callBackNumber != null) {
          this.service.requestCallBack(this.callBackNumber, this.enquiryText).subscribe(
            data => {
             // this.loading = false;
              this.spinner.hide();
              this.callBackNumber = "";
              this.enquiryText = "";
              this.successMsz = "Your enquiry has been sent.";
              this.onlySuccessMsz.show()
              this.timeOutSuccessMsz();
            });

        } else {
          //this.loading = false;
          this.spinner.hide();
          this.attentionMsz = "Please enter valid phone number to get call back.";
          this.onlyAttentionMsz.show();
          this.timeOutAttentionMsz();
        }
      } else {
       // this.loading = false;
        this.spinner.hide();
        this.attentionMsz = "You have entered more then 500 characters. Please limit it to 500 characters only";
        this.onlyAttentionMsz.show();
        this.timeOutAttentionMsz();
      }
    } else {
     // this.loading = false;
      this.spinner.hide();
      this.attentionMsz = "Please enter your query in the enquiry section to get the call back.";
      this.onlyAttentionMsz.show();
      this.timeOutAttentionMsz();
    }
  }

  callBackOnlyRequest() {
  //  this.loading = true;
    this.spinner.show();
    var pattern = /(7|8|9)\d{9}/;
    if (pattern.test(this.callBackOnly) && this.callBackOnly != "" && this.callBackOnly != undefined && this.callBackOnly != null) {
      this.service.requestCallBack(this.callBackOnly, "").subscribe(
        data => {
         // this.loading = false;
          this.spinner.hide();
          this.callBackOnly = "";
          this.successMsz = "Call back request has been sent.";
          this.onlySuccessMsz.show();
          this.timeOutSuccessMsz();
        });

    } else {
     // this.loading = false;
      this.spinner.hide();
      this.attentionMsz = "Please enter valid phone number to get instant call back.";
      this.onlyAttentionMsz.show();
      this.timeOutAttentionMsz();

    }
  }

  redirectRestaurantMenu() {
    this.activateGuard.setCanActivate(true);
    this.router.navigate(['/menu']);
  }

  sameCuisine() {
    this.activateGuard.setCanActivate(true);
    this.router.navigate(['/menu']);
  }

  restoreFoodTypeFromCache() {
    // this.storage.getItem('items').subscribe((items) => {
    //   if (items != null) {
    //     let jsonObj: any = items;
    //     let myheader = JSON.parse(jsonObj);
    //     this.cartService.itmes = myheader;

    //   }
    // }, () => {

    // });
    this.storage.getItem('foodType').subscribe((foodType: any) => {
      if (foodType != null && foodType != undefined) {
        this.cartService.foodType = JSON.parse(foodType);
      }
    }, () => { });
  }

  timeOutErrorMsz() {
    setTimeout(() => {
      this.onlyErrorMsz.hide();
    }, 3000);
  }

  timeOutAttentionMsz() {
    setTimeout(() => {
      this.onlyAttentionMsz.hide();
    }, 3000);
  }

  timeOutSuccessMsz() {
    setTimeout(() => {
      this.onlySuccessMsz.hide();
    }, 3000);
  }


}
