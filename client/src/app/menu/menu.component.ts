import {
  Component, OnInit, Input, Inject, ViewChild, ViewEncapsulation
} from '@angular/core';
import { OrderItem } from '../shared/models/orderItem';
import { Router, ActivatedRoute } from '@angular/router';
import { MainService } from '../shared/services/main.service';
import { CartService } from '../shared/services/cart.service';
//import { PageScrollConfig } from 'ngx-page-scroll';
import { AppComponent } from '../app.component';
import { LocalStorage } from '@ngx-pwa/local-storage';
import * as constants from '../shared/services/cart.service';
import { formatDate, PlatformLocation } from '@angular/common';
import { Observable, timer } from 'rxjs';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { DOCUMENT } from '@angular/common';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DemoRegd } from '../shared/models/Demo_Regd.model';
import { NgxSpinnerService } from "ngx-spinner";
 

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})


export class MenuComponent implements OnInit {

  _showHideBottomBar = true;
  _timeout = null;
  @ViewChild('changePlatter') public changePlatter;
  @ViewChild('onlyAttentionMsz') public onlyAttentionMsz;
  @ViewChild("custInfoPanel") custInfoPanel;
  @ViewChild('onlySuccessMsz') public onlySuccessMsz;
  @ViewChild('onlyErrorMsz') public onlyErrorMsz;
  @ViewChild('owlCar') public owlCar: OwlOptions;
  @ViewChild('owlCarMob') public owlCarMob: OwlOptions;

  @Input() visible: boolean = false;
  @Input() restaurantId: any;
  cameraImages: any = [];
  selectAddOn: Array<any> = [];
  myForm: FormGroup;
  showFiller = false;
  successMsz: string;
  errorMsz: string;
  orderPage: boolean = false;
  cartPage: boolean = true;
  tooltip: any;
  title: string;
  menus: Array<any> = [];
  morningEmenu: any;
  mEmenu: Array<any> = [];
  isMenu: boolean = false;
  isPopUp = false;
  isPopUpCust = false;
  dishAddOn: Array<any> = [];
  addOnDetails: Array<any> = [];
  isdishAddOn = false;
  addOnId: number;
  item: any;
  term: any;
  timer: Observable<number>;
  addOnByCustomer: string;
  selectedSize: any;
  selectedPrice: number;
  attentionMsz: string;
  tempReplacableItems: any;
  tempSection: any
  public loading = true;
  leftNavDisabled = false;
  rightNavDisabled = false;
  leftNavDisabledMob = false;
  rightNavDisabledMob = false;
  index = 0;
  tempItemForToolTip;
  tempSectionForToolTip;
  currentSelectedMenu: any;
  currentSelectedItem: any;
  studentName: string
  phone: string;
  email: string;
  courseId: number;
  panelOpenState = false;
  analyticsSent: boolean = false;
  whatsAppURL = "";
  facebookURL = "";
  linkedInURL = "";
  mailURL = "";
  yLink = undefined;
  /* ***** Constructor ends here ****** */
  customOptions1: OwlOptions = {
    stagePadding: 50,
    autoWidth: false,
    margin: 20,
    dots: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 5
      }
    }
  }

  instImagesOptions: OwlOptions = {
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    items: 1,
    margin: 30,
    stagePadding: 30,
    smartSpeed: 450
  }


  constructor(public dialog: MatDialog, @Inject(DOCUMENT) private document: any, location: PlatformLocation, private bottomSheet: MatBottomSheet, public appComponent: AppComponent,
    public service: MainService, public router: Router, private activatedRoute: ActivatedRoute,
    public cartService: CartService, private formBuilder: FormBuilder,
    public analyticsService: GoogleAnalyticsService,
    public storage: LocalStorage,public spinner: NgxSpinnerService) {

    if (this.service.textFromURL != undefined && this.service.textFromURL != null && this.service.textFromURL != "") {
      this.term = this.service.textFromURL;
      this.service.textFromURL = "";
    }
  }


  getYoutubeLink(youtubeDemo: string) {
    var url_string = youtubeDemo; //window.location.href
    var url = new URL(url_string);
    var c = url.searchParams.get("v");
    this.yLink = c;
    return c;
  }
  getParameterValue() {
    var promise = new Promise((resolve, reject) => {
     
      this.activatedRoute.queryParams.subscribe(params => {
        const id = params['id'];
        if (id != undefined && Number(id)) {

          this.service.getReferedCourse(id,-1).subscribe((data: any) => {
            if (data != null) {
              this.service.selectPlatter = data.courses[0];
              this.analyticsService.eventEmitter("Menu page direct", this.service.selectPlatter.name, this.activatedRoute.pathFromRoot.toString())
              this.analyticsSent = true;
              this.service.getRestaurantInfo(this.service.selectPlatter.vendor.countryId).subscribe((data) => {
                this.service.selectRestaurant(data);
                this.spinner.hide();
                resolve(0);
              })
              this.storage.setItem('selectedPlatter', JSON.stringify(this.service.selectPlatter)).subscribe(() => { }, () => { });

            } else {
              this.analyticsService.eventEmitter("Menu page direct", "No course found", this.activatedRoute.pathFromRoot.toString())
              this.analyticsSent = true;
              this.spinner.hide();
              this.errorMsz = "Course no longer available. Please help us to find you better courses";
              this.onlyErrorMsz.show();
              this.timeOutErrorMsz()
              this.router.navigate(['/home']);
            }

          })
        } else {
          this.spinner.hide();
          resolve(0);
        }
      });
    });
    return promise;
  }

  ngOnInit() {
   
    if (this.service.navigationArray.length == 0) {
      this.cartService.restoreNavigationFromCache().then(() => {
      });
    } else {
      this.service.setNavigation("Menu")
    }
    this.getParameterValue().then(() => {
      this.spinner.show();
      this.cartService.watchMan();
      this.myForm = this.formBuilder.group({
        name: ['', Validators.required],
        hobbies: this.formBuilder.array([])
      });
      if (this.service.selectPlatter == undefined && this.service.selectPlatter == null) {
        this.cartService.restorePlatterFromCache();
      }

      this.cartService.restoreFromCache().then(() => this.callInit());

    })
  }

  activateInstitue(id: number) {
    this.router.navigate(['/institute'], { queryParams: { id: id } });
  }

  getVedorRaiting(rating: number) {
    return this.service.vedorMenuRaiting(rating);
    //return '<i class="fa fa-star" style="color:#f3be5f !important" aria-hidden="true"></i>';
  }

  openPDF(fileURL) {
    window.open(fileURL, '_blank');
  }
  callInit() {
 
    this.spinner.hide()
    if (!this.analyticsSent) {
      this.analyticsService.eventEmitter("Menu page", this.service.selectPlatter.name, this.activatedRoute.pathFromRoot.toString())
    }
    if (this.service.selectPlatter == undefined) {
      this.attentionMsz = "Please select the Course to be able to explore more options.";
      this.onlyAttentionMsz.show();
      this.timer = timer(2000); // 5000 millisecond means 5 seconds
      this.timer.subscribe(() => {
        this.onlyAttentionMsz.hide();
        this.router.navigate(['/home']);
      });
    }

    this.updateData();

  }

  // this.cartService.updateGrandTotal()
  // this.cartCustomerLogin = {
  //   appId: null,
  //   device: null,
  //   orgId: 3,
  //   phoneNumber: '',
  // };
  // }

  goTo(location: string): void {
    window.location.hash = location;
  }
  updateData() {
    this.service.headerTitle = this.title;
    this.menus[0] = this.service.selectPlatter;
    this.service.menus[0] = this.service.selectPlatter;
    this.updateShareLinks();
  }

  updateShareLinks() {
    this.whatsAppURL="http://wa.me/?text=Hi, I found this course. You can Enroll "+encodeURIComponent(this.service.selectPlatter.name)+" using "+encodeURIComponent('http://coursedge.org/#/menu?id='+this.service.selectPlatter.courseId);
    this.mailURL="mailto:?subject="+encodeURIComponent(this.service.selectPlatter.name)+" &body="+
                 encodeURIComponent('http://www.coursedge.org/#/menu?id='+this.service.selectPlatter.courseId);
    this.facebookURL="https://www.facebook.com/sharer/sharer.php?u="+
                  encodeURIComponent('http://www.coursedge.org/#/menu?id='+this.service.selectPlatter.courseId)
                  +"&title="+encodeURIComponent(this.service.selectPlatter.name);
    this.linkedInURL="http://www.linkedin.com/shareArticle?mini=true&url="+ 
                  encodeURIComponent('http://www.coursedge.org/#/menu?id='+this.service.selectPlatter.courseId)+
                  "&title="+encodeURIComponent(this.service.selectPlatter.name)
                  +"&source="+encodeURIComponent('http://www.coursedge.org/#/menu?id='+this.service.selectPlatter.courseId);
  }

  /* ***** method to show cart page  */
  showOrderSummeryPage() {
    //this.router.navigate(['/cart'])
    if (this.cartService.getBadgeCount() > 0) {
      this.cartService.isOpen = true;
    }
  }


  clubReplacements(menu: any) {
    for (const sectionKey of Object.keys(menu.sections)) {
      for (const itemkey of Object.keys(menu.sections[sectionKey].items)) {
        if (menu.sections[sectionKey].items[itemkey].isDishReplaceable == false && menu.sections[sectionKey].items[itemkey].replacementsList != null) {
          menu.sections[sectionKey].items[itemkey]["repListTemp"] = [];
          for (var i = 0; i < menu.sections[sectionKey].items[itemkey].replacementsList.length; i++) {
            for (var j = 0; j < menu.sections[sectionKey].items.length; j++) {
              if (menu.sections[sectionKey].items[itemkey].replacementsList[i] == menu.sections[sectionKey].items[j].dishId) {
                menu.sections[sectionKey].items[itemkey]["repListTemp"].push(menu.sections[sectionKey].items[j]);
              }
            }
          }
        }
      }
    }
  }

  selectedAddOn(option: string) {
    this.isPopUpCust = false;
    this.addOnByCustomer = option;
  }


  openBottomSheet(item: any, section: any): void {
    this.bottomSheet.open(ReplacementOptions, {
      data: { tempReplacableItems: item, section: section, menuComponent: this },
    });
  }

  openBottomSheetToolTip(item: any, section: any, tooltip: any): void {
    this.tooltip = tooltip;
    this.tempItemForToolTip = item;
    this.tempSectionForToolTip = section;
    if (tooltip.isOpen()) {
      tooltip.close();
    } else {
      tooltip.open();
    }
  }

  openLinkToolTip(item: any, section: any, id: any) {
    this.tooltip.close();
    if (section.platterSectionType != 'ADD_ON') {
      let isAdded = this.cartService.replaceWithSelectedItemTemp(item, section, id);
      if (isAdded) {
        this.storage.setItem('items', JSON.stringify(this.cartService.itmes)).subscribe(() => { }, () => { });
        // this.storage.setItem('tempAddedItem', JSON.stringify(this.service.selectPlatter)).subscribe(() => { }, () => { });
        this.successMsz = "Cart Updated!";
        this.onlySuccessMsz.show();
        this.timeOutSuccessMsz();
      }
    }
  }

  replaceWithSelectedItem = function (item, selectedSection, id) {
    if (selectedSection.platterSectionType != 'ADD_ON') {
      this.cartService.replaceWithSelectedItemTemp(item, selectedSection, id);
    }
  }

  hideCustInfoDiv() {
    this.custInfoPanel.nativeElement.classList.remove("show");
  }

  keepSamePlatter() {

    this.changePlatter.hide();
    this.showOrderSummeryPage();
  }

  showCustomerPage() {
    this.visible = true;
  }
  closeCustomerPage() {
    this.visible = false;
  }

  itemDetails(item) {
    this.isPopUp = true;
    this.item = item;
    if (this.item.dishSize.length > 0) {
      this.selectedSize = this.item.dishSize[0];
    }
  }

  onChange(newValue) {
    this.selectedSize = newValue;
    this.selectedPrice = this.selectedSize.price;
  }
  customizedAddOn(item) {
    this.isPopUpCust = true;
    this.item = item;
    this.addOnDetails = [];
    for (let i = 0; i < item.addOn.length; i++) {
      for (let j = 0; j < this.dishAddOn.length; j++) {
        if (this.dishAddOn[j].addOnId == item.addOn[i]) {
          this.addOnDetails.push(this.dishAddOn[j]);
        }
      }
    }
  }

  changePlatterCall() {
    this.router.navigate(['/courses']);
  }

  closePopUp() {
    this.isPopUp = false;
  }
  closePopUp2() {
    this.isPopUpCust = false;
  }
  addToCart(item, menu) {
 
    this.service.getRestaurantInfo(menu.vendor.countryId).subscribe((data) => {
      this.spinner.show();
      this.service.selectRestaurant(data);
      if(this.cartService.couponApplied.length>0){
        this.cartService.verifyCoupon(menu.courseId);
      }
      let response = this.cartService.addToCart(item, menu);
      this.service.tempAddedItem = menu;
      if (response == constants.ADDON_ADDED) {
        this.spinner.hide();
        this.successMsz = "Added " + item.name + " to the cart!";
        this.onlySuccessMsz.show();
        this.timeOutSuccessMsz();
      } else if (response == constants.PLATTER_ADDED) {
        this.spinner.hide();
        this.cartService.itmes = JSON.parse(JSON.stringify(this.cartService.itmes)); //ew Map<number, OrderItem>(this.cartService.itmes); //his.cartService.itmes.concat(this.cartService.itmes);
        this.showOrderSummeryPage();
      } else if (response == constants.ADDON_PLATTER) {
        this.spinner.hide();
        this.successMsz = "Added " + item.name + " course from " + menu.name + " to the cart!";
        this.onlySuccessMsz.show();
        this.timeOutSuccessMsz();
      } else if (response == constants.OVERRIDE_PLATTER) {
        this.spinner.hide();
        this.currentSelectedItem = item;
        this.currentSelectedMenu = menu;
        this.changePlatter.show();
      } else if (response == constants.CANT_CLUB) {
        this.spinner.hide();
        this.errorMsz = "We're sorry you can not club this AddOn with current platter";
        this.onlyErrorMsz.show();
        this.timeOutErrorMsz()
      } else {
        this.spinner.hide();
        this.errorMsz = "No idea what is happening";
        this.onlyErrorMsz.show();
        this.timeOutErrorMsz()
        
      }
      this.isPopUp = false;
    })
   
  }

  removePlatter() {
    this.cartService.removePlatters();
  }

  tempItem: any;
  tempMenu: any;

  checkandAddToCart(item, menu) {
    this.tempItem = item;
    this.tempMenu = menu;
    if (menu.vendor.countryId != null) {

      if (item.courseId != undefined && this.existingPlatter(menu)) {
        if (this.alreadyAddedAddon(item.courseId, menu.courseId)) {
          this.attentionMsz = item.name + " is already added to the cart";
          this.onlyAttentionMsz.show();
          this.timeOutErrorMsz()
          return false;
        } else {
          this.addToCart(this.tempItem, this.tempMenu);
        }
      } else if (this.cartService.getBadgeCount() > 0 && !this.existingPlatter(menu)) {
        this.changePlatter.show();
      } else {
        this.addToCart(this.tempItem, this.tempMenu);
      }
    }
  }

  courseDurationInWeeks(days) {
    return days / 7;
  }

  existingPlatter(menu: any) {
    if (this.cartService.getBadgeCount() > 0) {
      if (this.cartService.itmes[menu.menuId] != undefined && this.cartService.itmes[menu.menuId].menuId == menu.menuId) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  alreadyAddedAddon(itemId: number, menuId: number) {
    var added = false;
    if (this.cartService.getBadgeCount() > 0 && (this.cartService.itmes[menuId] != undefined)) {
      if (this.cartService.itmes[menuId].addOn.length > 0) {
        for (let j = 0; j < this.cartService.itmes[menuId].addOn.length; j++) {
          if (this.cartService.itmes[menuId].addOn[j].itemId == itemId) {
            added = true;
          }
        }
      }
    }
    return added;
  }


  overridePlatter() {
    this.changePlatter.hide();
    delete this.cartService.itmes;
    this.cartService.itmes = new Map<number, OrderItem>();
    this.storage.removeItem('items').subscribe(() => {
      this.addToCart(this.tempItem, this.tempMenu);
    }, () => { });
  }

  /* To check it selected platter is added to the cart of not */
  alreadyAdded() {
    if (this.cartService.getBadgeCount() > 0) {
      for (const checkKey of Object.keys(this.cartService.itmes)) {
        if (this.service.selectPlatter != undefined && this.cartService.itmes[checkKey].menuId == this.service.selectPlatter.courseId) {
          return true;
        }
      }
    } else {
      return false;
    }
    return false;
  }

  leftBoundStat(reachesLeftBound: boolean) {
    this.leftNavDisabled = reachesLeftBound;
  }

  rightBoundStat(reachesRightBound: boolean) {
    this.rightNavDisabled = reachesRightBound;
  }

  leftBoundStatMob(reachesLeftBound: boolean) {
    this.leftNavDisabledMob = reachesLeftBound;
  }

  rightBoundStatMob(reachesRightBound: boolean) {
    this.rightNavDisabledMob = reachesRightBound;
  }

  onIndexChanged(idx) {
    this.index = idx;
  }

  onSnapAnimationFinished() {
  }

  returnValidImages(images) {
    var arrayImage: any = [];
    for (const index of Object.keys(images)) {
      if (images[index] != "" && images[index] != null) {
        arrayImage.push(images[index]);
      }
    }
    return arrayImage;
  }
  timeOutErrorMsz() {
    setTimeout(() => {
      this.onlyErrorMsz.hide();
    }, 3000);
  }

  timeOutAttentionMsz() {
    setTimeout(() => {
      this.onlyAttentionMsz.hide();
    }, 3000);
  }

  timeOutSuccessMsz() {
    setTimeout(() => {
      this.onlySuccessMsz.hide();
    }, 3000);
  }

  askForDemo(courseId, name, countryId, vendorId, platterId) {
    this.analyticsService.eventEmitter("Course demo form", this.service.selectPlatter.name, "/menu");
    this.openDialog(courseId, name, countryId, vendorId, platterId, this.service.selectPlatter.type);
  }

  openDialog(courseId, name, countryId, vendorId, platterId, type): void {
    var courseName = name;
    var date = new Date();
    const dialogRef = this.dialog.open(RegdDialog, {
      width: '350px',
      disableClose: true,
      autoFocus: true,
      hasBackdrop: false,
      data: {
        name: this.studentName,
        phone: this.phone,
        email: this.email,
        courseId: courseId,
        courseName: courseName,
        countryId: countryId,
        vendorId: vendorId,
        menuComponent: this,
        platterId: platterId,
        type: type,
      }
    });
  }

  // roundOff(price) {
  //   return Math.trunc(price);
  // }


}

@Component({
  selector: 'replacementOptions',
  templateUrl: 'replacementOptions.html',
  styleUrls: ['./menu.component.scss'],
})
export class ReplacementOptions {
  constructor(private storage: LocalStorage, public service: MainService, private bottomSheetRef: MatBottomSheetRef<ReplacementOptions>, private cartService: CartService, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
  }
  openLink(event: MouseEvent, item: any, section: any, id: any, menuComponent: MenuComponent): void {
    this.bottomSheetRef.dismiss();
    if (section.platterSectionType != 'ADD_ON') {
      let isAdded = this.cartService.replaceWithSelectedItemTemp(item, section, id);
      if (isAdded) {
        this.storage.setItem('items', JSON.stringify(this.cartService.itmes)).subscribe(() => { }, () => { });
        // this.storage.setItem('tempAddedItem', JSON.stringify(this.service.selectPlatter)).subscribe(() => { }, () => { });
        menuComponent.successMsz = "Cart Updated!";
        menuComponent.onlySuccessMsz.show();
        setTimeout(() => {
          menuComponent.onlySuccessMsz.hide();
        }, 3000);
      }
    }
    event.preventDefault();
  }

}

export interface DialogData {
  name: string,
  phone: number;
  email: string;
  courseId: number;
  courseName: string;
  date: string;
  type: string;
}

@Component({
  selector: 'reg-dialog',
  templateUrl: 'regdDialog.html',
})

export class RegdDialog {

  constructor(public dialogRef: MatDialogRef<RegdDialog>, @Inject(MAT_DIALOG_DATA) public data: DialogData, private mainSerivce: MainService,
  private spinner:NgxSpinnerService) {

  }
  successMsz: string;
  minDate = new Date();
  date = new Date();
  temp = this.date.setDate(this.date.getDate() + 7);
  maxDate = new Date(this.temp);
  resultVal: any;
  public loading;
  onNoClick(): void {
    this.dialogRef.close();
  }

  onOKClick(result: any) {
    var emailMatch = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
    var phoneMatch = /^[2-9]{2}[0-9]{8}$/;
    var date;
    var phone = new String(result.phone);
    if (result.name == "" || result.name == undefined) {
      result.menuComponent.errorMsz = "Please enter correct Name";
      result.menuComponent.onlyErrorMsz.show();
      result.menuComponent.timeOutErrorMsz()
      return false;
    }
    if (result.email == "" || result.email == undefined || !emailMatch.test(result.email)) {
      result.menuComponent.errorMsz = "Please enter correct email address";
      result.menuComponent.onlyErrorMsz.show();
      result.menuComponent.timeOutErrorMsz()
      return false;
    }
    if (result.phone == "" || result.phone == undefined || phone.length != 10) {
      result.menuComponent.errorMsz = "Please enter correct 10 digit mobile number";
      result.menuComponent.onlyErrorMsz.show();
      result.menuComponent.timeOutErrorMsz()
      return false;
    } else if (phone.length == 10) {
      phone = "+91" + phone;
      result.phone = phone;
    }

    if (result.date != "" && result.date != undefined && result.date != null) {
      date = formatDate(new Date(result.date), 'yyyy-MM-dd HH:mm', 'en-US', '+0530');
    }

    if (result.name != "" && result.email != "" && result.phone != "" && result.date != "")
      this.dialogRef.close();

    result.menuComponent.loading = true;

    this.mainSerivce.loginCustomer(result.phone)
      .subscribe(
        data => {
  
          this.spinner.hide()
          this.mainSerivce.openOtpBox();
          this.mainSerivce.confirmed().subscribe(confirmed => {
            if (confirmed) {
         
              this.spinner.show()
              this.verifyOauthOtp(this.mainSerivce.otp, result.phone, result);
            }
          }
          );

        }, err => {
          //show error msg
        }
      );
  }
  verifyOauthOtp(value: any, phone: string, result) {
    this.mainSerivce.verifyOtp(value, phone).subscribe(
      data => {
    
        this.spinner.hide()
        if (data == true) {
          var date = null
          if (result.date != "" && result.date != undefined && result.date != null) {
            date = formatDate(new Date(result.date), 'yyyy-MM-dd HH:mm', 'en-US', '+0530');
          }
          this.mainSerivce.demoRegistration(new DemoRegd(
            result.name,
            result.email,
            result.phone,
            result.countryId,
            result.vendorId,
            result.courseId,
            date,
            result.platterId, this.mainSerivce.noOfattendees)).subscribe(
              data => {
                result.menuComponent.successMsz = "Thanks for the enrollment!";
                result.menuComponent.loading = false;
                result.menuComponent.onlySuccessMsz.show();
                setTimeout(() => {
                  result.menuComponent.onlySuccessMsz.hide();
                }, 3000);
              });
        } else {
          result.menuComponent.errorMsz = "Wrong OTP. Please try again";
          result.menuComponent.onlyErrorMsz.show();
          setTimeout(() => {
            result.menuComponent.onlyErrorMsz.hide();
          }, 3000);
          this.openOTPDialog(phone, result);
        }

      }, err => {
        result.menuComponent.errorMsz = "Wrong OTP. Please try again";
        result.menuComponent.onlyErrorMsz.show();
        setTimeout(() => {
          result.menuComponent.onlyErrorMsz.hide();
        }, 3000);
        this.openOTPDialog(phone, result);
      })
  }

  openOTPDialog(phoneNumber, result) {
    this.mainSerivce.openOtpBox();
    this.mainSerivce.confirmed().subscribe(confirmed => {
      if (confirmed) {
    
        this.spinner.show()
        this.verifyOauthOtp(this.mainSerivce.otp, phoneNumber, result);
      }
    });
  }

}