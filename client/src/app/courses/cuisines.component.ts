import { Component, OnInit, ChangeDetectorRef,ViewEncapsulation } from '@angular/core';
import { Router} from '@angular/router';
import { ActivateGuardService } from '../shared/services/activate-guard.service';
import { MainService } from '../shared/services/main.service';
import { CartService } from '../shared/services/cart.service';
import { AppComponent } from '../app.component'
import { LocalStorage } from '@ngx-pwa/local-storage';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';


@Component({
  selector: 'app-cuisines',
  templateUrl: './cuisines.component.html',
  styleUrls: ['./cuisines.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CuisinesComponent implements OnInit {

  intro:string="Welocme to Let's Chalk Talk. Please text us you queries. We'll help you as soon as possible.";
  selectedSort:string="selected";
  selectedMode:string="selected";
  selectedLevel:string="selected"
  term:any;
  loading=false;
  constructor(public cd: ChangeDetectorRef,
    public appComponent: AppComponent,
    public service: MainService,
    private router: Router,
    private activateGuard: ActivateGuardService,
    public cartService: CartService,
    protected storage: LocalStorage,
    public analyticsService :GoogleAnalyticsService) {
  }
  
  customOptions1: OwlOptions = {
    stagePadding: 50,
    // merge:true,
    autoWidth:false,
    margin:10,
    center:true,
    dots:false,
    navSpeed: 700,
    items:1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  }

  customOptionsBar: OwlOptions = {
    stagePadding: 50,
    autoWidth:false,
    margin:10,
    dots:false,
    touchDrag:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
  }

  // @HostListener('document:keyup', ['$event'])
  // @HostListener('document:click', ['$event'])
  // @HostListener('document:wheel', ['$event'])
  // resetTimer() {
  //   this.cartService.notifyUserAction();
  // }

  ngOnInit() {
    this.service.setNavigation("Courses")
    this.analyticsService.eventEmitter("List Courses", "Course", "/courses")
    this.cartService.restoreCusinesFromCache().then((val) => {
         if(this.service.restaurant==undefined || this.service.cuisines==undefined){
              this.cartService.restoreFromCache().then(() =>  this.selectDefaultCuisine());
          }else{
            this.selectDefaultCuisine();
          }
    })
  }

  // setFoodType(val:string,event:any){
  //   if(event.target.checked){
  //     this.term =val;
  //     this.cartService.foodType=this.term;
  //   }else{
  //     this.term="";
  //     this.cartService.foodType=this.term;
  //   }
  //   this.storage.setItem('foodType', JSON.stringify(String(this.cartService.foodType))).subscribe(() => { }, () => { });
  // }

  selectCuisine(data: any) {
    this.service.locationToMenuFlag=true;
    this.service.selectedCuisine = data;
    this.sortByOptions();
  }
  
  selectDefaultCuisine(){
    if(this.service.selectedCuisine !=undefined){
      for (let key of Object.keys(this.service.cuisines)) {
        if(this.service.cuisines[key].name==this.service.selectedCuisine.name){
          this.service.cuisines[key]["flag"]='true';
        }else{
          this.service.cuisines[key]["flag"]='false';
        }
      }
    }else{
      if(this.service.cuisines!=undefined){
        this.service.cuisines[0]["flag"] ='true';
        this.selectCuisine(this.service.cuisines[0])
      }else{
        this.router.navigate(['/home']);
      }
    }
  }

  activeHomeDivSection(){
    this.router.navigate(['/home']);
  }

  activateInstitue(id:number){
    this.router.navigate(['/institute'], { queryParams: { id: id } });
  }

  activeCartSection(menu:any){
    var duplicateObject = JSON.parse(JSON.stringify( menu ));
    this.service.selectPlatter=duplicateObject;
    this.storage.setItem('selectedPlatter', JSON.stringify(menu)).subscribe(() => { }, () => { });
    this.activateGuard.setCanActivate(true);
    this.router.navigate(['/menu']);
  }

  calcluateCourseDuration(menu:any){
   var courseDuration=0;
   var courseDurationInHr=0;
    for (let key of Object.keys(menu.sections)) {
      if(menu.sections[key].platterSectionType=="MAIN_PLATTER"){
          for (let index of Object.keys(menu.sections[key].items)) {
            var durHr=menu.sections[key].items[index].courseDurationInHr;
            if(durHr!=null&& durHr>0){
              if(!menu.sections[key].items[index].isDishReplaceable){
               courseDurationInHr+=durHr;
              }
            }else{
              if(menu.sections[key].items[index].courseDuration!=null){
                if(!menu.sections[key].items[index].isDishReplaceable){
                courseDuration+=menu.sections[key].items[index].courseDuration;
                }
              }else{
                courseDuration+=0;
             }
          }
          }
      }
      }
      if(courseDuration>0 ||courseDurationInHr>0){
       return this.service.calculateDuration(courseDuration,courseDurationInHr);
      }else{
        return "N/A";
      }
    }

    sortByOptions(){  

      this.storage.getItem('allCuisine').subscribe((cuisines) => {
        if (cuisines != null && cuisines != undefined) {
          let jsonObj: any = cuisines;
          let cuisine = JSON.parse(jsonObj);
          this.service.cuisines = cuisine;
          if(this.selectedMode!="selected"){
          this.service.selectedCuisine.courses=   this.service.cuisines[0].courses.filter(a => a.course_format==null?"":a.course_format.toLowerCase()
            .match(this.selectedMode.toLowerCase())); 
        }else{
          this.service.selectedCuisine= this.service.cuisines[0];
        }
        } 
        }, () => { });
        
  }
    
    sortByLowtoHighPrice(){
      this.service.selectedCuisine.menus.sort((a,b) => b.price>a.price?-1:1);
    }

    sortByHightoLowPrice(){
      this.service.selectedCuisine.menus.sort((a,b) => a.price>b.price?-1:1);
    }

    sortByLowtoHighRating(){
      this.service.selectedCuisine.menus.sort((a,b) => b.rating>a.rating?-1:1);
    }

    sortByHightoLowRating(){
      this.service.selectedCuisine.menus.sort((a,b) => a.rating>b.rating?-1:1);
    }
}
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'custFilterIT'})
export class custFilter implements PipeTransform {
  transform(value: any[], term: string): any[] {
   if(term!=undefined && term!="" && term !="undefined"){
    var val = value.filter((x:any) => x.vendor.vendorName.toLowerCase().match(term.toLowerCase()))
    if(val.length==0){
      return [-1];
    }else{
    return val;
    }
   }else{
     return value;
   }
  }
}
