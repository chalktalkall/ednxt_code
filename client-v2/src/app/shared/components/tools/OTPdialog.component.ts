import { Component, HostListener, OnInit, Inject } from '@angular/core';
import { MainService } from '../../services/main.service';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: 'otp-confirm-dialog',
  templateUrl: './OTPdialog.component.html',
  styleUrls:['./OTPdialog.component.css']
})
export class OTPdialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    otp: number
  },
  public service: MainService, private mdDialogRef: MatDialogRef<OTPdialogComponent> ) {

  }

  ngOnInit() {
    this.mdDialogRef.disableClose = true;
  }

  public cancel() {
    this.mdDialogRef.disableClose = false;
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }
  public confirm(value) {
    var res = parseInt(value);
    if(isNaN(res)){
      
    }
    if(!isNaN(res) && value!="" && value!=undefined){
      this.service.otp=value;
      this.close(true);
    }else{
    }
  }
  @HostListener("keydown.esc")
  public onEsc() {
    this.close(false);
  }
}