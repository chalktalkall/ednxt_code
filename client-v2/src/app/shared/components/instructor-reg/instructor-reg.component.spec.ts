import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InstructorRegComponent } from './instructor-reg.component';

describe('InstructorRegComponent', () => {
  let component: InstructorRegComponent;
  let fixture: ComponentFixture<InstructorRegComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
