import {  Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MainService } from '../../services/main.service';
import { NgxSpinnerService } from "ngx-spinner";

export interface DialogData{

  name: string,
  email: string,
  phone: string,
  shortDescription: string,
  experience: 0,
  highestEducation:string,
  speciality: string
  city:string,
  website: string
  linkedInProfile:string,
  countryId: 0,
}

@Component({
  selector: 'app-instructor-reg',
  templateUrl: './instructor-reg.component.html',
  styleUrls: ['./instructor-reg.component.scss']
})
export class InstructorRegComponent implements OnInit {

  successMsz:string;
  resultVal: any;
  public loading;

  constructor(public spinner: NgxSpinnerService,public dialogRef: MatDialogRef<InstructorRegComponent>,@Inject(MAT_DIALOG_DATA) public data: DialogData, private mainSerivce: MainService) {

  }

  ngOnInit(): void {
  }
 
  onNoClick(): void {
    this.dialogRef.close();
  }

  onOKClick(result: any) {

    var phone = new String(result.phone);
    if (result.name == "" || result.name == undefined) {
      result.location.errorMsz = "Please enter correct Name";
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz()
      return false;
    }
  
    if (result.phone == "" || result.phone == undefined || phone.length !=10) {
      result.location.errorMsz = "Please enter correct 10 digit mobile number";
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz()
      return false;
    }else if(phone.length ==10){
      phone="+91"+phone;
      result.phone=phone;
    }

    // if (result.speciality == "" || result.speciality == undefined) {
    //   result.location.errorMsz = "Please Speciality";
    //   result.location.onlyErrorMsz.show();
    //   result.location.timeOutErrorMsz()
    //   return false;
    // }
    
    if (result.name != "" && result.phone != "" && result.date != "")
      this.dialogRef.close();

   this.spinner.show();// = true;
   
    this.mainSerivce.loginCustomer(result.email )
      .subscribe(
        data => {
          this.spinner.hide(); //= false;
          this.mainSerivce.openOtpBox();
          this.mainSerivce.confirmed().subscribe(confirmed => {
            if (confirmed) {
              //  this.spinner.show()   //= true;
              this.verifyOauthOtp(this.mainSerivce.otp, result.email,result);
            }
          }
          );

        },err => {
          //show error msg
        }
      );
  }
  verifyOauthOtp(value: any, phone: string,result:any) {
    this.mainSerivce.verifyOtp(value, phone).subscribe(
      data => {
        // result.loading=false;
        if(data){
          this.mainSerivce.registerInstructor(result).subscribe(
            data=>{
              result.location.loading=false;
              this.responseCheck(data,result);
            });
        }else{
          result.location.errorMsz = "Wrong OTP. Please try again";
          result.location.onlyErrorMsz.show();
          setTimeout(() => {
            result.location.onlyErrorMsz.hide();
          }, 3000);
          this.openOTPDialog(phone,result);
        }
        
      },err => {
        result.location.errorMsz = "Wrong OTP. Please try again";
        result.location.onlyErrorMsz.show();
        setTimeout(() => {
          result.location.onlyErrorMsz.hide();
        }, 3000);
         this.openOTPDialog(phone,result);
      })
  }

  
  responseCheck(data:any,result:any){
    
    if(data.result=="SUCCESS"){
      result.location.successMsz="Thank you for the registration. We'll get in touch with you ASAP.";
      result.location.onlySuccessMsz.show();
      result.location.timeOutErrorMsz();
    }else{
      result.location.errorMsz=data.message;
      result.location.onlyErrorMsz.show();
      result.location.timeOutErrorMsz();
    }
}

  openOTPDialog(phoneNumber,result){
    this.mainSerivce.openOtpBox();
    this.mainSerivce.confirmed().subscribe(confirmed => {
      if (confirmed) {
        this.loading=true;
        this.verifyOauthOtp(this.mainSerivce.otp,phoneNumber,result);
        }
      });
  }
}
