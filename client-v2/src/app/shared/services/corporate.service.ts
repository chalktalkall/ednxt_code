import { Injectable } from '@angular/core';
import { RequestOptions, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { MainService } from '../services/main.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Subject, timer, Subscription, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CorporateService {
  constructor(private router:Router,public mainService: MainService,private http: HttpClient, public storage: LocalStorage) { }

    
  getCoursesByCriteria() {
    var  bodyString = {
        "courseType": this.mainService.selectedCityIds,
        "classMode": this.mainService.selectedClassMode,
        "plateCount":this.mainService.noOfattendees
      };
    const Url = this.mainService.baseUrl + 'vendorPlatterMenu/allCoursesByCriteria';
    return this.http.post(Url, bodyString);
  }
  getAllFeatureTrainings() {  
    return this.http.get(this.mainService.baseUrl + 'vendorPlatterMenu/featureTrainings');
  }
}
