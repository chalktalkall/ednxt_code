import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { MatDialog } from '@angular/material/dialog';
import {MainService} from './main.service';
 

@Injectable({
  providedIn: 'root'
})
export class WebinarService {

  selectedWebinar: any;
  initialWebinarData:any;
  webinarList:any;
  constructor(private mainService:MainService,private OTPdialog: MatDialog, private storage: LocalStorage, private router: Router, private http: HttpClient, location: Location) {
    
  }
  getAllActiveWebinars() {  
    return this.http.get(this.mainService.baseUrl + 'vendorPlatterMenu/allActiveWebinars');
  }

  restorePlatterFromCache() {
    var promise = new Promise((resolve, reject) => {
    this.storage.getItem('selectedWebinar').subscribe((selectedWebinar) => {
      if (selectedWebinar != null && selectedWebinar != undefined) {
        let jsonObj: any = selectedWebinar;
        let webinar = JSON.parse(jsonObj);
        this.selectedWebinar= webinar;
      //  this.allCuisined();
      } 
    }, () => { }) ;
    
    if(this.mainService.restaurant==null || this.mainService.restaurant==undefined){
      this.storage.getItem('restaurant').subscribe((restaurant) => {
        if (restaurant != null) {
          let jsonObj: any = restaurant;
          let restInfo = JSON.parse(jsonObj);
          this.mainService.restaurant = restInfo;
         
        }resolve(0);
      }, () => { });
    }else{
      resolve(0);
    }
  });
   return promise;
  }

  getRestaurantFromCache(){
    var promise = new Promise((resolve, reject) => {
      if(this.mainService.restaurant==null || this.mainService.restaurant==undefined){
        this.storage.getItem('restaurant').subscribe((restaurant) => {
          if (restaurant != null) {
            let jsonObj: any = restaurant;
            let restInfo = JSON.parse(jsonObj);
            this.mainService.restaurant = restInfo;
           
          }resolve(0);
        }, () => { });
      }else{
        resolve(0);
      }
    });
    return promise;
  }

}
