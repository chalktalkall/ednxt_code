export interface Cart {
            itemId: number,
              restaurantId: string,
              userId: string,
              name : string,
              description : string,
              rectangularImage: string,
              imageUrl: string,
              smallImageUrl: string,
              price: number,
              displayPrice: number,
              itemType: string,
}
