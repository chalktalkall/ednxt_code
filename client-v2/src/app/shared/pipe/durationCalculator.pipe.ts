import { Pipe, PipeTransform } from '@angular/core';
import { MainService } from '../services/main.service';
@Pipe({
  name: 'durationCal'
})
export class DurationCal implements PipeTransform {
    constructor(public service:MainService){

    }
  transform(menu: any, args?: any): any {
    var courseDuration=0;
    var courseDurationInHr=0;
     for (let key of Object.keys(menu.sections)) {
       if(menu.sections[key].platterSectionType=="MAIN_PLATTER"){
           for (let index of Object.keys(menu.sections[key].items)) {
 
             var durHr=menu.sections[key].items[index].courseDurationInHr;
             if(durHr!=null&& durHr>0){
               if(!menu.sections[key].items[index].isDishReplaceable){
                 courseDurationInHr+=durHr;
               }
             }else{
             if(menu.sections[key].items[index].courseDuration!=null){
               if(!menu.sections[key].items[index].isDishReplaceable){
                 courseDuration+=menu.sections[key].items[index].courseDuration;
               }
             }else{
               courseDuration+=0;
             }
           }
           }
 
       }
       }
       if(courseDuration>0 ||courseDurationInHr>0){
        return this.service.calculateDuration(courseDuration,courseDurationInHr);
       }else{
         return "N/A";
       }
  }

}