import { Component, OnInit } from '@angular/core';
import { CartService } from '../../shared/services/cart.service';


@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  
  constructor(public cartService: CartService) { }

  ngOnInit(): void {
  }

}
