import { Component, OnInit,Output, EventEmitter, ViewChild,ViewEncapsulation } from '@angular/core';
import { MainService } from '../../shared/services/main.service';
import { CartService } from '../../shared/services/cart.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { LocalStorage } from '@ngx-pwa/local-storage';
import FuzzySearch from 'fuzzy-search';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation:ViewEncapsulation.None,
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  @ViewChild('changeLabDialog') public changeLabDialog;
  @ViewChild('onlyAttentionMsz') public onlyAttentionMsz;
  @ViewChild('onlySuccessMsz') public onlySuccessMsz;
  @ViewChild('onlyErrorMsz') public onlyErrorMsz;
  constructor(public service: MainService, protected storage: LocalStorage,   private router: Router, public cartService: CartService,public spinner: NgxSpinnerService) { }
  myPlaceHolder = "Search Course";
  successMsz: string;
  attentionMsz: string;
  errorMsz: string;
  searcher;
  inputSearch:string;
  username:string;

  onSearch($event) {
    this.myPlaceHolder = $event.term == '' ? 'Search Course' : '';
  }
  ngOnInit() {
    this.getCourseType();
  }

 
  clearData(){
    this.service.selectedCuisine=[];
    this.service.cuisines=[];
    this.service.selectedCityIds=[];
  }

  getSearchInput(){
   this.clearData();
    this.storage.setItem('allCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
    const result = this.searcher.search(this.inputSearch);
    if(result.length>0){
      for(var i=0;i<result.length;i++){
        this.service.selectedCityIds.push(result[i].cuisineTypeId);
      }
      this.activeCuisineDivSection();
    }else{
      this.errorMsz = "We're Sorry, Currently We Don't Have any active Course For '"+this.inputSearch+"'";
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
    }

  }
  getCourseType() {
    this.service.getCourseType().subscribe(
      data => {
        this.service.courseTypeList = data;
         this.searcher = new FuzzySearch( this.service.courseTypeList, ['name'], {
          caseSensitive: false,
        });
      });

  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }
  public _toggleOpened(): void {
    this.cartService.isOpen = !this.cartService.isOpen;
  }

  activeCuisineDivSection() {
    if (true) {
      this.getCuisineByLocation(this.service.lat, this.service.lng);
    }
  }

  getCuisineByLocation(lat, lng) {
    //this.loading = true;
    this.spinner.show();
    if (true) {
      this.service.getCuisineByLocation(lat, lng, this.service.selectedCityIds, this.service.selectedClassMode).pipe(map((data: any) => {

        //this.loading = false;
        this.spinner.hide();
        if (data.length == 0) {
          this.errorMsz = "We're Sorry, Currently We Don't Have any active Course For '"+this.inputSearch+"'";
          this.onlyErrorMsz.show();
          this.timeOutErrorMsz();
          return false;
        }

        this.service.initialCuisineData = data.sort((a, b) => a.name.localeCompare(b.name));
        this.service.cuisines = data.sort((a, b) => a.name.localeCompare(b.name));
        this.storage.setItem('allCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.service.selectedCuisine = undefined;
       
       // this.cuisineLength = Object.keys(this.service.cuisines).length;
        var countryId = null;

        // this.service.getRestaurantInfo(MainService.countryId).pipe(
        //   map((data: any) => {
        //     this.service.selectRestaurant(data);
        //     this.cuisineDiv();
        //   })).subscribe();
        this.inputSearch="";
        this.cuisineDiv();

      }
      )).subscribe();
    }
  }

  cuisineDiv() {
    this.router.navigate(['/courses']);
    if (this.service.cuisines == undefined) {
      this.attentionMsz = "Currently we don't have any courses for the selected criteria";
      this.onlyAttentionMsz.show();
      this.timeOutAttentionMsz();
    } else if (this.service.cuisines.length == 0) {
      this.errorMsz = "Currently we don't have any courses for the the selected criteria";
      this.onlyErrorMsz.show();
      this.timeOutErrorMsz();
      return false;
    } else {
      if (this.service.selectedCuisine != undefined) {
        for (let key of Object.keys(this.service.cuisines)) {
          if (this.service.cuisines[key].name == this.service.selectedCuisine.name) {
            this.service.cuisines[key]["flag"] = 'true';
          } else {
            this.service.cuisines[key]["flag"] = 'false';
          }
        }
        this.storage.setItem('allCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.selectCuisine(this.service.selectedCuisine)
      } else {
        this.service.cuisines[0]["flag"] = 'true';
        this.storage.setItem('allCuisine', JSON.stringify(this.service.cuisines)).subscribe(() => { }, () => { });
        this.selectCuisine(this.service.cuisines[0])
      }
    }
  }
  selectCuisine(data: any) {
    this.service.locationToMenuFlag = true;
    this.goWithCuisine(data);
  }

  goWithCuisine(datta) {
    this.service.selectedCuisine = datta;
    this.service.getAllModes(this.service.selectedCuisine.courses);
  }
  timeOutErrorMsz() {
    setTimeout(() => {
      this.onlyErrorMsz.hide();
    }, 3000);
  }

  timeOutAttentionMsz() {
    setTimeout(() => {
      this.onlyAttentionMsz.hide();
    }, 3000);
  }

  timeOutSuccessMsz() {
    setTimeout(() => {
      this.onlySuccessMsz.hide();
    }, 3000);
  }
}
