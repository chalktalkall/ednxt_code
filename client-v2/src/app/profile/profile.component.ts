import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MainService } from '../shared/services/main.service';
import { CartService } from '../shared/services/cart.service';
import { CartComponent } from '../cart/cart.component'
import { LocalStorage } from '@ngx-pwa/local-storage';
import { map } from "rxjs/operators";
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';
import { NgxSpinnerService } from "ngx-spinner";
 

export class CustomerLogin {
  appId: any;
  device: any;
  orgId: number;
  phoneNumber: any;
}

export class CustomerInfo {
  phonenumber: any;
  email: string;
  fullname: string;
}


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})


export class ProfileComponent extends CartComponent implements OnInit {
  title: string;
  // Islogin = false;
  @ViewChild('onlyErrorMsz') public onlyErrorMsz;
  @ViewChild('onlySuccessMsz') public onlySuccessMsz;
  @ViewChild('onlyAttentionMsz') onlyAttentionMsz;
  errorMsz: string;
  successMsz: string;
  attentionMsz: string;
  login = false;
  SignUp = false;
  firstPage = true; 
  customer: CustomerInfo;
  customerLogin: any = CustomerLogin;
  customerData: any;
  customerInfoProfile: any;
  addressByRestaurant: any;
  orderHistory: any;
  invoiceLinkPrefix: string;
  customerFromInfoProfile: FormGroup;
  loading: boolean = true;
  rForm: FormGroup;
  post: any;
  mobile: string = '';
  name: string = '';
  customerDemoRegd:any;

  //phone:any;
  constructor(
    public cartComponent: CartComponent,
    private formBuilder: FormBuilder,
    public router: Router,
    public service: MainService,
    public cartService: CartService,
    public storage: LocalStorage,
    public googleAnalyticsService:GoogleAnalyticsService,public spinner: NgxSpinnerService) {

    super(service,googleAnalyticsService, cartService, router, storage,spinner);

    this.rForm = formBuilder.group({
      'name': [null, Validators.required],
      'mobile': [null, Validators.required],
      'email': [null, Validators.required]
    });

  }

  // @HostListener('document:keyup', ['$event'])
  // @HostListener('document:click', ['$event'])
  // @HostListener('document:wheel', ['$event'])
  // resetTimer() {
  //   this.cartService.notifyUserAction();
  // }
  addPost(post) {
    this.name = post.name;
    this.mobile = post.mobile;
  }

  ngOnInit() {
    this.setupForm();
    this.callInit();
  }

  setupForm(){
    this.customerFromInfoProfile = new FormGroup({
      'username': new FormControl(''),
      'email': new FormControl(''),
      'address': new FormControl(''),
      'mobile': new FormControl('')

    });
  }

  initFormData(customeData:any){
    this.customerFromInfoProfile = new FormGroup({
      'username': new FormControl({value:customeData.firstName+" "+customeData.lastName, disabled:true}),
      'email': new FormControl({value:customeData.email, disabled:true}),
      'address': new FormControl({value:customeData.address, disabled:true}),
      'mobile': new FormControl({value:customeData.phone,disabled:true})
    });
  }


  callInit() {
   // this.loading=true;
    this.spinner.show();
    if (this.service.isLogin) {
      this.storage.getItem('email').subscribe((phoneNumber) => {
        if (phoneNumber != null && phoneNumber != undefined) {
          this.cacheCustEmail = phoneNumber;
          this.service.getLatestOrderHistory(this.cacheCustEmail, MainService.organizationId, 20, false).pipe(
            map((data: any) => {
              this.orderHistory = data.ordersSummary;
              this.invoiceLinkPrefix = data.invoiceLinkPrefix;
              this.service.getCustomerData(this.cacheCustEmail, this.customerLogin.orgId).pipe(
                map((data: any) => {
                  this.customerInfoProfile = data.customer;
                  this.addressByRestaurant = data.addressByRestaurant;
                  this.service.isLogin = true;
                  this.initFormData(data.customer);
                  //this.getCustomerRegdDemo(data.customer.customerId);
                 // this.loading=false;
                  this.spinner.hide();
                  this.storage.setItem('email', this.cacheCustEmail).subscribe(() => { }, () => { });
                  // this.callInit();
                })).subscribe();
            })).subscribe();
        } else {
          this.service.isLogin = false;
          this.router.navigate(['/profile']);
        }
      });

    } else if (this.service.isLogin == false) {
      this.storage.getItem('email').subscribe((phoneNumber) => {
        if (phoneNumber != null && phoneNumber != undefined) {
          this.cacheCustEmail = phoneNumber;
          this.service.getLatestOrderHistory(this.cacheCustEmail, MainService.organizationId, 20, false).pipe(
            map((data: any) => {
              this.orderHistory = data.ordersSummary;
              this.invoiceLinkPrefix = data.invoiceLinkPrefix;
              this.service.getCustomerData(this.cacheCustEmail, this.customerLogin.orgId).pipe(
                map((data: any) => {
                  this.customerInfoProfile = data.customer;
                  this.addressByRestaurant = data.addressByRestaurant;
                  this.service.isLogin = true;
                  this.initFormData(data.customer); 
                  this.storage.setItem('email', this.cacheCustEmail).subscribe(() => { }, () => { });
                  this.spinner.hide();
                })).subscribe();
            })).subscribe();
        } else {
          this.service.isLogin = false;
         // this.loading=false;
          this.spinner.hide();
        }
      }, () => { });


    } else {
      this.service.isLogin = false;
    }

    this.title = this.service.headerTitle;
    this.customer = {
      phonenumber: "+91" + '',
      email: '',
      fullname: '',
    };
    this.customerLogin = {
      appId: null,
      device: null,
      orgId: 11,
    };
  }

  getUserName(data:any){

    if( data!=undefined&& data.customer!=undefined){
      if(data.customer.firstName!=""){
          this.service.userName=data.customer.firstName;

      }else{
        this.service.userName=data.customer.lastName;
      }
    }
  }

  loadingUpdatedAddress() {
    this.service.getCustomerData(this.customerLogin.phoneNumber, MainService.organizationId).pipe(
      map((data: any) => {
        this.customerInfoProfile = data.customer;
         this.getUserName(data)
        this.addressByRestaurant = data.addressByRestaurant;
        this.service.isLogin = true;
        if (this.customerLogin.phoneNumber.length > 10) {
          this.storage.setItem('custPhoneNo', this.customerLogin.phoneNumber).subscribe(() => { }, () => { });
        }
       // this.loading = false;
        this.spinner.hide();
      })).subscribe();
  }

  validateEmail(email) {
    var emailMatch = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    if (email != undefined && email != '' && email != null) {
      if (emailMatch.test(email)) {
        if(this.customerBasicInfo!=undefined)
          this.customerBasicInfo.email = email;
        return true;
      } else {
        this.attentionMsz = "Please enter a valid email address to get the invoice";
        this.onlyAttentionMsz.show();
        setTimeout(() => {
          this.onlyAttentionMsz.hide();
        }, 3000);
        return false;
      }
    } else {
      this.attentionMsz = "Please enter a valid email address to get the invoice";
      this.onlyAttentionMsz.show();
      setTimeout(() => {
        this.onlyAttentionMsz.hide();
      }, 3000);
      return false; 
    }
  }

  loginCustomer(emailInput:any) {
    this.service.otpMessage="Grab a cup of coffee while we upadate your inbox with the Registration Code";
    var email=emailInput.value;
    if(this.validateEmail(email)){
      this.customerLogin.email=email;
   //   this.loading = true;
      this.spinner.show();
      this.service.loginCustomer(email)
      .subscribe(
        data => {
          this.service.otpMessage="";
         // this.loading = false;
          this.spinner.hide();
          this.service.openOtpBox();
          this.service.confirmed().subscribe(confirmed => {
            if (confirmed) {
            //  this.loading = true;
              this.spinner.show();
              this.verifyOauthOtp(this.service.otp, email);
            }
          });
        },
        err => {
        }
      );
    }else{
      return false;
    }
  }

  loginCustomerProfile() {

    this.service.getLatestOrderHistory(this.customerLogin.email, MainService.organizationId, 20, false).pipe(
      map((data: any) => {
        this.orderHistory = data.ordersSummary;
        this.invoiceLinkPrefix = data.invoiceLinkPrefix;
        this.service.getCustomerData(this.customerLogin.email, MainService.organizationId).pipe(
          map((data: any) => {
            if (data.customer != null) {
              this.customerInfoProfile = data.customer;
              this.getUserName(data);
              this.addressByRestaurant = data.addressByRestaurant;
              this.service.isLogin = true;
             // this.loading = false;
              this.spinner.hide();
              this.initFormData(data.customer);
            //  this.getCustomerRegdDemo(data.customer.customerId);
              this.storage.setItem('email', this.customerLogin.email).subscribe(() => { }, () => { });
            } else {
              this.attentionMsz = data.message;
              this.onlyAttentionMsz.show();
              this.loading = false;
              this.spinner.hide();
            }
          })).subscribe();
      })).subscribe();
  }

  //  getCustomerRegdDemo(customerId:number){
  //     this.service.getCustomerDemoRegd(customerId).subscribe(
  //       data => {
  //         this.customerDemoRegd = data;
  //         this.loading = false;
  //     });
  //  }



  updateAddress(id, customerId, event: any) {
    for (var i = 0; i < this.addressByRestaurant.length; i++) {
      if (this.addressByRestaurant[i] != null) {
        for (var j = 0; j < this.addressByRestaurant[i].customerAddress.length; j++) {
          if (this.addressByRestaurant[i].customerAddress[j].id == id) {
            this.service.updateAddress(id, customerId, event.srcElement.customerAddress.value,
              event.srcElement.deliveryArea.value,
              this.addressByRestaurant[i].customerAddress[j].city, this.addressByRestaurant[i].customerAddress[j].state).pipe(
                map((data: any) => {
                  if (data.status == "success") {
                    this.successMsz = "Address has been updated";
                    this.onlySuccessMsz.show();
                  } else if (data.status == "error") {
                    this.errorMsz = "Something went wrong." + data.message;
                    this.onlyErrorMsz.show();
                  }
                }
                )).subscribe();
          }
        }
      }
    }
  }

  deleteAddress(id: any, customerId: any) {
  //  this.loading = true;
    this.spinner.show();
    for (var i = 0; i < this.addressByRestaurant.length; i++) {
      if (this.addressByRestaurant[i] != null) {
        for (var j = 0; j < this.addressByRestaurant[i].customerAddress.length; j++) {
          if (this.addressByRestaurant[i].customerAddress[j].id == id) {
            this.addressByRestaurant[i].customerAddress.splice(this.addressByRestaurant[i].customerAddress.indexOf(this.addressByRestaurant[i].customerAddress[j]), 1);
            this.service.deleteAddress(id, customerId).pipe(
              map((data: any) => {
                if (data.status == "success") {
                 // this.loading = false;
                  this.spinner.hide();
                  this.successMsz = "Address has been removed successfully";
                  this.onlySuccessMsz.show();
                } else if (data.status == "error") {
                  this.errorMsz = "Something went wrong." + data.message;
                  this.onlyErrorMsz.show();
                }
              }
              )).subscribe();

          }
        }
      }
    }

  }
  //verify Auth Otp Used in Log In

  verifyOauthOtp(value: any, phone: string) {
    this.service.verifyOtp(value, phone).subscribe(
      data => {
        // this.loading=false;
         this.spinner.hide();
        if(data==true){
          this.loginCustomerProfile();
        }else{
          this.errorMsz = "Wrong OTP. Please try again";
          this.onlyErrorMsz.show();
          setTimeout(() => {
            this.onlyErrorMsz.hide();
          }, 3000);
          this.openOTPDialog(phone);
        }
        
      },err => {
        this.errorMsz = "Wrong OTP. Please try again";
        this.onlyErrorMsz.show();
        setTimeout(() => {
          this.onlyErrorMsz.hide();
        }, 3000);
         this.openOTPDialog(phone);
      })
  }

  openOTPDialog(phoneNumber){
    this.service.openOtpBox();
    this.service.confirmed().subscribe(confirmed => {
      if (confirmed) {
       // this.loading=true;
        this.spinner.show();
        this.verifyOauthOtp(this.service.otp,phoneNumber);
        }
      });
  }
  redirectLogin() {
    this.login = true;
    this.firstPage = false;
  }
  redirectSignUp() {
    this.login = false;
    this.firstPage = false;
    this.SignUp = true;
  }
  redirectProfile() {
    this.service.isLogin = true;
    this.SignUp = false;
    this.login = false;
    this.firstPage = false;
  }

  showMenuPage() {

  }

  getBadgeCount() {
    return this.cartService.getBadgeCount();
  }
}

