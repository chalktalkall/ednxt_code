import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InstituteCoursesComponent } from './institute-courses.component';

describe('InstituteCoursesComponent', () => {
  let component: InstituteCoursesComponent;
  let fixture: ComponentFixture<InstituteCoursesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InstituteCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstituteCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
