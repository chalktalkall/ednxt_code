import { Component, Injectable,Output, EventEmitter} from '@angular/core';
import{Router, NavigationEnd} from '@angular/router';

declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


@Injectable()
export class AppComponent {
  title: string;
  loading = false;


  //dtOptions: DataTables.Settings = {};
  @Output() public sidenavToggle = new EventEmitter();
  constructor(public router: Router) { 
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd){
          gtag('config', 'G-Q3YF0159YV', 
                {
                  'page_path': event.urlAfterRedirects
                }
               );
       }
    }
 )}

 onOpen(any){

 }


}
