import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatBottomSheetModule, MAT_BOTTOM_SHEET_DEFAULT_OPTIONS } from '@angular/material/bottom-sheet';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MainService } from './shared/services/main.service';
import { ActivateGuardService } from './shared/services/activate-guard.service';

import { AppComponent } from './app.component';
import { LocationComponent } from './home/location.component';
import { MenuComponent, ReplacementOptions, RegdDialog } from './menu/menu.component';
import { CartComponent } from './cart/cart.component';
import { ProfileComponent } from './profile/profile.component';
import { CONST_ROUTING } from './app.route';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { CartService } from './shared/services/cart.service';
import { MapList } from './shared/pipe/mapList.pipe';
import { CapitalizePipe } from './shared/pipe/capitalize.pipe';
//import { TaxService } from './shared/services/tax.service';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ProfileSignoutComponent } from './profile/profileSignout.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';

import { CuisinesComponent, custFilter } from './courses/cuisines.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatBadgeModule } from '@angular/material/badge';
import { MatExpansionModule } from '@angular/material/expansion';
import { OTPdialogComponent } from './shared/components/tools/OTPdialog.component';
import { PrivacyPoliciesComponent } from './policies/privacy-policies/privacy-policies.component';
import { TermsConditionsComponent } from './policies/terms-conditions/terms-conditions.component';
import { ContactUsComponent } from './policies/contact-us/contact-us.component';
import { AboutUsComponent } from './policies/about-us/about-us.component';
import { TrainerModelComponent } from './shared/components/trainer-model/trainer-model.component';
import { FooterComponent } from './navigation/footer/footer.component';
import { InstructorRegComponent } from './shared/components/instructor-reg/instructor-reg.component';
import { InstituteCoursesComponent, custInstFilter } from './institute-courses/institute-courses.component';
import { GoogleAnalyticsService } from './shared/services/google-analytics.service';
import { DurationCal } from './shared/pipe/durationCalculator.pipe';
import { SidebarModule } from 'ng-sidebar';
import { NgxSpinnerModule } from "ngx-spinner";
import { CookieServiceModule, CookieConfig } from 'cookie-service-banner';
import { SwiperModule } from "swiper/angular";

// @Injectable()
// export class MyHammerConfig extends HammerGestureConfig {
//   overrides = <any>{
//     'pan': { direction: Hammer.DIRECTION_All },
//     'swipe': { direction: Hammer.DIRECTION_VERTICAL },
//   };

//   buildHammer(element: HTMLElement) {
//     const mc = new Hammer(element, {
//       touchAction: 'auto',
//       inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput,
//       recognizers: [
//         [Hammer.Swipe, {
//           direction: Hammer.DIRECTION_HORIZONTAL
//         }]
//       ]
//     });
//     return mc;
//   }
// }

const testLibConfig: CookieConfig = {
  header: {
    title: "Cookie Consent Banner",
    message: "We use cookies to improve our Website functionality and to provide you with a better browsing experience. Detailed information on the use of cookies on this Website is provided in our Privacy Policy. By using this Website or clicking on 'Got it', you consent to the use of cookies ",
    domain: "www.coursedge.org",
    ga_id: "UA-123456-1",
    color: '#fff',
    bcolor: '#000'
  },
  acceptButton: {
    enable: true,
    accept: "Got it!",
    color: '#fff',
    bcolor: 'rgb(16 117 188)'
  },
  allowButton: {
    enable: false,
    allow: "Okay",
    color: '#000',
    bcolor: '#f36e15f5'
  },
  declineButton: {
    enable: false,
    deny: "Refuse Cookie",
    color: '#000',
    bcolor: '#fff'
  },
  learnMoreLink: {
    enable: true,
    learnMore: "Privacy Polices",
    link: "#/policies",
    color: '#3D9BFF'
  },
  review: {
    enable: false,
    message: "Review My consentement",
    color: "",
    bcolor: "",
  }
}

@NgModule({
  declarations: [
    AppComponent,
    LocationComponent,
    MenuComponent,
    ReplacementOptions,
    CartComponent,
    ProfileComponent,
    ProfileSignoutComponent,
    NotFoundComponent,
    MapList,
    CapitalizePipe,
    custFilter,
    custInstFilter,
    HeaderComponent,
    SidenavListComponent,
    CuisinesComponent,
    RegdDialog,
    OTPdialogComponent,
    PrivacyPoliciesComponent,
    TermsConditionsComponent,
    ContactUsComponent,
    AboutUsComponent,
    TrainerModelComponent,
    FooterComponent,
    InstructorRegComponent,
    InstituteCoursesComponent,
    DurationCal

  ],
  imports: [
    MatNativeDateModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CONST_ROUTING,
    MatBottomSheetModule,
    MatSelectModule,
    MatTabsModule,
    MatSidenavModule,
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    NgSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatBadgeModule,
    MatExpansionModule,
    MatCardModule,
    SidebarModule.forRoot(),
    CookieServiceModule,
    CookieServiceModule.forRoot(testLibConfig),
    NgxSpinnerModule,
    // FacebookModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    // ScrollToModule.forRoot(),
    Ng2SearchPipeModule,
    // ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    TooltipModule.forRoot(),
    SwiperModule
  ],
  exports: [
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule,
    MatSidenavModule,
    MatGridListModule,
    FlexLayoutModule,
    RegdDialog,
    TrainerModelComponent,
    InstructorRegComponent,
    OTPdialogComponent
  ],
  entryComponents: [ReplacementOptions, RegdDialog, TrainerModelComponent, InstructorRegComponent, OTPdialogComponent],
  providers: [MainService, ActivateGuardService, CartService, CartComponent, GoogleAnalyticsService,  
    { provide: MAT_BOTTOM_SHEET_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }],
  bootstrap: [AppComponent]

})
export class AppModule {

}
