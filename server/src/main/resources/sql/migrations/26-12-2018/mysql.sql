CREATE TABLE `INVOICES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countryId` int(11) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `openTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closeTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `bill` float DEFAULT '0',
  `invoiceId` varchar(30) DEFAULT '',
  `checkType` int(11) DEFAULT '0',
  `deliveryDateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deliveryAddress` varchar(300) DEFAULT '',
  `orderId` int(11) DEFAULT '0',
  `transactionId` varchar(100) DEFAULT NULL,
  `transactionStatus` varchar(45) DEFAULT NULL,
  `roundOffTotal` int(11) DEFAULT '0',
  `taxJsonObject` varchar(500) DEFAULT NULL,
  `responseCode` varchar(45) DEFAULT NULL,
  `paymentType` varchar(45) DEFAULT NULL,
  `orderSource` varchar(45) DEFAULT NULL,
  `rewards` int(11) DEFAULT '0',
  `editOrderRemark` varchar(250) DEFAULT NULL,
  `firstOrder` tinyint(4) DEFAULT '0',
  `creditBalance` float DEFAULT '0',
  `lastInvoiceAmount` float DEFAULT '0',
  `modifiedTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `closeTime_UNIQUE` (`closeTime`),
  KEY `idx_invoiceId` (`invoiceId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;

ALTER TABLE `CUSTOMERS` 
ADD COLUMN `longitude` DOUBLE NULL AFTER `stripeId`,
ADD COLUMN `latitude` DOUBLE NULL AFTER `longitude`;

ALTER TABLE `CUSTOMERS` 
DROP COLUMN `deliveryArea`,
DROP COLUMN `deliveryTime`;

