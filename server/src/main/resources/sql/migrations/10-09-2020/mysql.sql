
ALTER TABLE `pepcare_path`.`DEMO_REGD` 
ADD COLUMN `attendeesCount` INT NULL AFTER `vendorId`;

ALTER TABLE `pepcare_path`.`DISHES` 
CHANGE COLUMN `description` `description` VARCHAR(5000) NULL DEFAULT NULL ,
CHANGE COLUMN `shortDescription` `shortDescription` VARCHAR(5000) NULL DEFAULT NULL ;


ALTER TABLE `pepcare_path`.`SECTION_DISHES` 
CHANGE COLUMN `shortDescription` `shortDescription` VARCHAR(5000) NULL DEFAULT NULL ;


ALTER TABLE `USER_LMS` 
ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT AFTER `root_Account_id`,
ADD PRIMARY KEY (`id`);

ALTER TABLE `USER_LMS` 
CHANGE COLUMN `root_Account_id` `root_account_id` INT NULL DEFAULT NULL ;


