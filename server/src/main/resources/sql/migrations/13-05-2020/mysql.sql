ALTER TABLE `PLATTERS` 
ADD COLUMN `vendorMargin` FLOAT NULL DEFAULT 0 AFTER `validity`;

ALTER TABLE `INVOICES` 
ADD COLUMN `tokenAmount` FLOAT NULL DEFAULT 0 AFTER `razorPayId`,
ADD COLUMN `vendorMargin` FLOAT NULL DEFAULT 0 AFTER `tokenAmount`;

ALTER TABLE `VENDOR` 
ADD COLUMN `vendorMargin` FLOAT NULL DEFAULT 0 AFTER `introduction`;

ALTER TABLE `pepcare_path`.`VENDOR` 
ADD COLUMN `rating` FLOAT NULL DEFAULT 0 AFTER `vendorMargin`;

------------------------ 02-06-2020----------------

CREATE TABLE `pepcare_path`.`Trainer` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `phone` VARCHAR(15) NULL,
  `email` VARCHAR(50) NULL,
  `experience` INT NULL,
  `Trainercol` VARCHAR(45) NULL,
  `speciality` VARCHAR(300) NULL,
  `shortDescription` VARCHAR(500) NULL,
  `highestEducation` VARCHAR(100) NULL,
  `linkedInProfile` VARCHAR(45) NULL,
  `website` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`));
  
ALTER TABLE `pepcare_path`.`Trainer` 
ADD COLUMN `countryId` INT NULL AFTER `city`;


ALTER TABLE `pepcare_path`.`PLATTERS` 
ADD COLUMN `demo_url` VARCHAR(100) NULL AFTER `vendorMargin`;

