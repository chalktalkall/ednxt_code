
ALTER TABLE `PLATTERS` 
CHANGE COLUMN `timeForPrepartion` `timeForPrepartion` FLOAT NULL DEFAULT NULL ;

ALTER TABLE `EDIT_INVOICES` 
CHANGE COLUMN `invoiceJson` `invoiceJson` VARCHAR(50000) NULL DEFAULT NULL ;

ALTER TABLE `pepcare_path`.`VENDOR` 
ADD COLUMN `description` VARCHAR(1000) NULL AFTER `servingDistance`;