
ALTER TABLE `TAXTYPES` 
ADD COLUMN `min_value` FLOAT NULL DEFAULT '0' AFTER `status`,
ADD COLUMN `max_value` FLOAT NULL DEFAULT '0' AFTER `minValue`;

#new 15-02-2021

CREATE TABLE `DB_CONFIG` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `dbType` TINYINT NOT NULL,
  `dbUrl` VARCHAR(100) NOT NULL,
  `dbPort` INT NOT NULL,
  `username` VARCHAR(50) NOT NULL,
  `passsword` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`));

  
  ALTER TABLE `DB_CONFIG` 
ADD COLUMN `vendorId` INT NOT NULL AFTER `passsword`;

ALTER TABLE `DB_CONFIG` 
ADD COLUMN `univId` INT NOT NULL AFTER `campusId`,
CHANGE COLUMN `vendorId` `campusId` INT NOT NULL ;

ALTER TABLE `DB_CONFIG` 
ADD COLUMN `dbView` VARCHAR(45) NOT NULL AFTER `univId`,
ADD COLUMN `dbInvoiceTable` VARCHAR(45) NULL AFTER `dbView`;

ALTER TABLE `CANVAS_COURSES` 
ADD COLUMN `days_offered` VARCHAR(100) NULL AFTER `status`,
ADD COLUMN `start_time` VARCHAR(45) NULL AFTER `days_offered`,
ADD COLUMN `end_time` VARCHAR(45) NULL AFTER `start_time`,
ADD COLUMN `non_regular_times` VARCHAR(100) NULL AFTER `end_time`,
ADD COLUMN `session` VARCHAR(45) NULL AFTER `non_regular_times`,
ADD COLUMN `max_enrollement` INT NULL AFTER `session`,
ADD COLUMN `total_enrollment` INT NULL AFTER `max_enrollement`,
ADD COLUMN `course_level` VARCHAR(45) NULL AFTER `total_enrollment`,
ADD COLUMN `instructor` VARCHAR(45) NULL AFTER `course_level`,
ADD COLUMN `campus` VARCHAR(100) NULL AFTER `instructor`,
ADD COLUMN `course_material` VARCHAR(500) NULL AFTER `campus`,
ADD COLUMN `credits` FLOAT NULL AFTER `course_material`,
ADD COLUMN `course_dept` VARCHAR(45) NULL AFTER `credits`;

ALTER TABLE `INVOICES` 
ADD COLUMN `courseSource` VARCHAR(45) NULL AFTER `classMode`;



