package com.emanage.enums.till;

public enum HandoverStatus {
    OPEN, CLOSE, DELETE;
}
