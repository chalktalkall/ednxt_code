package com.emanage.enums.till;

public enum TransactionType {
    DEBIT, CREDIT
}
