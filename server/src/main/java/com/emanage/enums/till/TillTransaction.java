package com.emanage.enums.till;

public enum TillTransaction {
    NEW, UPDATE, SUCCESS, CANCEL
}
