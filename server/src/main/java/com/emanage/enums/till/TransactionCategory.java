package com.emanage.enums.till;

public enum TransactionCategory {
    DEBIT, CREDIT, CASH, OTHER
}
