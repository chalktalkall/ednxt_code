package com.emanage.enums.till;

public enum TillTransactionStatus {
    PENDING, SUCCESS, FAILED, CANCELLED
}
