package com.emanage.enums.till;

public enum TransactionStatus {
    DELIVERED, CANCELLED, DISPATCHED
}
