/**
 * 
 */
package com.emanage.enums.order;

/**
 * @author rahul
 *
 */
public enum SourceType {

	TABLE, PHONE, ONLINE, COUNTER;
	
}
