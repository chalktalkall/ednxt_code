/**
 * 
 */
package com.emanage.enums.order;

/**
 * @author rahul
 
 */
public enum Status {
	NEW, PENDING, READY,DELIVERED,PAID,CANCELLED,OUTDELIVERY,CONFIRMDELIVERY,EDITMONEYOUT,EDITMONEYIN;
}
