/**
 * 
 */
package com.emanage.enums.order;

/**
 * @author rahul
 *
 */
public enum DestinationType {

	TABLE, HOME, COUNTER;
	
}
