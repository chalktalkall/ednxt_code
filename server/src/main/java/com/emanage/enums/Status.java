/**
 * 
 */
package com.emanage.enums;

/**
 * @author rahul
 *
 */
public enum Status {

	ACTIVE, INACTIVE;
}
