/**
 * 
 */
package com.emanage.enums.check;

/**
 * @author rahul
 *
 */
public enum AdditionalCategories {
	Discount, Charges;
}
