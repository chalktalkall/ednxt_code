/**
 * 
 */
package com.emanage.enums.check;

/**
 * @author rahul
 *
 */
public enum Status {

	Paid, Unpaid, Readytopay, Cancel,Pending;
}
