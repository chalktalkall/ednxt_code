/**
 * 
 */
package com.emanage.enums.check;

/**
 * @author rahul
 *
 */
public enum PaymentMode {
	CREDIT, CASH,ROOM, EMC,SUBSCRIPTION, PG , COD ,PG_PENDING, PENDING,Credit_Machine,Customer_Credit,Third_Party,MOBIKWIK_WALLET,WALLET_PENDING,PAYTM_PENDING,PAYTM,ANY;
}
