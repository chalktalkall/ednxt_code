/**
 * 
 */
package com.emanage.enums.check;

/**
 * @author rahul
 *
 */
public enum CheckType {
	Table, TakeAway, Delivery, MorningExpress;
}
