/**
 * 
 */
package com.emanage.enums.check;

/**
 * @author rahul
 *
 */
public enum BasePaymentType {

	CREDIT, CASH, PREPAID, DNC;
}
