package com.emanage.enums.report;

/**
 * Created by rahul on 3/1/2017.
 */
public enum ReportFormat {

    XLS("xls"), PDF("pdf"), HTML("html"), DOC("doc"), ODT("odt"), ODS("ods");

    private String value;

    private ReportFormat(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
