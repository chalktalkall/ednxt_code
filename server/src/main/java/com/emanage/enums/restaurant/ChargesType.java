/**
 * 
 */
package com.emanage.enums.restaurant;

/**
 * @author rahul
 *
 */
public enum ChargesType {

	PERCENTAGE, ABSOLUTE;
}
