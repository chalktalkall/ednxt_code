package com.emanage.enums.giftCard;

/**
 * Created by rahul on 5/27/2017.
 */
public enum GiftCardStatus {
    NEW, PRINT, ACTIVE, INACTIVE, REDEEMED, PAYMENT_AWAITING;
}
