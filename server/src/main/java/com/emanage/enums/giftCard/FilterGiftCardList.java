package com.emanage.enums.giftCard;

/**
 * Created by rahul on 5/28/2017.
 */
public enum FilterGiftCardList {

    PURCHASED, RECEIVED, ALL;
}
