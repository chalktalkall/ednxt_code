package com.emanage.enums.credit;

public enum BilligCycle {
    ONE_OFF, onEachSUN, on1stDayOfMonth, on15thOfEveryMonth, on1stAnd15thOfEveryMonth
}
