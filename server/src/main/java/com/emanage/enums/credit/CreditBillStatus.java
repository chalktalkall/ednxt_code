package com.emanage.enums.credit;

public enum CreditBillStatus {

    NEW, PAID, PARTIALLY_PAID, DELIVERED
}
