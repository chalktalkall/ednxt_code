package com.emanage.enums.credit;


/**
 * @author Abhishek
 */
public enum CustomerCreditAccountStatus {
    ACTIVE, INACTIVE, SUSPENDED;
}
