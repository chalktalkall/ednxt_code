package com.emanage.enums.credit;

public enum CreditTransactionStatus {
    PENDING, SUCCESS, FAILED, CANCELED
}
