package com.emanage.dao;

import java.util.Date;
import java.util.List;

import com.emanage.domain.BookedPlatter;
import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.Platter;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.Sections;
import com.emanage.enums.Status;

public interface VendorPlatterDAO {

	public void addPlatter(Platter menu);
	public List<Platter> listPlattersByVendorId(Integer vendorId);
	public List<Platter> allPlattersByStatus(Integer vendorId, Status status);
	public Platter getPlatter(Integer id);
	public void removePlatter(Integer id);
	public void removeSections(List<Integer> sectionIds);
	public Integer getVendorIdUsingPlatterId(Integer id);
	
	public List<Platter> listPlatters();
	public List<Platter> allPosPlatters(Integer countryId, Status status,boolean posStatus);
	public List<Platter> listPlattersByCountry(Integer countryId);

	public Platter getPlatterByPlatterName(String name, Integer restaurantId);
	public void addSection(Sections section); 
	
	public List<Platter> getPlatterByLatlong(double latitud, double longitude, Date date, Integer plateCount);
	public List<Platter> getCoursesByLatlong(double latitud, double longitude,String[] courseTypeArray,String classMode);
	
	public SectionDishes getSectionDish(Integer id);
	
	public List<SectionDishes> getSectionDishByDishId(Integer id);
	public void saveSectionDish(SectionDishes sectionDish);
	
	public void addPlatterBooking(BookedPlatter menu);
	public List<BookedPlatter> listBookedPlattersByVendorIdAndDate(Integer vendorId, Date date);
	public Integer getBookedPlatterCountByDateandId(Integer vendorId, Date date);
	public List<Platter> listFetureCourses();
	public List<Canvas_Courses> listFeturedCanvasCourses();
	
	public List<Canvas_Courses> listFeturedCanvasCourses(Integer unniversityId);
	
	public  List<Platter> getActiveWebinars() ;
	public List<Platter> getCoursesByCriteria(String[] courseTypeArray,String classMode,Integer studentCount);
	
	List<Platter> listFetureTrainings();
	
	public List<Canvas_Courses> getCoursesByCriteriaNew(String[] courseTypeArray);
	List<Canvas_Courses> getCoursesByCriteriaCampusId(String[] courseTypeArray,Integer campusId);
	
	public List<Canvas_Courses> getPopularCourses() ;
	
	
}
