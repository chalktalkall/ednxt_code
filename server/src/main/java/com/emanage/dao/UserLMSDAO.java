package com.emanage.dao;

import java.util.List;

import com.emanage.domain.User_LMS;
import com.emanage.enums.LMS_NAME;

public interface UserLMSDAO {

	List<User_LMS> userLMSList(Integer customerId, LMS_NAME lms,Integer root_Account_id);
	void addUserLMS(User_LMS userLMS);
	
}
