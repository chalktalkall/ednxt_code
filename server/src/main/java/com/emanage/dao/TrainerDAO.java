package com.emanage.dao;

import java.util.List;

import com.emanage.domain.Trainer;
import com.emanage.dto.ResponseDTO;

public interface TrainerDAO {

	ResponseDTO saveTrainer(Trainer presentation);
	List<Trainer> getAllActiveTrainer();
	List<Trainer> getAllTrainer();
	Trainer getTrainer(Integer id);
	ResponseDTO deleteTrainer(int id);
	Trainer getTrainerById(Integer id);
	ResponseDTO editTrainer(Trainer presentation);
}
