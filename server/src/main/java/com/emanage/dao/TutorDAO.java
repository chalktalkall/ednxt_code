package com.emanage.dao;

import com.emanage.domain.TutorRequest;

public interface TutorDAO {

	void saveTutorRequest(TutorRequest request);
}
