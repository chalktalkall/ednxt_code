/**
 * 
 */
package com.emanage.dao;

import java.util.List;

import com.emanage.domain.Section;

/**
 * @author rahul
 *
 */
public interface SectionDAO {

	public void addSection(Section section);
	public void removeSections(List<Integer> sectionIds);
}
