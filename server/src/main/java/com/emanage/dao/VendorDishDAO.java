package com.emanage.dao;

import java.util.List;

import com.emanage.domain.VendorDish;

public interface VendorDishDAO {

	public VendorDish addVendorDish(VendorDish dish);
	public void updateMenuModificationTime(Integer dishId);
	public List<VendorDish> listVendorDishByVendorId(Integer restaurantId);
	public void removeVendorDish(Integer id) throws Exception;
	public List<VendorDish> getVendorDishes(Integer[] ids);
	public VendorDish getVendorDish(Integer id);
}
