package com.emanage.dao;

import java.util.Date;
import java.util.List;

import com.emanage.domain.EditInvoices;
import com.emanage.domain.Invoice;

public interface InvoiceDAO {
	
	public void addInvoice(Invoice invoice);
	public void removeInvoice(Integer id);
	public Invoice getInvoice(Integer id);
	public Invoice getInvoiceByRazorPayId(String orderId);
	public List<Invoice> getInvoiceByInvoiceId(String invoiceId);
	public Invoice getInvoiceByCustId(Integer countryId, Integer custId);
	public List<Invoice> getAllOpenInvoices(Integer countryId);
	public List<Integer> getAllInvoiceNos();
	public List<Invoice> getAllInvoiceIdByCountryId(Integer countryId);
	public List<?> getClosedInvoicesByDate(Integer countryId, Date startDate, Date endDate);
	public List<Invoice> getDailyInvoice(Integer countryId, Date startDate, Date endDate);
	public List<String> getUniqueDishTypes(Integer countryId);
	public List<?> getDailySalesRecords(Integer countryId, Date startDate);
	public List<Invoice> getAllInvoices(List<Integer> ids);
	public List<?> getMonthlyBillSummary(Integer countryId, Date startDate, Date endDate);
	public List<Invoice> getDailyInvoiceByFfc(Integer ffcId,Date startTime, Date endTime);
	public List<Invoice> getCustomersInvoiceList(String email,Integer customerId,Integer countryId);
	public List<Invoice> getCustomersInvoiceListByYear(String phone,Integer customerId,Integer countryId,Date startDate,Date endDate);

	
	
	public void addEditInvoice(EditInvoices editCheck);
	public void removeEditInvoice(Integer id);
	public EditInvoices getEditInvoice(Integer id);
	public List<EditInvoices> getEditInvoiceListByInvoiceNo(Integer invoiceNo);
}
