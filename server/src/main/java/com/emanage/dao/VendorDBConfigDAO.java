package com.emanage.dao;

import java.util.List;

import com.emanage.domain.DBConfig;

public interface VendorDBConfigDAO {

	 DBConfig addDBConfig(DBConfig dbConfig);
	 DBConfig getDBConfig(Integer dbConfig);
	 DBConfig getCampusDBConfig(Integer campusId);
	 List<DBConfig> listDBConfigByCountryId(Integer countryId);
	 void removeDBConfig(Integer dbConfigId);
		
}
