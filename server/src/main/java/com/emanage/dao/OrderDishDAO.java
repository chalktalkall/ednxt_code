package com.emanage.dao;

import java.util.List;

public interface OrderDishDAO {

	public void removeOrderDishes(List<Integer> orderDishIds);
}
