package com.emanage.dao;

import java.util.List;

import com.emanage.domain.Vendor;

public interface VendorDAO {

	public Vendor addVendor(Vendor vendor);
	public List<Vendor> listVendor();
	public List<Vendor> listVendorById(Integer countryId);
	public List<Vendor> listVendorByOrgId(Integer orgId);
	public void removeVendor(Integer vendorId);
	public Vendor getVendor(Integer vendorId);
	public void saveVendor(Vendor vendor);
	List<Vendor> listActiveVendorById(Integer countryId);
	
}
