package com.emanage.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.DemoRegdDAO;
import com.emanage.domain.DemoRegd;
import com.emanage.dto.RegResultDTO;

@Repository
public class DemoRegdDAOImpl implements DemoRegdDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addDemoRegd(DemoRegd demoRegd) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(demoRegd);
	}

	@Override
	public List<DemoRegd> listDemoRegd() {
		// TODO Auto-generated method stub
		
		return sessionFactory.getCurrentSession().createQuery("from DemoRegd").list();
	}

	@Override
	public List<DemoRegd> listDemoRegd(Integer vendorId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(DemoRegd.class)
				.add(Restrictions.eq("vendorId", vendorId))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		// return sessionFactory.getCurrentSession().get;
	}

	@Override
	public void removeDemoRegd(Integer id) throws Exception {
		// TODO Auto-generated method stub
		DemoRegd reg = getDemoRegd(id);
		if (reg != null) {
			sessionFactory.getCurrentSession().delete(reg);
		}
	}

	@Override
	public List<DemoRegd> getDemoRegd(Integer[] ids) {
		// TODO Auto-generated method stub
		
		sessionFactory.getCurrentSession().createCriteria(DemoRegd.class)
				.add(Restrictions.in("id",ids))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		return null;
	}

	@Override
	public DemoRegd getDemoRegd(Integer id) {
		// TODO Auto-generated method stub
		return (DemoRegd)sessionFactory.getCurrentSession().get(DemoRegd.class, id);
	}

	@Override
	public List<RegResultDTO> listDemoByUserId(Integer userId) {
		// TODO Auto-generated method stub
		String query ="Select \n" + 
				"d.demoDate,\n" + 
				"v.businessName,\n" + 
				"s.name\n" + 
				"FROM DEMO_REGD d\n" + 
				"INNER JOIN VENDOR v ON d.vendorId = v.vendorId Inner JOIN PLATTERS s ON s.platterId=d.courseId\n" + 
				" where customerId="+userId;
		
		Query queryResult = sessionFactory.getCurrentSession().createSQLQuery(query);
		
		List<RegResultDTO> regList = new ArrayList();
		List<Object> result = (List<Object>) queryResult.list();
		Iterator itr = result.iterator();
		while (itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			RegResultDTO res =  new RegResultDTO();
			res.courseName= (String) obj[2];
			res.date= (Timestamp) obj[0];
			res.instName=  (String) obj[1];
			
			regList.add(res);
		}
		
		
		return regList;
	}

}
