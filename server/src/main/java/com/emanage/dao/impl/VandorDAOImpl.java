package com.emanage.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.VendorDAO;
import com.emanage.domain.Vendor;
import com.emanage.enums.Status;

@Repository
public class VandorDAOImpl implements VendorDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Vendor addVendor(Vendor vendor) {
		sessionFactory.getCurrentSession().saveOrUpdate(vendor);
		return vendor;
	}

	@Override
	public void saveVendor(Vendor vendor) {
		sessionFactory.getCurrentSession().saveOrUpdate(vendor);
	}
	
	
	@Override
	public Vendor getVendor(Integer vendorId) {
		return (Vendor) sessionFactory.getCurrentSession().get(Vendor.class, vendorId);
	}
	
	@Override
	public List<Vendor> listVendor() {
		return sessionFactory.getCurrentSession().createQuery("from Vendor").list();
	}

	@Override
	public List<Vendor> listVendorById(Integer countryId) {
		return sessionFactory.getCurrentSession().createCriteria(Vendor.class).add(Restrictions.eq("countryId", countryId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Vendor> listActiveVendorById(Integer countryId) {
		return sessionFactory.getCurrentSession().createCriteria(Vendor.class).add(Restrictions.eq("countryId", countryId)).add(Restrictions.eq("status", Status.ACTIVE)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}
	
	@Override
	public List<Vendor> listVendorByOrgId(Integer orgId) {
		return sessionFactory.getCurrentSession().createCriteria(Vendor.class).add(Restrictions.eq("orgId", orgId)).add(Restrictions.eq("status", Status.ACTIVE)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public void removeVendor(Integer id) {
		Vendor vendor = (Vendor) sessionFactory.getCurrentSession().load(Vendor.class, id);
		if (null != vendor) {
			sessionFactory.getCurrentSession().delete(vendor);
		}
	}

}
