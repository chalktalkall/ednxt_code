package com.emanage.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.VendorDBConfigDAO;
import com.emanage.domain.DBConfig;
import com.emanage.domain.VendorDish;


@Repository
public class VendorDBConfigDAOImpl implements VendorDBConfigDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public DBConfig addDBConfig(DBConfig dbConfig) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(dbConfig);
		return 	dbConfig;
	}

	@Override
	public DBConfig getDBConfig(Integer dbConfigId) {
		// TODO Auto-generated method stub
		return (DBConfig) sessionFactory.getCurrentSession().get(DBConfig.class, dbConfigId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DBConfig> listDBConfigByCountryId(Integer univId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(DBConfig.class).add(Restrictions.eq("univId", univId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public void removeDBConfig(Integer dbConfigId) {
		// TODO Auto-generated method stub
		DBConfig dbconfig=getDBConfig(dbConfigId);
		if(dbconfig!=null) {
			sessionFactory.getCurrentSession().delete(dbconfig);
		}
	}

	@Override
	public DBConfig getCampusDBConfig(Integer campusId) {
		// TODO Auto-generated method stub
		return (DBConfig) sessionFactory.getCurrentSession().createCriteria(DBConfig.class).add(Restrictions.eq("campusId", campusId)).
				setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).uniqueResult();
	
	}


		
}
