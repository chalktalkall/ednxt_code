package com.emanage.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.UserLMSDAO;
import com.emanage.domain.User_LMS;
import com.emanage.enums.LMS_NAME;

@Repository
public class UserLMSDAOImpl implements UserLMSDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<User_LMS> userLMSList(Integer customerId, LMS_NAME lms,Integer root_account_id) {

		return sessionFactory.getCurrentSession().createCriteria(User_LMS.class).add(Restrictions.eq("customer_id", customerId)).
		add(Restrictions.eq("lms", lms)).add(Restrictions.eq("root_account_id", root_account_id))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

	}

	@Override
	public void addUserLMS(User_LMS userLMS) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(userLMS);

	}

}
