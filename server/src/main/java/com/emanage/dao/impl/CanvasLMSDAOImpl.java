package com.emanage.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.CanvasLMSDAO;
import com.emanage.domain.Canvas_Courses;
import com.emanage.enums.Status;


@Repository
public class CanvasLMSDAOImpl implements CanvasLMSDAO{

	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addCanvasCourse(Canvas_Courses course) {
		sessionFactory.getCurrentSession().saveOrUpdate(course);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Canvas_Courses> getCourseList(Integer rootAccountId, Integer vendorId) {
		return sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class).add(Restrictions.eq("root_account_id", rootAccountId)).
				add(Restrictions.eq("vendorId", vendorId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public Canvas_Courses getCourse(Integer id, Integer vendorId) {
		// TODO Auto-generated method stub
		return  (Canvas_Courses) sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class).add(Restrictions.eq("id", id)).
				add(Restrictions.eq("vendorId", vendorId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).uniqueResult();
	}
	
	@Override
	public Canvas_Courses getCourseById(Integer course_id) {
		// TODO Auto-generated method stub
		 return (Canvas_Courses) sessionFactory.getCurrentSession().get(Canvas_Courses.class, course_id);
		//return  (Canvas_Courses) sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class).add(Restrictions.eq("courseId", course_id)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).uniqueResult();
	}
	
	@Override
	public Canvas_Courses getCourseByLMSCourseId(Integer id,Integer vendorId) {
		// TODO Auto-generated method stub
		 return (Canvas_Courses) sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class).add(Restrictions.eq("id", id)).
					add(Restrictions.eq("vendorId", vendorId)).uniqueResult();
		//return  (Canvas_Courses) sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class).add(Restrictions.eq("courseId", course_id)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).uniqueResult();
	}

	@Override
	public List<Canvas_Courses> getCourseList(Integer vendorId) {
		// TODO Auto-generated method stub
		 return sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class).
				add(Restrictions.eq("vendorId", vendorId)).add(Restrictions.eq("status", Status.ACTIVE)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}
	
	@Override
	public List<Canvas_Courses> getAllCourseList(Integer vendorId) {
		// TODO Auto-generated method stub
		 return sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class).
				add(Restrictions.eq("vendorId", vendorId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public void deleteLMSCourse(Canvas_Courses course) {
		// TODO Auto-generated method stub
		if (null != course) {
			sessionFactory.getCurrentSession().delete(course);
		}
		
	}

}
