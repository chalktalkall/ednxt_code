package com.emanage.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.VendorDishDAO;
import com.emanage.domain.VendorDish;

@Repository
public class VendorDishDAOImpl implements VendorDishDAO         {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public VendorDish addVendorDish(VendorDish vendorDish) {
		// TODO Auto-generated method stub
		 sessionFactory.getCurrentSession().saveOrUpdate(vendorDish);
		 return vendorDish;
	}

	@Override
	public void updateMenuModificationTime(Integer vendorDishId) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().createSQLQuery("update MENUS SET modifiedTime=CURRENT_TIMESTAMP() where menuId IN (select distinct(menuId) FROM MENU_SECTION JOIN (select distinct(sectionId) as sectionId FROM SECTION_DISH where dishId=" + vendorDishId + " ) sec where sec.sectionId = MENU_SECTION.sectionId)").executeUpdate();

	}

	@Override
	public List<VendorDish> listVendorDishByVendorId(Integer vendorId) {
		// TODO Auto-generated method stub
		return  sessionFactory.getCurrentSession().createCriteria(VendorDish.class).add(Restrictions.eq("vendorId", vendorId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public void removeVendorDish(Integer id) throws Exception {
		// TODO Auto-generated method stub
		VendorDish vendorDish = (VendorDish) sessionFactory.getCurrentSession().load(VendorDish.class, id);
		if (null != vendorDish) {
			sessionFactory.getCurrentSession().delete(vendorDish);
		}
	}

	@Override
	public List<VendorDish> getVendorDishes(Integer[] ids) {
		// TODO Auto-generated method stub
		return (List<VendorDish>) sessionFactory.getCurrentSession().createCriteria(VendorDish.class).add(Restrictions.in("dishId", ids)).list();
	}

	@Override
	public VendorDish getVendorDish(Integer id) {
		// TODO Auto-generated method stub
		return (VendorDish) sessionFactory.getCurrentSession().get(VendorDish.class, id);
	}

	
}
