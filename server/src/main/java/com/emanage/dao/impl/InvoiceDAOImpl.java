/**
 * 
 */
package com.emanage.dao.impl;


import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.InvoiceDAO;
import com.emanage.domain.EditInvoices;
import com.emanage.domain.Invoice;
import com.emanage.domain.OrderDish;
import com.emanage.enums.check.Status;
import com.emanage.utility.StringUtility;

/**
 * @author rahul
 *
 */
@Repository
public class InvoiceDAOImpl implements InvoiceDAO {

	final static Logger logger = Logger.getLogger(InvoiceDAOImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addInvoice(Invoice invoice) {
		sessionFactory.getCurrentSession().saveOrUpdate(invoice);
	}

	@Override
	public void removeInvoice(Integer id) {
		Invoice invoice = (Invoice) sessionFactory.getCurrentSession().load(Invoice.class, id);
		if (null != invoice) {
			sessionFactory.getCurrentSession().delete(invoice);
		}		
	}

	@Override
	public Invoice getInvoice(Integer id) {
		return (Invoice) sessionFactory.getCurrentSession().get(Invoice.class, id);
	}

	@Override
	public List<Invoice> getInvoiceByInvoiceId(String invoiceId) {
		return sessionFactory.getCurrentSession().createCriteria(Invoice.class).add(Restrictions.eq("invoiceId", invoiceId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}
	

	@Override
	public Invoice getInvoiceByCustId(Integer countryId, Integer custId) {
		List<Invoice> invoiceForTable = sessionFactory.getCurrentSession().createCriteria(Invoice.class).add(Restrictions.and(Restrictions.eq("customerId", custId), Restrictions.eq("countryId", countryId),Restrictions.ne("status", Status.Pending), Restrictions.ne("status", Status.Paid),Restrictions.ne("status", Status.Unpaid), Restrictions.ne("status", Status.Cancel))).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		if (invoiceForTable != null && invoiceForTable.size() > 0) {
			return invoiceForTable.get(0);
		} else {
			return null;
		}
	}

	
	@Override
	public List<Invoice> getAllOpenInvoices(Integer countryId) {
		return sessionFactory.getCurrentSession().createCriteria(Invoice.class).add(Restrictions.and(Restrictions.eq("countryId", countryId), Restrictions.ne("status", Status.Paid),Restrictions.ne("status", Status.Pending), Restrictions.ne("status", Status.Cancel))).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}
	
	@Override
	public List<Integer> getAllInvoiceNos() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
		criteria.setProjection( Projections.projectionList().add( Projections.property("invoiceNo")));

		List<Integer> ids=criteria.list();
		return ids;
	}
	
	@Override
	public List getClosedInvoicesByDate(Integer countryId, Date startDate, Date endDate) {
		//return sessionFactory.getCurrentSession().createCriteria(Check.class).add(Restrictions.and(Restrictions.eq("restaurantId", restaurantId), Restrictions.ne("status", Status.Unpaid), Restrictions.ne("status", Status.Cancel), Restrictions.between("closeTime", startDate, endDate))).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		String sqlQuery = "SELECT sum(bill), checkType FROM Invoice WHERE countryId = :countryId AND closeTime > :startDate AND closeTime < :endDate group by checkType)";
		Query query = sessionFactory.getCurrentSession().createQuery(sqlQuery).setParameter("countryId", countryId).setParameter("startDate", startDate).setParameter("endDate", endDate);
		return query.list();

	}
	
	@Override
	public List<Invoice> getDailyInvoice(Integer countryId, Date startDate, Date endDate) {
		return sessionFactory.getCurrentSession().createCriteria(Invoice.class).add(Restrictions.and(Restrictions.eq("countryId", countryId), Restrictions.gt("deliveryDateTime", startDate), Restrictions.lt("deliveryDateTime", endDate))).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}
	
	@Override
	public List<String> getUniqueDishTypes(Integer countryId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderDish.class);
		criteria.setProjection( Projections.distinct(Projections.property("dishType")));

		List<String> ids=criteria.list();
		return ids;
		
	}
	
	@Override
	public List getDailySalesRecords(Integer countryId, Date startDate) {
		String sqlQuery = "select sum(c.bill), c.checkType, i.dishType, sum(i.price) FROM (SELECT bill, checkType FROM Invoice WHERE countryId = :countryId AND openTime > :startDate) c JOIN Invoice.orders o JOIN o.orderDishes i group by c.checkType, i.dishType)";
		Query query = sessionFactory.getCurrentSession().createQuery(sqlQuery).setParameter("countryId", countryId).setParameter("startDate", startDate);
		return query.list();
	}
	
	@Override
	public List<Invoice> getAllInvoices(List<Integer> ids) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
		criteria.add(Restrictions.in("id", ids));
		return criteria.list();
	}
	
	@Override
	public List getMonthlyBillSummary(Integer countryId, Date startDate, Date endDate) {
		String sqlQuery = "SELECT sum(bill) as totalBill, sum(additionalChargesValue1) as totalTax1, sum(additionalChargesValue2) as totalTax2, sum(additionalChargesValue3) as totalTax3, sum(bill+additionalChargesValue1+additionalChargesValue2+additionalChargesValue3) as totalInclTaxes FROM Invoice WHERE countryId = :countryId AND openTime >= :startDate AND openTime <= :endDate";
		Query query = sessionFactory.getCurrentSession().createQuery(sqlQuery).setParameter("countryId", countryId).setParameter("startDate", startDate).setParameter("endDate", endDate);
		return query.list();
	}

	@Override
	public List<Invoice> getCustomersInvoiceList(String email, Integer custId,Integer countryId) {
		// TODO Auto-generated method stub
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
		if (countryId != null && countryId > 0) {
			criteria = criteria.add(Restrictions.eq("countryId", countryId));
		}
		boolean disjunctionPresent = false;
		Disjunction disjunction = Restrictions.disjunction();
		if (custId != null && custId > 0) {
			disjunction.add(Restrictions.eq("customerId", custId));
			disjunctionPresent = true;
		}
		if (!StringUtility.isNullOrEmpty(email)) {
			disjunction.add(Restrictions.eq("email", email));
			disjunctionPresent = true;
		}
		if (disjunctionPresent) {
			criteria = criteria.add(disjunction);
		}
		return criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Invoice> getCustomersInvoiceListByYear(String phone,Integer custId, Integer countryId,Date startDate,Date endDate) {
		/*String sqlQuery = "from Check WHERE restaurantId = :restaurantId AND closeTime>:startDate AND closeTime <:endDate AND phone:phone AND Or )";
		Query query = sessionFactory.getCurrentSession().createQuery(sqlQuery).setParameter("restaurantId", restaurantId).setParameter("startDate", startDate).setParameter("endDate",endDate);
		return query.list();*/
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
		if (countryId != null && countryId > 0) {
			criteria = criteria.add(Restrictions.eq("countryId", countryId));
		}
		if (startDate!= null ) {
			criteria = criteria.add(Restrictions.gt("closeTime", startDate));
		}
		if (endDate!= null ) {
			criteria = criteria.add(Restrictions.le("closeTime", endDate));
		}
		boolean disjunctionPresent = false;
		Disjunction disjunction = Restrictions.disjunction();
		if (custId != null && custId > 0) {
			disjunction.add(Restrictions.eq("customerId", custId));
			disjunctionPresent = true;
		}
		if (!StringUtility.isNullOrEmpty(phone)) {
			disjunction.add(Restrictions.eq("phone", phone));
			disjunctionPresent = true;
		}
		if (disjunctionPresent) {
			criteria = criteria.add(disjunction);
		}
		return criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Invoice> getAllInvoiceIdByCountryId(Integer countryId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(Invoice.class).add(Restrictions.eq("countryId", countryId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Invoice> getDailyInvoiceByFfc(Integer ffcId, Date startTime,Date endTime) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(Invoice.class).add(Restrictions.and(Restrictions.eq("kitchenScreenId", ffcId), Restrictions.gt("deliveryTime", startTime), Restrictions.lt("deliveryTime", endTime), Restrictions.ne("status", Status.Cancel))).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

	}

	@Override
	public void addEditInvoice(EditInvoices editInvoice) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(editInvoice);
	}

	@Override
	public void removeEditInvoice(Integer id) {
		// TODO Auto-generated method stub
		EditInvoices editInvoice= (EditInvoices) sessionFactory.getCurrentSession().load(Invoice.class, id);
		if (null != editInvoice) {
			sessionFactory.getCurrentSession().delete(editInvoice);
		}	
	}

	@Override
	public EditInvoices getEditInvoice(Integer id) {
		// TODO Auto-generated method stub
		return (EditInvoices) sessionFactory.getCurrentSession().get(Invoice.class, id);
	}

	@Override
	public List<EditInvoices> getEditInvoiceListByInvoiceNo(Integer invoiceNo) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(EditInvoices.class).add(Restrictions.eq("invoiceId",invoiceNo)).list();

	}

	@Override
	public Invoice getInvoiceByRazorPayId(String orderId) {
		// TODO Auto-generated method stub
		return (Invoice) sessionFactory.getCurrentSession().createCriteria(Invoice.class).add(Restrictions.eq("razorPayId", orderId)).uniqueResult();
	}

}
