package com.emanage.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.TutorDAO;
import com.emanage.domain.TutorRequest;

@Repository
public class TutorDAOImpl implements TutorDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveTutorRequest(TutorRequest request) {
		// TODO Auto-generated method stub
		 sessionFactory.getCurrentSession().saveOrUpdate(request);;
	}

}
