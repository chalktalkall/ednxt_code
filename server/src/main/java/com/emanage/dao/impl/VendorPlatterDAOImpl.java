package com.emanage.dao.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.constants.PlatterType;
import com.emanage.dao.VendorPlatterDAO;
import com.emanage.domain.BookedPlatter;
import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.Platter;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.Sections;
import com.emanage.domain.Vendor;
import com.emanage.dto.VendorsIdListDTO;
import com.emanage.enums.ClassMode;
import com.emanage.enums.Status;
import com.emanage.service.VendorService;
import com.emanage.utility.DateUtil;

@Repository
public class VendorPlatterDAOImpl implements VendorPlatterDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	VendorService vendorService;

	@Override
	public void addPlatter(Platter menu) {
		// TODO Auto-generated method stub
		menu.setDescription(menu.getDescription());
		sessionFactory.getCurrentSession().saveOrUpdate(menu);

	}

	@Override
	public List<Platter> listPlattersByVendorId(Integer vendorId) {
		// TODO Auto-generated method stub
		removeOrphanData();
		return sessionFactory.getCurrentSession().createCriteria(Platter.class)
				.add(Restrictions.eq("vendorId", vendorId))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Platter> allPlattersByStatus(Integer vendorId, Status status) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(Platter.class)
				.add(Restrictions.and(Restrictions.eq("status", status),
						Restrictions.eq("platterType", PlatterType.COURSE), Restrictions.eq("vendorId", vendorId)))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

	}

	@Override
	public Platter getPlatter(Integer id) {
		// TODO Auto-generated method stub
		return (Platter) sessionFactory.getCurrentSession().get(Platter.class, id);
	}

	@Override
	public void removePlatter(Integer id) {
		// TODO Auto-generated method stub
		Platter menu = (Platter) sessionFactory.getCurrentSession().load(Platter.class, id);
		if (null != menu) {
			sessionFactory.getCurrentSession().delete(menu);
		}
	}

	@Override
	public List<Platter> listPlatters() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Platter").list();
	}

	@Override
	public List<Platter> allPosPlatters(Integer countryId, Status status, boolean posStatus) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(Platter.class).add(Restrictions.and(
				Restrictions.eq("countryId", countryId),
				Restrictions.or(Restrictions.eq("status", status), Restrictions.eq("platterType", PlatterType.COURSE),
						Restrictions.eq("posVisible", posStatus))))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Platter> listPlattersByCountry(Integer countryId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(Platter.class)
				.add(Restrictions.eq("countryId", countryId))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public Platter getPlatterByPlatterName(String name, Integer vendorId) {
		// TODO Auto-generated method stub
		Query queryS = sessionFactory.getCurrentSession()
				.createQuery("from  Platter where  name=:name AND vendorId=:vendorId");
		queryS.setParameter("name", name);
		queryS.setParameter("vendorId", vendorId);

		return (Platter) queryS.uniqueResult();
	}

	@Override
	public void addSection(Sections section) {
		sessionFactory.getCurrentSession().saveOrUpdate(section);

	}

	@Override
	public void removeSections(List<Integer> sectionIds) {
		Integer[] sectionIdsArr = new Integer[sectionIds.size()];
		List<Sections> sections = (List<Sections>) sessionFactory.getCurrentSession().createCriteria(Sections.class)
				.add(Restrictions.in("sectionId", sectionIds.toArray(sectionIdsArr))).list();
		for (Sections section : sections) {
			sessionFactory.getCurrentSession().delete(section);
		}
	}

	public void removeOrphanData() {

		// Query query =sessionFactory.getCurrentSession().createSQLQuery("SET
		// SQL_SAFE_UPDATES = 0;");
		Query queryS = sessionFactory.getCurrentSession().createSQLQuery(
				"DELETE FROM SECTION_DISHES WHERE sectionDishId not in (SELECT sectionDishId from SECTION_DISH)");

		// query.executeUpdate();
		queryS.executeUpdate();
	}

	public List<Platter> getPlatterByLatlong(double latitud, double longitude, Date date, Integer customerPlateCount) {
		List<VendorsIdListDTO> vendorList = getListOfNearestVendors(latitud, longitude, "");
		List<Platter> allPlatter = new ArrayList<>();
		for (VendorsIdListDTO vendor : vendorList) {
			Integer availPlates = vendor.maxPlateCount - getBookedPlatterCountByDateandId(vendor.vendorId, date);

			List<Platter> platterList = null;
			DateUtil dateFormater = new DateUtil();
			String deliveryTimeD = dateFormater.getFormatedDateTimeyyyyMMddhhmmss(date);
			if (availPlates >= customerPlateCount) {
				platterList = sessionFactory.getCurrentSession().createCriteria(Platter.class)
						.add(Restrictions.and(Restrictions.and(Restrictions.eq("vendorId", vendor.vendorId),
								Restrictions.eq("platterType", PlatterType.COURSE),
								Restrictions.and(Restrictions.le("minAvail", customerPlateCount)),
								Restrictions.and(Restrictions.ge("maxAvail", customerPlateCount)),
								Restrictions.and(Restrictions.eq("status", Status.ACTIVE)),
								Restrictions.sqlRestriction(
										"(DATE_ADD( CURRENT_TIMESTAMP( ), INTERVAL timeForPrepartion HOUR ) <= '"
												+ deliveryTimeD + "')"))))
						.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

				for (Platter platter : platterList) {
					platter.setBusinessPotrateImageURL(vendor.portrateImageUrl);
					allPlatter.add(platter);
				}

			}
		}
		return allPlatter;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Platter> getCoursesByLatlong(double latitud, double longitude, String[] courseTypeArray,
			String classMode) {
		List<VendorsIdListDTO> vendorList = getListOfNearestVendors(latitud, longitude, classMode);
		List<Platter> allPlatter = new ArrayList<>();
		HashMap<Integer, Platter> allPlatterMap = new HashMap<>();

		for (VendorsIdListDTO vendor : vendorList) {
			Vendor vend = vendorService.getVendor(vendor.vendorId);
			List<Platter> platterList = new ArrayList<Platter>();
			for (String courseType : courseTypeArray) {
				List<Platter> platter = null;
				if ("Offline Classes".equalsIgnoreCase(classMode.trim())) {
					platter = sessionFactory.getCurrentSession().createCriteria(Platter.class)
							.add(Restrictions.and(
									Restrictions.and(Restrictions.eq("vendorId", vendor.vendorId),
											Restrictions.and(Restrictions.ilike("tagList",
													String.valueOf(courseType.trim()), MatchMode.ANYWHERE)),
											Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
													Restrictions.eq("platterType", PlatterType.COURSE))),
									Restrictions.and(Restrictions.or(Restrictions.eq("classMode", ClassMode.Offline),
											Restrictions.eq("classMode", ClassMode.Both)))))
							.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
				} else if ("Online Classes".equalsIgnoreCase(classMode.trim())) {
					platter = sessionFactory.getCurrentSession().createCriteria(Platter.class)
							.add(Restrictions.and(Restrictions.and(Restrictions.eq("vendorId", vendor.vendorId),
									Restrictions.and(Restrictions.ilike("tagList", String.valueOf(courseType.trim()),
											MatchMode.ANYWHERE)),
									Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
											Restrictions.eq("platterType", PlatterType.COURSE)),
									Restrictions.and(Restrictions.or(Restrictions.eq("classMode", ClassMode.Both),
											Restrictions.eq("classMode", ClassMode.Online))))))
							.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
				} else {
					platter = sessionFactory.getCurrentSession().createCriteria(Platter.class)
							.add(Restrictions.and(Restrictions.and(Restrictions.eq("vendorId", vendor.vendorId),
									Restrictions
											.and(Restrictions.ilike("tagList", courseType.trim(), MatchMode.ANYWHERE)),
									Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
											Restrictions.eq("platterType", PlatterType.COURSE)))))
							.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
				}

				if (platter != null && platter.size() > 0) {
					platterList.addAll(platter);
				}

			}
			for (Platter platter : platterList) {
				try {

					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					Date dateobj = new Date();
					String currenntDate = df.format(dateobj);

					if (platter.getValidity() != null && "1000-01-01 00:00:00".contains(platter.getValidity())) {
						platter.setValidity(null);
						platter.setDistance(vendor.distanceBetween);
						platter.setBusinessPotrateImageURL(vend.getBusinessPortraitImageUrl());
						platter.setBusinessDescription(vend.getDescription());
						List<String> imageURL = new ArrayList<String>();
						imageURL.add(vend.getInstituteImageURLF());
						imageURL.add(vend.getInstituteImageURLS());
						imageURL.add(vend.getInstituteImageURLT());
						imageURL.add(vend.getInstituteImageURLFO());
						imageURL.add(vend.getInstituteImageURLFI());
						platter.setInstituteImages(imageURL);
						platter.setCertificate(vend.isCertificate());
						platter.setCertifiedTrainer(vend.isCertifiedTrainer());
						platter.setPlacementAssistance(vend.isPlacementAssistance());
						platter.setMetroStation(vend.isMetroStation());
						platter.setAssessmentKit(vend.isAssessmentKit());
						platter.setEbook(vend.isEbook());

						platter.setInstituteName(vend.getBusinessName());
						platter.setBusinessIntro(vend.getIntroduction());
						if (platter.getVendorMargin() == null || platter.getVendorMargin() == 0)
							platter.setVendorMargin(vend.getVendorMargin());
						// allPlatter.add(platter);
						platter.setRating(vend.getRating());
						allPlatterMap.put(platter.getPlatterId(), platter);
					} else if (platter.getValidity() != null
							&& DateUtil.compareDate(platter.getValidity(), currenntDate)) {
						platter.setDistance(vendor.distanceBetween);
						platter.setBusinessPotrateImageURL(vend.getBusinessPortraitImageUrl());
						platter.setBusinessDescription(vend.getDescription());
						List<String> imageURL = new ArrayList<String>();

						if (vend.getInstituteImageURLF() != null && vend.getInstituteImageURLF().trim().length() > 0)
							imageURL.add(vend.getInstituteImageURLF());

						if (vend.getInstituteImageURLS() != null && vend.getInstituteImageURLS().trim().length() > 0)
							imageURL.add(vend.getInstituteImageURLS());

						if (vend.getInstituteImageURLT() != null && vend.getInstituteImageURLT().trim().length() > 0)
							imageURL.add(vend.getInstituteImageURLT());

						if (vend.getInstituteImageURLFO() != null && vend.getInstituteImageURLFO().trim().length() > 0)
							imageURL.add(vend.getInstituteImageURLFO());

						if (vend.getInstituteImageURLFI() != null && vend.getInstituteImageURLFI().trim().length() > 0)
							imageURL.add(vend.getInstituteImageURLFI());

						platter.setInstituteImages(imageURL);
						platter.setCertificate(vend.isCertificate());
						platter.setCertifiedTrainer(vend.isCertifiedTrainer());
						platter.setPlacementAssistance(vend.isPlacementAssistance());
						platter.setMetroStation(vend.isMetroStation());
						platter.setAssessmentKit(vend.isAssessmentKit());
						platter.setEbook(vend.isEbook());

						platter.setInstituteName(vend.getBusinessName());
						platter.setBusinessIntro(vend.getIntroduction());

						if (platter.getVendorMargin() == null || platter.getVendorMargin() == 0)
							platter.setVendorMargin(vend.getVendorMargin());
						// allPlatter.add(platter);
						platter.setRating(vend.getRating());
						allPlatterMap.put(platter.getPlatterId(), platter);
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
		for (Integer key : allPlatterMap.keySet()) {
			allPlatter.add(allPlatterMap.get(key));
		}
		return allPlatter;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<VendorsIdListDTO> getListOfNearestVendors(double latitud, double longitude, String classMode) {

		List<VendorsIdListDTO> vendorlist = new ArrayList<VendorsIdListDTO>();
		String hqlQuery2 = "";
		if ("Online Classes".equalsIgnoreCase(classMode)) {
			hqlQuery2 = "SELECT vendorId ,vendorName FROM VENDOR where status=0";

		} else if ("Offline Classes".equalsIgnoreCase(classMode)) {
			hqlQuery2 = "SELECT * FROM ( SELECT vendorId,servingDistance,3956 * 2 * ASIN(SQRT(POWER(SIN((:latitude - abs(latitude)) * pi()/180 / 2), 2) + COS(:latitude * pi()/180 ) * COS(abs(latitude) * pi()/180)  * POWER(SIN((:longitude -longitude) * pi()/180 / 2), 2) )) as  distance FROM VENDOR where status=0 ORDER BY distance) R WHERE R.distance <= R.servingDistance";
		} else {
			hqlQuery2 = "SELECT vendorId,servingDistance,3956 * 2 * ASIN(SQRT(POWER(SIN((:latitude - abs(latitude)) * pi()/180 / 2), 2) + COS(:latitude * pi()/180 ) * COS(abs(latitude) * pi()/180)  * POWER(SIN((:longitude -longitude) * pi()/180 / 2), 2) )) as  distance FROM VENDOR where status=0";
		}
		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hqlQuery2);
			if ("Online Classes".equalsIgnoreCase(classMode)) {

			} else {
				query.setDouble("latitude", latitud);
				query.setDouble("longitude", longitude);
			}

			List<Object> result = (List<Object>) query.list();
			Iterator itr = result.iterator();
			while (itr.hasNext()) {
				Object[] obj = (Object[]) itr.next();
				VendorsIdListDTO vendor = new VendorsIdListDTO();
				if ("Online Classes".equalsIgnoreCase(classMode)) {
					vendor.vendorId = (Integer) obj[0];
				} else {
					vendor.vendorId = (Integer) obj[0]; // Integer.parseInt((String) obj[0]);
					vendor.distanceBetween = (Double) obj[2];
				}

				vendorlist.add(vendor);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vendorlist;
	}

	@Override
	public SectionDishes getSectionDish(Integer dishId) {

		Query queryS = sessionFactory.getCurrentSession()
				.createQuery("from  SectionDishes where  sectionDishId=:dishId");
		queryS.setParameter("dishId", dishId);
		return (SectionDishes) queryS.uniqueResult();
	}

	@Override
	public void saveSectionDish(SectionDishes sectionDish) {

		Query query = sessionFactory.getCurrentSession().createSQLQuery("SET SQL_SAFE_UPDATES = 0;");
		query.executeUpdate();

		Query queryS = sessionFactory.getCurrentSession().createSQLQuery(
				"UPDATE SECTION_DISHES set alcoholic=:alcoholic, contents=:contents, description=:description,"
						+ " imageUrl=:imageUrl, disabled=:disabled, dishTypeId=:dishTypeId, name=:name, vegetarian=:vegetarian,"
						+ " shortDescription=:shortDescription, quantityInG=:quantityInG, rectangularImageUrl=:rectangularImageUrl, "
						+ " price=:price ,couseDetailsURL=:couseDetailsURL, online=:online,courseDuration=:courseDuration,courseDurationInHr=:courseDurationInHr  where  dishId=:dishId");

		queryS.setParameter("alcoholic", sectionDish.getAlcoholic());
		queryS.setParameter("contents", sectionDish.getContents());
		queryS.setParameter("description", sectionDish.getDescription());
		queryS.setParameter("imageUrl", sectionDish.getImageUrl());
		queryS.setParameter("disabled", sectionDish.getDisabled());
		queryS.setParameter("dishTypeId", sectionDish.getDishTypeId());
		queryS.setParameter("name", sectionDish.getName());
		queryS.setParameter("vegetarian", sectionDish.getVegetarian());
		queryS.setParameter("shortDescription", sectionDish.getShortDescription());
		queryS.setParameter("quantityInG", sectionDish.getQuantityInG());
		queryS.setParameter("rectangularImageUrl", sectionDish.getRectangularImageUrl());
		queryS.setParameter("price", sectionDish.getPrice());
		queryS.setParameter("dishId", sectionDish.getDishId());
		queryS.setParameter("couseDetailsURL", sectionDish.getCouseDetailsURL());
		queryS.setParameter("online", sectionDish.isOnline());
		queryS.setParameter("courseDuration", sectionDish.getCourseDuration());
		queryS.setParameter("courseDurationInHr", sectionDish.getCourseDurationInHr());

		queryS.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SectionDishes> getSectionDishByDishId(Integer id) {
		// TODO Auto-generated method stub
		Query queryS = sessionFactory.getCurrentSession().createQuery("from  SectionDishes where  dishId=:dishId");
		queryS.setParameter("dishId", id);
		return queryS.list();
	}

	@Override
	public void addPlatterBooking(BookedPlatter bookedPlatter) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(bookedPlatter);

	}

	@Override
	public List<BookedPlatter> listBookedPlattersByVendorIdAndDate(Integer vendorId, Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBookedPlatterCountByDateandId(Integer vendorId, Date date) {

		DateUtil dateFormater = new DateUtil();
		String formatedDate = dateFormater.getFormatedDateTimeyyyyMMdd(date);
		Integer count = null;
		String hqlQuery2 = "Select Sum(count) from BOOKED_PLATTER where Date(date)='" + formatedDate
				+ "'&& vendorId=:vendorId";

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hqlQuery2);

			query.setInteger("vendorId", vendorId);

			if (query.uniqueResult() != null) {
				count = ((BigDecimal) query.uniqueResult()).intValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (count == null) {
			count = 0;
		}
		return count;
	}

	@Override
	public Integer getVendorIdUsingPlatterId(Integer id) {
		// TODO Auto-generated method stub
		Integer result = null;
		String hqlQuery2 = "Select vendorId from PLATTERS where platterId=:id";

		try {
			Query query = sessionFactory.getCurrentSession().createSQLQuery(hqlQuery2);
			query.setDouble("id", id);

			result = (Integer) query.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Platter> listFetureCourses() {

		List<Platter> platterList = sessionFactory.getCurrentSession().createCriteria(Platter.class)
				.add(Restrictions.and(Restrictions.and(Restrictions.eq("featureCourse", true),
						Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
								Restrictions.eq("platterType", PlatterType.COURSE)))))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		return platterList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Platter> listFetureTrainings() {

		List<Platter> platterList = sessionFactory.getCurrentSession().createCriteria(Platter.class)
				.add(Restrictions.and(Restrictions.and(Restrictions.eq("featureCourse", true),
						Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
								Restrictions.eq("platterType", PlatterType.ON_DEMAND)))))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		return platterList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Platter> getActiveWebinars() {
		return sessionFactory.getCurrentSession().createCriteria(Platter.class).add(Restrictions
				.and(Restrictions.eq("status", Status.ACTIVE), Restrictions.eq("platterType", PlatterType.WEBINAR)))
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Platter> getCoursesByCriteria(String[] courseTypeArray, String classMode, Integer studentCount) {
		List<Platter> platterList = new ArrayList<Platter>();
		for (String courseType : courseTypeArray) {
			List<Platter> platter = null;
			if ("Offline Classes".equalsIgnoreCase(classMode.trim())) {
				platter = sessionFactory.getCurrentSession().createCriteria(Platter.class)
						.add(Restrictions.and(
								Restrictions.and(
										Restrictions.and(Restrictions.ilike("tagList",
												String.valueOf(courseType.trim()), MatchMode.ANYWHERE)),
										Restrictions.and(Restrictions.le("minAvail", studentCount)),
										Restrictions.and(Restrictions.ge("maxAvail", studentCount)),
										Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
												Restrictions.eq("platterType", PlatterType.ON_DEMAND))),
								Restrictions.and(Restrictions.or(Restrictions.eq("classMode", ClassMode.Offline),
										Restrictions.eq("classMode", ClassMode.Both)))))
						.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
			} else if ("Online Classes".equalsIgnoreCase(classMode.trim())) {
				platter = sessionFactory.getCurrentSession().createCriteria(Platter.class)
						.add(Restrictions.and(Restrictions.and(
								Restrictions.and(Restrictions.ilike("tagList", String.valueOf(courseType.trim()),
										MatchMode.ANYWHERE)),
								Restrictions.and(Restrictions.le("minAvail", studentCount)),
								Restrictions.and(Restrictions.ge("maxAvail", studentCount)),
								Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
										Restrictions.eq("platterType", PlatterType.ON_DEMAND)),
								Restrictions.and(Restrictions.or(Restrictions.eq("classMode", ClassMode.Both),
										Restrictions.eq("classMode", ClassMode.Online))))))
						.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
			} else {
				platter = sessionFactory.getCurrentSession().createCriteria(Platter.class)
						.add(Restrictions.and(Restrictions.and(
								Restrictions.and(Restrictions.ilike("tagList", courseType.trim(), MatchMode.ANYWHERE)),
								Restrictions.and(Restrictions.le("minAvail", studentCount)),
								Restrictions.and(Restrictions.ge("maxAvail", studentCount)),
								Restrictions.and(Restrictions.eq("status", Status.ACTIVE),
										Restrictions.eq("platterType", PlatterType.ON_DEMAND)))))
						.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
			}

			if (platter != null && platter.size() > 0) {
				platterList.addAll(platter);
			}

		}

		return platterList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Canvas_Courses> getCoursesByCriteriaNew(String[] courseTypeArray) {

		List<Canvas_Courses> canvasList = new ArrayList<Canvas_Courses>();

		for (String courseType : courseTypeArray) {
			List<Canvas_Courses> courses = null;
			courses = sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class)
					.add(Restrictions.and(Restrictions.and(
							Restrictions.and(Restrictions.ilike("tagList", courseType.trim(), MatchMode.ANYWHERE)),
							Restrictions.and(Restrictions.eq("status", Status.ACTIVE)))))
					.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

			if (courses != null && courses.size() > 0) {
				canvasList.addAll(courses);
			}
		}
		return canvasList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Canvas_Courses> getCoursesByCriteriaCampusId(String[] courseTypeArray, Integer campusId) {

		List<Canvas_Courses> canvasList = new ArrayList<Canvas_Courses>();

		for (String courseType : courseTypeArray) {
			List<Canvas_Courses> courses = null;
			courses = sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class)
					.add(Restrictions.and(Restrictions.and(
							Restrictions.and(Restrictions.ilike("tagList", courseType.trim(), MatchMode.ANYWHERE)),
							Restrictions.and(Restrictions.eq("vendorId", campusId)),
							Restrictions.and(Restrictions.eq("status", Status.ACTIVE)))))
					.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

			if (courses != null && courses.size() > 0) {
				canvasList.addAll(courses);
			}
		}
		return canvasList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Canvas_Courses> listFeturedCanvasCourses() {

		List<Canvas_Courses> platterList = sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class)
				.add(Restrictions.and(Restrictions.and(Restrictions.eq("isFeatured", true)),
						Restrictions.and(Restrictions.eq("status", Status.ACTIVE))))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		return platterList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Canvas_Courses> listFeturedCanvasCourses(Integer campusId) {

		List<Canvas_Courses> platterList = sessionFactory.getCurrentSession().createCriteria(Canvas_Courses.class)
				.add(Restrictions.and(Restrictions.and(Restrictions.eq("isFeatured", true)),
						Restrictions.and(Restrictions.and(Restrictions.eq("vendorId", campusId)),
								Restrictions.and(Restrictions.eq("status", Status.ACTIVE)))))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		return platterList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Canvas_Courses> getPopularCourses() {

		List<Canvas_Courses> courses = new ArrayList<Canvas_Courses>();
		// TODO Auto-generated method stub
//		Query query =  sessionFactory.getCurrentSession().createQuery("from Canvas_Courses c where c.price = (select MAX(cc.price) from Canvas_Courses as cc JOIN Vendor as v with cc.vendorId=v.vendorId where cc.is_public=0 )");
//		query.setMaxResults(3);
//		return query.list();
		// List<Canvas_Courses> courses = null;
		//String hqlQuery2 = "SELECT  * from CANVAS_COURSES WHERE price = (select MAX(cc.price) from CANVAS_COURSES as cc JOIN VENDOR as v on cc.vendorId=v.vendorId where cc.is_public=0 and v.status=0) limit 3";
		String hqlQuery2 = "select cc.* from CANVAS_COURSES as cc RIGHT JOIN VENDOR as v on cc.vendorId=v.vendorId and  v.status=0 and cc.is_public=0 ORDER BY cc.price DESC limit 3";
//		try {
//			Query query = sessionFactory.getCurrentSession().createSQLQuery(hqlQuery2);
//			courses = (List<Canvas_Courses>)query.list();
//			System.out.println(courses.size());
//			System.out.println(courses.get(0).getId());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		

		Query query = sessionFactory.getCurrentSession().createSQLQuery(hqlQuery2);
		List<Object> result = (List<Object>) query.list();
		Iterator itr = result.iterator();
		while (itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			Canvas_Courses course = new Canvas_Courses();

			course.setCourseId((Integer) obj[0]); // Integer.parseInt((String) obj[0]);
			course.setId((Integer) obj[1]);
			course.setVendorId((Integer)obj[2]);
			course.setRoot_account_id((Integer)obj[3]);
			course.setName((String)obj[5]);
			course.setStart_at((Date)obj[6]);
			course.setEnd_at((Date)obj[7]);
			byte status = (byte) obj[8];
			if(status==0) {
				course.setIs_public(true);
			}else {
				course.setIs_public(false);
			}
			
			course.setCourse_code((String)obj[9]);
			course.setDefault_view((String)obj[10]);
			course.setTerm((String)obj[12]);
			course.setSyllabus_body((String)obj[13]);
			course.setSections((String)obj[14]);
			course.setImage_download_url((String)obj[15]);
			course.setImage_download_url((String)obj[16]);
			course.setPublic_description((String)obj[17]);
			course.setCourse_format((String)obj[18]);
			course.setPrice((Float)obj[19]);
			//course.setTagList((String)obj[19]);
			
			courses.add(course);
		}
		return courses;
	}

}
