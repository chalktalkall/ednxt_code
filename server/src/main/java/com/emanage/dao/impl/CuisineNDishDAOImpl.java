package com.emanage.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.CuisineNDishDAO;
import com.emanage.domain.CountryDishType;
import com.emanage.domain.CountrySectionType;
import com.emanage.domain.CuisineType;


@Repository
public class CuisineNDishDAOImpl implements CuisineNDishDAO{


	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public void addCuisineType(CuisineType dishType) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(dishType);
	}

	@Override
	public List<CuisineType> listCuisineTypes() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(CuisineType.class).list();
	}
	
	@Override
	public List<CuisineType> listCuisineTypesByUniversity(Integer universityId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(CuisineType.class).add(Restrictions.eq("countryId", universityId)).list();
	}

	@Override
	public List<CuisineType> listCuisineTypesByCountryId(Integer countryId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(CuisineType.class).add(Restrictions.eq("countryId", countryId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

	}

	@Override
	public void removeCuisineType(Integer couisineId) {
		// TODO Auto-generated method stub
		CuisineType cuisineType = (CuisineType) sessionFactory.getCurrentSession().load(CuisineType.class, couisineId);
		if (null != cuisineType) {
			sessionFactory.getCurrentSession().delete(cuisineType);
		}
	}

	@Override
	public CuisineType getCuisineType(Integer couisineId) {
		// TODO Auto-generated method stub
		return (CuisineType)sessionFactory.getCurrentSession().get(CuisineType.class, couisineId);
	}

	
	//Now making DishType 
	
	@Override
	public void addDishType(CountryDishType dishType) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(dishType);
	}

	@Override
	public List<CountryDishType> listDishTypes() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(CountryDishType.class).list();
	}

	@Override
	public List<CountryDishType> listDishTypesByCountryId(Integer countryId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(CountryDishType.class).add(Restrictions.eq("countryId", countryId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public void removeDishType(Integer dishId) {
		// TODO Auto-generated method stub
		CuisineType dishType = (CuisineType) sessionFactory.getCurrentSession().load(CuisineType.class, dishId);
		if (null != dishType) {
			sessionFactory.getCurrentSession().delete(dishType);
		}
	}

	@Override
	public CountryDishType getDishType(Integer dishId) {
		// TODO Auto-generated method stub
		return (CountryDishType)sessionFactory.getCurrentSession().get(CountryDishType.class, dishId);
	}

	@Override
	public void addSectionType(CountrySectionType sectionType) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(sectionType);
	}

	@Override
	public List<CountrySectionType> listSectionTypes() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(CountrySectionType.class).list();
	}

	@Override
	public List<CountrySectionType> listSectionTypesByCountryId(Integer countryId) {
		
		return sessionFactory.getCurrentSession().createCriteria(CountrySectionType.class).add(Restrictions.eq("countryId", countryId)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}


	@Override
	public void removeSectionType(Integer sectionId) {
		// TODO Auto-generated method stub
		CountrySectionType sectionType = (CountrySectionType) sessionFactory.getCurrentSession().load(CountrySectionType.class, sectionId);
		if (null != sectionType) {
			sessionFactory.getCurrentSession().delete(sectionType);
		}
	}

	@Override
	public CountrySectionType getSectionType(Integer sectionTypeId) {
		// TODO Auto-generated method stub
		return (CountrySectionType)sessionFactory.getCurrentSession().get(CountrySectionType.class, sectionTypeId);
	}

	@Override
	public CuisineType getCuisineTypeByName(String name) {
		// TODO Auto-generated method stub
		return (CuisineType) sessionFactory.getCurrentSession().createCriteria(CuisineType.class).add(Restrictions.eq("name", name)).uniqueResult();
	}



}
