package com.emanage.dao.impl;

import java.util.List;
import java.util.Optional;

import org.eclipse.birt.report.engine.emitter.pptx.writer.Presentation;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emanage.dao.TrainerDAO;
import com.emanage.domain.Platter;
import com.emanage.domain.Trainer;
import com.emanage.dto.ResponseDTO;

@Repository
public class TrainerDAOImpl implements TrainerDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public ResponseDTO saveTrainer(Trainer trainer) {
		// TODO Auto-generated method stub
		ResponseDTO response = new ResponseDTO();
		try {
			sessionFactory.getCurrentSession().save(trainer);
			response.message="Trainer added";
			response.result="SUCCESS";
		}catch(Exception e) {
			response.message=e.getMessage();
			response.result="Fail";
		}
		
		return response;
	}

	@Override
	public List<Trainer> getAllActiveTrainer() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(Platter.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Trainer> getAllTrainer() {
		// TODO Auto-generated method stub
		return  sessionFactory.getCurrentSession().createCriteria(Platter.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public Trainer getTrainer(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseDTO deleteTrainer(int id) {
		
		ResponseDTO response = new ResponseDTO();
		// TODO Auto-generated method stub
		Trainer trainer =  getTrainerById(id);
		if(trainer!=null) {
			sessionFactory.getCurrentSession().delete(trainer);
			response.message="Deleted succefually";
			response.result="success";
		}else {
			response.message="Trainer doesn't exist";
			response.result="fail";
		}
		return response;
	}

	@Override
	public Trainer getTrainerById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseDTO editTrainer(Trainer presentation) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
