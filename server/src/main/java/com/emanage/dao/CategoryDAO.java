/**
 * 
 */
package com.emanage.dao;

import java.util.List;

import com.emanage.domain.Category;

/**
 * @author rahul
 *
 */
public interface CategoryDAO {
	public void addCategory(Category category);
	public List<Category> listCategory();
	public List<Category> listCategoryByRestaurant(Integer restaurantId);
	public void removeCategory(Integer id);
	public Category getCategory(Integer id);
}
