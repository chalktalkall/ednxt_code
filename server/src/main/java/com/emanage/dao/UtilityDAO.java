package com.emanage.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.emanage.domain.Customer;
import com.emanage.domain.Invoice;

/**
 * @author rahul
 *
 */
public interface UtilityDAO {

	public void updateData(List<Object> data);
	public void removeCustomer(List<Integer> customerList);	
	public List<Invoice> getAllChecks(int lowerCount, int upperCount);
    public int getLastCustomerId();
	public List<Customer> getAllCustomer(int lowerCount, int upperCount, boolean all);
	public List<String> listDuplicateCustomer(Integer orgId);
	public List<Customer> listAllDuplicateCustomer(String mobileNo);
	public void updateCustomerDetails(HashMap<Integer, Set<Integer>> duplicateCustomer);
	public Invoice getLastChecks();
}
