package com.emanage.dao;

import java.util.List;

import com.emanage.domain.Canvas_Courses;

public interface CanvasLMSDAO {

	void addCanvasCourse(Canvas_Courses course);
	List<Canvas_Courses> getCourseList(Integer rootAccountId,Integer vendorId);
	List<Canvas_Courses> getCourseList(Integer vendorId);
	Canvas_Courses getCourse(Integer courseId,Integer vendorId);
	Canvas_Courses getCourseById(Integer course_id);
	Canvas_Courses getCourseByLMSCourseId(Integer id, Integer vendorId);
	List<Canvas_Courses> getAllCourseList(Integer vendorId);
	void deleteLMSCourse(Canvas_Courses course);
	
}
