package com.emanage.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationLatLong {

	public Float latitude;
	public Float longnitude;
	public String date;
	public Integer plateCount;
	public Date getDate() {
		Date deliveryTimeD =null;
		String format ="yyyy-MM-dd HH:mm"; 
		SimpleDateFormat formatterD = new SimpleDateFormat(format);
		try {
		deliveryTimeD = formatterD.parse(this.date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return deliveryTimeD;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
