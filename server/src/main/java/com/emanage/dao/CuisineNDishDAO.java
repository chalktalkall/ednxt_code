/**
 * 
 */
package com.emanage.dao;

import java.util.List;

import com.emanage.domain.CountryDishType;
import com.emanage.domain.CountrySectionType;
import com.emanage.domain.CuisineType;


/**
 * @author Rahul
 *
 */
public interface CuisineNDishDAO {
	
	
	public void addCuisineType(CuisineType dishType);
	public List<CuisineType> listCuisineTypes();
	public List<CuisineType> listCuisineTypesByCountryId(Integer countryId);
	public void removeCuisineType(Integer couisineId);
	public CuisineType getCuisineType(Integer couisineId);
	public CuisineType getCuisineTypeByName(String name);
	
	
	public void addDishType(CountryDishType dishType);
	public List<CountryDishType> listDishTypes();
	public List<CountryDishType> listDishTypesByCountryId(Integer countryId);
	public void removeDishType(Integer tagId);
	public CountryDishType getDishType(Integer dishId);
	
	
	public void addSectionType(CountrySectionType sectionType);
	public List<CountrySectionType> listSectionTypes();
	public List<CountrySectionType> listSectionTypesByCountryId(Integer countryId);
	public void removeSectionType(Integer sectionId);
	public CountrySectionType getSectionType(Integer sectionTypeId);
	List<CuisineType> listCuisineTypesByUniversity(Integer universityId);
}
