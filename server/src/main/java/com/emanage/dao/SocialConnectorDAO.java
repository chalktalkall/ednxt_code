package com.emanage.dao;

import java.util.List;

import com.emanage.domain.SocialConnector;


public interface SocialConnectorDAO {

	public void addSocialConnector(SocialConnector connector);
	public SocialConnector getSocialConnector(Integer id);
	public List<SocialConnector> listSocialConnectorByOrgId(Integer orgId);
	public void removeSocialConnector(Integer id) throws Exception;
}
