package com.emanage.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorsIdListDTO {

	public Integer vendorId;	
	public Integer maxPlateCount;
	public String portrateImageUrl; 
//	public Float servingDistance;	
//	public  Float distanceBetween;
}
