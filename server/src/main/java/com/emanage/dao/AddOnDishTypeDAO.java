/**
 * 
 */
package com.emanage.dao;

import java.util.List;

import com.emanage.domain.AddOnDishType;

/**
 * @author rahul
 *
 */
public interface AddOnDishTypeDAO {
	public void addDishType(AddOnDishType dishType);
	public List<AddOnDishType> listDishTypes();
	public List<AddOnDishType> listDishTypesByRestaurantId(Integer restaurantId);
	public void removeDishType(Integer id);
	public AddOnDishType getDishType(Integer id);
}
