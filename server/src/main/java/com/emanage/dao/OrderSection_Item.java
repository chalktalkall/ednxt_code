package com.emanage.dao;

public class OrderSection_Item {

	
	public Integer itemId;
	public Integer countryId;
	public Integer  vendorId;
	public String name;
	public float price;
	public String quantityInG;
	public String itemType;
	public Boolean isDishReplaceable;
	
	public Integer getItemId() {
		return itemId;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public String getName() {
		return name;
	}
	public float getPrice() {
		return price;
	}
	public String getQuantityInG() {
		return quantityInG;
	}
	public String getItemType() {
		return itemType;
	}
	public Boolean getIsDishReplaceable() {
		return isDishReplaceable;
	}

}
