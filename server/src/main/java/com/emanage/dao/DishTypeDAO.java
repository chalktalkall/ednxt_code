/**
 * 
 */
package com.emanage.dao;

import java.util.List;

import com.emanage.domain.AddOnDish_Size;
import com.emanage.domain.DishSize;
import com.emanage.domain.DishType;
import com.emanage.domain.Dish_Size;

/**
 * @author rahul
 *
 */
public interface DishTypeDAO {
	public void addDishType(DishType dishType);
	public List<DishType> listDishTypes();
	public List<DishType> listDishTypesByRestaurantId(Integer restaurantId);
	public void removeDishType(Integer id);
	public DishType getDishType(Integer id);
	
	public void addDishSize(DishSize dishType);
	public List<DishSize> listDishSizeByRestaurantId(Integer restaurantId);
	public void removeDishSize(Integer id);
	public DishSize getDishSize(Integer id);
	
	public Dish_Size getDish_Size(Integer id,Integer dishId);
	public AddOnDish_Size getAddOnDish_Size(Integer id,Integer addOnDishId);
	public List<Dish_Size> getDish_SizeListbyDishId(Integer dishId);
	public List<AddOnDish_Size> getAddOnDish_SizeListbyDishId(Integer addOnDishId);
}
