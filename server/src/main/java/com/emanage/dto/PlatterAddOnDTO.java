package com.emanage.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author rahul .
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlatterAddOnDTO {

	  public Integer quantity;
	  public Integer itemId;
	  public String instructions;
}
