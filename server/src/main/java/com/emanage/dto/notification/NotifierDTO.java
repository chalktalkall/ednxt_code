package com.emanage.dto.notification;

/**
 * @author rahul 
 *
 */
public class NotifierDTO {
  public String appKey;
  public String deviceType;
  public int restaurantId;
}
