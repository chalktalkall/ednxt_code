package com.emanage.dto.notification;

public class SenderDTO {

	public int restaurantId;
	public String iosKey;
	public String gcmAppKey;
	public String email;
	public String name;
	public String password;
}
