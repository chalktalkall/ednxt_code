package com.emanage.dto.notification;

import java.util.List;

/**
 * @author rahul 
 *
 */
public class ResultDTO {
 public String resultCode;
 public List<String> mobileNo;
 public String message;
}
