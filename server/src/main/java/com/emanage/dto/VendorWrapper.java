package com.emanage.dto;

import java.io.Serializable;

public class VendorWrapper implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer vendorId;
	
	private String vendorName;
	
	private String businessContact;
	
	private String businessLogoImageUrl;
	
	private String businessLandscapeImageUrl;
	
	private String businessMobileLandscapeImageUrl;

	private String address;

	private String description;

	private String introduction;
	
	private Float rating;
	
	private Integer countryId;

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getBusinessContact() {
		return businessContact;
	}

	public void setBusinessContact(String businessContact) {
		this.businessContact = businessContact;
	}

	public String getBusinessLandscapeImageUrl() {
		return businessLandscapeImageUrl;
	}

	public void setBusinessLandscapeImageUrl(String businessLandscapeImageUrl) {
		this.businessLandscapeImageUrl = businessLandscapeImageUrl;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getBusinessLogoImageUrl() {
		return businessLogoImageUrl;
	}

	public void setBusinessLogoImageUrl(String businessLogoImageUrl) {
		this.businessLogoImageUrl = businessLogoImageUrl;
	}

	public String getBusinessMobileLandscapeImageUrl() {
		return businessMobileLandscapeImageUrl;
	}

	public void setBusinessMobileLandscapeImageUrl(String businessMobileLandscapeImageUrl) {
		this.businessMobileLandscapeImageUrl = businessMobileLandscapeImageUrl;
	}

}
