package com.emanage.dto;

import java.util.List;

public class Order_SectionDTO {
	
	public Integer sectionId;
	public String name;
	public String shortName;
	public Float price;
	public String platterSectionType;
	public List<OrderSection_Item> items;
	
	public Integer getSectionId() {
		return sectionId;
	}
	public String getName() {
		return name;
	}
	public String getShortName() {
		return shortName;
	}
	public Float getPrice() {
		return price;
	}
	public String getPlatterSectionType() {
		return platterSectionType;
	}
	public List<OrderSection_Item> getItems() {
		return items;
	}
	

}
