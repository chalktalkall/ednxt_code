package com.emanage.dto;

import java.util.Date;
import java.util.List;

import com.emanage.domain.Coupon;
import com.emanage.dto.DiscountDTO;
import com.emanage.enums.check.Status;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author rahul .
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
public class OrderDTO {

    public String id;
    public String checkId;
    public String orderType;
    public Status paymentStatus;
    public Date deliveryTime;
    public Date orderTime;
    public String customerName;
    public List<OrderPlatterDTO> items;
    public Integer fulfillmentCenterId ;
    public String customerMobNo;
    public Integer customerId;
    public String customerEmail;
    public String status;
    public String invoiceId;
    public String paymentMethod;
    public String orderSource;
    public Long changeAmount;
    public Date lastModified;
    public String deliveryAddress;
    public String deliveryAgent;
    public Integer deliveryAgentId;
    public Float discountAmount;
    public Float discountPercentage;
    public String instructions;
    public List<DiscountDTO> discountList;
    public String deliveryDateTime;
    public String paidStatus;
    public String taxJsonObj;
    public boolean isfirstOrder;
    public String remark;
    public boolean isEdited;
    public float creditBalance;
    public List<Coupon> couponApplied;
    public List<String> couponCode;
    public double orderAmount;
    public boolean allowEdit;
    public Float tokenAmount;
    public String classMode;
    
    public Integer noOfPeople;
    
}


