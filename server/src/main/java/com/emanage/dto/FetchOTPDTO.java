package com.emanage.dto;

import javax.validation.constraints.Min;

import com.emanage.validator.Email;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true) 
public class FetchOTPDTO {
	
	public String phoneNumber;
    
    public String device="";
    
    public String appId="";
    
    @Email(message="Invalid email")
    public String email="";
    
    @Min(1)
    public int orgId;
    
    @Min(1)
    public int countryId;
}
