package com.emanage.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DemoRegDTO {

	public String fullName;
	
	public String phone;

	public String demoDate;
	
	public String email;
	
	public Integer vendorId;
	
	public Integer countryId;
	
	public Integer courseId;
	
	public Integer platterId;
	
	public Integer attendeesCount;
	
	public Date getDate() {
		Date deliveryTimeD =null;
		String format ="yyyy-MM-dd HH:mm"; 
		SimpleDateFormat formatterD = new SimpleDateFormat(format);
		try {
			if(this.demoDate!=null) {
				deliveryTimeD = formatterD.parse(this.demoDate);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return deliveryTimeD;
	}
	public void setDate(String date) {
		this.demoDate = date;
	}
	
}
