package com.emanage.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetCourseRequestDTO {

	public Integer courseId;
	public Integer eventId;

	
}
