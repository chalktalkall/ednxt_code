package com.emanage.dto;

public class DataSourceDTO {

	String jdbc_drive;
	String db_url;
	String dbName;
	String user;
	String password;
	Integer port;

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJdbc_drive() {
		return jdbc_drive;
	}

	public void setJdbc_drive(String jdbc_drive) {
		this.jdbc_drive = jdbc_drive;
	}

	public String getDb_url() {
		return db_url;
	}

	public void setDb_url(String db_url) {
		this.db_url = db_url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
