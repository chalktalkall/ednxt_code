package com.emanage.dto;

public class DishCouponCalcDTO {
	
	public String couponName;
	public double calcAmount;
	
	public String getCouponName() {
		return couponName;
	}
	
	public double getCalcAmount() {
		return calcAmount;
	}
	
	
}
