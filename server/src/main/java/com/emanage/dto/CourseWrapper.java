package com.emanage.dto;

import java.io.Serializable;

import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.Vendor;
import com.emanage.utility.DateUtil;

public class CourseWrapper  implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer courseId;

	private Integer id;
    
	private Integer vendorId;
    
    private String name;
    
    private String start_at;
    
    private String end_at;
    
    private String course_code;
    
    private String default_view;
    
    private String term; //json string
    
    private String time_zone;
    
    private String syllabus_body; // html string
    
    private String sections; //json string
    
    private String image_download_url;
    
    private String public_description;
    
    private String course_format;
   
    private Float price;
    
    private VendorWrapper vendor;
    
    private Long  duration;
    
    private Integer counrtyId;

	public CourseWrapper(Canvas_Courses courses, Vendor vendor) {
    	this.id=courses.getId();
    	this.courseId=courses.getCourseId();
    	this.vendorId=courses.getVendorId();
    	this.name=courses.getName();
    	this.start_at=courses.getStart_at()!=null?DateUtil.getFormatedDateTimeyyyyMMddhhmmss(courses.getStart_at()):null;
    	this.end_at=courses.getEnd_at()!=null?DateUtil.getFormatedDateTimeyyyyMMddhhmmss(courses.getEnd_at()):null;
    	this.course_code=courses.getCourse_code();
    	this.default_view=courses.getDefault_view();
    	this.term=courses.getTerm();
    	this.time_zone=courses.getTime_zone();
    	this.syllabus_body=courses.getSyllabus_body();
    	this.sections=courses.getSections();
    	this.image_download_url=courses.getImage_download_url();
    	this.public_description=courses.getPublic_description();
    	this.course_format=courses.getCourse_format();
    	this.price=courses.getPrice();
    	if(vendor!=null) {
    	this.vendor=updateVendor(vendor);
    	}
    	this.duration=this.start_at!=null&&this.end_at!=null?DateUtil.findDateDifference(this.start_at, this.end_at):null;
    }
    
    VendorWrapper updateVendor(Vendor vendor) {
    	
    	VendorWrapper vendorWrap = new VendorWrapper();
    	
    	vendorWrap.setVendorName(vendor.getVendorName());
    	vendorWrap.setAddress(vendor.getAddress());
    	vendorWrap.setBusinessContact(vendor.getBusinessContact());
    	vendorWrap.setBusinessLandscapeImageUrl(vendor.getBusinessLandscapeImageUrl());
    	vendorWrap.setBusinessLogoImageUrl(vendor.getBusinessPortraitImageUrl());
    	vendorWrap.setDescription(vendor.getDescription());
    	vendorWrap.setBusinessMobileLandscapeImageUrl(vendor.getInstituteImageURLF());
    	vendorWrap.setIntroduction(vendor.getIntroduction());
    	vendorWrap.setVendorId(vendor.getVendorId());
    	vendorWrap.setRating(vendor.getRating());
    	vendorWrap.setCountryId(vendor.getCountryId());
    	
    	return vendorWrap;
    	
    }
    
    public Integer getCounrtyId() {
		return counrtyId;
	}

	public void setCounrtyId(Integer counrtyId) {
		this.counrtyId = counrtyId;
	}
    
    public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
    
    public Long getDuration() {
  		return duration;
  	}

  	public void setDuration(Long duration) {
  		this.duration = duration;
  	}
  	
	public VendorWrapper getVendor() {
		return vendor;
	}

	public void setVendor(VendorWrapper vendor) {
		this.vendor = vendor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStart_at() {
		
		return start_at;
	}

	public void setStart_at(String start_at) {
		this.start_at = start_at;
	}

	public String getEnd_at() {
		return end_at;
	}

	public void setEnd_at(String end_at) {
		this.end_at = end_at;
	}

	public String getCourse_code() {
		return course_code;
	}

	public void setCourse_code(String course_code) {
		this.course_code = course_code;
	}

	public String getDefault_view() {
		return default_view;
	}

	public void setDefault_view(String default_view) {
		this.default_view = default_view;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getTime_zone() {
		return time_zone;
	}

	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}

	public String getSyllabus_body() {
		return syllabus_body;
	}

	public void setSyllabus_body(String syllabus_body) {
		this.syllabus_body = syllabus_body;
	}

	public String getSections() {
		return sections;
	}

	public void setSections(String sections) {
		this.sections = sections;
	}

	public String getImage_download_url() {
		return image_download_url;
	}

	public void setImage_download_url(String image_download_url) {
		this.image_download_url = image_download_url;
	}

	public String getPublic_description() {
		return public_description;
	}

	public void setPublic_description(String public_description) {
		this.public_description = public_description;
	}

	public String getCourse_format() {
		return course_format;
	}

	public void setCourse_format(String course_format) {
		this.course_format = course_format;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

}
