package com.emanage.dto;

import java.util.List;

import com.emanage.domain.JsonAddOn;
import com.emanage.dto.Order_SectionDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by rahul
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDishDTO {

    public String name;
    public String type;
    public Integer quantity;
    public Float price;
    public String instructions;
    public Order_SectionDTO[] sections;
    public List<JsonAddOn> addOn;
    public Integer dishSizeId;
    public Integer menuId;
    public String smallImageUrl;
    public String itemType;
    public String dishSizeName; //to show on kitchen screens

}