package com.emanage.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorsIdListDTO {

	public Integer vendorId;	
	public Integer maxPlateCount;
	public String portrateImageUrl; 
//	public Float servingDistance;	
	public  Double distanceBetween;
	public String description;
}
