package com.emanage.dto.credit;

import java.util.Date;

public class CreditPaymentInfoDTO {

	public int customerId;
	public float creditBillamount;
	public float paymentMade;
	public Date creditBillDate;
	
	
}
