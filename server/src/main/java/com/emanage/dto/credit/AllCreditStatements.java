package com.emanage.dto.credit;

import java.util.List;

import com.emanage.domain.CreditBill;

/**
 * Created by Abhishek on 11/20/2016.
 */
public class AllCreditStatements {
    public List<CreditBill> creditBills;
    public CreditStatementDTO latestStatement;
}
