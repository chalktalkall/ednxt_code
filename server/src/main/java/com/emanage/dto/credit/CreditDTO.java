package com.emanage.dto.credit;

import java.util.Date;

public class CreditDTO {

	public int customerId;
	public String customerFirstName;
	public String customerLastName;
	public String phone;
	public String emailId;
	
	public String creditName;
	public Date lastModifiedDate;
	public String status;
	public float maxLimit;
	public float balance;
	public int creditTypeId;
	public String billingCycle;
	
}
