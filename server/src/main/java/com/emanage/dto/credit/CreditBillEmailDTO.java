package com.emanage.dto.credit;

/**
 * Created by Abhishek on 12/21/2016.
 */
public class CreditBillEmailDTO {
    public String email;
    public String billId;
    public Integer orgId;

}
