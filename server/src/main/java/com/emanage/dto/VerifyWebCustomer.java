package com.emanage.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.emanage.validator.Phone;

public class VerifyWebCustomer {

	 @Phone(message="Invalid Mobile No.")
	 public String mobileNo;
	 
	 @Min(1)
	 public int orgID;
	 
	 @Min(100000)
	 public int otp;
	 
}
