package com.emanage.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorListRequest {

	public Integer orgId;
	public Integer universityId;

	
}
