package com.emanage.dto.saleRegister;

import java.util.Date;

public class ConflictedSale {
	public String restaurantName;
	public int restaurantId;
	public int orderId;
	public String orderStatus;
	public String fulfillmentCenterName;
	 public int fulfillmentCenterId;
	 public int checkId;
	 public String transactionId;
	 public String transaction_type;
	 public float transaction_amount;
	 public Date transaction_time;
	 public String invoiceId;
	 public float orderAmount;
	 public String check_status;
	 public float roundOffTotal;
	 public String phone;
}
