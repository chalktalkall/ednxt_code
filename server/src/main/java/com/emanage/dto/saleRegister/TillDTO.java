package com.emanage.dto.saleRegister;

import java.sql.Timestamp;

public class TillDTO {
  public String tillName;
  public Float balance;
  public Integer fulfillmentCenterId;
  public String tillId;
  public Timestamp openingTime;
  
}
