package com.emanage.dto.saleRegister;

public class TillHandOverDTO {
    public String tillId;
    public Float balance;
    public Integer fromUserId;
    public Integer toUserId;
    public String remarks;
	public String status;
}
