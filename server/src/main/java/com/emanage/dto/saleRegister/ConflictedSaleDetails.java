package com.emanage.dto.saleRegister;

import java.util.Date;

public class ConflictedSaleDetails {
	 public int orderId;
	 public String orderStatus;
	 public int checkId;
	 public String transactionId;
	 public String transactionType;
	 public float transactionAmount;
	 public Date transactionDate;
	 public String invoiceId;
	 public float orderAmount;
	 public String checkStatus;
	 public float roundOffTotal;
	 public String phone;
}
