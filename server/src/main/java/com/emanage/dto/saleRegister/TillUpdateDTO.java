package com.emanage.dto.saleRegister;

public class TillUpdateDTO {
  public String tillId;
  public Float amount;
  public Integer checkId;
  public String category;
  public String remarks;
}
