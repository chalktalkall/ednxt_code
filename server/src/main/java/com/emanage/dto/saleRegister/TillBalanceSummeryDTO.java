package com.emanage.dto.saleRegister;

public class TillBalanceSummeryDTO {
   public PaymentHistoryDTO transactionSummary;
   public float Current_Cash_Balance;
   public String tillName;
   public String ffcName;
   public String Till_Owner;
   public float ADD_CASH;
   public float WITHDRAW_CASH;
   public float TRANSACTION_CASH;
   public float Initial_Cash_Balance;
}
