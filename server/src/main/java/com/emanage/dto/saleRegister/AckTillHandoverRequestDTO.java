package com.emanage.dto.saleRegister;

public class AckTillHandoverRequestDTO {
  public String tillId;
  public Float expectedBalance;
  public Float actualBalance;
  public Float variance;
  public String remarks;
  public String status;
}
