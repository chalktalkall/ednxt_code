package com.emanage.dto.saleRegister;

import java.util.List;

public class ConflictedSaleByRestaurant {
public String restaurantName;
public int restaurantId;
public List<ConflictedSaleAtFulfillmentCenter> conflictedSaleAtFulfillmentCenter;
}
