package com.emanage.dto.saleRegister;

import com.emanage.enums.till.TillTransactionStatus;

/**
 * Created by rahul on 1/10/2017.
 */
public class CancelOrderDTO {
    public String paymentType;
    public int checkId;
    public TillTransactionStatus status = TillTransactionStatus.SUCCESS;
    public boolean createCreditTransaction = true;
}
