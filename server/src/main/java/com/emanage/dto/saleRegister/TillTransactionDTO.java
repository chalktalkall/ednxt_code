package com.emanage.dto.saleRegister;

import java.util.List;

public class TillTransactionDTO {
public String tillId;
public String tillName;
public Float balance;
public List<TransactionDTO> transactionList;
}
