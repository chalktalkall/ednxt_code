package com.emanage.dto.saleRegister;

import java.sql.Timestamp;

public class SaleTransaction {
	public String transactionId;
	public Float amount;
	public Timestamp time;
	public String paymentType;
	public Integer checkId;
	public String status;
}
