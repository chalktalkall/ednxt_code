package com.emanage.dto.saleRegister;

import java.util.List;

public class ConflictedSaleDTO {
  public List<ConflictedSaleByRestaurant> conflictedSaleAtRestaurant;
}
