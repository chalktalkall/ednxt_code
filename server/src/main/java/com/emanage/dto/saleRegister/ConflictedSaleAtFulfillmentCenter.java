package com.emanage.dto.saleRegister;

import java.util.List;

public class ConflictedSaleAtFulfillmentCenter {
 public String fulfillmentCenterName;
 public int fulfillmentCenterId;
 public List<ConflictedSaleDetails> conflictedSale;
 	
}
