package com.emanage.dto;

import java.util.List;

import com.emanage.domain.Customer;

public class CustomerDataDTO {
	
	public Customer customer;
	public List<CustomerAddressDTO> addressByRestaurant;
	public String status;
	public String message; 
	  

}