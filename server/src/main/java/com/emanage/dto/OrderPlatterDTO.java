package com.emanage.dto;

import java.util.List;

import com.emanage.domain.JsonAddOn;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author rahul
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderPlatterDTO {

   
    public Integer quantity;
    public Integer menuId;
    public Integer lms_course_id;
    public String instructions;
    public List<Order_SectionDTO> sections;    
    public List<JsonAddOn> addOn;
  
     //to show on kitchen screens

}