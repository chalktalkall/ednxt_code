package com.emanage.dto.giftCrad;

import java.util.Date;

/**
 * Created by rahul on 5/25/2017.
 */
public class GiftCardRedeemtionDTO extends GiftCardSellDTO {

    public Date redeemedOn;
    public int redeemedByCustomerId;
    public int redeemedByUserId;

}
