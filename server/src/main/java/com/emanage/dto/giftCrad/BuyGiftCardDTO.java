package com.emanage.dto.giftCrad;

import org.hibernate.validator.constraints.Email;

import com.emanage.validator.Phone;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by rahul on 5/25/2017.
 */
public class BuyGiftCardDTO {

    @Min(0)
    public float amount;

    @Phone(message = "Please provide valid mobile no with country code")
    public String mobileNoOfRecipient;

    @NotNull
    @Min(2)
    public String category;

    public String message;

    @Email
    public String emailIdOfRecipient;

    public int purchaserCustomerId;

    public String paymentMode;

    public String paymentStatus = "UNPAID";

    public String invoiceId;

}
