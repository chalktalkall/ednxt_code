package com.emanage.dto.giftCrad;

/**
 * Created by rahul on 6/2/2017.
 */
public class CustomerCreditAccountOpenINFODTO {

    public int fulfillmentCenterId;
    public String address;
    public int creditAccountTypeId;
}
