package com.emanage.dto.giftCrad;

public class CreateAndActivateGiftCardDTO {
	public String category;
	public float amount;
	public String msg;
	public String invoiceId;
	public int expireAfterDayCount;
}
