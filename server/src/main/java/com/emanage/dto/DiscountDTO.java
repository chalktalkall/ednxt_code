package com.emanage.dto;


import com.emanage.enums.check.AdditionalCategories;
import com.emanage.enums.restaurant.ChargesType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscountDTO {

	public long id;
	public long dcId;
	public String name;
	public AdditionalCategories category;
	public ChargesType type;
	public float value;
}
