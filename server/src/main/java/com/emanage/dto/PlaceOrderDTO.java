package com.emanage.dto;


/**
 * @author rahul
 */
public class PlaceOrderDTO {

    public CustomerDTO customer;
    public OrderDTO order;
    public Integer countryId;
 
}
