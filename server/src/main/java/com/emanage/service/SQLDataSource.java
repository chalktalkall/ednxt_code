package com.emanage.service;

import java.sql.Connection;
import java.util.List;

import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.Invoice;
import com.emanage.domain.Vendor;
import com.emanage.dto.DataSourceDTO;

public interface SQLDataSource {
	
	List<Canvas_Courses> getMSSQLViewData(DataSourceDTO datasource,String ViewName,Vendor vendor);
	void updateMSSQLInvoiceData(Invoice invoice,DataSourceDTO datasource,String tableName);
	Connection getMySQLConnection(DataSourceDTO datasource);
	Connection getOracleSQLConnection(DataSourceDTO datasource);
	Canvas_Courses getViewDataCourse(Vendor vendor, Integer courseId);

}
