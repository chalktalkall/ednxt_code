package com.emanage.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.emanage.domain.Trainer;
import com.emanage.dto.ResponseDTO;

public interface TrainerService {

	ResponseDTO saveTrainer(Trainer traier,HttpServletRequest request);
	List<Trainer> getAllActiveTrainer();
	List<Trainer> getAllTrainer();
	Trainer getTrainer(Integer id);
	ResponseDTO deleteTrainer(int id);
	Trainer getTrainerById(Integer id);
	ResponseDTO editTrainer(Trainer presentation);
}
