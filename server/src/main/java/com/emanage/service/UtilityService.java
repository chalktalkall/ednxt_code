package com.emanage.service;

import com.emanage.dto.ResponseDTO;
import com.emanage.dto.notification.ResultDTO;

public interface UtilityService {
	//@PreAuthorize("hasAnyRole('manager','admin')")
	public ResultDTO formateMobileNo(Integer orgId, int batchSize);
	//@PreAuthorize("hasAnyRole('manager','admin')")
	public ResultDTO listInvalidMobileNoCustomer(Integer orgId);
	//@PreAuthorize("hasAnyRole('manager','admin')")
	public ResultDTO removeInvalidCustomerWithInvalidPhone(Integer orgId,int batchSize);
	//@PreAuthorize("hasAnyRole('manager','admin')")
	public ResponseDTO sendTestOTP(String otpDetails);
	//@PreAuthorize("hasAnyRole('manager','admin')")
	public ResponseDTO formateMobileNoOfChecks(int batchSize);
	public ResponseDTO removeDuplicateCustomer(Integer orgId);
}
