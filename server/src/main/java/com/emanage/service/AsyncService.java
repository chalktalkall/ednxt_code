package com.emanage.service;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import com.emanage.domain.Customer;
import com.emanage.domain.Invoice;
import com.emanage.domain.Restaurant;
import com.emanage.domain.Trainer;
import com.emanage.domain.TutorRequest;
import com.emanage.domain.Vendor;
import com.emanage.dto.DemoRegDTO;
import com.emanage.dto.FetchOTPDTO;
import com.emanage.dto.ResponseDTO;

public interface AsyncService {

	public void emailException(String excpetionLogs,HttpServletRequest request) throws MessagingException, UnsupportedEncodingException;
	public void callBackRequest(String message,String subject, HttpServletRequest request) throws MessagingException, UnsupportedEncodingException;
	
	
	public String emailCheckFromServer(HttpServletRequest request,Invoice check, String emailAddr, Restaurant rest, String templateName, String subject, String sender, String refund, String reason, float extraAmount) throws MessagingException, UnsupportedEncodingException;

	public void emailCheckFromServerNew(HttpServletRequest request,Invoice check, String emailAddr, Restaurant rest, String templateName, String subject, String sender, String refund, String reason, float extraAmount) throws MessagingException, UnsupportedEncodingException;
	void emailTutorRequest(Customer customer, TutorRequest tutorRequest, HttpServletRequest request)
			throws MessagingException, UnsupportedEncodingException;
	
	void emailTrainerRequest(Trainer trainer, HttpServletRequest request)
			throws MessagingException, UnsupportedEncodingException;

	void sendDemoEnrollmentEmail(HttpServletRequest request, DemoRegDTO regd, Customer customer)
			throws MessagingException;
	ResponseDTO emailOTP(HttpServletRequest request, String message, Customer customer,FetchOTPDTO fetchOTP) throws MessagingException;

	void registerUserForCanvas(Integer vendorId, Integer customerId, Integer canvasCourseId);
	void saveCourseIntoClientDB(Vendor vendor, Invoice invoice);

}
