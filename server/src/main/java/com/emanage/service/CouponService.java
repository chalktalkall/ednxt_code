/**
 * 
 */
package com.emanage.service;

import java.util.List;

import com.emanage.domain.Coupon;
import com.emanage.domain.CouponResponse;
import com.emanage.domain.JsonCouponInfo;
import com.emanage.domain.Restaurant;
import com.emanage.enums.CouponState;



public interface CouponService {


	public void addCoupon(Coupon coupon);
	public void updateCoupon(Coupon coupon);
	public void removeCoupon(Integer id);
	
	public List<Coupon> listCoupon();
	public List<Coupon> listCouponByResturantId(Integer restaurantId);
	
	public Coupon getCouponById(Integer CouponId);
	//multiple coupon allowed with same code
	public List<Coupon> getAllCouponsByCode(String CouponCode, Integer restaurantId);
	//only one enable coupon possible with a rest
	public Coupon getEnabledCouponByCode(String couponCode, Integer restaurantId);
	
	public List<Coupon> getCouponListByCouponState(CouponState state, Integer restaurantId);
	
			
	public CouponResponse getCouponDef(JsonCouponInfo coupInfo);
	public boolean IsCustomerAssociate(Integer couponID, Integer custID);
	
	public Restaurant getRestaurant(Coupon cpn);
	
	
}