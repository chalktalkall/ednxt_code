package com.emanage.service;

import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.model.api.DesignFileException;
import org.eclipse.birt.report.model.api.activity.SemanticException;

import com.emanage.enums.report.ReportFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rahul on 3/1/2017.
 */
public interface BirtReportService {

    public void orderSourcesReportOrg(int orgId, ReportFormat format, HttpServletRequest request, HttpServletResponse response) throws EngineException, IOException, DesignFileException, SemanticException;

    public void orderSourcesReportRestaurantId(int restaurantId, String format);

    public void orderSourcesReportFFC(int ffcId, String format);

}
