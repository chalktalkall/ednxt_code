package com.emanage.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.emanage.service.StripeService;
import com.emanage.utility.ChargeRequest;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

@Service
public class StripeServiceImpl implements StripeService {
	  

	    @PostConstruct
	    public void init() throws FileNotFoundException {
	    	   Properties prop = new Properties();
				String propFileName = "stripePay.properties";
				InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

				if (inputStream != null) {
					try {
						prop.load(inputStream);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
				String stripeSecretKey = prop.getProperty("secret_key");
	    	
	    	
	        Stripe.apiKey = stripeSecretKey;
	    }

	    @Override
	    public Charge charge(ChargeRequest chargeRequest) throws StripeException, IOException {
	        Map<String, Object> chargeParams = new HashMap<>();
	        System.out.println(chargeRequest.getAmount());
	        chargeParams.put("amount", chargeRequest.getAmount());
	        chargeParams.put("currency", chargeRequest.getCurrency());
	        chargeParams.put("description", chargeRequest.getDescription());
	        if(chargeRequest.getStripeToken()!=null) {
	        	 chargeParams.put("source",chargeRequest.getStripeToken());
	        }else {
	        	 chargeParams.put("source", "");
	        }
	        return Charge.create(chargeParams);
	    }
}
