package com.emanage.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.TrainerDAO;
import com.emanage.domain.Trainer;
import com.emanage.dto.ResponseDTO;
import com.emanage.service.AsyncService;
import com.emanage.service.TrainerService;

@Service
public class TrainerServiceImpl implements TrainerService {

	@Autowired
	TrainerDAO trainerDAO;
	
	@Autowired
	AsyncService asycMailService;
	
	@Override
	@Transactional
	public ResponseDTO saveTrainer(Trainer trainer,HttpServletRequest request) {
		// TODO Auto-generated method stub
		ResponseDTO response = trainerDAO.saveTrainer(trainer);
		if("success".equalsIgnoreCase(response.result))
			try {
				asycMailService.emailTrainerRequest(trainer, request);
			} catch (UnsupportedEncodingException | MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return response;
	}

	@Override
	@Transactional
	public List<Trainer> getAllActiveTrainer() {
		// TODO Auto-generated method stub
		return trainerDAO.getAllActiveTrainer();
	}

	@Override
	@Transactional
	public List<Trainer> getAllTrainer() {
		// TODO Auto-generated method stub
		return trainerDAO.getAllTrainer();
	}

	@Override
	@Transactional
	public Trainer getTrainer(Integer id) {
		// TODO Auto-generated method stub
		return trainerDAO.getTrainer(id);
	}

	@Override
	@Transactional
	public ResponseDTO deleteTrainer(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Trainer getTrainerById(Integer id) {
		// TODO Auto-generated method stub
		return trainerDAO.getTrainerById(id);
	}

	@Override
	@Transactional
	public ResponseDTO editTrainer(Trainer trainer) {
		// TODO Auto-generated method stub
		return trainerDAO.editTrainer(trainer);
	}

	
}
