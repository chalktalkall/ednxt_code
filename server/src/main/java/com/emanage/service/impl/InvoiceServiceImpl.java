package com.emanage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.InvoiceDAO;
import com.emanage.dao.RestaurantDAO;
import com.emanage.domain.Coupon;
import com.emanage.domain.EditInvoices;
import com.emanage.domain.Invoice;
import com.emanage.domain.Restaurant;
import com.emanage.service.CouponService;
import com.emanage.service.InvoiceService;
import com.emanage.utility.StringUtility;

@Service
@Lazy
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceDAO invoicedao;
	
	@Autowired
	private RestaurantDAO restaurantDAO;
	
	@Autowired
	private CouponService couponService;
	
	@Override
	@Transactional
	public void addInvoice(Invoice invoice) {
		// TODO Auto-generated method stub
		if (StringUtility.isNullOrEmpty(invoice.getInvoiceId())) {
			Restaurant rest=restaurantDAO.getRestaurant(invoice.getCountryId());
			rest.setInvoiceStartCounter(rest.getInvoiceStartCounter()+1);
			restaurantDAO.saveResaurant(rest);
			//User user = userDAO.getUser(check.getRestaurantId());
			//user.setInvoiceStartCounter(user.getInvoiceStartCounter() + 1);
			//userDAO.saveUser(user);
			invoice.setInvoiceId(rest.getInvoicePrefix() + String.format("%08d", rest.getInvoiceStartCounter()));
		}
		invoicedao.addInvoice(invoice);
	}

	@Override
	@Transactional
	public void removeInvoice(Integer id) {
		// TODO Auto-generated method stub
		invoicedao.removeInvoice(id);
	}

	@Override
	@Transactional
	public Invoice getInvoice(Integer id) {
		// TODO Auto-generated method stub
		
		Invoice invoice    = invoicedao.getInvoice(id);
		if(invoice.getCoupon_Applied()!=null){
			List<Coupon> couponList = new ArrayList<Coupon>();
			if(invoice.getCoupon_Applied().size()>0){
				for(Coupon coupon : invoice.getCoupon_Applied()){
					couponList.add(couponService.getCouponById(coupon.getCoupanId()));
				}
				invoice.setCoupon_Applied(couponList);
				}
		}
		return invoice;
		 
	}

	@Override
	@Transactional
	public List<Invoice> getInvoiceByInvoiceId(String invoiceId) {
		// TODO Auto-generated method stub
		
		List<Invoice> invoiceList   = invoicedao.getInvoiceByInvoiceId(invoiceId);
		List<Invoice> invoiceArr = new ArrayList<Invoice>();
		
		for(Invoice invoice : invoiceList){
			if(invoice.getCoupon_Applied()!=null){
				List<Coupon> couponList = new ArrayList<Coupon>();
				if(invoice.getCoupon_Applied().size()>0){
					for(Coupon coupon : invoice.getCoupon_Applied()){
						couponList.add(couponService.getCouponById(coupon.getCoupanId()));
					}
					invoice.setCoupon_Applied(couponList);
					}
			}
			invoiceArr.add(invoice);
		}
		
		return invoiceArr;
		
	}

	@Override
	@Transactional
	public Invoice getInvoiceByCustId(Integer countryId, Integer custId) {
		// TODO Auto-generated method stub
		return invoicedao.getInvoiceByCustId(countryId, custId);
	}

	@Override
	@Transactional
	public List<Invoice> getAllOpenInvoices(Integer countryId) {
		// TODO Auto-generated method stub
		return invoicedao.getAllOpenInvoices(countryId);
	}

	@Override
	@Transactional
	public List<Integer> getAllInvoiceNos() {
		// TODO Auto-generated method stub
		return invoicedao.getAllInvoiceNos();
	}

	@Override
	@Transactional
	public List<Invoice> getAllInvoiceIdByCountryId(Integer countryId) {
		// TODO Auto-generated method stub
		return invoicedao.getAllInvoiceIdByCountryId(countryId);
	}

	@Override
	@Transactional
	public List getClosedInvoicesByDate(Integer countryId, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return invoicedao.getClosedInvoicesByDate(countryId, startDate, endDate);
	}

	@Override
	@Transactional
	public List<Invoice> getDailyInvoice(Integer countryId, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		
		List<Invoice> invoiceList =  new ArrayList<Invoice>();
		for(Invoice invoice : invoicedao.getDailyInvoice(countryId, startDate, endDate)){
			if(invoice!=null){
				if(invoice.getCoupon_Applied()!=null){
					List<Coupon> couponList = new ArrayList<Coupon>();
					if(invoice.getCoupon_Applied().size()>0){
						for(Coupon coupon : invoice.getCoupon_Applied()){
							couponList.add(couponService.getCouponById(coupon.getCoupanId()));
						}
						invoice.setCoupon_Applied(couponList);
						}
				}
			}
			invoiceList.add(invoice);
		}
		return invoiceList;
		
	}

	@Override
	@Transactional
	public List<String> getUniqueDishTypes(Integer countryId) {
		// TODO Auto-generated method stub
		return invoicedao.getUniqueDishTypes(countryId);
	}

	@Override
	@Transactional
	public List getDailySalesRecords(Integer countryId, Date startDate) {
		// TODO Auto-generated method stub
		return invoicedao.getDailySalesRecords(countryId, startDate);
	}

	@Override
	@Transactional
	public List<Invoice> getAllInvoices(List<Integer> ids) {
		// TODO Auto-generated method stub
		return invoicedao.getAllInvoices(ids);
	}

	@Override
	@Transactional
	public List getMonthlyBillSummary(Integer countryId, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return invoicedao.getMonthlyBillSummary(countryId, startDate, endDate);
	}

	@Override
	@Transactional
	public List<Invoice> getDailyInvoiceByFfc(Integer ffcId, Date startTime, Date endTime) {
		// TODO Auto-generated method stub
		return invoicedao.getDailyInvoiceByFfc(ffcId, startTime, endTime);
	}

	@Override
	@Transactional
	public List<Invoice> getCustomersInvoiceList(String email, Integer customerId, Integer countryId) {
		// TODO Auto-generated method stub
		return invoicedao.getCustomersInvoiceList(email, customerId, countryId);
	}

	@Override
	@Transactional
	public List<Invoice> getCustomersInvoiceListByYear(String phone, Integer customerId, Integer countryId,
			Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return invoicedao.getCustomersInvoiceListByYear(phone, customerId, countryId, startDate, endDate);
	}

	@Override
	@Transactional
	public void addEditInvoice(EditInvoices editCheck) {
		// TODO Auto-generated method stub
		invoicedao.addEditInvoice(editCheck);
	}

	@Override
	@Transactional
	public void removeEditInvoice(Integer id) {
		// TODO Auto-generated method stub
		invoicedao.removeEditInvoice(id);
	}

	@Override
	@Transactional
	public EditInvoices getEditInvoice(Integer id) {
		// TODO Auto-generated method stub
		return invoicedao.getEditInvoice(id);
	}

	@Override
	@Transactional
	public List<EditInvoices> getEditInvoiceListByInvoiceNo(Integer invoiceNo) {
		// TODO Auto-generated method stub
		return invoicedao.getEditInvoiceListByInvoiceNo(invoiceNo);
	}

	@Override
	@Transactional
	public Invoice getInvoiceByRazorPayId(String orderId) {
		// TODO Auto-generated method stub
		Invoice invoice    = invoicedao.getInvoiceByRazorPayId(orderId);;
		if(invoice!=null && invoice.getCoupon_Applied()!=null){
			List<Coupon> couponList = new ArrayList<Coupon>();
			if(invoice.getCoupon_Applied().size()>0){
				for(Coupon coupon : invoice.getCoupon_Applied()){
					couponList.add(couponService.getCouponById(coupon.getCoupanId()));
				}
				invoice.setCoupon_Applied(couponList);
				}
		}
		
		return invoice;
	}

}
