package com.emanage.service.impl;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.emanage.controller.CashRegisterController;
import com.emanage.domain.CheckDishResponse;
import com.emanage.domain.Customer;
import com.emanage.domain.DBConfig;
import com.emanage.domain.Invoice;
import com.emanage.domain.InvoiceResponse;
import com.emanage.domain.OrderAddOn;
import com.emanage.domain.Platter;
import com.emanage.domain.Restaurant;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.Trainer;
import com.emanage.domain.TutorRequest;
import com.emanage.domain.Vendor;
import com.emanage.dto.DataSourceDTO;
import com.emanage.dto.DemoRegDTO;
import com.emanage.dto.FetchOTPDTO;
import com.emanage.dto.ResponseDTO;
import com.emanage.enums.DataSourceType;
import com.emanage.enums.credit.BilligCycle;
import com.emanage.enums.credit.CustomerCreditAccountStatus;
import com.emanage.enums.order.Status;
import com.emanage.service.AsyncService;
import com.emanage.service.CanvasLMSService;
import com.emanage.service.CustomerCreditService;
import com.emanage.service.CustomerService;
import com.emanage.service.RestaurantService;
import com.emanage.service.SQLDataSource;
import com.emanage.service.TaxTypeService;
import com.emanage.service.VendorDBConfigService;
import com.emanage.service.VendorPlatterService;
import com.emanage.service.VendorService;
import com.emanage.utility.MailerUtility;
import com.emanage.utility.StringUtility;

@Service
@Lazy
public class AsyncServiceImpl implements AsyncService {

	final static Logger logger = Logger.getLogger(CashRegisterController.class);

	@Autowired
	private JavaMailSenderImpl mailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Autowired
	private TaxTypeService taxTypeService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private VendorService vendorService;

	@Autowired
	private VendorPlatterService vendorPlatterService;

	@Autowired
	private RestaurantService restaurantService;

	@Autowired
	private CanvasLMSService canvasLMSService;

	@Autowired
	@Qualifier("customerCreditAutomatedBilling")
	private CustomerCreditService customerCreditService;

	@Autowired
	private SQLDataSource sqlDataSource;

	@Autowired
	private VendorDBConfigService vendorDBConfigService;

	@Override
	@Async
	@Transactional
	public void emailException(String exceptionLogs, HttpServletRequest request)
			throws MessagingException, UnsupportedEncodingException {

		String resultPath;
		if (request != null) {
			// String scheme = request.getScheme();
			String serverName = request.getServerName();
			resultPath = serverName;
		} else {
			resultPath = "Exception Logs";
		}

		MailerUtility mailUtil = new MailerUtility();
		final Context ctx = new Context();
		String subject = resultPath + " : " + "Exception Alert !";
		ctx.setVariable("exceptionLogs", exceptionLogs);
		logger.info("***********************serverName sending exception***********************************");

		final MimeMessage mimeMessage = mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

		message.setSubject(subject);
		String senderEmail = mailUtil.username;
		InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail, "Exception Express!");
		message.setFrom(restaurantEmailAddress);
		message.setReplyTo(restaurantEmailAddress);

		if ("www. us.in".equalsIgnoreCase(resultPath)) {
			message.setTo("");
			message.addCc("");
			message.addCc("");
			message.addCc("");
			message.addCc("");
		} else {
			message.setTo("errahulsharma.cs@gmail.com");
			// message.addCc("");
		}

		final String htmlContent = templateEngine.process("exceptionExpress", ctx);
		message.setText(htmlContent, true);
		String oldUsername = null;
		String oldPassword = null;
		String oldHost = null;
		String oldProtocol = null;
		Integer oldPort = -1;
		if (!StringUtility.isNullOrEmpty(mailUtil.username) && !StringUtility.isNullOrEmpty(mailUtil.password)) {
			oldUsername = mailSender.getUsername();
			oldPassword = mailSender.getPassword();
			oldHost = mailSender.getHost();
			oldProtocol = mailSender.getProtocol();
			oldPort = mailSender.getPort();
			mailSender.setUsername(oldUsername);
			mailSender.setPassword(oldPassword);
			mailSender.setHost(oldHost);
			mailSender.setProtocol(oldProtocol);
			mailSender.setPort(oldPort);
		}

		mailSender.send(mimeMessage);
		logger.info("***********************sending end***********************************");
	}

	@Override
	public void callBackRequest(String message, String subject, HttpServletRequest request)
			throws MessagingException, UnsupportedEncodingException {
		// TODO Auto-generated method stub

	}

	@Override
	public String emailCheckFromServer(HttpServletRequest request, Invoice check, String emailAddr, Restaurant rest,
			String templateName, String subject, String sender, String refund, String reason, float extraAmount)
			throws MessagingException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		return null;
	}

	@Async
	public void emailCheckFromServerLocal(HttpServletRequest request, Customer customer, Invoice invoice,
			String emailAddr, Restaurant rest, String templateName, String subject, String sender, String refund,
			String reason, float extraAmount) throws MessagingException, UnsupportedEncodingException {

		// Prepare the evaluation context
		if (emailAddr != null && (!"".equalsIgnoreCase(emailAddr))) {
			final Context ctx = new Context();
			logger.info("invoiceId : " + invoice.getInvoiceNo());
			Vendor vendor = vendorService.getVendor(invoice.getVendorId());
			logger.info("***********************sending check from default***********************************");
			if (invoice != null) {

				Float waiveOff = null;
				InvoiceResponse checkResponse = new InvoiceResponse(invoice, taxTypeService, waiveOff, rest);

				templateName = "defaultemailbill";
				subject = rest.getBussinessName() + " : Receipt";

				ctx.setVariable("checkRespone", checkResponse);
				if (invoice.getCustomerId() > 0) {
					String description = "";
					if (customer.getCredit() != null) {
						if (customer.getCredit().getCreditBalance() != 0) {
							description = setDescription(invoice.getOrders().get(0).getStatus(), customer, invoice,
									refund, reason, extraAmount, rest);
						} else if (invoice.getOrders().get(0).getStatus() == Status.CANCELLED) {
							description = "Thanks for choosing  us";
						}
					} else {
						if (invoice.getOrders().get(0).getStatus() != Status.CANCELLED) {
							description = "Thanks for choosing  us. Here is your order summary for order no."
									+ invoice.getOrderId();
						} else if (invoice.getOrders().get(0).getStatus() == Status.CANCELLED) {
							description = "Thanks for choosing us";
						}
					}

					ctx.setVariable("description", description);
					ctx.setVariable("customer", customer);
				}
				if (rest != null) {
					if (rest.isRoundOffAmount()) {
						invoice.setRoundOffTotal(Math.round(checkResponse.getRoundedOffTotal()));
						checkResponse.setRoundedOffTotal(Math.round(checkResponse.getRoundedOffTotal()));
					} else {
						invoice.setRoundOffTotal(checkResponse.getRoundedOffTotal());
					}
				}
				ctx.setVariable("user", rest);
				ctx.setVariable("vendor", vendor);
				ctx.setVariable("redirectURL", "https://letschalktalk.com");

				Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
				cal.setTime(invoice.getOpenTime());
				DateFormat formatter1;
				formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				formatter1.setTimeZone(cal.getTimeZone());
				ctx.setVariable("checkDate", formatter1.format(cal.getTime()));
				Map<String, com.emanage.domain.JsonDish> itemsMap = new TreeMap<String, com.emanage.domain.JsonDish>();
				List<CheckDishResponse> items = checkResponse.getItems();

				List<com.emanage.domain.JsonAddOn> jsonAdd = new ArrayList<com.emanage.domain.JsonAddOn>();
				for (CheckDishResponse item : items) {
					if (itemsMap.containsKey(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""))) {
						com.emanage.domain.JsonDish jsonDish = itemsMap
								.get(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""));
						jsonDish.setPrice(jsonDish.getPrice() + item.getPrice());
						jsonDish.setQuantity(jsonDish.getQuantity() + 1);
					} else {
						com.emanage.domain.JsonDish jsonDish = new com.emanage.domain.JsonDish();
						jsonDish.setQuantity(1);
						jsonDish.setName(item.getName());
						jsonDish.setId(item.getDishId());
						jsonDish.setPrice(item.getPrice());
						// jsonDish.setDishSize(item.getDishSize());
						if (item.getSections() != null) {
							jsonDish.setSection(Arrays.asList(item.getSections()));
						}
						List<OrderAddOn> orderAddOn = item.getAddOnresponse();
						if (orderAddOn != null) {
							for (OrderAddOn oad : orderAddOn) {
								com.emanage.domain.JsonAddOn jsonAddOn = new com.emanage.domain.JsonAddOn();
								jsonAddOn.setItemId(oad.getAddOnId());
								jsonAddOn.setDishId(item.getDishId());
								jsonAddOn.setName(oad.getName());
								jsonAddOn.setQuantity(oad.getQuantity());
								jsonAddOn.setPrice(oad.getPrice());
								jsonAddOn.setDishSize(oad.getDishSize());
								jsonAdd.add(jsonAddOn);
								jsonDish.setAddOns(jsonAdd);
							}
						}
						itemsMap.put(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""), jsonDish);
					}
				}

				ctx.setVariable("itemsMap", itemsMap);
			} else {
				logger.info("No check Found for " + emailAddr);
			}
			// Vendor vendor= vendorService.getVendor(invoice.getVendorId());
			// Prepare message using a Spring helper
			final MimeMessage mimeMessage = mailSender.createMimeMessage();
			final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

			message.setSubject(subject);
			if (rest != null) {
				// String senderEmail = StringUtility.isNullOrEmpty(rest.getMailUsername()) ?
				// user.getUsername() : rest.getMailUsername();
				String senderEmail = rest.getMailUsername();
				InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
				message.setFrom(restaurantEmailAddress);
				message.setReplyTo(restaurantEmailAddress);
			}
			message.setTo(emailAddr);
			if (vendor != null && vendor.getAlertMail() != null && vendor.getAlertMail().trim().length() > 0) {
				message.setBcc(vendor.getAlertMail());
			}
			// Create the HTML body using Thymeleaf

			final String htmlContent = templateEngine.process(templateName, ctx);
			message.setText(htmlContent, true); // true = isHtml
			String oldUsername = null;
			String oldPassword = null;
			String oldHost = null;
			String oldProtocol = null;
			Integer oldPort = -1;
			if (!StringUtility.isNullOrEmpty(rest.getMailUsername())
					&& !StringUtility.isNullOrEmpty(rest.getMailPassword())) {
				oldUsername = mailSender.getUsername();
				oldPassword = mailSender.getPassword();
				oldHost = mailSender.getHost();
				oldProtocol = mailSender.getProtocol();
				oldPort = mailSender.getPort();
				if (sender != null) {
					mailSender.setUsername(MailerUtility.username);
					mailSender.setPassword(MailerUtility.password);
				} else {
					mailSender.setUsername(rest.getMailUsername());
					mailSender.setPassword(rest.getMailPassword());
				}
				mailSender.setHost(rest.getMailHost());
				mailSender.setProtocol(rest.getMailProtocol());
				mailSender.setPort(rest.getMailPort());
			}
			mailSender.send(mimeMessage);
			logger.info("email sent for checkId :" + invoice.getInvoiceNo());
			if (!StringUtility.isNullOrEmpty(oldUsername) && !StringUtility.isNullOrEmpty(oldPassword)) {
				mailSender.setUsername(oldUsername);
				mailSender.setPassword(oldPassword);
				mailSender.setHost(oldHost);
				mailSender.setProtocol(oldProtocol);
				mailSender.setPort(oldPort);
			}
			logger.info(
					"***********************Email Sent Successfully sending end***********************************");
		}

	}

	String setDescription(Status status, Customer customer, Invoice check, String refund, String reason,
			float extraAmount, Restaurant restaurant) {

		if (customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE
				&& customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
			// Restaurant restaurant = getRestaurant(check.getRestaurantId());
			if (status == Status.CANCELLED && "CREDIT".equalsIgnoreCase(refund)) {
				return "Your order no." + check.getOrderId() + " (of  Total Rs." + check.getRoundOffTotal()
						+ ") has been cancelled. We owe you  Rs " + Math.abs(customer.getCredit().getCreditBalance())
						+ "," + "which will be automatically deducted from your next order amount."
						+ "Should there be any concern pls call at " + restaurant.getBussinessPhoneNo()
						+ ".  Assuring you of our best, always";
			} else if (status == Status.CANCELLED && "CASH".equalsIgnoreCase(refund)) {
				return "Your order with order No. " + check.getOrderId()
						+ " has been cancelled. We have initiated the refund as CASH. One of our delivery boy will hand over it to you soon";
			} else if ("COMPLETED".equalsIgnoreCase(refund) && "DELIVERED".equalsIgnoreCase(reason)) {
				return "Your order no." + check.getOrderId() + " (of  Total Rs." + check.getRoundOffTotal()
						+ ") has been DELIVERED. Your current balance with " + restaurant.getRestaurantName() + " is "
						+ Math.abs(customer.getCredit().getCreditBalance()) + "."
						+ "Should there be any concern pls call at " + restaurant.getBussinessPhoneNo()
						+ ".  Assuring you of our best, always";
			} else if ("UPDATE".equalsIgnoreCase(refund) && "DISCOUNT".equalsIgnoreCase(reason)) {
				return "Your order 	no." + check.getOrderId() + " has been discounted by Rs " + extraAmount
						+ ". We owe you  Rs " + Math.abs(customer.getCredit().getCreditBalance()) + "."
						+ "which will be automatically deducted from your next order amount. Should there be any concern pls call at "
						+ restaurant.getBussinessPhoneNo() + ".  Assuring you of our best, always";
			} else if ("UPDATE".equalsIgnoreCase(refund) && "ADDED".equalsIgnoreCase(reason)) {
				return "Your order no." + check.getOrderId() + " (item of Rs " + extraAmount
						+ " has been cancelled). We owe you  Rs " + Math.abs(customer.getCredit().getCreditBalance())
						+ ","
						+ "which will be automatically deducted to your next order amount. Should there be any concern pls call at "
						+ restaurant.getBussinessPhoneNo() + ".  Assuring you of our best, always";
			} else if ("UPDATE".equalsIgnoreCase(refund) && "CHARGED".equalsIgnoreCase(reason)) {
				return "Your order no." + check.getOrderId() + " has been updated (item of Rs " + extraAmount
						+ " has been added). You owe us  Rs " + Math.abs(customer.getCredit().getCreditBalance()) + ""
						+ ",which will be automatically added to your next order amount. Should there be any concern pls call at "
						+ restaurant.getBussinessPhoneNo() + ".  Assuring you of our best, always";
			} else if ("PAYMENT_TYPE_CHANGED".equalsIgnoreCase(reason) && refund != null) {
				if (customer.getCredit().getCreditBalance() < 0) {
					return "Payment type for order no." + check.getOrderId() + " has been changed to "
							+ check.getOrders().get(0).getPaymentStatus() + " from " + refund + ". We owe you Rs "
							+ Math.abs(customer.getCredit().getCreditBalance()) + ","
							+ "which will be automatically deducted from your next order amount. Should there be any concern pls call at "
							+ restaurant.getBussinessPhoneNo() + ".  Assuring you of our best, always";
				} else if (customer.getCredit().getCreditBalance() > 0) {
					return "Payment type for order no. " + check.getOrderId() + " has been changed to "
							+ check.getOrders().get(0).getPaymentStatus() + " from " + refund + ". You owe us Rs "
							+ Math.abs(customer.getCredit().getCreditBalance()) + ","
							+ "which will be automatically added to your next order amount.We request you to make this payment at the earliest. Should there be any concern pls call at "
							+ restaurant.getBussinessPhoneNo() + ".  Assuring you of our best, always";
				} else {
					return "Payment type for order no. " + check.getOrderId() + " has been changed to "
							+ check.getOrders().get(0).getPaymentStatus() + " from " + refund + ".You owe us Rs "
							+ Math.abs(customer.getCredit().getCreditBalance()) + "."
							+ "Should there be any concern pls call at " + restaurant.getBussinessPhoneNo()
							+ ".  Assuring you of our best, always";

				}
			} else {
				return "Thanks for choosing us. Here is your order summary for order no." + check.getOrderId();
			}
		} else {
			// Restaurant restaurant = getRestaurant(check.getRestaurantId());
			if (customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE
					&& customer.getCredit().getCreditType().getBillingCycle() != BilligCycle.ONE_OFF) {
				if (status == Status.CANCELLED) {
					if (check.getStatus() == com.emanage.enums.check.Status.Paid) {
						return "Your order No. " + check.getOrderId() + " (of  Total Rs." + check.getRoundOffTotal()
								+ ") has been cancelled. This amount has been adjusted to your current credit balance. Now you owe us Rs "
								+ Math.abs(customer.getCredit().getCreditBalance()) + "."
								+ "Should there be any concern pls call at " + restaurant.getBussinessPhoneNo()
								+ ".  Assuring you of our best, always.";
					} else {
						return "";
					}
				} else {
					return "Thanks for choosing us. Here is your order summary for order no." + check.getOrderId();
				}
			} else {
				return "Thanks for choosing us. Here is your order summary for order no." + check.getOrderId();
			}
		}
	}

	@Override
	@Async
	@Transactional
	public void emailCheckFromServerNew(HttpServletRequest request, Invoice check, String emailAddr, Restaurant rest,
			String templateName, String subject, String sender, String refund, String reason, float extraAmount)
			throws MessagingException, UnsupportedEncodingException {
		// TODO Auto-generated method stub

		Customer customer = customerService.getCustomer(check.getCustomerId());
		emailCheckFromServerLocal(request, customer, check, emailAddr, rest, templateName, subject, sender, refund,
				reason, extraAmount);

	}

	@Override
	@Transactional
	@Async
	public void emailTutorRequest(Customer customer, TutorRequest tutorRequest, HttpServletRequest request)
			throws MessagingException, UnsupportedEncodingException {

		Restaurant rest = restaurantService.getRestaurant(customer.getRestaurantId());

		final Context ctx = new Context();

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
		DateFormat formatter1;
		formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		formatter1.setTimeZone(cal.getTimeZone());
		ctx.setVariable("checkDate", formatter1.format(cal.getTime()));

		ctx.setVariable("customer", customer);
		ctx.setVariable("tutorRequest", tutorRequest);
		ctx.setVariable("user", rest);
		ctx.setVariable("redirectURL", "https://letschalktalk.com");

		String subject = "Request recieved";
		// ctx.setVariable("exceptionLogs", exceptionLogs);
		logger.info("***********************serverName sending requets tutor email***********************************");

		final MimeMessage mimeMessage = mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

		message.setSubject(subject);
		if (rest != null) {
			// String senderEmail = StringUtility.isNullOrEmpty(rest.getMailUsername()) ?
			// user.getUsername() : rest.getMailUsername();
			String senderEmail = rest.getMailUsername();
			InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
			message.setFrom(restaurantEmailAddress);
			message.setReplyTo(restaurantEmailAddress);
		}

		if (customer.getEmail().length() > 1) {
			message.setTo(customer.getEmail());
			if (rest.getAlertMail() != null && rest.getAlertMail().length() > 3) {
				String[] emails = { "errahulsharma.cs@gmail.com", rest.getAlertMail() };
				message.setBcc(emails);
			}

		}

		// Create the HTML body using Thymeleaf

		final String htmlContent = templateEngine.process("tutorRequest", ctx);
		message.setText(htmlContent, true); // true = isHtml
		if (!StringUtility.isNullOrEmpty(rest.getMailUsername())
				&& !StringUtility.isNullOrEmpty(rest.getMailPassword())) {

			mailSender.setUsername(rest.getMailUsername());
			mailSender.setPassword(rest.getMailPassword());
			mailSender.setHost(rest.getMailHost());
			mailSender.setProtocol(rest.getMailProtocol());
			mailSender.setPort(rest.getMailPort());
		}
		mailSender.send(mimeMessage);

		logger.info("***********************Email Sent Successfully sending end***********************************");
	}

	@Override
	@Transactional
	@Async
	public void emailTrainerRequest(Trainer trainer, HttpServletRequest request)
			throws MessagingException, UnsupportedEncodingException {

		Restaurant rest = restaurantService.getRestaurant(trainer.getCountryId());

		final Context ctx = new Context();

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
		DateFormat formatter1;
		formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		formatter1.setTimeZone(cal.getTimeZone());
		String description = "Thanks for the registration. We'll get back to you shortly.";
		ctx.setVariable("checkDate", formatter1.format(cal.getTime()));

		ctx.setVariable("trainer", trainer);
		ctx.setVariable("description", description);
		ctx.setVariable("user", rest);
		ctx.setVariable("redirectURL", "https://coursedge.org");

		String subject = "Request recieved";
		// ctx.setVariable("exceptionLogs", exceptionLogs);
		logger.info("***********************serverName sending requets tutor email***********************************");

		final MimeMessage mimeMessage = mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

		message.setSubject(subject);
		if (rest != null) {
			// String senderEmail = StringUtility.isNullOrEmpty(rest.getMailUsername()) ?
			// user.getUsername() : rest.getMailUsername();
			String senderEmail = rest.getMailUsername();
			InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
			message.setFrom(restaurantEmailAddress);
			message.setReplyTo(restaurantEmailAddress);
		}

		if (trainer.getEmail().length() > 1) {
			message.setTo(trainer.getEmail());
			if (rest.getAlertMail() != null && rest.getAlertMail().length() > 3) {
				String[] emails = { "errahulsharma.cs@gmail.com", rest.getAlertMail() };
				message.setBcc(emails);
			}

		}

		// Create the HTML body using Thymeleaf

		final String htmlContent = templateEngine.process("trainerRequest", ctx);
		message.setText(htmlContent, true); // true = isHtml
		if (!StringUtility.isNullOrEmpty(rest.getMailUsername())
				&& !StringUtility.isNullOrEmpty(rest.getMailPassword())) {

			mailSender.setUsername(rest.getMailUsername());
			mailSender.setPassword(rest.getMailPassword());
			mailSender.setHost(rest.getMailHost());
			mailSender.setProtocol(rest.getMailProtocol());
			mailSender.setPort(rest.getMailPort());
		}
		mailSender.send(mimeMessage);

		logger.info("***********************Email Sent Successfully sending end***********************************");
	}

	@Override
	@Transactional
	@Async
	public void sendDemoEnrollmentEmail(HttpServletRequest request, DemoRegDTO regd, Customer customer)
			throws MessagingException {
		final Context ctx = new Context(request.getLocale());
		String templateName = "demoEnrollment";

		Vendor vendor = vendorService.getVendor(regd.vendorId);
		SectionDishes sCourse = null;
		Platter platter = null;

		if (regd.platterId != null) {
			platter = vendorPlatterService.getPlatter(regd.platterId);
			ctx.setVariable("course", platter);
		} else if (regd.courseId != null) {
			sCourse = vendorPlatterService.getSectionDish(regd.courseId);
			ctx.setVariable("course", sCourse);
		}

		Restaurant rest = restaurantService.getRestaurant(vendor.getCountryId());
		ctx.setVariable("customer", customer);
		ctx.setVariable("vendor", vendor);
		ctx.setVariable("regd", regd);
		ctx.setVariable("rest", rest);
		ctx.setVariable("redirectURL", "https://letschalktalk.com");

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
		DateFormat formatter1;
		formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		formatter1.setTimeZone(cal.getTimeZone());
		ctx.setVariable("checkDate", formatter1.format(cal.getTime()));

		final MimeMessage mimeMessage = mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

		message.setSubject("Enrollment Notification");
		if (rest != null) {

			String senderEmail = rest.getMailUsername();
			InternetAddress restaurantEmailAddress = null;
			try {
				restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			message.setFrom(restaurantEmailAddress);
			message.setReplyTo(restaurantEmailAddress);
		}
		message.setTo(customer.getEmail());
		if (vendor != null && vendor.getAlertMail() != null) {
			String[] emails = { vendor.getAlertMail(), "errahulsharma.cs@gmail.com", rest.getAlertMail() };
			message.setBcc(emails);
		}
		// Create the HTML body using Thymeleaf

		final String htmlContent = templateEngine.process(templateName, ctx);
		message.setText(htmlContent, true); // true = isHtml

		if (!StringUtility.isNullOrEmpty(rest.getMailUsername())
				&& !StringUtility.isNullOrEmpty(rest.getMailPassword())) {

			mailSender.setUsername(rest.getMailUsername());
			mailSender.setPassword(rest.getMailPassword());

			mailSender.setHost(rest.getMailHost());
			mailSender.setProtocol(rest.getMailProtocol());
			mailSender.setPort(rest.getMailPort());
		}
		mailSender.send(mimeMessage);
	}

	@Override
	@Transactional
	public ResponseDTO emailOTP(HttpServletRequest request, String otpMessage, Customer customer, FetchOTPDTO fetchOTP)
			throws MessagingException {
		final Context ctx = new Context(request.getLocale());
		logger.info("sending email to" + customer.getEmail());
		String templateName = "loginOTP";

		ResponseDTO response = new ResponseDTO();
		Restaurant rest = null;
		if (fetchOTP.orgId > 0) {
			rest = restaurantService.getParentRestaurant(fetchOTP.orgId);
		} else if (fetchOTP.countryId > 0) {
			rest = restaurantService.getRestaurant(fetchOTP.countryId);
		}
		ctx.setVariable("name", customer.getFirstName());
		ctx.setVariable("otpMessage", otpMessage);
		// ctx.setVariable("rest", rest);

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
		DateFormat formatter1;
		formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		formatter1.setTimeZone(cal.getTimeZone());
		ctx.setVariable("checkDate", formatter1.format(cal.getTime()));

		final MimeMessage mimeMessage = mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

		message.setSubject("CoursEdge Login OTP");
		if (rest != null) {

			String senderEmail = rest.getMailUsername();
			InternetAddress restaurantEmailAddress = null;
			try {
				restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
			} catch (UnsupportedEncodingException e) {
				response.message = "Failed to send OTP, invalid email address";
				response.result = "Failed";
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			message.setFrom(restaurantEmailAddress);
			message.setReplyTo(restaurantEmailAddress);
		}
		message.setTo(customer.getEmail());

		// String[] emails = {rest.getAlertMail() };
		// message.setBcc(emails);
		// Create the HTML body using Thymeleaf

		final String htmlContent = templateEngine.process(templateName, ctx);
		message.setText(htmlContent, true); // true = isHtml

		if (!StringUtility.isNullOrEmpty(rest.getMailUsername())
				&& !StringUtility.isNullOrEmpty(rest.getMailPassword())) {

			mailSender.setUsername(rest.getMailUsername());
			mailSender.setPassword(rest.getMailPassword());

			mailSender.setHost(rest.getMailHost());
			mailSender.setProtocol(rest.getMailProtocol());
			mailSender.setPort(rest.getMailPort());
		}
		try {
			mailSender.send(mimeMessage);
			response.message = "OTP sent";
			response.result = "Success";
		} catch (Exception e) {
			response.message = "Failed to send OTP, " + e.getMessage();
			response.result = "Failed";
		}
		return response;
	}

	@Override
	@Transactional
	@Async
	public void registerUserForCanvas(Integer vendorId, Integer customerId, Integer canvasCourseId) {
		// TODO Auto-generated method stub
		if (canvasLMSService.getUserDetails(customerId, vendorId) == null) {
			canvasLMSService.createUser(vendorId, customerId);
			canvasLMSService.enrollUser(customerId, vendorId, canvasCourseId);
		} else {
			canvasLMSService.enrollUser(customerId, vendorId, canvasCourseId);
		}

	}

	@Override
	@Transactional
	@Async
	public void saveCourseIntoClientDB(Vendor vendor, Invoice invoice) {
		// TODO Auto-generated method stub
		DBConfig dataSource = vendorDBConfigService.getCampusDBConfig(vendor.getVendorId());
		if (dataSource.getDbType() == DataSourceType.MSSQL) {
			DataSourceDTO sourceDTO = new DataSourceDTO();
			sourceDTO.setDb_url(dataSource.getDbUrl());
			sourceDTO.setUser(dataSource.getUsername());
			sourceDTO.setPassword(dataSource.getPassword());
			sourceDTO.setPort(dataSource.getDbPort());
			sourceDTO.setDbName("");
			sqlDataSource.updateMSSQLInvoiceData(invoice, sourceDTO, dataSource.getDbInvoiceTable());
		}

	}

}
