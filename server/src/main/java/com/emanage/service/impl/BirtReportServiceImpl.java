package com.emanage.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.HTMLServerImageHandler;
import org.eclipse.birt.report.engine.api.IGetParameterDefinitionTask;
import org.eclipse.birt.report.engine.api.IPDFRenderOption;
import org.eclipse.birt.report.engine.api.IParameterDefn;
import org.eclipse.birt.report.engine.api.IRenderOption;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.DesignFileException;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.OdaDataSetHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.SessionHandle;
import org.eclipse.birt.report.model.api.SlotHandle;
import org.eclipse.birt.report.model.api.SlotIterator;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.enums.report.ReportFormat;
import com.emanage.service.BirtReportService;
import com.ibm.icu.util.ULocale;

/**
 * Created by rahul on 2/27/2017.
 */
@Service
@Transactional
@Lazy
public class BirtReportServiceImpl extends BirtService implements BirtReportService {

    final static Logger logger = Logger.getLogger(BirtReportServiceImpl.class);

    static void updateDataSet(ElementFactory designFactory, ReportDesignHandle designHandle, String dataSetName) throws SemanticException {
        SlotHandle dataSets = designHandle.getDataSets();
        SlotIterator iter = (SlotIterator) dataSets.iterator();
        while (iter.hasNext()) {
            DesignElementHandle dsHandle = (DesignElementHandle) iter.next();
            if (dsHandle instanceof OdaDataSetHandle && dsHandle.getName().equals(dataSetName)) {
                OdaDataSetHandle datasetHandle = (OdaDataSetHandle) dsHandle;
                logger.info("Changing DataSet/Query: " + dsHandle.getName());
                logger.info("Query Text: " + datasetHandle.getQueryText());
                // add WHERE Clause
                datasetHandle.setQueryText(datasetHandle.getQueryText() + " WHERE ID = 'xxxxx'");
            } else {
                logger.info("dataSet Not Used: " + dsHandle.getName());
            }
        }
    }

    public void generateReport(IReportRunnable report, HttpServletResponse response, HttpServletRequest request) throws Exception {
        IRunAndRenderTask runAndRenderTask = reportEngine.createRunAndRenderTask(report);
        response.setContentType(reportEngine.getMIMEType("html"));
        IRenderOption options = new RenderOption();
        HTMLRenderOption htmlOptions = new HTMLRenderOption(options);
        htmlOptions.setOutputFormat("html");
        htmlOptions.setImageHandler(new HTMLServerImageHandler());
        htmlOptions.setBaseImageURL(request.getContextPath() + IMAGE_FOLDER);
        htmlOptions.setImageDirectory(servletContext.getRealPath(IMAGE_FOLDER));
        runAndRenderTask.setRenderOption(htmlOptions);
        runAndRenderTask.getAppContext().put(EngineConstants.APPCONTEXT_BIRT_VIEWER_HTTPSERVET_REQUEST, request);
        IGetParameterDefinitionTask task = reportEngine.createGetParameterDefinitionTask(report);
        Map<String, Object> params = new HashMap<>();
        for (Object h : task.getParameterDefns(false)) {
            IParameterDefn def = (IParameterDefn) h;
            if (def.getDataType() == IParameterDefn.TYPE_INTEGER) {
                params.put(def.getName(), Integer.parseInt(request.getParameter(def.getName())));
            } else {
                params.put(def.getName(), request.getParameter(def.getName()));
            }
        }
        try {
            htmlOptions.setOutputStream(response.getOutputStream());
            runAndRenderTask.run();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            runAndRenderTask.close();
        }
    }

    @Override
    public void orderSourcesReportOrg(int orgId, ReportFormat format, HttpServletRequest request, HttpServletResponse response) throws EngineException, IOException, DesignFileException, SemanticException {
        ServletContext sc = request.getSession().getServletContext();
        logger.info("orderSourcesReportOrg orgId=" + orgId + " format=" + format.toString());
        updateDataSource(sc.getRealPath(REPORT_DIR) + "/order_source.rptdesign");
        IReportRunnable iReportRunnable = reportEngine.openReportDesign(sc.getRealPath(REPORT_DIR) + "/order_source.rptdesign");
        IRunAndRenderTask runAndRenderTask = reportEngine.createRunAndRenderTask(iReportRunnable);
        renderReport(sc, runAndRenderTask, format, response, request);
        runAndRenderTask.getAppContext().put(EngineConstants.APPCONTEXT_BIRT_VIEWER_HTTPSERVET_REQUEST, request);
        runAndRenderTask.run();
        runAndRenderTask.close();
    }

    @Override
    public void orderSourcesReportRestaurantId(int restaurantId, String format) {

    }

    @Override
    public void orderSourcesReportFFC(int ffcId, String format) {

    }

    private void updateDataSource(String reportName) throws DesignFileException, SemanticException, IOException {
        SessionHandle session = designEngine.newSessionHandle(ULocale.ENGLISH);
        designHandle = session.openDesign(reportName);
        designHandle.getDataSources().add(getDataSource(designHandle.getElementFactory()));
        updateDataSet(designHandle.getElementFactory(), designHandle, "");
        designHandle.saveAs(reportName);
        designHandle.close();
    }

    private void renderReport(ServletContext sc, IRunAndRenderTask runAndRenderTask, ReportFormat format, HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.setContentType(reportEngine.getMIMEType(format.toString()));
        IRenderOption options = new RenderOption();
        switch (format) {
            case DOC:
                break;
            case HTML:
                HTMLRenderOption htmlOptions = new HTMLRenderOption(options);
                htmlOptions.setOutputFormat(format.toString());
                htmlOptions.setOutputStream(response.getOutputStream());
                htmlOptions.setImageHandler(new HTMLServerImageHandler());
                htmlOptions.setBaseImageURL(request.getContextPath() + "/images");
                htmlOptions.setImageDirectory(sc.getRealPath(IMAGE_FOLDER));
                htmlOptions.setEmbeddable(true);
                runAndRenderTask.setRenderOption(htmlOptions);
                break;
            case XLS:
                break;
            case PDF:
                PDFRenderOption pdfOptions = new PDFRenderOption(options);
                pdfOptions.setOutputFormat("pdf");
                pdfOptions.setOption(IPDFRenderOption.PAGE_OVERFLOW, IPDFRenderOption.FIT_TO_PAGE_SIZE);
                pdfOptions.setOutputStream(response.getOutputStream());
                runAndRenderTask.setRenderOption(pdfOptions);
                break;
            case ODT:
                break;
            case ODS:
               /* RenderOption renderOption = new RenderOption();
                options.setOutputFileName("D:/main-workspace/ServletDemo/reports/" + fileName + "." + format);
                options.setSupportedImageFormats("PNG;GIF;JPG;BMP;SWF;SVG");
                // options.setOption(PDFRenderOption.PAGE_OVERFLOW,
                options.setOption(PDFRenderOption.PAGE_OVERFLOW, IPDFRenderOption.FIT_TO_PAGE_SIZE);
                options.setOption(IRenderOption.HTML_PAGINATION, Boolean.FALSE);
                options.setOutputFormat(format);
                task.setRenderOption(options);*/
                break;
            default:
                break;
        }
    }
}
