/**
 * 
 */
package com.emanage.service.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.emanage.controller.CashRegisterController;
import com.emanage.dao.RestaurantDAO;
import com.emanage.domain.CheckDishResponse;
import com.emanage.domain.CreditTransactions;
import com.emanage.domain.Customer;
import com.emanage.domain.Discount_Charges;
import com.emanage.domain.DishType;
import com.emanage.domain.Invoice;
import com.emanage.domain.InvoiceResponse;
import com.emanage.domain.Nutrientes;
import com.emanage.domain.OrderAddOn;
import com.emanage.domain.OrderSource;
import com.emanage.domain.PaymentType;
import com.emanage.domain.Restaurant;
import com.emanage.domain.Vendor;
import com.emanage.dto.OrderStatusDTO;
import com.emanage.enums.check.PaymentMode;
import com.emanage.enums.credit.BilligCycle;
import com.emanage.enums.credit.CreditTransactionStatus;
import com.emanage.enums.credit.CustomerCreditAccountStatus;
import com.emanage.enums.order.Status;
import com.emanage.service.CustomerCreditService;
import com.emanage.service.CustomerService;
import com.emanage.service.DeliveryAreaService;
import com.emanage.service.DishTypeService;
import com.emanage.service.InvoiceService;
import com.emanage.service.RestaurantService;
import com.emanage.service.TaxTypeService;
import com.emanage.service.UserService;
import com.emanage.service.VendorService;
import com.emanage.utility.MailerUtility;
import com.emanage.utility.StringUtility;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * @author  rahul
 *
 */
@Service
@Lazy
public class RestaurantServiceImpl implements RestaurantService {

	final static Logger logger = Logger.getLogger(CashRegisterController.class);

	
	@Autowired
	private RestaurantDAO restaurantDAO;
	
	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@Autowired
	private SpringTemplateEngine templateEngine;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TaxTypeService taxTypeService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private DeliveryAreaService deliveryAreaService;
	
	@Autowired
	private InvoiceService checkService;

	@Autowired
	private VendorService vendorService;
	
//	@Autowired
//	private OrderController orderController;
//	
	@Autowired
	private DishTypeService dishTypeService;
	
	@Autowired
	@Qualifier("customerCreditAutomatedBilling") 
	private CustomerCreditService customerCreditService;
	
	@Override
	@Transactional
	public void addRestaurant(Restaurant restaurant) {
		restaurantDAO.addRestaurant(restaurant);
		if(restaurant.getParentRestaurantId()!=null)
		    userService.createServiceUser(restaurant);
	}

	@Override
	@Transactional
	public List<Restaurant> listRestaurant() {
		return restaurantDAO.listRestaurant();
	}

    @Override
    @Transactional
	public List<Restaurant> listRestaurantById(Integer restaurantId) {
		//return restaurantDAO.listRestaurantById(restaurantId);
		return restaurantDAO.listRestaurantById(restaurantId);
	}

	@Override
	@Transactional
	public void removeRestaurant(Integer id) {
		restaurantDAO.removeRestaurant(id);
	}

    @Override
    @Transactional
	public Restaurant getRestaurant(Integer id) {
		return restaurantDAO.getRestaurant(id);
	}

	@Override
	@Transactional
	public List<Restaurant> listRestaurantByParentId(Integer parentId){
        return restaurantDAO.listRestaurantByParentId(parentId);
    }

	@Override
	@Transactional
	public Restaurant getRestaurantByName(String restaurantName) {
		return restaurantDAO.getRestaurantByName(restaurantName);
	}

	@Override
	@Transactional
	public List<Discount_Charges> listDiscountCharges(Integer restId) {
		// TODO Auto-generated method stub
		return restaurantDAO.listDiscountCharges(restId);
	}

	@Override
	@Transactional
	public void addDC(Discount_Charges discount_Charges) {
		// TODO Auto-generated method stub
		restaurantDAO.addDC(discount_Charges);
	}

	@Override
	@Transactional
	public void removeDC(Integer id) {
		// TODO Auto-generated method stub
		restaurantDAO.removeDC(id);
	}

	@Override
	@Transactional
	public Discount_Charges getDCById(Integer id) {
		// TODO Auto-generated method stub
		return restaurantDAO.getDCById(id);
	}

	@Override
	@Transactional
	public List<Nutrientes> getNutirentList(Integer restId) {
		// TODO Auto-generated method stub
		return restaurantDAO.getNutirentList(restId);
	}

	@Override
	@Transactional
	public void removeNutrientes(Integer id) {
		// TODO Auto-generated method stub
		restaurantDAO.removeNutrientes(id);

    }

	@Override
	@Transactional
	public void addNutrientes(Nutrientes nutrientes) {
		// TODO Auto-generated method stub
		DishType dishType= dishTypeService.getDishType(nutrientes.getDishTypeId());
		nutrientes.setDishType(dishType.getName());
		restaurantDAO.addNutrientes(nutrientes);
	}

	@Override
	@Transactional
	public Nutrientes getNutrientes(Integer id) {
		// TODO Auto-generated method stub
		return restaurantDAO.getNutrientes(id);
	}

	@Override
	@Transactional
	public Nutrientes getByNutrientesByNameType(String name, String dishType,
			Integer restId) {
		// TODO Auto-generated method stub
		return restaurantDAO.getByNutrientesByNameType(name, dishType, restId);
	}

	@Override
	@Transactional
	public void addOrderSource(OrderSource orderSource) {
		// TODO Auto-generated method stub
		restaurantDAO.addOrderSource(orderSource);
	}

	@Override
	@Transactional
	public List<OrderSource> listOrderSourcesByOrgId(Integer orgId) {
		// TODO Auto-generated method stub
		return restaurantDAO.listOrderSourcesByOrgId(orgId);
	}

	@Override
	@Transactional
	public void removeOrderSources(Integer id) {
		// TODO Auto-generated method stub
		restaurantDAO.removeOrderSources(id);
	}

	@Override
	@Transactional
	public OrderSource getOrderSources(Integer id) {
		// TODO Auto-generated method stub
		return restaurantDAO.getOrderSources(id);
	}

	@Override
	@Transactional
	public void addPaymentType(PaymentType paymentType) {
		// TODO Auto-generated method stub
		restaurantDAO.addPaymentType(paymentType);
	}

	@Override
	@Transactional
	public List<PaymentType> listPaymentTypeByOrgId(Integer orgId) {
		// TODO Auto-generated method stub
		return restaurantDAO.listPaymentTypeByOrgId(orgId);
	}

	@Override
	@Transactional
	public void removePaymentType(Integer id) {
		// TODO Auto-generated method stub
		restaurantDAO.removePaymentType(id);
	}

	@Override
	@Transactional
	public PaymentType getPaymentType(Integer id) {
		return restaurantDAO.getPaymentType(id);
	}

    @Override
    @Transactional
	public PaymentType getPaymentTypeByName(Integer restaurantId, String name) {
		try{
		return restaurantDAO.getPaymentTypeByName(name, getParentRestaurant(restaurantId).getRestaurantId());
		}catch(NullPointerException nExp){
			logger.info("NE, Could not get Parent Restaurant of Restaurant id="+restaurantId);
		}
		catch (Exception e) {
			logger.info("Could not get Parent Restaurant of Restaurant id="+restaurantId);
		}
		return null;
    }

	@Override
	@Transactional
	public Restaurant getParentRestaurant(int restaurantId) {
		Restaurant restaurant=getRestaurant(restaurantId);
		if(restaurant.getParentRestaurantId()!=null && restaurantId!=restaurant.getParentRestaurantId())
		{
			return getRestaurant(restaurant.getParentRestaurantId());
		}
		return restaurant;
	}

	@Override
	@Transactional
	public List<Integer> getAllOrganisation() {
		logger.info("Getting list organisation list");
		return restaurantDAO.getAllOrganisation();
	}

	@Override
	public String getRestaurantUnitInfoForAssociatedCustomer(int customerId, String property) {
		return restaurantDAO.getRestaurantUnitInfoForAssociatedCustomer(customerId,property);
	}

	@Override
	public OrderStatusDTO checkPaytmOrderStatus(Integer orderId) throws IOException {
		JsonObject json = new JsonObject();
		OrderStatusDTO paymentStatus=  new OrderStatusDTO();
		if(orderId!=null){
			String propFileName = "paytmDetails.properties";
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			Properties prop = new Properties();
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			 String mid = prop.getProperty("MID");
			 String statusURL=prop.getProperty("statusURL");
			 json.addProperty("MID",mid);
			 json.addProperty("ORDERID",orderId.toString());
			 String url = statusURL+json.toString();
			 String result = callURL(url,null);
			 return parseNSavePaytm(result);
		}else{
			paymentStatus.responseMsz = "Invalid orderId";
		  return paymentStatus;
		}
	}
	
	@Override
	public OrderStatusDTO checkCitrusOrderStatus(Integer orderId) throws IOException {
		OrderStatusDTO paymentStatus=  new OrderStatusDTO();
		if(orderId!=null){
			String propFileName = "citrusDetails.properties";
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			Properties prop = new Properties();
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			 String statusURL=prop.getProperty("statusAPI");
			 String access_key = prop.getProperty("access_key");
			 String url = statusURL+orderId;
			 String result = null;
			try {
				result = callURL(url,access_key);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONObject jsonObject = null;
			try {
				 jsonObject = XML.toJSONObject(result);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 return parseNSaveCitrus(jsonObject.toString());
		}else{
			paymentStatus.responseMsz = "Invalid orderId";
		  return paymentStatus;
		}
	}

	public OrderStatusDTO parseNSaveCitrus(String jsonLine) {
		
		OrderStatusDTO paymentStatus=  new OrderStatusDTO();
		     JsonElement jelement = new JsonParser().parse(jsonLine);
		     JsonObject  jobjectReal = jelement.getAsJsonObject();
		     JsonObject txnEnquiryResponse = jobjectReal.getAsJsonObject("txnEnquiryResponse");
		     JsonObject  enquiryResponse = txnEnquiryResponse.getAsJsonObject("enquiryResponse");
		     JsonObject  jobObject = enquiryResponse.getAsJsonObject();
		     logger.info("Josn Object: "+jobObject);
		     JsonElement amount = jobObject.get("amount");
		     JsonElement respCode  = jobObject.get("respCode");
		     JsonElement message = jobObject.get("respMsg");
		     JsonElement checkId = jobObject.get("merchantTxnId");
		     if(checkId!=null){
			     paymentStatus.checkId = checkId.getAsString();
				 paymentStatus.orderAmount = amount.getAsString();
				 
				 String status = resolveCitrusResponseCode(respCode.getAsString());
				 
				 logger.info("Other status: : "+status);
				 if("0".equalsIgnoreCase(respCode.getAsString()) || "14".equalsIgnoreCase(respCode.getAsString())){
					 paymentStatus.orderStatus = "SUCCESS";
					 if(checkId!=null){
					    Invoice check = checkService.getInvoice(Integer.parseInt(checkId.getAsString()));
					    logger.info("Success :"+check.getInvoiceId());
					    if(check !=null && !("SUCCESS".equalsIgnoreCase(check.getTransactionStatus()))){
					    	check.setTransactionStatus("SUCCESS");
					    	check.setResponseCode(respCode.getAsString());
					    	check.setStatus(com.emanage.enums.check.Status.Paid);
							check.getOrders().get(0).setPaymentStatus(PaymentMode.PG.toString());
					    	checkService.addInvoice(check);
					    }
					 }
				 }/*else if("3".equalsIgnoreCase(respCode.getAsString()) || 
						  "2".equalsIgnoreCase(respCode.getAsString()) ||
						  "5".equalsIgnoreCase(respCode.getAsString()) || 
						  "7".equalsIgnoreCase(respCode.getAsString()) ||
						  "4".equalsIgnoreCase(respCode.getAsString()) ||
						  "1".equalsIgnoreCase(respCode.getAsString())){
					 Check check = checkService.getCheck(Integer.parseInt(checkId.getAsString()));
					 if(check !=null){
						 check.setTransactionStatus(status);
					     check.setResponseCode(respCode.getAsString());
					     check = cancelInvoice(check);
					     checkService.addCheck(check);
						 paymentStatus.orderStatus = status;
					 }
				}*/else{
					Invoice check = checkService.getInvoice(Integer.parseInt(checkId.getAsString()));
					 if(check !=null){
						 check.setTransactionStatus(status);
					     check.setResponseCode(respCode.getAsString());
					     checkService.addInvoice(check);
						 paymentStatus.orderStatus =status;
					 }
				}
				paymentStatus.responseMsz =message.getAsString();
		     }else{
		    	 paymentStatus.responseMsz="Can't fine any order with this checkId.";
		    	 paymentStatus.orderStatus="Error";
		     }
		    return paymentStatus;
		}
	
	@Override
	public String resolveCitrusResponseCode(String pgRespCode){
		String status="";
		if("0".equalsIgnoreCase(pgRespCode)){
			 status = "SUCCESS";
		 } else if("1".equalsIgnoreCase(pgRespCode)){
			 status = "FAILED";
		 }else if("2".equalsIgnoreCase(pgRespCode)){
			 status = "User Dropped";
		 }else if("3".equalsIgnoreCase(pgRespCode)){
			 status = "CANCELED";
		 }else if("4".equalsIgnoreCase(pgRespCode)){
			 status = "FORWARDED";
		 }else if("5".equalsIgnoreCase(pgRespCode)){
			 status ="PG_FORWARD_FAIL";
		 }else if("7".equalsIgnoreCase(pgRespCode)){
			 status = "SESSION_EXPIRED";
		 }else if("14".equalsIgnoreCase(pgRespCode)){
			 status = "SUCCESS_ON_VERIFICATION";
		 }else if("16".equalsIgnoreCase(pgRespCode)){
			 status = "REVERSED";
		 }else if("24".equalsIgnoreCase(pgRespCode)){
			 status = "User Request arrived";
		 }else if("25".equalsIgnoreCase(pgRespCode)){
			 status = "Checkout page rendered";
		 }else{
			 status = "Pending";
		 }
		return status;
	}
	public Invoice cancelInvoice(Invoice check){
//		try {
//			orderController.restoreStock(check,0);
//		} catch (ParseException e) {
//			e.printStackTrace();
//			//emailException(ExceptionUtils.getStackTrace(e),request);
//			logger.info("Exception mail sent");
//		}
		check.setBill(0);
		check.setRoundOffTotal(0);
		CreditTransactions ct = customerCreditService.getLastPendingTransaction(check.getCustomerId());
		if(ct!=null){
			if(ct.getStatus()==CreditTransactionStatus.PENDING){
			try {
				customerCreditService.updateBillRecoveryTransaction("FAILED",check.getCustomerId(), check.getCreditBalance(),"CREDIT", "Setting status failed of existing pending transaction");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				///restaurantService.emailException(ExceptionUtils.getStackTrace(e),request);
				logger.info("Exception mail sent");
			}
		}
		}
		check.setCreditBalance(0);
		//check.setOutCircleDeliveryCharges(0);
		check.setStatus(com.emanage.enums.check.Status.Cancel);
		check.getOrders().get(0).setStatus(com.emanage.enums.order.Status.CANCELLED);
		logger.info("This order has been cancelled :  InvoiceId"+ check.getInvoiceId() );
		return check;
	}
	
    public OrderStatusDTO parseNSavePaytm(String jsonLine) {
    	 OrderStatusDTO paymentStatus=  new OrderStatusDTO();
	     JsonElement jelement = new JsonParser().parse(jsonLine);
	     JsonObject  jobject = jelement.getAsJsonObject();
	     JsonElement amount = jobject.get("TXNAMOUNT");
	     JsonElement status  = jobject.get("STATUS");
	     JsonElement message = jobject.get("RESPMSG");
	     JsonElement checkId = jobject.get("ORDERID");
	     JsonElement responseCode = jobject.get("RESPCODE");
	     paymentStatus.checkId = checkId.getAsString();
		 paymentStatus.orderAmount = amount.getAsString();
		 if("TXN_SUCCESS".equalsIgnoreCase(status.getAsString())){
			 paymentStatus.orderStatus = status.getAsString();
			 if(checkId!=null){
				 Invoice check = checkService.getInvoice(Integer.parseInt(checkId.getAsString()));
			    if(check !=null && !("TXN_SUCCESS".equalsIgnoreCase(check.getTransactionStatus()))){
			    	check.setTransactionStatus(status.getAsString());
			    	check.setResponseCode(responseCode.getAsString());
			    	check.setStatus(com.emanage.enums.check.Status.Paid);
					check.getOrders().get(0).setPaymentStatus(PaymentMode.PAYTM.toString());
			    	checkService.addInvoice(check);
			    }
			 }
		 }else {
			 Invoice check = checkService.getInvoice(Integer.parseInt(checkId.getAsString()));
			 if(check !=null){
				 check.setTransactionStatus(status.getAsString());
			     check.setResponseCode(responseCode.getAsString());
			     checkService.addInvoice(check);
				 paymentStatus.orderStatus = status.getAsString();
			 }
			 }
		 paymentStatus.responseMsz =message.getAsString();
	    return paymentStatus;
	}
    
    public static String callURL(String myURL,String access_keyCitrus) {
        StringBuilder sb = new StringBuilder();
        URLConnection urlConn = null;
        InputStreamReader in = null;
        try {
            URL url = new URL(myURL);
            urlConn = url.openConnection();
            if(access_keyCitrus!=null){
            	urlConn.setRequestProperty("access_key",access_keyCitrus);
            	urlConn.setRequestProperty("Content-Type","application/json");
            }
            if (urlConn != null)
                urlConn.setReadTimeout(60 * 1000);
            if (urlConn != null && urlConn.getInputStream() != null) {
                in = new InputStreamReader(urlConn.getInputStream(),
                        Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(in);
                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        sb.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            in.close();
        } catch (Exception e) {
            throw new RuntimeException("Exception while calling URL:" + myURL, e);
        }

        return sb.toString();
    }
    
    
    public void emailException(String exceptionLogs, HttpServletRequest request) throws MessagingException, UnsupportedEncodingException{
    	
    	String resultPath;
    	if(request!=null){
	    	//String scheme = request.getScheme();
			String serverName = request.getServerName();
			resultPath =serverName;
    	}else{
    		resultPath  = "Exception Logs";
    	}
    	
    	MailerUtility mailUtil =  new MailerUtility();
	    final Context ctx = new Context();
	    String subject = resultPath+" : "+"Exception Alert !";
	    ctx.setVariable("exceptionLogs", exceptionLogs);
        logger.info("***********************serverName sending exception***********************************");
		
	    final MimeMessage mimeMessage = mailSender.createMimeMessage();
	    final MimeMessageHelper message =
	        new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

	        message.setSubject(subject);
	        String senderEmail = mailUtil.username;
	    	InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail,"Exception Express!");
	    	message.setFrom(restaurantEmailAddress);
	    	message.setReplyTo(restaurantEmailAddress);
	    
	    	if("www. us.in".equalsIgnoreCase(resultPath)){
	    		message.setTo("");
	    		message.addCc("");
	    		message.addCc("");
	    		message.addCc("");
	    		message.addCc("");
	    	}else{
	    		message.setTo("errahulsharma.cs@gmail.com");
//	    		message.addCc("");
	    	}

	    final String htmlContent = templateEngine.process("exceptionExpress", ctx);
	    message.setText(htmlContent, true); 
	    String oldUsername = null;
	    String oldPassword = null;
	    String oldHost = null;
	    String oldProtocol = null;
	    Integer oldPort = -1;
	    if (!StringUtility.isNullOrEmpty(mailUtil.username) && !StringUtility.isNullOrEmpty(mailUtil.password)) {
            oldUsername = mailSender.getUsername();
            oldPassword = mailSender.getPassword();
	    	oldHost = mailSender.getHost();
	    	oldProtocol = mailSender.getProtocol();
	    	oldPort = mailSender.getPort();
            mailSender.setUsername(oldUsername);
            mailSender.setPassword(oldPassword);
	    	mailSender.setHost(oldHost);
	    	mailSender.setProtocol(oldProtocol);
	    	mailSender.setPort(oldPort);
	    }

	    mailSender.send(mimeMessage);
	    logger.info("***********************sending end***********************************");
    }
  
    /*
     * 
     * */
   
   public void alertMail(String emailMessage,String subject,HttpServletRequest request) throws MessagingException, UnsupportedEncodingException{
    	
    	MailerUtility mailUtil =  new MailerUtility();
	    final Context ctx = new Context();
	    //String subject = resultPath+" : "+"Exception Alert !";
	    ctx.setVariable("exceptionLogs", emailMessage);
        logger.info("***********************serverName sending exception***********************************");
		
	    final MimeMessage mimeMessage = mailSender.createMimeMessage();
	    final MimeMessageHelper message =
	        new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

	    message.setSubject(subject);
	    String senderEmail = mailUtil.username;
	    	InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail,subject);
	    	message.setFrom(restaurantEmailAddress);
	    	message.setReplyTo(restaurantEmailAddress);
    		message.setTo("errahulsharma.cs@gmail.com");
    		//message.setTo("");
	    	

	    final String htmlContent = templateEngine.process("exceptionExpress", ctx);
	    message.setText(htmlContent, true); 
	    String oldUsername = null;
	    String oldPassword = null;
	    String oldHost = null;
	    String oldProtocol = null;
	    Integer oldPort = -1;
	    if (!StringUtility.isNullOrEmpty(mailUtil.username) && !StringUtility.isNullOrEmpty(mailUtil.password)) {
            oldUsername = mailSender.getUsername();
            oldPassword = mailSender.getPassword();
	    	oldHost = mailSender.getHost();
	    	oldProtocol = mailSender.getProtocol();
	    	oldPort = mailSender.getPort();
        mailSender.setUsername(oldUsername);
        mailSender.setPassword(oldPassword);
	    	mailSender.setHost(oldHost);
	    	mailSender.setProtocol(oldProtocol);
	    	mailSender.setPort(oldPort);
	    }

	    mailSender.send(mimeMessage);
	    logger.info("***********************sending end***********************************");
    }

   
    public void callBackRequest(String emailMessage,String subject,HttpServletRequest request) throws MessagingException, UnsupportedEncodingException{
     	
     	MailerUtility mailUtil =  new MailerUtility();
 	    final Context ctx = new Context();
 	    //String subject = resultPath+" : "+"Exception Alert !";
 	    ctx.setVariable("exceptionLogs", emailMessage);
         logger.info("***********************serverName sending request call back***********************************");
 		
 	    final MimeMessage mimeMessage = mailSender.createMimeMessage();
 	    final MimeMessageHelper message =
 	        new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

 	    message.setSubject(subject);
 	    String senderEmail = mailUtil.username;
 	    	InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail,subject);
 	    	message.setFrom(restaurantEmailAddress);
 	    	message.setReplyTo(restaurantEmailAddress);
     	
     	 	message.setTo("contact@ us.in");
     	 	message.setCc("errahulsharma.cs@gmail.com");
     	 	
 	    final String htmlContent = templateEngine.process("exceptionExpress", ctx);
 	    message.setText(htmlContent, true); 
 	    String oldUsername = null;
 	    String oldPassword = null;
 	    String oldHost = null;
 	    String oldProtocol = null;
 	    Integer oldPort = -1;
 	    if (!StringUtility.isNullOrEmpty(mailUtil.username) && !StringUtility.isNullOrEmpty(mailUtil.password)) {
             oldUsername = mailSender.getUsername();
             oldPassword = mailSender.getPassword();
 	    	oldHost = mailSender.getHost();
 	    	oldProtocol = mailSender.getProtocol();
 	    	oldPort = mailSender.getPort();
         mailSender.setUsername(oldUsername);
         mailSender.setPassword(oldPassword);
 	    	mailSender.setHost(oldHost);
 	    	mailSender.setProtocol(oldProtocol);
 	    	mailSender.setPort(oldPort);
 	    }

 	    mailSender.send(mimeMessage);
 	    logger.info("***********************sending end***********************************");
     }
@Override
public void updateNutrientsByDishType(Integer dishID, String dishTypeName) {
	// TODO Auto-generated method stub
	restaurantDAO.updateNutrientsByDishType(dishID, dishTypeName);
}



public String emailCheckFromServer(HttpServletRequest request,Invoice check, String emailAddr, Restaurant rest,String templateName, String subject, String sender, String refund, String reason, float extraAmount) throws MessagingException, UnsupportedEncodingException{
	Customer customer = customerService.getCustomer(check.getCustomerId());
	emailCheckFromServerLocal(request, customer, check,  emailAddr,  rest, templateName,  subject,  sender,  refund,  reason,  extraAmount);
	return null;
}

public void emailCheckFromServerLocal(HttpServletRequest request,Customer customer,Invoice invoice, String emailAddr, Restaurant rest,String templateName, String subject, String sender, String refund, String reason, float extraAmount) throws MessagingException, UnsupportedEncodingException{

	// Prepare the evaluation context
	if(emailAddr!=null && (!"".equalsIgnoreCase(emailAddr))){
	    final Context ctx = new Context();
        logger.info("invoiceId : "+invoice.getInvoiceNo());
        Vendor vendor = vendorService.getVendor(invoice.getVendorId());
        logger.info("***********************sending check from default***********************************");
		if (invoice != null) {

			Float waiveOff =null;
			InvoiceResponse checkResponse = new InvoiceResponse(invoice,taxTypeService,waiveOff,rest);

			templateName="defaultemailbill";
			subject=rest.getBussinessName()+" : Receipt";
			
			ctx.setVariable("checkRespone", checkResponse);
			if (invoice.getCustomerId() > 0) {
				String description="";
				if(customer.getCredit()!=null){
					if(customer.getCredit().getCreditBalance()!=0){
					  description  = setDescription(invoice.getOrders().get(0).getStatus(),customer,invoice,refund,reason,extraAmount, rest);
					}else if(invoice.getOrders().get(0).getStatus()==Status.CANCELLED){
						description="Thanks for choosing  us";
					}
				}else{
					if(invoice.getOrders().get(0).getStatus()!=Status.CANCELLED){
						description = "Thanks for choosing  us. While we're busy whipping up your order, here is your order summary for order no."+invoice.getOrderId();
					}else if(invoice.getOrders().get(0).getStatus()==Status.CANCELLED){
						description="Thanks for choosing us";
					}
				}
				
				ctx.setVariable("description", description);
				ctx.setVariable("customer", customer);
			} 
			if(rest!=null){
				if(rest.isRoundOffAmount()){
					invoice.setRoundOffTotal(Math.round(checkResponse.getRoundedOffTotal()));
					checkResponse.setRoundedOffTotal(Math.round(checkResponse.getRoundedOffTotal()));
				}else{
					invoice.setRoundOffTotal(checkResponse.getRoundedOffTotal());
				}
				}
			ctx.setVariable("user", rest);
			ctx.setVariable("vendor", vendor);
			
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
			cal.setTime(invoice.getOpenTime());
			DateFormat formatter1;
			formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			formatter1.setTimeZone(cal.getTimeZone());
			ctx.setVariable("checkDate", formatter1.format(cal.getTime()));
			Map<String, com.emanage.domain.JsonDish> itemsMap = new TreeMap<String, com.emanage.domain.JsonDish>();
			List<CheckDishResponse> items = checkResponse.getItems();

			List<com.emanage.domain.JsonAddOn> jsonAdd =  new ArrayList<com.emanage.domain.JsonAddOn>();
			for (CheckDishResponse item : items) {
				if (itemsMap.containsKey(item.getDishId()+""+item.getDishSize().replaceAll("\\s",""))) {
					com.emanage.domain.JsonDish jsonDish = itemsMap.get(item.getDishId()+""+item.getDishSize().replaceAll("\\s",""));
					jsonDish.setPrice(jsonDish.getPrice() + item.getPrice());
					jsonDish.setQuantity(jsonDish.getQuantity() + 1);
				}
				else {
					com.emanage.domain.JsonDish jsonDish = new com.emanage.domain.JsonDish();
					jsonDish.setQuantity(1);
					jsonDish.setName(item.getName());
					jsonDish.setId(item.getDishId());
					jsonDish.setPrice(item.getPrice());
					//jsonDish.setDishSize(item.getDishSize());
					System.out.println("seding section data");
					//System.out.println(item.getSections().length);
					if(item.getSections()!=null) {
					jsonDish.setSection(Arrays.asList(item.getSections()));
					}
					List<OrderAddOn> orderAddOn = item.getAddOnresponse();
					if(orderAddOn!=null){
					for( OrderAddOn oad : orderAddOn){
						com.emanage.domain.JsonAddOn jsonAddOn = new com.emanage.domain.JsonAddOn();
						jsonAddOn.setItemId(oad.getAddOnId());
						jsonAddOn.setDishId(item.getDishId());
						jsonAddOn.setName(oad.getName());
						jsonAddOn.setQuantity(oad.getQuantity());
						jsonAddOn.setPrice(oad.getPrice());
						jsonAddOn.setDishSize(oad.getDishSize());
						jsonAdd.add(jsonAddOn);
						jsonDish.setAddOns(jsonAdd);
					}
				}
					itemsMap.put(item.getDishId()+""+item.getDishSize().replaceAll("\\s",""), jsonDish);
				}
			}

			ctx.setVariable("itemsMap", itemsMap);
		} else {
			logger.info("No check Found for "+emailAddr);
		}
		//Vendor vendor= vendorService.getVendor(invoice.getVendorId());
	    // Prepare message using a Spring helper
	    final MimeMessage mimeMessage = mailSender.createMimeMessage();
	    final MimeMessageHelper message =
	        new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

	    message.setSubject(subject);
	    if (rest != null) {
	    	//String senderEmail = StringUtility.isNullOrEmpty(rest.getMailUsername()) ? user.getUsername() : rest.getMailUsername();
	    	String senderEmail=rest.getMailUsername();
	    	InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
	    	message.setFrom(restaurantEmailAddress);
	    	message.setReplyTo(restaurantEmailAddress);
	    }
	    message.setTo(emailAddr);
	    if(vendor!=null && vendor.getAlertMail()!=null) {
	    		message.setBcc(vendor.getAlertMail());
	    }
	    // Create the HTML body using Thymeleaf

	    final String htmlContent = templateEngine.process(templateName, ctx);
	    message.setText(htmlContent, true); // true = isHtml
	    String oldUsername = null;
	    String oldPassword = null;
	    String oldHost = null;
	    String oldProtocol = null;
	    Integer oldPort = -1;
	    if (!StringUtility.isNullOrEmpty(rest.getMailUsername()) && !StringUtility.isNullOrEmpty(rest.getMailPassword())) {
            oldUsername = mailSender.getUsername();
            oldPassword = mailSender.getPassword();
	    	oldHost = mailSender.getHost();
	    	oldProtocol = mailSender.getProtocol();
	    	oldPort = mailSender.getPort();
	    	if(sender!=null){
                mailSender.setUsername(MailerUtility.username);
                mailSender.setPassword(MailerUtility.password);
            }else{
	    	mailSender.setUsername(rest.getMailUsername());
	    	mailSender.setPassword(rest.getMailPassword());
	    	}
	    	mailSender.setHost(rest.getMailHost());
	    	mailSender.setProtocol(rest.getMailProtocol());
	    	mailSender.setPort(rest.getMailPort());
	    }
//	    System.out.println(":"+rest.getMailUsername());
//	    System.out.println(":"+rest.getMailPassword());
//	    System.out.println(":"+rest.getMailHost());
//	    System.out.println(":"+rest.getMailPort());

	    mailSender.send(mimeMessage);
        logger.info("email sent for checkId :" + invoice.getInvoiceNo());
	    if (!StringUtility.isNullOrEmpty(oldUsername) && !StringUtility.isNullOrEmpty(oldPassword)) {
	    	mailSender.setUsername(oldUsername);
	    	mailSender.setPassword(oldPassword);
	    	mailSender.setHost(oldHost);
	    	mailSender.setProtocol(oldProtocol);
	    	mailSender.setPort(oldPort);
	    }
	    logger.info("***********************Email Sent Successfully sending end***********************************");
	}
	 
}

String setDescription(Status status,Customer customer,Invoice check,String refund,String reason,float extraAmount, Restaurant restaurant) {

    if (customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE && customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
     //   Restaurant restaurant = getRestaurant(check.getRestaurantId());
        if(status==Status.CANCELLED && "CREDIT".equalsIgnoreCase(refund)){
    		return "Your order no."+check.getOrderId()+" (of  Total Rs."+check.getRoundOffTotal()+") has been cancelled. We owe you  Rs "+Math.abs(customer.getCredit().getCreditBalance())+","
    				+ "which will be automatically deducted from your next order amount."
    				+ "Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";
    	}else if(status==Status.CANCELLED && "CASH".equalsIgnoreCase(refund)){
    		return "Your order with order No. "+check.getOrderId()+" has been cancelled. We have initiated the refund as CASH. One of our delivery boy will hand over it to you soon";
    	}else if("COMPLETED".equalsIgnoreCase(refund)&& "DELIVERED".equalsIgnoreCase(reason)){
    		return "Your order no."+check.getOrderId()+" (of  Total Rs."+check.getRoundOffTotal()+") has been DELIVERED. Your current balance with "+restaurant.getRestaurantName()+" is "+Math.abs(customer.getCredit().getCreditBalance())+"."
    				+"Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";
    	}else if("UPDATE".equalsIgnoreCase(refund) && "DISCOUNT".equalsIgnoreCase(reason)){
    		return "Your order 	no."+check.getOrderId()+" has been discounted by Rs "+extraAmount+". We owe you  Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
    				+ "which will be automatically deducted from your next order amount. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";
    	}else if("UPDATE".equalsIgnoreCase(refund) && "ADDED".equalsIgnoreCase(reason)){
    		return "Your order no."+check.getOrderId()+" (item of Rs "+extraAmount+" has been cancelled). We owe you  Rs "+Math.abs(customer.getCredit().getCreditBalance())+","
    				+ "which will be automatically deducted to your next order amount. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";
    	}else if("UPDATE".equalsIgnoreCase(refund) && "CHARGED".equalsIgnoreCase(reason)){
    		return "Your order no."+check.getOrderId()+" has been updated (item of Rs "+extraAmount+" has been added). You owe us  Rs "+Math.abs(customer.getCredit().getCreditBalance())+""
    				+ ",which will be automatically added to your next order amount. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";
    	}else if("PAYMENT_TYPE_CHANGED".equalsIgnoreCase(reason) && refund!=null){
    		if(customer.getCredit().getCreditBalance()<0){
    			return "Payment type for order no."+check.getOrderId()+" has been changed to "+check.getOrders().get(0).getPaymentStatus()+" from "+refund+". We owe you Rs "+Math.abs(customer.getCredit().getCreditBalance())+","
	    				+ "which will be automatically deducted from your next order amount. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";
    		}else if(customer.getCredit().getCreditBalance()>0){
    			return "Payment type for order no. "+check.getOrderId()+" has been changed to "+check.getOrders().get(0).getPaymentStatus()+" from "+refund+". You owe us Rs "+Math.abs(customer.getCredit().getCreditBalance())+","
	    				+ "which will be automatically added to your next order amount.We request you to make this payment at the earliest. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";
    		}else{
    			return "Payment type for order no. "+check.getOrderId()+" has been changed to "+check.getOrders().get(0).getPaymentStatus()+" from "+refund+".You owe us Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
    					+ "Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always";

    		}
  		}
		else {
			return "Thanks for choosing us. While our kitchen is busy whipping up your order, here is your order summary for order no."+check.getOrderId();
    	}
}else {
	//Restaurant restaurant =  getRestaurant(check.getRestaurantId());
	if(customer.getCredit().getStatus()==CustomerCreditAccountStatus.ACTIVE && customer.getCredit().getCreditType().getBillingCycle()!=BilligCycle.ONE_OFF){
		if(status==Status.CANCELLED){
			if(check.getStatus()==com.emanage.enums.check.Status.Paid){
				return "Your order No. "+check.getOrderId()+" (of  Total Rs."+check.getRoundOffTotal()+") has been cancelled. This amount has been adjusted to your current credit balance. Now you owe us Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
    				+ "Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuring you of our best, always.";
			}else{
				return "";
			}
			}else{
			return "Thanks for choosing us. While our kitchen is busy whipping up your order, here is your order summary for order no."+check.getOrderId();
		}
	}else{
		return "Thanks for choosing us. While our kitchen is busy whipping up your order, here is your order summary for order no."+check.getOrderId();
	}
	}
 }

@Override
public String emailCheckFromServerNew(HttpServletRequest request, Invoice check, String emailAddr, Restaurant rest,
		String templateName, String subject, String sender, String refund, String reason, float extraAmount)
		throws MessagingException, UnsupportedEncodingException {
	// TODO Auto-generated method stub
	
	Customer customer = customerService.getCustomer(check.getCustomerId());
	emailCheckFromServerLocal(request, customer, check,  emailAddr,  rest, templateName,  subject,  sender,  refund,  reason,  extraAmount);
	
	return null;
}

}
