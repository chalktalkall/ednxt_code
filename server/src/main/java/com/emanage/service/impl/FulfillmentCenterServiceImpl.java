package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.FulfillmentCenterDAO;
import com.emanage.domain.FulfillmentCenter;
import com.emanage.service.FulfillmentCenterService;

@Service
@Lazy
public class FulfillmentCenterServiceImpl  implements FulfillmentCenterService{

	@Autowired
	FulfillmentCenterDAO kitchenServiceDao;
	
	@Override
	@Transactional
	public void addKitchenScreen(FulfillmentCenter kitchenScreen) {
		kitchenServiceDao.addKitchenScreen(kitchenScreen);	
	}

	@Override
	@Transactional
	public void removeKitchenScreen(int id) {
		// TODO Auto-generated method stub
		kitchenServiceDao.removeKitchenScreen(id);
	}

	@Override
	@Transactional
	public List<FulfillmentCenter> getKitchenScreens(int restaurantId) {
		return kitchenServiceDao.getKitchenScreens(restaurantId);
	}

	@Override
	@Transactional
	public FulfillmentCenter getKitchenScreen(int id) {
		// TODO Auto-generated method stub
		return kitchenServiceDao.getKitchenScreen(id);
	}

	
}
