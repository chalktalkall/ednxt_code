package com.emanage.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emanage.dao.CanvasLMSDAO;
import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.Coupon;
import com.emanage.domain.CuisineType;
import com.emanage.domain.Customer;
import com.emanage.domain.DBConfig;
import com.emanage.domain.Invoice;
import com.emanage.domain.Vendor;
import com.emanage.dto.DataSourceDTO;
import com.emanage.enums.DataSourceType;
import com.emanage.enums.Status;
import com.emanage.service.CouponService;
import com.emanage.service.CuisineNDishService;
import com.emanage.service.CustomerService;
import com.emanage.service.SQLDataSource;
import com.emanage.service.VendorDBConfigService;
import com.emanage.service.VendorService;
import com.emanage.utility.DateUtil;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

@Service
public class SQLDataSourceImpl implements SQLDataSource {

	@Autowired
	CustomerService customerService;
	
	@Autowired
	CanvasLMSDAO canvasLMSDAO;
	
	@Autowired
	VendorService vendorService;
	
	@Autowired
	CuisineNDishService cusineNDishTypeService;
	
	@Autowired
	CouponService couponService;
	
	@Autowired
	private VendorDBConfigService vendorDBConfigService;
	
	
	@Override
	public List<Canvas_Courses> getMSSQLViewData(DataSourceDTO dataSource,String viewName,Vendor vendor) {

		  SQLServerDataSource ds = new SQLServerDataSource();
	        ds.setUser(dataSource.getUser());
	        ds.setPassword(dataSource.getPassword());
	        ds.setServerName(dataSource.getDb_url());
	        ds.setPortNumber(dataSource.getPort());
	        ds.setDatabaseName(dataSource.getDbName());
	        
	        try (Connection con = ds.getConnection();Statement stmt = con.createStatement();) {
	        	ResultSet rs =  stmt.executeQuery("SELECT * FROM "+viewName);
	        	 while (rs.next()) {
	        		 addNParseCourse(rs, vendor);
		            }
	        	 con.close();
	        	return null;
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        
		return null;
	}
	
	@Override
	public Canvas_Courses getViewDataCourse(Vendor vendor,Integer courseId) {
		
		DBConfig dataSource = vendorDBConfigService.getCampusDBConfig(vendor.getVendorId());
		if (dataSource.getDbType() == DataSourceType.MSSQL) {
			DataSourceDTO sourceDTO = new DataSourceDTO();
			sourceDTO.setDb_url(dataSource.getDbUrl());
			sourceDTO.setUser(dataSource.getUsername());
			sourceDTO.setPassword(dataSource.getPassword());
			sourceDTO.setPort(dataSource.getDbPort());
			sourceDTO.setDbName("");
			return getMSSQLViewDataCourse(sourceDTO, dataSource.getDbView(),vendor,courseId);
		}
		return null;
		
	}
	

	public Canvas_Courses getMSSQLViewDataCourse(DataSourceDTO dataSource,String viewName,Vendor vendor,Integer courseId) {

		  SQLServerDataSource ds = new SQLServerDataSource();
	        ds.setUser(dataSource.getUser());
	        ds.setPassword(dataSource.getPassword());
	        ds.setServerName(dataSource.getDb_url());
	        ds.setPortNumber(dataSource.getPort());
	        ds.setDatabaseName(dataSource.getDbName());
	        
	        try (Connection con = ds.getConnection();Statement stmt = con.createStatement();) {
	        	ResultSet rs =  stmt.executeQuery("SELECT * FROM "+viewName+" where id="+courseId);
	        	 while (rs.next()) {
	        		return  parseCourse(rs, vendor);
		            }
	        	 con.close();
	        	return null;
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        
		return null;
	}
	
	
	private Canvas_Courses parseCourse(ResultSet resultSet,Vendor vendor) {
		Canvas_Courses course =  null;
		
		try {
			
			if(resultSet!=null && resultSet.getInt("status")==1) {
				course=new Canvas_Courses();
				if(resultSet.getInt("status")==1)
					course.setStatus(Status.ACTIVE);
				if(resultSet.getString("course_code")!=null)
					course.setCourse_code(resultSet.getString("course_code"));
				if(resultSet.getInt("id")>-1)
					course.setId(resultSet.getInt("id"));
				if(resultSet.getString("course_image")!=null)
					course.setImage_download_url(resultSet.getString("course_image"));
				if(resultSet.getBoolean("is_public"))
					course.setIs_public(resultSet.getBoolean("is_public"));
				if(resultSet.getString("name")!=null)
					course.setName(resultSet.getString("name"));
				if(resultSet.getString("public_description")!=null)
					course.setPublic_description(resultSet.getString("public_description"));
				if(resultSet.getFloat("price")>-1)
					course.setPrice(resultSet.getFloat("price"));
				if(resultSet.getString("class_mode") !=null)
					course.setCourse_format(resultSet.getString("class_mode"));
				if(resultSet.getString("syllabus_body") !=null)
					course.setSyllabus_body(resultSet.getString("syllabus_body"));
				if(resultSet.getString("term")!=null)
					course.setTerm(resultSet.getString("term"));
				if(resultSet.getString("time_zone")!=null)
					course.setTime_zone(resultSet.getString("time_zone"));
				if(resultSet.getString("days_offered")!=null)
					course.setDays_offered(resultSet.getString("days_offered"));	
				if(resultSet.getString("start_time")!=null)
					course.setStart_time(resultSet.getString("start_time"));
				if(resultSet.getString("end_time")!=null)
					course.setEnd_time(resultSet.getString("end_time"));
				if(resultSet.getString("non_regular_times")!=null)
					course.setNon_regular_times(resultSet.getString("non_regular_times"));
				if(resultSet.getString("session")!=null)
					course.setSession(resultSet.getString("session"));
				if(resultSet.getInt("max_enrollement")>-1)
					course.setMax_enrollement(resultSet.getInt("max_enrollement"));
				if(resultSet.getInt("total_enrollment")>-1)
					course.setTotal_enrollment(resultSet.getInt("total_enrollment"));
				if(resultSet.getString("course_level")!=null)
					course.setCourse_level(resultSet.getString("course_level"));
				if(resultSet.getString("instructor")!=null)
					course.setInstructor(resultSet.getString("instructor"));
				if(resultSet.getString("campus")!=null)
					course.setCampus(resultSet.getString("campus"));
				if(resultSet.getString("course_material")!=null)
					course.setCourse_material(resultSet.getString("course_material"));
				if(resultSet.getFloat("Credits")>-1)
					course.setCredits(resultSet.getFloat("Credits"));
				if(resultSet.getString("course_dept")!=null)
					course.setCourse_dept(resultSet.getString("course_dept"));

				
				try {
					if(resultSet.getString("start_at")!=null && resultSet.getString("start_at")!="null")
						course.setStart_at(DateUtil.convertStringDateToGMTDate(resultSet.getString("start_at"), "yyyy-MM-dd'T'HH:mm:ss", course.getTime_zone()));
					
					if(resultSet.getString("end_at")!=null && resultSet.getString("end_at")!="null")
						course.setEnd_at(DateUtil.convertStringDateToGMTDate(resultSet.getString("end_at"), "yyyy-MM-dd'T'HH:mm:ss", course.getTime_zone()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return course;
	}
	
//	private String convertToUniCode(String description) {
//		return description.replaceAll( "\\(o\\)", "\u00B0" )
//				.replaceAll( "\\(C\\)", "\u00a9")
//				.replaceAll(  "\\(R\\)", "\u00AE" )
//				.replaceAll( "\\(TM\\)", "\u2122");
//	}
	
	private Canvas_Courses addNParseCourse(ResultSet resultSet,Vendor vendor) {
		Canvas_Courses course =  new Canvas_Courses();
		
		try {
		if(resultSet!=null) {
			
			Canvas_Courses canvasCourse =canvasLMSDAO.getCourseByLMSCourseId(resultSet.getInt("id"), vendor.getVendorId());
			if(canvasCourse!=null) {
				course=canvasCourse;
			}
			if(resultSet.getInt("status")==1) {
				course.setStatus(Status.ACTIVE);
			}else {
				course.setStatus(Status.INACTIVE);
				}

			if(resultSet.getString("course_code")!=null)
				course.setCourse_code(resultSet.getString("course_code"));
			if(resultSet.getFloat("price")>-1)
				course.setPrice(resultSet.getFloat("price"));

			if(resultSet.getInt("id")>0)
				course.setId(resultSet.getInt("id"));
			if(resultSet.getString("course_image")!=null)
				course.setImage_download_url(resultSet.getString("course_image"));
			if(resultSet.getBoolean("is_public"))
				course.setIs_public(resultSet.getBoolean("is_public"));
			if(resultSet.getString("name")!=null)
				course.setName(resultSet.getString("name"));
			if(resultSet.getString("public_description")!=null)
				course.setPublic_description(resultSet.getString("public_description"));
			if(resultSet.getFloat("price")>-1)
				course.setPrice(resultSet.getFloat("price"));
			if(resultSet.getString("class_mode") !=null)
				course.setCourse_format(resultSet.getString("class_mode"));
			if(resultSet.getString("syllabus_body") !=null)
				course.setSyllabus_body(resultSet.getString("syllabus_body"));
//			if(resultSet.getString("term")!=null)
//				course.setTerm(JSONObject.stringToValue(resultSet.getString("term")).toString());
			if(resultSet.getString("time_zone")!=null)
				course.setTime_zone(resultSet.getString("time_zone"));
			if(resultSet.getString("days_offered")!=null)
				course.setDays_offered(resultSet.getString("days_offered"));	
			if(resultSet.getString("start_time")!=null)
				course.setStart_time(resultSet.getString("start_time"));
			if(resultSet.getString("end_time")!=null)
				course.setEnd_time(resultSet.getString("end_time"));
			if(resultSet.getString("non_regular_times")!=null)
				course.setNon_regular_times(resultSet.getString("non_regular_times"));
			if(resultSet.getString("session")!=null)
				course.setSession(resultSet.getString("session"));
			if(resultSet.getInt("max_enrollement")>-1)
				course.setMax_enrollement(resultSet.getInt("max_enrollement"));
			if(resultSet.getInt("total_enrollment")>-1)
				course.setTotal_enrollment(resultSet.getInt("total_enrollment"));
			if(resultSet.getString("course_level")!=null)
				course.setCourse_level(resultSet.getString("course_level"));
			if(resultSet.getString("instructor")!=null)
				course.setInstructor(resultSet.getString("instructor"));
			if(resultSet.getString("campus")!=null)
				course.setCampus(resultSet.getString("campus"));
			if(resultSet.getString("course_material")!=null)
				course.setCourse_material(resultSet.getString("course_material"));
			if(resultSet.getFloat("Credits")>-1)
				course.setCredits(resultSet.getFloat("Credits"));
			if(resultSet.getString("course_dept")!=null)
				course.setCourse_dept(resultSet.getString("course_dept"));
			
			course.setVendorId(vendor.getVendorId());
			if(vendor.getRoot_Account_id()==null) {
				vendor.setRoot_Account_id(course.getRoot_account_id());
				vendorService.saveVendor(vendor);
			}
			CuisineType tag = cusineNDishTypeService.getCuisineTypeByName(course.getName());
			if(tag==null) {
				course.setTagList(course.getName());
				CuisineType cusineType = new CuisineType();
				cusineType.setCountryId(vendor.getCountryId());
				cusineType.setName(course.getName());
				cusineNDishTypeService.addCuisineType(cusineType);
			}else {
				course.setTagList(course.getName());
			}
				System.out.println(resultSet.getString("start_at") +""+course.getTime_zone());
			try {
				if(resultSet.getString("start_at")!=null && resultSet.getString("start_at")!="null")
					course.setStart_at(DateUtil.convertStringDateToGMTDate(resultSet.getString("start_at"), "yyyy-MM-dd HH:mm", course.getTime_zone()));
				
				if(resultSet.getString("end_at")!=null && resultSet.getString("end_at")!="null")
					course.setEnd_at(DateUtil.convertStringDateToGMTDate(resultSet.getString("end_at"), "yyyy-MM-dd HH:mm", course.getTime_zone()));
			} catch (ParseException | SQLException e) {
				e.printStackTrace();
			}
			
			canvasLMSDAO.addCanvasCourse(course);
		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return course;
	}
	
	@Override
	public Connection getMySQLConnection(DataSourceDTO datasource) {

		return null;
	}

	@Override
	public Connection getOracleSQLConnection(DataSourceDTO datasource) {

		return null;
	}

	@Override
	public void updateMSSQLInvoiceData(Invoice invoice, DataSourceDTO dataSource, String invoiceTableName) {

		Customer cust  = customerService.getCustomer(invoice.getCustomerId());
		 SQLServerDataSource ds = new SQLServerDataSource();
	        ds.setUser(dataSource.getUser());
	        ds.setPassword(dataSource.getPassword());
	        ds.setServerName(dataSource.getDb_url());
	        ds.setPortNumber(dataSource.getPort());
	        ds.setDatabaseName(dataSource.getDbName());
	        
	        try (Connection con = ds.getConnection();) {
	        	PreparedStatement stmt=con.prepareStatement("insert into "+invoiceTableName+" ("
	        			+ "fname,lname,email,payment_status,svc_charge,coupon_amt,coupon_code,order_total,course_amount_paid)"
	        			+ " values(?,?,?,?,?,?,?,?,?)");
	        	stmt.setString(1,cust.getFirstName()); 
	    		stmt.setString(2,cust.getLastName());
	    		stmt.setString(3,invoice.getEmail());
	    		stmt.setString(4,invoice.getTransactionStatus());
	    		stmt.setFloat(5,0);
	    		boolean couponAdded=false;
	    		for(Coupon coupon: invoice.getCoupon_Applied()) {
	    			Coupon coup = couponService.getCouponById(coupon.getCoupanId());
	    			stmt.setFloat(6,(float) coup.getFlatRules().getDiscountValue());
	    			stmt.setString(7,coup.getCouponCode());
	    			couponAdded=true;
	    		}
	    		if(!couponAdded) {
	    			stmt.setFloat(6,0);
	    			stmt.setString(7,"");
	    		}
	    		stmt.setFloat(8,invoice.getBill());
	    		stmt.setFloat(9,(float) invoice.getRoundOffTotal());
	        	stmt.executeUpdate();
	        	con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		  
		
		
	}

}
