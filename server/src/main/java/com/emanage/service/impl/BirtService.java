package com.emanage.service.impl;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.model.api.DesignConfig;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.IDesignEngine;
import org.eclipse.birt.report.model.api.IDesignEngineFactory;
import org.eclipse.birt.report.model.api.OdaDataSourceHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;


/**
 * Created by rahul on 2/22/2017.
 */

@Service
@Lazy
public class BirtService implements DisposableBean, ApplicationContextAware {

    public static final String dataSourceName = "Data Source";
    public static final String IMAGE_FOLDER = "/WEB-INF/images";
    public static final String REPORT_DIR = "/WEB-INF/Reports";
    final static Logger logger = Logger.getLogger(BirtService.class);
    private final String dataSource = "org.eclipse.birt.report.data.oda.jdbc";
    @Autowired
    public ServletContext servletContext;
    public ApplicationContext context;
    //    public SessionHandle session;
//    public ReportDesignHandle reportDesignHandle;
//    public OdaDataSourceHandle odaDataSourceHandle;
    public IReportEngine reportEngine;
    public IDesignEngine designEngine;
    //    public ElementFactory elementFactory;
    public StructureFactory structFactory;
    public ReportDesignHandle designHandle;
    @Value("${jdbc.databaseurl}")
    private String jdbcUrl;
    @Value("${jdbc.username}")
    private String jdbcUserName;
    @Value("${jdbc.password}")
    private String jdbcPassword;
    @Value("${jdbc.driverClassName}")
    private String jdbcDriverClass;
    @Value("${jdbc.dialect}")
    private String jdbcDialect;

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     * This will Shutdown the Reporting Platform and destroy report engine
     *
     * @throws Exception in case of shutdown errors.
     *                   Exceptions will get logged but not rethrown to allow
     *                   other beans to release their resources too.
     */
    @Override
    public void destroy() throws Exception {
        logger.info("destroy birt reporting engine and shutdown platform");
        reportEngine.destroy();
        Platform.shutdown();
    }

    /**
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        logger.info("setApplicationContext");
        this.context = applicationContext;
    }

    /**
     * This method will be called after all dependency to
     * IoC is resolved once for lifeCycle of Bean.
     *
     * @throws BirtException
     */
    @PostConstruct
    protected void initializeEngine() throws BirtException {
        logger.info("initialize Birt Design and Report Engine..");
        EngineConfig config = new EngineConfig();
        config.getAppContext().put("BirtIReportEngine", this.context);
        Platform.startup(config);
        setUpReportEngine(config);
        setUpDesignEngine();
        //buildDataSource();
    }

    /**
     * This method will create a new ReportEngine if not
     *
     * @param config
     * @return IDesignEngine
     */
    private void setUpReportEngine(EngineConfig config) throws BirtException {
        logger.info("SetUp Report Engine..");
        if (reportEngine == null) {
            //Configure the Engine and start the Platform
            try {
                IReportEngineFactory iReportEngineFactory = (IReportEngineFactory) Platform
                        .createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
                reportEngine = iReportEngineFactory.createReportEngine(config);
            } catch (Exception e) {
                logger.error("Error creating design engine: " + e.getMessage(), e);
                throw e;
            }
        }
    }

    /**
     * This method will create a new DesignEngine
     *
     * @return IDesignEngine
     */
    private void setUpDesignEngine() throws BirtException {
        logger.info("SetUp Design Engine..");
        if (designEngine == null) {
            //Configure the Engine and start the Platform
            DesignConfig designConfig = new DesignConfig();
            try {
                IDesignEngineFactory iDesignEngineFactory = (IDesignEngineFactory) Platform
                        .createFactoryObject(IDesignEngineFactory.EXTENSION_DESIGN_ENGINE_FACTORY);
                designEngine = iDesignEngineFactory.createDesignEngine(designConfig);
            } catch (Exception e) {
                logger.error("Error creating design engine: " + e.getMessage(), e);
                throw e;
            }
        }
    }

    public OdaDataSourceHandle getDataSource(ElementFactory elementFactory) throws SemanticException {
        logger.info("SetUp Data Source.");
        OdaDataSourceHandle odaDataSourceHandle = elementFactory.newOdaDataSource(dataSourceName, dataSource);
        odaDataSourceHandle.setProperty("odaDriverClass", jdbcDriverClass);
        odaDataSourceHandle.setProperty("odaURL", jdbcUrl);
        odaDataSourceHandle.setProperty("odaUser", jdbcUserName);
        odaDataSourceHandle.setProperty("odaPassword", jdbcPassword);
        logger.info("Success SetUp Data Source..");
        return odaDataSourceHandle;
    }


}
