package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.KitchenScreenDAO;
import com.emanage.domain.KitchenScreen;
import com.emanage.service.KitchenScreenService;

@Service
@Lazy
public class KitchenScreenServiceImpl  implements KitchenScreenService{

	@Autowired
	KitchenScreenDAO kitchenServiceDao;
	
	@Override
	@Transactional
	public void addKitchenScreen(KitchenScreen kitchenScreen) {
		// TODO Auto-generated method stub
		kitchenServiceDao.addKitchenScreen(kitchenScreen);
		
	}

	@Override
	@Transactional
	public void removeKitchenScreen(int id) {
		// TODO Auto-generated method stub
		kitchenServiceDao.removeKitchenScreen(id);
	}

	@Override
	@Transactional
	public List<KitchenScreen> getKitchenScreens(int restaurantId) {
		// TODO Auto-generated method stub
		return kitchenServiceDao.getKitchenScreens(restaurantId);
	}

	@Override
	@Transactional
	public KitchenScreen getKitchenScreen(int id) {
		// TODO Auto-generated method stub
		return kitchenServiceDao.getKitchenScreen(id);
	}

	
}
