package com.emanage.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.TutorDAO;
import com.emanage.domain.Customer;
import com.emanage.domain.Customers;
import com.emanage.domain.TutorRequest;
import com.emanage.dto.ResponseDTO;
import com.emanage.service.AsyncService;
import com.emanage.service.CustomerService;
import com.emanage.service.TutorService;

@Service
public class TutorServiceImpl implements TutorService {
	
	@Autowired
	private TutorDAO tutorDAO;

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private AsyncService asynService;
	
	@Override
	@Transactional
	public ResponseDTO saveTutorRequest(TutorRequest tutorRequest,HttpServletRequest request, HttpServletResponse response,String restaurantId) {
		// TODO Auto-generated method stub
		ResponseDTO responseDTO = new ResponseDTO(); 
		try {
		Customers customers= customerService.getCustomerInfoJSON(tutorRequest.getPhone(), null,restaurantId, request, response);
		if(customers!=null && customers.getCustomers().size()>0 && customers.getCustomers().get(0)!=null) {
			Customer customer = customerService.getCustomer(customers.getCustomers().get(0).getCustomerId());
			customer.setFirstName(tutorRequest.getName());
			customer.setEmail(tutorRequest.getEmail());
			customerService.addCustomer(customer);
			asynService.emailTutorRequest(customer, tutorRequest, request);
		}
		tutorDAO.saveTutorRequest(tutorRequest);
		responseDTO.message="Request recieved";
		responseDTO.result="SUCCESS";
		}catch (Exception e) {
			e.printStackTrace();
			responseDTO.message="Request failed";
			responseDTO.result="FAIL";
		}
		 
		 return responseDTO;
	}

	
}
