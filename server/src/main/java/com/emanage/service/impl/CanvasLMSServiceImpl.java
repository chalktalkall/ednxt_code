package com.emanage.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.CanvasLMSDAO;
import com.emanage.dao.UserLMSDAO;
import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.CuisineType;
import com.emanage.domain.Customer;
import com.emanage.domain.DBConfig;
import com.emanage.domain.User_Enrollment;
import com.emanage.domain.User_LMS;
import com.emanage.domain.Vendor;
import com.emanage.dto.DataSourceDTO;
import com.emanage.dto.lms.CreateUserCanvasDTO;
import com.emanage.dto.lms.EnrollUser;
import com.emanage.dto.lms.EnrollUserDTO;
import com.emanage.dto.lms.LMSPseudonym;
import com.emanage.dto.lms.LMSUserDTO;
import com.emanage.enums.DataSourceType;
import com.emanage.enums.LMS_NAME;
import com.emanage.enums.Status;
import com.emanage.service.CanvasLMSService;
import com.emanage.service.CuisineNDishService;
import com.emanage.service.CustomerService;
import com.emanage.service.SQLDataSource;
import com.emanage.service.VendorDBConfigService;
import com.emanage.service.VendorService;
import com.emanage.utility.DateUtil;
import com.google.gson.Gson;

@Service
public class CanvasLMSServiceImpl implements CanvasLMSService {

	@Autowired
	VendorService vendorService;

	@Autowired
	CustomerService customerService;

	@Autowired
	UserLMSDAO userLmsDao;
	
	@Autowired
	CanvasLMSDAO canvasLMSDAO;
	
	@Autowired
	CuisineNDishService cusineNDishTypeService;
	
	@Autowired
	VendorDBConfigService vendorDBConfigService;
	
	@Autowired
	SQLDataSource sqlDataSource;

	@Override
	@Transactional
	public Object getCourse(Integer courseId, Integer vendorId) {
		return canvasLMSDAO.getCourse(courseId, vendorId);
	}

	@Override
	@Transactional
	public List<Object> getCourseByCollegeCampus(Integer campusId) {

		Vendor vendor = vendorService.getVendor(campusId);
		if (LMS_NAME.CANVAS == vendor.getLms_name()) {
			try {
				JSONArray json = getCourseList(vendor.getLms_key(), null);
				if(json!=null) {
					for(int i=0;i<json.length();i++) {
						addNParseCourse(json.getJSONObject(i), campusId,vendor);
					}
				}
			
			} catch (IOException | JSONException e) {
				e.printStackTrace();
			}
		}else if(LMS_NAME.DB_VIEW == vendor.getLms_name()) {
			DBConfig dbConfig= vendorDBConfigService.getCampusDBConfig(campusId);
			if(dbConfig.getDbType()==DataSourceType.MSSQL) {
				
				DataSourceDTO dataSource =  new DataSourceDTO();
				
				dataSource.setDbName("");
				dataSource.setUser(dbConfig.getUsername());
				dataSource.setPassword(dbConfig.getPassword());
				dataSource.setDb_url(dbConfig.getDbUrl());
				dataSource.setPort(dbConfig.getDbPort());
				sqlDataSource.getMSSQLViewData(dataSource, dbConfig.getDbView(),vendor);
			}
			
		}
		return null;
	}
	
	@Override
	@Transactional
	public Canvas_Courses getCourseByCanvasId(Vendor vendor,Integer id) {

		//Vendor vendor = vendorService.getVendor(vendorId);
		if (LMS_NAME.CANVAS == vendor.getLms_name()) {
			try {
				
				JSONObject json = getCanvasCourseById(vendor.getLms_key(),id);
				
				return parseCourse(json, vendor.getVendorId(),vendor);
				
			} catch (IOException | JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private Canvas_Courses addNParseCourse(JSONObject jsonObj,Integer vendorId,Vendor vendor) {
		Canvas_Courses course =  new Canvas_Courses();
		
		try {
		if(jsonObj.has("workflow_state")) {
			
			Canvas_Courses canvasCourse =canvasLMSDAO.getCourseByLMSCourseId(jsonObj.getInt("id"), vendorId);
			if(canvasCourse!=null) {
				course=canvasCourse;
			}
			if(jsonObj.has("workflow_state") && jsonObj.getString("workflow_state").equalsIgnoreCase("available")) {
				course.setStatus(Status.ACTIVE);
			}else {
				course.setStatus(Status.INACTIVE);
				}
			if(jsonObj.has("account_id"))
				course.setAccount_id(jsonObj.getInt("account_id"));
			if(jsonObj.has("course_code")) {
				String [] value= jsonObj.getString("course_code").split("@");
				if(value.length>1) {
					course.setCourse_code(value[0]);
					course.setPrice(Float.parseFloat(value[1]));
				}else {
					course.setCourse_code(jsonObj.getString("course_code"));
				}
			}
			if(jsonObj.has("course_format"))
				course.setCourse_format(jsonObj.getString("course_format"));
			if(jsonObj.has("default_view"))
				course.setDefault_view(jsonObj.getString("default_view"));
			if(jsonObj.has("enrollment_term_id"))
				course.setEnrollment_term_id(jsonObj.getInt("enrollment_term_id"));
			if(jsonObj.has("id"))
				course.setId(jsonObj.getInt("id"));
			if(jsonObj.has("image_download_url"))
				course.setImage_download_url(jsonObj.getString("image_download_url"));
			if(jsonObj.has("is_public"))
				course.setIs_public(jsonObj.getBoolean("is_public"));
			if(jsonObj.has("name"))
				course.setName(convertToUniCode(jsonObj.getString("name")));
			if(jsonObj.has("public_description"))
				course.setPublic_description(convertToUniCode(jsonObj.getString("public_description")));
			if(jsonObj.has("root_account_id"))
				course.setRoot_account_id(jsonObj.getInt("root_account_id"));
			if(jsonObj.has("sections"))
				course.setSections(jsonObj.getString("sections"));
			if(jsonObj.has("syllabus_body"))
				course.setSyllabus_body(convertToUniCode(jsonObj.getString("syllabus_body")));
			if(jsonObj.has("term"))
				course.setTerm(jsonObj.getString("term"));
			if(jsonObj.has("time_zone"))
				course.setTime_zone(jsonObj.getString("time_zone"));
			
			course.setVendorId(vendorId);
			if(vendor.getRoot_Account_id()==null) {
				vendor.setRoot_Account_id(course.getRoot_account_id());
				vendorService.saveVendor(vendor);
			}
			CuisineType tag = cusineNDishTypeService.getCuisineTypeByName(course.getName());
			if(tag==null) {
				course.setTagList(course.getName());
				CuisineType cusineType = new CuisineType();
				cusineType.setCountryId(vendor.getCountryId());
				cusineType.setName(course.getName());
				cusineNDishTypeService.addCuisineType(cusineType);
			}else {
				course.setTagList(course.getName());
			}
				
			try {
				if(jsonObj.has("start_at") && jsonObj.getString("start_at")!="null")
					course.setStart_at(DateUtil.convertStringDateToGMTDate(jsonObj.getString("start_at"), "yyyy-MM-dd'T'HH:mm:ss", course.getTime_zone()));
				
				if(jsonObj.has("end_at") && jsonObj.getString("end_at")!="null")
					course.setEnd_at(DateUtil.convertStringDateToGMTDate(jsonObj.getString("end_at"), "yyyy-MM-dd'T'HH:mm:ss", course.getTime_zone()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			canvasLMSDAO.addCanvasCourse(course);
		}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return course;
	}

	private String convertToUniCode(String description) {
		return description.replaceAll( "\\(o\\)", "\u00B0" )
				.replaceAll( "\\(C\\)", "\u00a9")
				.replaceAll(  "\\(R\\)", "\u00AE" )
				.replaceAll( "\\(TM\\)", "\u2122");
	}
	private Canvas_Courses parseCourse(JSONObject jsonObj,Integer vendorId,Vendor vendor) {
		Canvas_Courses course =  null;
		
		try {
			
			if(jsonObj!=null && jsonObj.has("workflow_state") && jsonObj.getString("workflow_state").equalsIgnoreCase("available")) {
				course=new Canvas_Courses();
				if(jsonObj.has("account_id"))
					course.setAccount_id(jsonObj.getInt("account_id"));
				if(jsonObj.has("course_code"))
					course.setCourse_code(jsonObj.getString("course_code"));
				if(jsonObj.has("course_format"))
					course.setCourse_format(jsonObj.getString("course_format"));
				if(jsonObj.has("default_view"))
					course.setDefault_view(jsonObj.getString("default_view"));
				if(jsonObj.has("enrollment_term_id"))
					course.setEnrollment_term_id(jsonObj.getInt("enrollment_term_id"));
				if(jsonObj.has("id"))
					course.setId(jsonObj.getInt("id"));
				if(jsonObj.has("image_download_url"))
					course.setImage_download_url(jsonObj.getString("image_download_url"));
				if(jsonObj.has("is_public"))
					course.setIs_public(jsonObj.getBoolean("is_public"));
				if(jsonObj.has("name"))
					course.setName(jsonObj.getString("name"));
				if(jsonObj.has("public_description"))
					course.setPublic_description(jsonObj.getString("public_description"));
				if(jsonObj.has("root_account_id"))
					course.setRoot_account_id(jsonObj.getInt("root_account_id"));
				if(jsonObj.has("sections"))
					course.setSections(jsonObj.getString("sections"));
				if(jsonObj.has("syllabus_body"))
					course.setSyllabus_body(jsonObj.getString("syllabus_body"));
				if(jsonObj.has("term"))
					course.setTerm(jsonObj.getString("term"));
				if(jsonObj.has("time_zone"))
					course.setTime_zone(jsonObj.getString("time_zone"));
				
				try {
					if(jsonObj.has("start_at") && jsonObj.getString("start_at")!="null")
						course.setStart_at(DateUtil.convertStringDateToGMTDate(jsonObj.getString("start_at"), "yyyy-MM-dd'T'HH:mm:ss", course.getTime_zone()));
					
					if(jsonObj.has("end_at") && jsonObj.getString("end_at")!="null")
						course.setEnd_at(DateUtil.convertStringDateToGMTDate(jsonObj.getString("end_at"), "yyyy-MM-dd'T'HH:mm:ss", course.getTime_zone()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return course;
	}
	
	
	
	
	@Override
	@Transactional
	public List<Object> listActiveCourses() {
		return null;
	}

	@Override
	@Transactional
	public List<Object> createUser(Integer vendorId, Integer customerId) {

		Vendor vendor = vendorService.getVendor(vendorId);

		Customer customer = customerService.getCustomer(customerId);

		CreateUserCanvasDTO user = new CreateUserCanvasDTO();
		user.user = new LMSUserDTO();
		user.user.name = customer.getFirstName() + " " + customer.getLastName();
		user.user.terms_of_use = true;
		user.pseudonym = new LMSPseudonym();
		user.pseudonym.unique_id = customer.getEmail();
		user.pseudonym.unique_login = customer.getEmail();

		if (LMS_NAME.CANVAS == vendor.getLms_name()) {
			try {
				JSONObject jsonObj = createUser(vendor.getLms_key(), user);
				System.out.println(jsonObj);
				if (jsonObj != null) {
					System.out.println("has id");
					System.out.println(jsonObj.has("id"));
					User_LMS userData = new User_LMS();
					userData.setCustomer_id(customer.getCustomerId());
					userData.setLms(LMS_NAME.CANVAS);
					if(jsonObj.has("id")) {
						userData.setLms_user_id(jsonObj.getInt("id")); // need to add userId
					userData.setRoot_account_id(vendor.getRoot_Account_id());
					userLmsDao.addUserLMS(userData);
					}
				}
			} catch (IOException | JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	@Transactional
	public Object getUserDetails(Integer customerId, Integer vendorId) {

		Vendor vendor = vendorService.getVendor(vendorId);

		if (vendor.getRoot_Account_id() != null) {
			List<User_LMS> userLMS = userLmsDao.userLMSList(customerId, LMS_NAME.CANVAS, vendor.getRoot_Account_id());
			Integer userId = null;
			for (User_LMS user : userLMS) {
				userId = user.getLms_user_id();
			}

			if (userId != null) {
				if (LMS_NAME.CANVAS == vendor.getLms_name()) {
					try {
						JSONObject json = getUser(vendor.getLms_key(), userId);
						System.out.println(" json out put");
						System.out.println(json);
						return  json;
					} catch (IOException | JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}

	@Override
	@Transactional
	public List<Object> enrollUser(Integer customerId, Integer vendorId, Integer courseId) {

		System.out.println("enrolling user");
		Vendor vendor = vendorService.getVendor(vendorId);
		Canvas_Courses course = canvasLMSDAO.getCourse(courseId, vendorId);
		System.out.println(course);
		System.out.println(vendor.getRoot_Account_id());
		if (vendor.getRoot_Account_id() != null && course!=null) {
			List<User_LMS> userLMS = userLmsDao.userLMSList(customerId, LMS_NAME.CANVAS, vendor.getRoot_Account_id());
			Integer userId = null;
			for (User_LMS user : userLMS) {
				userId = user.getLms_user_id();
			}

			EnrollUserDTO user = new EnrollUserDTO();
			user.enrollment = new EnrollUser();
			user.enrollment.self_enrolled = true;
			user.enrollment.type = "StudentEnrollment";
			user.enrollment.enrollment_state = "active";
			user.enrollment.user_id = userId;
			

			if (LMS_NAME.CANVAS == vendor.getLms_name()) {
				try {
					JSONObject jsonObj = enrollUser(vendor.getLms_key(), courseId, user);
					System.out.println(jsonObj);
					if(jsonObj!=null) {
						
						Customer customer = customerService.getCustomer(customerId);
						customer.setTime_zone(course.getTime_zone());
						List<User_Enrollment> enrollments = customer.getEnrollments();
						if(enrollments==null) {
							enrollments = new ArrayList<User_Enrollment>();
						}
						
						User_Enrollment enrollment = new User_Enrollment();
						
						if(jsonObj.has("id"))
							enrollment.setId(jsonObj.getInt("id"));
						
						if(jsonObj.has("enrollment_state"))
							enrollment.setEnrollmennt_state(jsonObj.getString("enrollment_state"));
						
						if(jsonObj.has("type"))
							enrollment.setType(jsonObj.getString("type"));
						
						if(jsonObj.has("course_id"))
							enrollment.setType(jsonObj.getString("course_id"));
						
						if(jsonObj.has("created_at")) {
							try {
								if(course.getTime_zone()!=null)
									enrollment.setCreated_at(DateUtil.convertStringDateToGMTDate(jsonObj.getString("created_at"), "yyyy-MM-dd'T'HH:mm:ss", customer.getTime_zone()));
								
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						
						enrollment.setSelf_enrolled(true);
						enrollment.setTime_zone(customer.getTime_zone());
						enrollments.add(enrollment);
						customer.setEnrollments(enrollments);
						customerService.addCustomer(customer);
						System.out.println(" user enrolled");
					}
				} catch (IOException | JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static JSONArray getCourseList(String auth_Key, Object data)
			throws ClientProtocolException, IOException, JSONException {

		JSONArray jsonResult = null;
		HttpClient client = HttpClientBuilder.create().build();

		String lms_Canvas_Course = "https://canvas.instructure.com/api/v1/courses?"
		+"per_page=500"
		+ "include[]=syllabus_body"
	    + "&include[]=term" 
		+ "&include[]=total_scores"
	    + "&include[]=public_description"
	    + "&open_enrollment=true"
	    + "&include[]=course_image";

		HttpGet getReq = new HttpGet(lms_Canvas_Course);

		try {
			
			getReq.setHeader("Authorization", "Bearer " + auth_Key);
			HttpResponse response = client.execute(getReq);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			jsonResult = new JSONArray(result.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonResult;
	}
	
	public static JSONObject getCanvasCourseById(String auth_Key, Integer id)
			throws ClientProtocolException, IOException, JSONException {

		JSONObject jsonResult = null;
		HttpClient client = HttpClientBuilder.create().build();

		String lms_Canvas_Course = "https://canvas.instructure.com/api/v1/courses/"+id;

		HttpGet getReq = new HttpGet(lms_Canvas_Course);

		try {
			
			getReq.setHeader("Authorization", "Bearer " + auth_Key);
			HttpResponse response = client.execute(getReq);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			jsonResult = new JSONObject(result.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonResult;
	}

	private static JSONObject createUser(String auth_Key, CreateUserCanvasDTO data)
			throws ClientProtocolException, IOException, JSONException {

		JSONObject jsonResult = null;

		HttpClient client = HttpClientBuilder.create().build();

		String lms_Canvas_Course = "https://canvas.instructure.com/api/v1/accounts/self/users";
		HttpPost postReq = new HttpPost(lms_Canvas_Course);
		try {
			Gson gson = new Gson();
			postReq.setEntity(new StringEntity(gson.toJson(data), "UTF8"));
			postReq.setHeader("Authorization", "Bearer " + auth_Key);
			postReq.setHeader("Content-type", "application/json");
			StringEntity params = new StringEntity(gson.toJson(data));
			postReq.setEntity(params);
			//System.out.println(gson.toJson(data).toString());
			HttpResponse response = client.execute(postReq);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			//System.out.println(result.toString());
			jsonResult = new JSONObject(result.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonResult;
	}

	private static JSONObject getUser(String auth_Key, Integer customerId)
			throws ClientProtocolException, IOException, JSONException {

		JSONObject jsonResult = null;

		HttpClient client = HttpClientBuilder.create().build();

		String lms_Canvas_Course = "https://canvas.instructure.com/api/v1/users/" + customerId + "/profile";
		HttpGet getReq = new HttpGet(lms_Canvas_Course);
		try {
			getReq.setHeader("Authorization", "Bearer " + auth_Key);
			HttpResponse response = client.execute(getReq);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			jsonResult = new JSONObject(result.toString());
			// { "status": "unauthorized", "errors": [ { "message": "user not authorized to
			// perform that action" } ] } if user is not authorized

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonResult;

	}

	private static JSONObject enrollUser(String auth_Key, Integer courseId, EnrollUserDTO enrollUser)
			throws ClientProtocolException, IOException, JSONException {

		JSONObject jsonResult = null;

		HttpClient client = HttpClientBuilder.create().build();

		String lms_Canvas_Course = "https://canvas.instructure.com/api/v1/courses/" + courseId + "/enrollments";
		HttpPost postReq = new HttpPost(lms_Canvas_Course);
		try {
			Gson gson = new Gson();
			postReq.setEntity(new StringEntity(gson.toJson(enrollUser), "UTF8"));
			postReq.setHeader("Authorization", "Bearer " + auth_Key);
			postReq.setHeader("Content-type", "application/json");
			StringEntity params = new StringEntity(gson.toJson(enrollUser));
			postReq.setEntity(params);
			HttpResponse response = client.execute(postReq);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			jsonResult = new JSONObject(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonResult;

	}

	@Override
	@Transactional
	public Canvas_Courses getCourseById(Integer course_id) {
		// TODO Auto-generated method stub
		return canvasLMSDAO.getCourseById(course_id);
	}

	@Override
	@Transactional
	public List<Canvas_Courses> listCoursesByCollegeId(Integer vendorId) {
		// TODO Auto-generated method stub
		return canvasLMSDAO.getCourseList(vendorId);
	}
	
	@Override
	@Transactional
	public void updateLMSCourse(Canvas_Courses course) {
		// TODO Auto-generated method stub
		
		 canvasLMSDAO.addCanvasCourse(course);
	}

	@Override
	@Transactional
	public List<Canvas_Courses> getAllCourseList(Integer campusId) {
		// TODO Auto-generated method stub
		return canvasLMSDAO.getAllCourseList(campusId);
	}

	@Override
	@Transactional
	public void deleteLMSCourse(Canvas_Courses course) {
		// TODO Auto-generated method stub
		 canvasLMSDAO.deleteLMSCourse(course);
	}

	@Override
	@Transactional
	public List<Canvas_Courses> getAllCourseListByUniv(Integer univId) {
		// TODO Auto-generated method stub
		
		List<Vendor> univ =vendorService.listVendorByCountryId(univId);
		List<Canvas_Courses> courseList =  new ArrayList<Canvas_Courses>();
		for(Vendor vendor:univ) {
			courseList.addAll(canvasLMSDAO.getCourseList(vendor.getVendorId()));
		}
		return courseList;
	}

}
