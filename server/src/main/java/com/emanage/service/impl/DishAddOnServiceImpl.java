/**
 * 
 */
package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.DishAddOnDAO;
import com.emanage.domain.AddOnDish;
import com.emanage.domain.DishAddOn;
import com.emanage.service.DishAddOnService;

/**
 * @author rahul
 *
 */
@Service
@Lazy
public class DishAddOnServiceImpl implements DishAddOnService {

	@Autowired
	private DishAddOnDAO dishDAO;
	
	@Override
	@Transactional
	public void addDish(DishAddOn dish) {
		dishDAO.addDish(dish);
	}


	@Override
	@Transactional
	public void updateMenuModificationTime(Integer dishId) {
		dishDAO.updateMenuModificationTime(dishId);
	}
	@Override
	@Transactional
	public List<DishAddOn> listDishAddOn(Integer restaurantId) {
		return dishDAO.listDishAddOn(restaurantId);
	}

	@Override
	@Transactional
	public List<DishAddOn> listDishAddOnByDish(Integer dishId) {
		return dishDAO.listDishAddOnByDish(dishId);
	}

	@Override
	@Transactional
	public void removeDishAddOn(Integer id) throws Exception{
		dishDAO.removeDishAddOn(id);

	}


	@Override
	@Transactional
	public List<AddOnDish> listDishAddOnByRestaurant(Integer restaurantId) {
      return dishDAO.listDishAddOnByRestaurant(restaurantId);
	}


	@Override
	@Transactional
	public List<DishAddOn> listDishAddOnByFulfillmentCenter(Integer fulfillmentCenterId) {
		return dishDAO.listDishAddOnByFulfillmentCenter(fulfillmentCenterId);
	}


	/*@Override
	public List<AddOnDish> getDishes(Integer restaurantId) {
		// TODO Auto-generated method stub
		return null;
	}*/


	/*@Override
	@Transactional
	public List<AddOnDish> getDishes(Integer id) {
		return dishDAO.getDishes(id);
	}*/

	/*@Override
	@Transactional
	public AddOnDish getDish(Integer id) {
		return dishDAO.getDish(id);
	}*/
}
