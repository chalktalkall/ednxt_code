package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.VendorDishDAO;
import com.emanage.dao.VendorPlatterDAO;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.VendorDish;
import com.emanage.service.VendorDishService;

@Service
@Lazy
public class VendorDishServiceImpl implements VendorDishService{

	@Autowired
	private VendorDishDAO vendorDishDAO;
	
	@Autowired
	private VendorPlatterDAO vendorPlatterDAO;
	
	@Override
	@Transactional
	public VendorDish addVendorDish(VendorDish dish) {
		// TODO Auto-generated method stub
		updateSectionDish(dish);
		return vendorDishDAO.addVendorDish(dish);
	}
	
	 
	public void updateSectionDish(VendorDish dish) {
		List<SectionDishes> sectionDishes = vendorPlatterDAO.getSectionDishByDishId(dish.getDishId());
		if(sectionDishes.size()>0) {
			SectionDishes sectionDish = new SectionDishes();
			//sectionDish.setAlcoholic(dish.getAlcoholic());
			//sectionDish.setContents(dish.getContents());
			sectionDish.setCountryId(dish.getCountryId());
			sectionDish.setDescription(dish.getDescription());
			sectionDish.setDisabled(dish.getDisabled());
			sectionDish.setDishType(dish.getDishType());
			sectionDish.setDishTypeId(dish.getDishTypeId());
			sectionDish.setImageUrl(dish.getImageUrl());
			sectionDish.setDishId(dish.getDishId());
			sectionDish.setName(dish.getName());
			sectionDish.setPrice(dish.getPrice());
			//sectionDish.setQuantityInG(dish.getQuantityInG());
			sectionDish.setRectangularImageUrl(dish.getRectangularImageUrl());
			sectionDish.setShortDescription(dish.getShortDescription());
			//sectionDish.setVegetarian(dish.getVegetarian());
			sectionDish.setCouseDetailsURL(dish.getCourseDetailsURL());
			sectionDish.setOnline(dish.isOnline());
			sectionDish.setCourseDuration(dish.getCourseDuration());
			sectionDish.setCourseDurationInHr(dish.getCourseDurationInHr());
			System.out.println("---->"+sectionDish.getCourseDurationInHr());
			vendorPlatterDAO.saveSectionDish(sectionDish);
		}
		}
	
	@Override
	@Transactional
	public void updateMenuModificationTime(Integer dishId) {
		// TODO Auto-generated method stub
		vendorDishDAO.updateMenuModificationTime(dishId);
	}

	@Override
	@Transactional
	public List<VendorDish> listVendorDishByVendorId(Integer vendorId) {
		// TODO Auto-generated method stub
		return vendorDishDAO.listVendorDishByVendorId(vendorId);
	}

	@Override
	@Transactional
	public void removeVendorDish(Integer id) throws Exception {
		// TODO Auto-generated method stub
		vendorDishDAO.removeVendorDish(id);
	}

	@Override
	@Transactional
	public List<VendorDish> getVendorDishes(Integer[] ids) {
		// TODO Auto-generated method stub
		return vendorDishDAO.getVendorDishes(ids);
	}

	@Override
	@Transactional
	public VendorDish getVendorDish(Integer id) {
		// TODO Auto-generated method stub
		return vendorDishDAO.getVendorDish(id);
	}

	
	
}
