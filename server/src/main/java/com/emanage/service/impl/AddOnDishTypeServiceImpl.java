/**
 * 
 */
package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.AddOnDishTypeDAO;
import com.emanage.domain.AddOnDishType;
import com.emanage.service.AddOnDishTypeService;

/**
 * @author rahul
 *
 */
@Service
@Lazy
public class AddOnDishTypeServiceImpl implements  AddOnDishTypeService {

	@Autowired
	 AddOnDishTypeDAO dishTypeDAO;
	
	@Override
	@Transactional
	public void addDishType(AddOnDishType dishType) {
		dishTypeDAO.addDishType(dishType);
	}

	@Override
	@Transactional
	public List<AddOnDishType> listDishTypes() {
		return dishTypeDAO.listDishTypes();
	}

	@Override
	@Transactional
	public List<AddOnDishType> listDishTypesByRestaurant(Integer restaurantId) {
		return dishTypeDAO.listDishTypesByRestaurantId(restaurantId);
	}

	@Override
	@Transactional
	public void removeDishType(Integer id) {
		dishTypeDAO.removeDishType(id);
	}

	@Override
	@Transactional
	public AddOnDishType getDishType(Integer id) {
		return dishTypeDAO.getDishType(id);
	}

}
