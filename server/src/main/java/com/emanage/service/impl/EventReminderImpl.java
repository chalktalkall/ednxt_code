package com.emanage.service.impl;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.emanage.domain.CheckDishResponse;
import com.emanage.domain.Customer;
import com.emanage.domain.Invoice;
import com.emanage.domain.InvoiceResponse;
import com.emanage.domain.OrderAddOn;
import com.emanage.domain.Restaurant;
import com.emanage.domain.Vendor;
import com.emanage.enums.order.Status;
import com.emanage.service.CustomerService;
import com.emanage.service.EventReminder;
import com.emanage.service.InvoiceService;
import com.emanage.service.RestaurantService;
import com.emanage.service.TaxTypeService;
import com.emanage.service.VendorService;
import com.emanage.utility.StringUtility;

@Service
@Lazy
public class EventReminderImpl implements EventReminder {

	final static Logger logger = Logger.getLogger(EventReminderImpl.class);

	@Autowired
	InvoiceService invoice;

	@Autowired
	RestaurantService restService;

	@Autowired
	private JavaMailSenderImpl mailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Autowired
	private TaxTypeService taxTypeService;

	@Autowired
	private VendorService vendorService;

	@Autowired
	private CustomerService customerService;

	@Transactional
	public List<Invoice> invoiceList(int day) throws ParseException {
		// TODO Auto-generated method stub
		Restaurant restaurant = restService.getRestaurant(12); // India restaurant

		Date fromDate = null;
		Date toDate = null;
		DateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		formatter.setTimeZone(TimeZone.getTimeZone(restaurant.getTimeZone()));
		Calendar yesterday = Calendar.getInstance(TimeZone.getTimeZone(restaurant.getTimeZone()));
		yesterday.add(Calendar.DAY_OF_YEAR, day-1);
		yesterday.set(Calendar.HOUR_OF_DAY, 0);
		yesterday.set(Calendar.MINUTE, 0);
		yesterday.set(Calendar.SECOND, 0);
		yesterday.set(Calendar.MILLISECOND, 0);
		fromDate = yesterday.getTime();
		
		Calendar tomorrow = Calendar.getInstance(TimeZone.getTimeZone(restaurant.getTimeZone()));
		tomorrow.add(Calendar.DAY_OF_YEAR, day);
		tomorrow.set(Calendar.HOUR_OF_DAY, 0);
		tomorrow.set(Calendar.MINUTE, 0);
		tomorrow.set(Calendar.SECOND, 0);
		tomorrow.set(Calendar.MILLISECOND, 0);
		toDate = tomorrow.getTime();
		
		System.out.println(fromDate+"   :   "+toDate);
		List<Invoice> invoiceList = invoice.getDailyInvoice(restaurant.getRestaurantId(), fromDate, toDate);
		return invoiceList;
	}

	@Override
	public void beforeFiveDays() {
		// TODO Auto-generated method stub
		Integer days = 6;
		List<Invoice> invoiceList = null;
		try {
			invoiceList = invoiceList(days);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(invoiceList.size() + " 5 days");
		for (Invoice invoice : invoiceList) {
			reminderReadyState(invoice, "5");
		}
	}

	@Override
	public void beforeOneDays() {
		// TODO Auto-generated method stub
		Integer days = 2;
		List<Invoice> invoiceList = null;
		try {
			invoiceList = invoiceList(days);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(invoiceList.size() + " 1 days");
		for (Invoice invoice : invoiceList) {
			reminderReadyState(invoice, "1");
		}
	}

	@Override
	public void today() {
		int days = 1;
		List<Invoice> invoiceList = null;
		try {
			invoiceList = invoiceList(days);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(invoiceList.size() + " Today");
		for (Invoice invoice : invoiceList) {
			reminderReadyState(invoice, "Today");
		}

	}

	public void reminderReadyState(Invoice invoice, String day) {
		Customer customer = customerService.getCustomer(invoice.getCustomerId());
		Restaurant restaurant = restService.getRestaurant(invoice.getCountryId());
		try {
			sendEventNotificationEmail(customer, invoice, restaurant, "eventReminder", day);
		} catch (UnsupportedEncodingException | MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Transactional
	public void sendEventNotificationEmail(Customer customer, Invoice invoice, Restaurant rest, String templateName,
			String daysLeft) throws MessagingException, UnsupportedEncodingException {

		// Prepare the evaluation context
		if (customer != null && (!"".equalsIgnoreCase(customer.getEmail()) && customer.getEmail() != null)) {

			final Context ctx = new Context();
			logger.info("is Invoice null : " + invoice);
			String subject = "";
			logger.info("***********************sending check from default***********************************");
			if (invoice != null) {
				logger.info("invoiceId : " + invoice.getInvoiceNo());
				if("Today".equalsIgnoreCase(daysLeft)) {
					subject = rest.getBussinessName() + " : We're excited to serve you " + daysLeft + ". Event time "
							+ invoice.getDeliveryDateTime();
				}else {
					subject = rest.getBussinessName() + " : Reminder: No of days left for the event is " + daysLeft + ". Event time "
							+ invoice.getDeliveryDateTime();
				}
				
				Float waiveOff = null;
				InvoiceResponse checkResponse = new InvoiceResponse(invoice, taxTypeService, waiveOff, rest);

				templateName = "defaultemailbill";

				ctx.setVariable("checkRespone", checkResponse);
				if (invoice.getCustomerId() > 0) {
					String description = "";
					if (customer.getCredit() != null) {
						if (customer.getCredit().getCreditBalance() != 0) {
							// description =
							// setDescription(invoice.getOrders().get(0).getStatus(),customer,invoice,refund,reason,extraAmount,
							// rest);
						} else if (invoice.getOrders().get(0).getStatus() == Status.CANCELLED) {
							description = "Thanks for choosing  us";
						}
					} else {
						if (invoice.getOrders().get(0).getStatus() != Status.CANCELLED) {
							description = "Thanks for choosing  us";
						} else if (invoice.getOrders().get(0).getStatus() == Status.CANCELLED) {
							description = "Thanks for choosing us";
						}
					}

					ctx.setVariable("description", description);
					ctx.setVariable("customer", customer);
				}
				if (rest != null) {
					if (rest.isRoundOffAmount()) {
						invoice.setRoundOffTotal(Math.round(checkResponse.getRoundedOffTotal()));
						checkResponse.setRoundedOffTotal(Math.round(checkResponse.getRoundedOffTotal()));
					} else {
						invoice.setRoundOffTotal(checkResponse.getRoundedOffTotal());
					}
				}
				ctx.setVariable("user", rest);
				Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
				cal.setTime(invoice.getOpenTime());
				DateFormat formatter1;
				formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				formatter1.setTimeZone(cal.getTimeZone());
				ctx.setVariable("checkDate", formatter1.format(cal.getTime()));
				Map<String, com.emanage.domain.JsonDish> itemsMap = new TreeMap<String, com.emanage.domain.JsonDish>();
				List<CheckDishResponse> items = checkResponse.getItems();

				List<com.emanage.domain.JsonAddOn> jsonAdd = new ArrayList<com.emanage.domain.JsonAddOn>();
				for (CheckDishResponse item : items) {
					if (itemsMap.containsKey(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""))) {
						com.emanage.domain.JsonDish jsonDish = itemsMap
								.get(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""));
						jsonDish.setPrice(jsonDish.getPrice() + item.getPrice());
						jsonDish.setQuantity(jsonDish.getQuantity() + 1);
					} else {
						com.emanage.domain.JsonDish jsonDish = new com.emanage.domain.JsonDish();
						jsonDish.setQuantity(1);
						jsonDish.setName(item.getName());
						jsonDish.setId(item.getDishId());
						jsonDish.setPrice(item.getPrice());
						// jsonDish.setDishSize(item.getDishSize());
						System.out.println("seding section data");
						// System.out.println(item.getSections().length);
						if (item.getSections() != null) {
							jsonDish.setSection(Arrays.asList(item.getSections()));
						}
						List<OrderAddOn> orderAddOn = item.getAddOnresponse();
						if (orderAddOn != null) {
							for (OrderAddOn oad : orderAddOn) {
								com.emanage.domain.JsonAddOn jsonAddOn = new com.emanage.domain.JsonAddOn();
								jsonAddOn.setItemId(oad.getAddOnId());
								jsonAddOn.setDishId(item.getDishId());
								jsonAddOn.setName(oad.getName());
								jsonAddOn.setQuantity(oad.getQuantity());
								jsonAddOn.setPrice(oad.getPrice());
								jsonAddOn.setDishSize(oad.getDishSize());
								jsonAdd.add(jsonAddOn);
								jsonDish.setAddOns(jsonAdd);
							}
						}
						itemsMap.put(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""), jsonDish);
					}
				}

				ctx.setVariable("itemsMap", itemsMap);
			} else {
				logger.info("No check Found for " + customer.getEmail());
			}
			Vendor vendor = vendorService.getVendor(invoice.getVendorId());
			// Prepare message using a Spring helper
			final MimeMessage mimeMessage = mailSender.createMimeMessage();
			final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

			message.setSubject(subject);
			if (rest != null) {
				// String senderEmail = StringUtility.isNullOrEmpty(rest.getMailUsername()) ?
				// user.getUsername() : rest.getMailUsername();
				String senderEmail = rest.getMailUsername();
				InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
				message.setFrom(restaurantEmailAddress);
				message.setReplyTo(restaurantEmailAddress);
			}
			message.setTo(customer.getEmail());
			if (vendor != null && vendor.getAlertMail() != null) {
				message.setBcc(vendor.getAlertMail());
			}
			// Create the HTML body using Thymeleaf

			final String htmlContent = templateEngine.process(templateName, ctx);
			message.setText(htmlContent, true); // true = isHtml

			if (!StringUtility.isNullOrEmpty(rest.getMailUsername())
					&& !StringUtility.isNullOrEmpty(rest.getMailPassword())) {

				mailSender.setUsername(rest.getMailUsername());
				mailSender.setPassword(rest.getMailPassword());
				mailSender.setHost(rest.getMailHost());
				mailSender.setProtocol(rest.getMailProtocol());
				mailSender.setPort(rest.getMailPort());
			}

			mailSender.send(mimeMessage);
			logger.info("email sent for checkId :" + invoice.getInvoiceNo());
			logger.info(
					"***********************Email Sent Successfully sending end***********************************");
		}

	}

	@Override
	@Transactional
	public void eventReminder() {
		System.out.println("called cron job");
		today();
		beforeOneDays();
		beforeFiveDays();
	}

}
