package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.VendorDAO;
import com.emanage.domain.Vendor;
import com.emanage.service.VendorService;

@Service
@Lazy
public class VendorServiceImpl implements VendorService{

	@Autowired
	private VendorDAO vendorDAO;
	
	@Override
	@Transactional
	public Vendor addVendor(Vendor vendor) {
		// TODO Auto-generated method stub
		return vendorDAO.addVendor(vendor);
	}

	@Override
	@Transactional
	public List<Vendor> listVendor() {
		// TODO Auto-generated method stub
		return vendorDAO.listVendor();
	}

	@Override
	@Transactional
	public List<Vendor> listVendorByCountryId(Integer countryId) {
		// TODO Auto-generated method stub
		return vendorDAO.listVendorById(countryId);
	}

	@Override
	@Transactional
	public List<Vendor> listVendorByOrgId(Integer orgId) {
		// TODO Auto-generated method stub
		return vendorDAO.listVendorByOrgId(orgId);
	}

	@Override
	@Transactional
	public void removeVenor(Integer vendorId) {
		// TODO Auto-generated method stub
		vendorDAO.removeVendor(vendorId);
	}

	@Override
	@Transactional
	public Vendor getVendor(Integer vendorId) {
		// TODO Auto-generated method stub
		return vendorDAO.getVendor(vendorId);
	}

	@Override
	@Transactional
	public void saveVendor(Vendor vendor) {
		// TODO Auto-generated method stub
		vendorDAO.saveVendor(vendor);
	}

	@Override
	@Transactional
	public List<Vendor> listActiveVendorById(Integer countryId) {
		// TODO Auto-generated method stub
		return vendorDAO.listActiveVendorById(countryId);
	}


}
