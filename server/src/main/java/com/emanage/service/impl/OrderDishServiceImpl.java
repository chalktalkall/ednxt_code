/**
 * 
 */
package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.OrderDishDAO;
import com.emanage.service.OrderDishService;

/**
 * @author rahul
 *
 */
@Service
@Lazy
public class OrderDishServiceImpl implements OrderDishService {

	@Autowired
	private OrderDishDAO orderDishDAO;
	
	
	@Override
	@Transactional
	public void removeOrderDishes(List<Integer> orderDishIds) {
		orderDishDAO.removeOrderDishes(orderDishIds);

	}

}
