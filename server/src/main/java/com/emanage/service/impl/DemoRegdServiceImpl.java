package com.emanage.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.DemoRegdDAO;
import com.emanage.domain.DemoRegd;
import com.emanage.dto.RegResultDTO;
import com.emanage.service.CustomerService;
import com.emanage.service.DemoRegdService;
import com.emanage.service.VendorService;

@Service
public class DemoRegdServiceImpl implements DemoRegdService{

	@Autowired
	DemoRegdDAO demoDAO;
	
	@Autowired
	VendorService vendorService;
	
	@Autowired
	CustomerService customerService;
	
	@Override
	@Transactional
	public void addDemoRegd(DemoRegd demoRegd) {
		// TODO Auto-generated method stub
		demoDAO.addDemoRegd(demoRegd);
		
	}

	@Override
	@Transactional
	public List<DemoRegd> listDemoRegd() {
		// TODO Auto-generated method stub
		return demoDAO.listDemoRegd();
	}

	@Override
	@Transactional
	public List<DemoRegd> listDemoRegd(Integer vendorId) {
		// TODO Auto-generated method stub
		return demoDAO.listDemoRegd(vendorId);
	}

	@Override
	@Transactional
	public void removeDemoRegd(Integer id) throws Exception {
		// TODO Auto-generated method stub
		demoDAO.removeDemoRegd(id);
	}

	@Override
	@Transactional
	public List<DemoRegd> getDemoRegd(Integer[] ids) {
		// TODO Auto-generated method stub
		return demoDAO.getDemoRegd(ids);
	}

	@Override
	@Transactional
	public DemoRegd getDemoRegd(Integer id) {
		// TODO Auto-generated method stub
		return demoDAO.getDemoRegd(id);
	}

	@Override
	@Transactional
	public List<RegResultDTO> listDemoByUserId(Integer customerId) {
		// TODO Auto-generated method stub
		return demoDAO.listDemoByUserId(customerId);
	}

}
