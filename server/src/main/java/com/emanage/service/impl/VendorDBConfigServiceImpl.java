package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.VendorDBConfigDAO;
import com.emanage.domain.DBConfig;
import com.emanage.service.VendorDBConfigService;

@Service
public class VendorDBConfigServiceImpl implements VendorDBConfigService {

	@Autowired
	VendorDBConfigDAO vendorDBConfigDAO;
	
	@Override
	@Transactional
	public DBConfig addDBConfig(DBConfig dbConfig) {
		// TODO Auto-generated method stub
		return vendorDBConfigDAO.addDBConfig(dbConfig);
	}

	@Override
	@Transactional
	public DBConfig getDBConfig(Integer dbConfigId) {
		// TODO Auto-generated method stub
		return vendorDBConfigDAO.getDBConfig(dbConfigId);
	}

	@Override
	@Transactional
	public List<DBConfig> listDBConfigByCountryId(Integer univId) {
		// TODO Auto-generated method stub
		return vendorDBConfigDAO.listDBConfigByCountryId(univId);
	}

	@Override
	@Transactional
	public void removeDBConfig(Integer dbConfigId) {
		// TODO Auto-generated method stub
		vendorDBConfigDAO.removeDBConfig(dbConfigId);
	}

	@Override	
	@Transactional
	public DBConfig getCampusDBConfig(Integer campusId) {
		// TODO Auto-generated method stub
		return vendorDBConfigDAO.getCampusDBConfig(campusId);
	}

	
		
}
