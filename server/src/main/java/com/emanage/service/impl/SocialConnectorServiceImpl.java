package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.SocialConnectorDAO;
import com.emanage.domain.SocialConnector;
import com.emanage.service.SocialConnectorService;

@Service
public class SocialConnectorServiceImpl implements SocialConnectorService {

	
	@Autowired
	private SocialConnectorDAO socialConnectorDAO;
	
	@Override
	@Transactional
	public void addSocialConnector(SocialConnector connector) {
		// TODO Auto-generated method stub
		socialConnectorDAO.addSocialConnector(connector);
	}

	@Override
	@Transactional
	public List<SocialConnector> listSocialConnectorByOrgId(Integer orgId) {
		// TODO Auto-generated method stub
		return socialConnectorDAO.listSocialConnectorByOrgId(orgId);
	}

	@Override
	@Transactional
	public void removeSocialConnector(Integer id) throws Exception {
		// TODO Auto-generated method stub
		socialConnectorDAO.removeSocialConnector(id);
	}


	@Override
	@Transactional
	public SocialConnector getSocialConnector(Integer id) {
		// TODO Auto-generated method stub
		return socialConnectorDAO.getSocialConnector(id);
	}

}
