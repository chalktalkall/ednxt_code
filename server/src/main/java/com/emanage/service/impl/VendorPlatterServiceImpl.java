package com.emanage.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.constants.PlatterType;
import com.emanage.dao.VendorPlatterDAO;
import com.emanage.domain.BookedPlatter;
import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.CuisineMenu;
import com.emanage.domain.CuisineType;
import com.emanage.domain.Platter;
import com.emanage.domain.PlatterMenuWrapper;
import com.emanage.domain.Restaurant;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.Sections;
import com.emanage.domain.Vendor;
import com.emanage.dto.CourseWrapper;
import com.emanage.dto.LocationLatLong;
import com.emanage.enums.Status;
import com.emanage.service.CanvasLMSService;
import com.emanage.service.CuisineNDishService;
import com.emanage.service.RestaurantService;
import com.emanage.service.VendorPlatterService;
import com.emanage.service.VendorService;
import com.emanage.utility.DateUtil;

@Service
public class VendorPlatterServiceImpl implements VendorPlatterService {

	@Autowired
	private VendorPlatterDAO vendorPlatterDAO;
	
	@Autowired
	CuisineNDishService cndService;
	
	@Autowired
	RestaurantService restService;

	@Autowired
	VendorService vendorService;
	
	@Autowired
	CanvasLMSService canvasLMSService;

	@Override
	@Transactional
	public void addPlatter(Platter menu) {
		vendorPlatterDAO.addPlatter(menu);
	} 

	@Override
	@Transactional
	public List<Platter> listPlattersByVendorId(Integer vendorId) {
		return vendorPlatterDAO.listPlattersByVendorId(vendorId);
	}

	@Override
	@Transactional
	public List<Platter> allPlattersByStatus(Integer vendorId, Status status) {
		return vendorPlatterDAO.allPlattersByStatus(vendorId, status);
	}

	@Override
	@Transactional
	public Platter getPlatter(Integer id) {
		Platter platter = vendorPlatterDAO.getPlatter(id);
		for (Sections section : platter.getSections()) {

			for (SectionDishes dish : section.getDishes()) {
				if(dish!=null) {
				if (dish.getReplacementList() != null && dish.getReplacementList().length > 0) {
					StringBuffer nameString = new StringBuffer();
					
					for (String repDishId : dish.getReplacementList()) {
						if (repDishId != null && repDishId != "") {
							Integer dishId = Integer.parseInt(repDishId);

							for (SectionDishes dishInner : section.getDishes()) {
								if(dishInner!=null) {
								if ( dishId.toString().equalsIgnoreCase(dishInner.getDishId().toString())) {
									nameString.append(dishInner.getName() + " | ");
								}
								}
							}

						}

					}
					
					if (PlatterType.MAIN_PLATTER.name().equalsIgnoreCase(section.getPlatterSectionType().name())) {
						dish.setReplacementNames(nameString.toString());
					}
				}
			}
			}
		}

		return platter;
	}

	@Override
	@Transactional
	public void removePlatter(Integer id) {
		vendorPlatterDAO.removePlatter(id);
	}

	@Override
	@Transactional
	public List<Platter> listPlatters() {
		return vendorPlatterDAO.listPlatters();
	}

	@Override
	@Transactional
	public List<Platter> allPosPlatters(Integer countryId, Status status, boolean posStatus) {
		return vendorPlatterDAO.allPosPlatters(countryId, status, posStatus);
	}

	@Override
	@Transactional
	public List<Platter> listPlattersByCountry(Integer countryId) {
		return vendorPlatterDAO.listPlattersByCountry(countryId);
	}

	@Override
	@Transactional
	public Platter getPlatterByPlatterName(String name, Integer restaurantId) {
		return vendorPlatterDAO.getPlatterByPlatterName(name, restaurantId);
	}

	@Override
	@Transactional
	public void removeSections(List<Integer> sectionIds) {
		vendorPlatterDAO.removeSections(sectionIds);
	}

	@Override
	@Transactional
	public void addSection(Sections section) {
		// TODO Auto-generated method stub
		vendorPlatterDAO.addSection(section);
	}

	@Override
	@Transactional
	public List<Platter> getPlatterByLatlong(double latitud, double longitude,Date date, Integer plateCount) {
		return vendorPlatterDAO.getPlatterByLatlong(latitud, longitude, date, plateCount);
	}
	
//	@Override
//	@Transactional
//	public List<Platter> getCoursesByLatlong(double latitud, double longitude,
//			Integer[] courseTypeArray) {
//		String[] courseType= new String[courseTypeArray.length];
//		for(Integer i=0; i< courseTypeArray.length;i++) {
//			//int id =courseTypeArray[i];
//			CuisineType ct = cndService.getCuisineType(courseTypeArray[i]);
//			if(ct!=null) {
//				courseType[i]=ct.getName();
//			}
//		}
//		return vendorPlatterDAO.getCoursesByLatlong(latitud, longitude, courseType);
//	}

	@Override
	@Transactional
	public SectionDishes getSectionDish(Integer id) {
		// TODO Auto-generated method stub
		return (SectionDishes) vendorPlatterDAO.getSectionDish(id);
	}
	@Override
	@Transactional
	public void saveSectionDish(SectionDishes sectionDish) {
		// TODO Auto-generated method stub
		vendorPlatterDAO.saveSectionDish(sectionDish);
	}

	@Override
	@Transactional
	public void addPlatterBooking(BookedPlatter platter) {
		// TODO Auto-generated method stub
		vendorPlatterDAO.addPlatterBooking(platter);
	}

	@Override
	@Transactional
	public List<BookedPlatter> listBookedPlattersByVendorIdAndDate(Integer vendorId, Date date) {
		// TODO Auto-generated method stub
		return vendorPlatterDAO.listBookedPlattersByVendorIdAndDate(vendorId,date);
	}

	@Override
	@Transactional
	public Integer getBookedPlatterCountByDateandId(Integer vendorId, Date date) {
		// TODO Auto-generated method stub
		return vendorPlatterDAO.getBookedPlatterCountByDateandId(vendorId, date);
	}

	@Override
	@Transactional
	public Integer getVendorIdUsingPlatterId(Integer id) {
		// TODO Auto-generated method stub
		return vendorPlatterDAO.getVendorIdUsingPlatterId(id);
	}

	@Override
	@Transactional
	public List<CuisineMenu>  getCoursesByLatlong(LocationLatLong locationLatLong) {
		// TODO Auto-generated method stub
		
		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		if (locationLatLong != null) {
			
			String[] courseType= new String[locationLatLong.courseType.length];
			for(Integer i=0; i< locationLatLong.courseType.length;i++) {
				CuisineType ct = cndService.getCuisineType(locationLatLong.courseType[i]);
				if(ct!=null) {
					courseType[i]=ct.getName();
				}
			}
			
			double latitude = locationLatLong.latitude;
			double longitude = locationLatLong.longnitude;

			HashSet<String> cuisineList = new HashSet<String>();
			List<Platter> platterList = vendorPlatterDAO.getCoursesByLatlong(latitude, longitude, courseType,locationLatLong.classMode);
			
			if (platterList != null && platterList.size() > 0) {
				Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
				for (Platter platter : platterList) {
					cuisineList.add(platter.getTagList());
				}
				for (String type : courseType) {
					List<PlatterMenuWrapper> menuWrappers = new ArrayList<PlatterMenuWrapper>();
					CuisineMenu cuisineClass = new CuisineMenu();

					for (Platter platter : platterList) {

						if (platter!=null && platter.getTagList().trim().toLowerCase().indexOf(type.trim().toLowerCase()) !=-1) {
							menuWrappers.add(PlatterMenuWrapper.getMenuWrapper(platter, country.getTimeZone()));
						}
					}
					cuisineClass.setName(type);
					cuisineClass.setMenus(menuWrappers);
					cus.add(cuisineClass);
				}
			}
			return cus;
		}
		return cus;
	}
	@Override
	@Transactional
	public CuisineMenu getCourseById(Integer courseId) {
		// TODO Auto-generated method stub
		CuisineMenu cusineMenu = new CuisineMenu();
		Canvas_Courses canvasCourse  = canvasLMSService.getCourseById(courseId);
		List<CourseWrapper> menuWrappers = new ArrayList<CourseWrapper>();

		Vendor vendor =  vendorService.getVendor(canvasCourse.getVendorId());
		if (canvasCourse!=null) {
			menuWrappers.add(new CourseWrapper(canvasCourse, vendor));
		}
		
		cusineMenu.setName("On Demand");
		cusineMenu.setCourses(menuWrappers);
		cusineMenu.setCountryId(vendor.getCountryId());
		if(menuWrappers.size()==0) {
			return null;
		}else {
		return cusineMenu;
		}
	}
	
	@Override
	@Transactional
	public CuisineMenu getPlatterByPlatterId(Integer platterId) {
		// TODO Auto-generated method stub
		CuisineMenu cusineMenu = new CuisineMenu();
		Platter platter = vendorPlatterDAO.getPlatter(platterId);
		Vendor vend =  vendorService.getVendor(platter.getVendorId());
		List<PlatterMenuWrapper> menuWrappers = new ArrayList<PlatterMenuWrapper>();
		if (platter!=null) {
			try {
				
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date dateobj = new Date();
				String currenntDate = df.format(dateobj);

				if(platter.getValidity()!=null && "1000-01-01 00:00:00".contains(platter.getValidity())) {
					platter.setValidity(null);
					platter.setBusinessPotrateImageURL(vend.getBusinessPortraitImageUrl());
					platter.setBusinessDescription(vend.getDescription());
					List<String> imageURL = new ArrayList<String>();
					imageURL.add(vend.getInstituteImageURLF());
					imageURL.add(vend.getInstituteImageURLS());
					imageURL.add(vend.getInstituteImageURLT());
					imageURL.add(vend.getInstituteImageURLFO());
					imageURL.add(vend.getInstituteImageURLFI());
					platter.setInstituteImages(imageURL);
					platter.setCertificate(vend.isCertificate());
					platter.setCertifiedTrainer(vend.isCertifiedTrainer());
					platter.setPlacementAssistance(vend.isPlacementAssistance());
					platter.setMetroStation(vend.isMetroStation());
					platter.setInstituteName(vend.getBusinessName());
					platter.setBusinessIntro(vend.getIntroduction());
					platter.setAssessmentKit(vend.isAssessmentKit());
					platter.setEbook(vend.isEbook());
					
					if(platter.getVendorMargin()==null || platter.getVendorMargin()==0)
						platter.setVendorMargin(vend.getVendorMargin());
					platter.setRating(vend.getRating());
				}
				else if(platter.getValidity()!=null && DateUtil.compareDate(platter.getValidity(),currenntDate)) {
					
					platter.setBusinessPotrateImageURL(vend.getBusinessPortraitImageUrl());
					platter.setBusinessDescription(vend.getDescription());
					List<String> imageURL = new ArrayList<String>();
					
					if(vend.getInstituteImageURLF()!=null && vend.getInstituteImageURLF().trim().length()>0)
						imageURL.add(vend.getInstituteImageURLF());
					
					if(vend.getInstituteImageURLS()!=null && vend.getInstituteImageURLS().trim().length()>0)
						imageURL.add(vend.getInstituteImageURLS());
						
					if(vend.getInstituteImageURLT()!=null && vend.getInstituteImageURLT().trim().length()>0)
						imageURL.add(vend.getInstituteImageURLT());
					
					if(vend.getInstituteImageURLFO()!=null && vend.getInstituteImageURLFO().trim().length()>0)
						imageURL.add(vend.getInstituteImageURLFO());
					
					if(vend.getInstituteImageURLFI()!=null && vend.getInstituteImageURLFI().trim().length()>0)
						imageURL.add(vend.getInstituteImageURLFI());
					
					platter.setInstituteImages(imageURL);
					platter.setCertificate(vend.isCertificate());
					platter.setCertifiedTrainer(vend.isCertifiedTrainer());
					platter.setPlacementAssistance(vend.isPlacementAssistance());
					platter.setMetroStation(vend.isMetroStation());
					platter.setAssessmentKit(vend.isAssessmentKit());
					platter.setEbook(vend.isEbook());
					
					platter.setInstituteName(vend.getBusinessName());
					platter.setBusinessIntro(vend.getIntroduction());
					
					if(platter.getVendorMargin() ==null || platter.getVendorMargin()==0)
						platter.setVendorMargin(vend.getVendorMargin());
					platter.setRating(vend.getRating());
				}
				menuWrappers.add(PlatterMenuWrapper.getMenuWrapper(platter,null));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		
		
		cusineMenu.setName("On Demand");
		cusineMenu.setMenus(menuWrappers);
		cusineMenu.setCountryId(vend.getCountryId());
		if(menuWrappers.size()==0) {
			return null;
		}else {
		return cusineMenu;
		}
	}

	@Override
	@Transactional
	public List<CuisineMenu> getCoursesByInstituteId(Integer instituteId) {
		// TODO Auto-generated method stub
		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		HashSet<String> cuisineList = new HashSet<String>();
		List<Canvas_Courses> platterList = canvasLMSService.listCoursesByCollegeId(instituteId);
		Vendor vendor =  vendorService.getVendor(instituteId);
		if (platterList != null && platterList.size() > 0) {
			//Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
			for (Canvas_Courses platter : platterList) {   
				cuisineList.add(platter.getTagList());
			}
				List<CourseWrapper> menuWrappers = new ArrayList<CourseWrapper>();
				CuisineMenu cuisineClass = new CuisineMenu();

				for (Canvas_Courses courses : platterList) {
					if (courses!=null) {
						menuWrappers.add(new CourseWrapper(courses, vendor));
					}
					
				}
				cuisineClass.setName(vendor.getBusinessName());
				cuisineClass.setCourses(menuWrappers);
				cuisineClass.setCountryId(vendor.getCountryId());
				cus.add(cuisineClass);
			}
		
		return cus;
	}
	
	@Override
	@Transactional
	public List<CuisineMenu> getFeatureCourses() {
		// TODO Auto-generated method stub
		
		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		HashSet<String> cuisineList = new HashSet<String>();
		List<Platter> platterList = vendorPlatterDAO.listFetureCourses();
		if (platterList != null && platterList.size() > 0) {
			//Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
			for (Platter platter : platterList) {   
				cuisineList.add(platter.getTagList());
			}
				List<PlatterMenuWrapper> menuWrappers = new ArrayList<PlatterMenuWrapper>();
				CuisineMenu cuisineClass = new CuisineMenu();

				for (Platter platter : platterList) {
					Vendor vend =  vendorService.getVendor(platter.getVendorId());
					if (platter!=null) {
						menuWrappers  = getPlatterUpdated(platter,vend, menuWrappers);
					}
					
				}
				cuisineClass.setName("Feature Courses");
				cuisineClass.setMenus(menuWrappers);
				cus.add(cuisineClass);
			}
		return cus;
	}
	
	@Override
	@Transactional
	public List<CuisineMenu> getFeatureTrainings() {
		// TODO Auto-generated method stub
		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		HashSet<String> cuisineList = new HashSet<String>();
		List<Platter> platterList = vendorPlatterDAO.listFetureTrainings();
		if (platterList != null && platterList.size() > 0) {
			//Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
			for (Platter platter : platterList) {   
				cuisineList.add(platter.getTagList());
			}
				List<PlatterMenuWrapper> menuWrappers = new ArrayList<PlatterMenuWrapper>();
				CuisineMenu cuisineClass = new CuisineMenu();
				for (Platter platter : platterList) {
					Vendor vend =  vendorService.getVendor(platter.getVendorId());
					if (platter!=null) {
						menuWrappers  = getPlatterUpdated(platter,vend, menuWrappers);
					}
				}
				cuisineClass.setName("Feature Courses");
				cuisineClass.setMenus(menuWrappers);
				cus.add(cuisineClass);
			}
		return cus;
	}
	
	static List<PlatterMenuWrapper> getPlatterUpdated(Platter platter, Vendor vend,List<PlatterMenuWrapper> menuWrappers ) {
		
		try {
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date dateobj = new Date();
			String currenntDate = df.format(dateobj);

			if(platter.getValidity()!=null && "1000-01-01 00:00:00".contains(platter.getValidity())) {
				platter.setValidity(null);
				platter.setBusinessPotrateImageURL(vend.getBusinessPortraitImageUrl());
				platter.setBusinessDescription(vend.getDescription());
				List<String> imageURL = new ArrayList<String>();
				imageURL.add(vend.getInstituteImageURLF());
				imageURL.add(vend.getInstituteImageURLS());
				imageURL.add(vend.getInstituteImageURLT());
				imageURL.add(vend.getInstituteImageURLFO());
				imageURL.add(vend.getInstituteImageURLFI());
				platter.setInstituteImages(imageURL);
				
				platter.setCertificate(vend.isCertificate());
				platter.setCertifiedTrainer(vend.isCertifiedTrainer());
				platter.setPlacementAssistance(vend.isPlacementAssistance());
				platter.setMetroStation(vend.isMetroStation());
				platter.setAssessmentKit(vend.isAssessmentKit());
				platter.setEbook(vend.isEbook());
				
				platter.setInstituteName(vend.getBusinessName());
				platter.setBusinessIntro(vend.getIntroduction());     
				if(platter.getVendorMargin()==null || platter.getVendorMargin()==0)
					platter.setVendorMargin(vend.getVendorMargin());
				platter.setRating(vend.getRating());
				menuWrappers.add(PlatterMenuWrapper.getMenuWrapper(platter,null));
			}
			else if(platter.getValidity()!=null && DateUtil.compareDate(platter.getValidity(),currenntDate)) {
				
				platter.setBusinessPotrateImageURL(vend.getBusinessPortraitImageUrl());
				platter.setBusinessDescription(vend.getDescription());
				List<String> imageURL = new ArrayList<String>();
				
				if(vend.getInstituteImageURLF()!=null && vend.getInstituteImageURLF().trim().length()>0)
					imageURL.add(vend.getInstituteImageURLF());
				
				if(vend.getInstituteImageURLS()!=null && vend.getInstituteImageURLS().trim().length()>0)
					imageURL.add(vend.getInstituteImageURLS());
					
				if(vend.getInstituteImageURLT()!=null && vend.getInstituteImageURLT().trim().length()>0)
					imageURL.add(vend.getInstituteImageURLT());
				
				if(vend.getInstituteImageURLFO()!=null && vend.getInstituteImageURLFO().trim().length()>0)
					imageURL.add(vend.getInstituteImageURLFO());
				
				if(vend.getInstituteImageURLFI()!=null && vend.getInstituteImageURLFI().trim().length()>0)
					imageURL.add(vend.getInstituteImageURLFI());
				
				platter.setInstituteImages(imageURL);
				platter.setCertificate(vend.isCertificate());
				platter.setCertifiedTrainer(vend.isCertifiedTrainer());
				platter.setPlacementAssistance(vend.isPlacementAssistance());
				platter.setMetroStation(vend.isMetroStation());
				platter.setAssessmentKit(vend.isAssessmentKit());
				platter.setEbook(vend.isEbook());
				platter.setInstituteName(vend.getBusinessName());
				platter.setBusinessIntro(vend.getIntroduction());
				
				
				if(platter.getVendorMargin() ==null || platter.getVendorMargin()==0)
					platter.setVendorMargin(vend.getVendorMargin());
				platter.setRating(vend.getRating());
				menuWrappers.add(PlatterMenuWrapper.getMenuWrapper(platter,null));
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return menuWrappers;
	}

	@Override
	@Transactional
	public List<CuisineMenu> getActiveWebinars() {
		// TODO Auto-generated method stub
		
		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		HashSet<String> cuisineList = new HashSet<String>();
		List<Platter> platterList = vendorPlatterDAO.getActiveWebinars();
	
		if (platterList != null && platterList.size() > 0) {
			
			//Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
			for (Platter platter : platterList) {   
				cuisineList.add(platter.getTagList());
			}
				List<PlatterMenuWrapper> menuWrappers = new ArrayList<PlatterMenuWrapper>();
				CuisineMenu cuisineClass = new CuisineMenu();

				for (Platter platter : platterList) {
					
					if (platter!=null) {
						Vendor vend =  vendorService.getVendor(platter.getVendorId());
						menuWrappers  = getPlatterUpdated(platter,vend, menuWrappers);
					}
					
				}
				cuisineClass.setName("Webinars");
				cuisineClass.setMenus(menuWrappers);
				cus.add(cuisineClass);
			}
		
		return cus;
	}

	@Override
	@Transactional
	public List<CuisineMenu> getAllCourseByCriteria(LocationLatLong locationLatLong) {

		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		if (locationLatLong != null) {
			
			String[] courseType= new String[locationLatLong.courseType.length];
			for(Integer i=0; i< locationLatLong.courseType.length;i++) {
				CuisineType ct = cndService.getCuisineType(locationLatLong.courseType[i]);
				if(ct!=null) {
					courseType[i]=ct.getName();
				}
			}
			
			HashSet<String> cuisineList = new HashSet<String>();
			List<Platter> platterList = vendorPlatterDAO.getCoursesByCriteria(courseType,locationLatLong.classMode,locationLatLong.plateCount);
			
			if (platterList != null && platterList.size() > 0) {
					for (Platter platter : platterList) {   
						cuisineList.add(platter.getTagList());
					}
					List<PlatterMenuWrapper> menuWrappers = new ArrayList<PlatterMenuWrapper>();
					CuisineMenu cuisineClass = new CuisineMenu();

					for (Platter platter : platterList) {
						Vendor vend =  vendorService.getVendor(platter.getVendorId());
						if (platter!=null) {
							menuWrappers  = getPlatterUpdated(platter,vend, menuWrappers);
						}
					}
					cuisineClass.setName("Trainings");
					cuisineClass.setMenus(menuWrappers);
					cus.add(cuisineClass);
			}
			return cus;
		}
		return cus;
	}
	
	@Override
	@Transactional
	public List<CuisineMenu> getAllCourseByCriteriaNew(LocationLatLong locationLatLong) {

		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		if (locationLatLong != null && locationLatLong.courseType!=null) {
			String[] courseType= new String[locationLatLong.courseType.length];
			for(Integer i=0; i< locationLatLong.courseType.length;i++) {
				CuisineType ct = cndService.getCuisineType(locationLatLong.courseType[i]);
				if(ct!=null) {
					courseType[i]=ct.getName();
				}
			}
			
			HashSet<String> cuisineList = new HashSet<String>();
			List<Canvas_Courses> canvasCourses =  new ArrayList<Canvas_Courses>();
			if(locationLatLong.universityId!=null&& locationLatLong.universityId>0) {
				List<Vendor> vendorList= vendorService.listActiveVendorById(locationLatLong.universityId);
				for(Vendor vendor :vendorList) {
					canvasCourses.addAll(vendorPlatterDAO.getCoursesByCriteriaCampusId(courseType, vendor.getVendorId()));
				}
			}else {
				List<Vendor> vendorList=vendorService.listVendorByOrgId(locationLatLong.orgId);
				for(Vendor vendor :vendorList) {
					canvasCourses.addAll(vendorPlatterDAO.getCoursesByCriteriaCampusId(courseType, vendor.getVendorId()));
				}
			}
			//List<Platter> platterList = vendorPlatterDAO.getCoursesByCriteria(courseType,locationLatLong.classMode,locationLatLong.plateCount);
			
			if (canvasCourses != null && canvasCourses.size() > 0) {
					for (Canvas_Courses course : canvasCourses) {   
						cuisineList.add(course.getTagList());
					}
					List<CourseWrapper> menuWrappers = new ArrayList<CourseWrapper>();
					CuisineMenu cuisineClass = new CuisineMenu();

					for (Canvas_Courses course : canvasCourses) {
						Vendor vendor =  vendorService.getVendor(course.getVendorId());
						if (course!=null) {
							menuWrappers.add(new CourseWrapper(course, vendor));
						}
					}
					 
					cuisineClass.setName("Result");
					cuisineClass.setCourses(menuWrappers);
					cus.add(cuisineClass);
			}
			return cus;
		}
		return cus;
	}
	
	@Override
	@Transactional
	public List<CuisineMenu> getFeaturedCanvasCourses(Integer universityId) {
		// TODO Auto-generated method stub
		
		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		HashSet<String> cuisineList = new HashSet<String>();
		List<Canvas_Courses> platterList =new ArrayList<Canvas_Courses>(); 

		if(universityId!=null&& universityId>0) {
			List<Vendor> vendorList= vendorService.listActiveVendorById(universityId);
			for(Vendor vendor :vendorList) {
				platterList.addAll(vendorPlatterDAO.listFeturedCanvasCourses(vendor.getVendorId()));
			}
		}else {
			platterList.addAll(vendorPlatterDAO.listFeturedCanvasCourses());
		}
		
		if (platterList != null && platterList.size() > 0) {
			//Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
			for (Canvas_Courses platter : platterList) {   
				cuisineList.add(platter.getTagList());
			}
				List<CourseWrapper> menuWrappers = new ArrayList<CourseWrapper>();
				CuisineMenu cuisineClass = new CuisineMenu();

				for (Canvas_Courses courses : platterList) {
					Vendor vendor =  vendorService.getVendor(courses.getVendorId());
					if (courses!=null) {
						menuWrappers.add(new CourseWrapper(courses, vendor));
					}
					
				}
				cuisineClass.setName("Feature Courses");
				cuisineClass.setCourses(menuWrappers);
				cus.add(cuisineClass);
			}
		return cus;
	}

	@Override
	@Transactional
	public List<CuisineMenu> getPopularCourses() {
		// TODO Auto-generated method stub
		List<CuisineMenu> cus = new ArrayList<CuisineMenu>();
		HashSet<String> cuisineList = new HashSet<String>();
		List<Canvas_Courses> platterList =new ArrayList<Canvas_Courses>(); 

		
		platterList.addAll(vendorPlatterDAO.getPopularCourses());
		if (platterList != null && platterList.size() > 0) {
			//Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
			for (Canvas_Courses platter : platterList) {   
				cuisineList.add(platter.getTagList());
			}
				List<CourseWrapper> menuWrappers = new ArrayList<CourseWrapper>();
				CuisineMenu cuisineClass = new CuisineMenu();

				for (Canvas_Courses courses : platterList) {
					Vendor vendor =  vendorService.getVendor(courses.getVendorId());
					if (courses!=null) {
						menuWrappers.add(new CourseWrapper(courses, vendor));
					}
					
				}
				cuisineClass.setName("Feature Courses");
				cuisineClass.setCourses(menuWrappers);
				cus.add(cuisineClass);
			}
		return cus;
	}
}
