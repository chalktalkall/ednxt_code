package com.emanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.emanage.dao.CuisineNDishDAO;
import com.emanage.domain.CountryDishType;
import com.emanage.domain.CountrySectionType;
import com.emanage.domain.CuisineType;
import com.emanage.service.CuisineNDishService;

@Service
@Lazy
public class CuisineNDishServiceImpl implements CuisineNDishService {

	@Autowired
	private CuisineNDishDAO cuisineNdDishDAO;

	// Calling Cuisine Type DAO

	@Override
	@Transactional
	public void addCuisineType(CuisineType cuisineType) {
		cuisineNdDishDAO.addCuisineType(cuisineType);
	}

	@Override
	@Transactional
	public List<CuisineType> listCuisineTypes(Integer universityId) {
		if(universityId!=null && universityId>0) {
			return cuisineNdDishDAO.listCuisineTypesByUniversity(universityId);
		}else {
			return cuisineNdDishDAO.listCuisineTypes();
		}
	}

	@Override
	@Transactional
	public List<CuisineType> listCuisineTypesByCountryId(Integer countryId) {
		return cuisineNdDishDAO.listCuisineTypesByCountryId(countryId);
	}

	@Override
	@Transactional
	public void removeCuisineType(Integer couisineId) {
		cuisineNdDishDAO.removeCuisineType(couisineId);
	}

	@Override
	@Transactional
	public CuisineType getCuisineType(Integer couisineId) {
		return cuisineNdDishDAO.getCuisineType(couisineId);
	}

	// Now calling DishType DAO

	@Override
	@Transactional
	public void addDishType(CountryDishType dishType) {
		cuisineNdDishDAO.addDishType(dishType);
	}

	@Override
	@Transactional
	public List<CountryDishType> listDishTypes() {
		return cuisineNdDishDAO.listDishTypes();
	}

	@Override
	@Transactional
	public List<CountryDishType> listDishTypesByCountryId(Integer countryId) {
		return cuisineNdDishDAO.listDishTypesByCountryId(countryId);
	}

	@Override
	@Transactional
	public void removeDishType(Integer tagId) {
		cuisineNdDishDAO.removeDishType(tagId);
	}

	@Override
	@Transactional
	public CountryDishType getDishType(Integer dishId) {
		return cuisineNdDishDAO.getDishType(dishId);
	}

	@Override
	@Transactional
	public void addSectionType(CountrySectionType sectionType) {
		// TODO Auto-generated method stub
		cuisineNdDishDAO.addSectionType(sectionType);
	}

	@Override
	@Transactional
	public List<CountrySectionType> listSectionTypes() {
		// TODO Auto-generated method stub
		return cuisineNdDishDAO.listSectionTypes();
	}

	@Override
	@Transactional
	public List<CountrySectionType> listSectionTypesByCountryId(Integer countryId) {
		// TODO Auto-generated method stub
		return cuisineNdDishDAO.listSectionTypesByCountryId(countryId);
	}

	@Override
	@Transactional
	public void removeSectionType(Integer sectionId) {
		// TODO Auto-generated method stub
		cuisineNdDishDAO.removeSectionType(sectionId);
	}

	@Override
	@Transactional
	public CountrySectionType getSectionType(Integer sectionTypeId) {
		// TODO Auto-generated method stub
		return cuisineNdDishDAO.getSectionType(sectionTypeId);
	}

	@Override
	public CuisineType getCuisineTypeByName(String name) {
		// TODO Auto-generated method stub
		return cuisineNdDishDAO.getCuisineTypeByName(name);
	}

}
