/**
 * 
 */
package com.emanage.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.emanage.dao.CustomerDAO;
import com.emanage.dao.InvoiceDAO;
import com.emanage.dao.OrderDAO;
import com.emanage.domain.Coupon;
import com.emanage.domain.Customer;
import com.emanage.domain.CustomerCreditBill;
import com.emanage.domain.Dish_Size;
import com.emanage.domain.Invoice;
import com.emanage.domain.JsonAddOn;
import com.emanage.domain.Order;
import com.emanage.domain.OrderAddOn;
import com.emanage.domain.OrderDish;
import com.emanage.domain.Order_DCList;
import com.emanage.domain.Platter;
import com.emanage.domain.Restaurant;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.User;
import com.emanage.domain.Vendor;
import com.emanage.dto.DemoRegDTO;
import com.emanage.dto.DiscountDTO;
import com.emanage.dto.OrderDTO;
import com.emanage.dto.OrderDishDTO;
import com.emanage.dto.Order_SectionDTO;
import com.emanage.enums.credit.CustomerCreditAccountStatus;
import com.emanage.enums.order.Status;
import com.emanage.service.CouponService;
import com.emanage.service.CustomerService;
import com.emanage.service.DishTypeService;
import com.emanage.service.OrderService;
import com.emanage.service.RestaurantService;
import com.emanage.service.UserService;
import com.emanage.service.VendorPlatterService;
import com.emanage.service.VendorService;
import com.emanage.utility.DateUtil;
import com.emanage.utility.MailerUtility;
import com.emanage.utility.StringUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

/**
 * @author rahul
 *
 */
@Service
@Lazy
public class OrderServiceImpl implements OrderService {

	final static Logger logger = Logger.getLogger(OrderServiceImpl.class);
	@Autowired
	private OrderDAO orderDAO;

	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private RestaurantService restaurantServices;
	
	@Autowired
	private DishTypeService dishTypeService;
	
	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@Autowired
	private SpringTemplateEngine templateEngine;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private InvoiceDAO invoiceDAO;
	
	@Autowired
	private VendorService vendorService;
	
	@Autowired
	private VendorPlatterService vendorPlatterService;
	
	@Override
	@Transactional
	public List<Order> listOrders(Map<String, Object> queryMap) {
		return orderDAO.listOrders(queryMap);
	}
	
	@Override
	@Transactional
	public void addOrder(Order order) {
		orderDAO.addOrder(order);
	}

	@Override
	@Transactional
	public void removeOrder(Integer id) throws Exception {
		orderDAO.removeOrder(id);
	}

	
	@Override
	@Transactional
	public Order getOrder(Integer id) {
		return orderDAO.getOrder(id);
	}

	@Override
	@Transactional
	public List<Integer> getAllOpenOrderCheckIds(Integer restaurantId) {
		return orderDAO.getAllOpenOrderCheckIds(restaurantId);
	}

/*	public List<Order> getDailyDeliveryBoyInvoice(Integer restaurantId,Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return orderDAO.getDailyDeliveryBoyInvoice(restaurantId, startDate, endDate);
	}*/

	@Override
	@Transactional
	public List<OrderDTO> getOrders(Integer restaurantId,List<String> orderType, String ordersOfDay){
		Map<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("restaurantId", restaurantId);

		List<Status> orderStatusList = new ArrayList<>();
		for(String status : orderType){
			orderStatusList.add(Enum.valueOf(Status.class, status.toUpperCase()));
		}
		Date fromDate=null;
		Date toDate =null;
		if(orderStatusList.size() > 0 )
			queryMap.put("status",orderStatusList);

		Restaurant restaurant  =  restaurantServices.getRestaurant(restaurantId);
		Date now = null;

		if(ordersOfDay.equalsIgnoreCase("future")){
			 now = DateUtil.nowInSpecifiedTimeZone(restaurant.getTimeZone());
			 now = DateUtil.addToDate(now,1,0,0,restaurant.getTimeZone());
			 fromDate = DateUtil.getStartOfDay(now,restaurant.getTimeZone());
			 toDate = DateUtil.getEndOfDay(now,restaurant.getTimeZone());
		}else{
			now = DateUtil.yesterdaySpecifiedTimeZone(restaurant.getTimeZone());
			fromDate = DateUtil.getStartOfDay(now,restaurant.getTimeZone());
			now = DateUtil.nowInSpecifiedTimeZone(restaurant.getTimeZone());
			toDate = DateUtil.getEndOfDay(now,restaurant.getTimeZone());
		}

		queryMap.put("fromDate", fromDate);
		queryMap.put("toDate", toDate);

		List<Order> orderList= orderDAO.getOpenOrders(queryMap);
		List<OrderDTO> dtoList = new ArrayList<OrderDTO>();
		
		for(Order order : orderList){
			OrderDTO dto = getOrderDTOFromOrder(order);
			if(dto != null)
				dtoList.add(dto);
			dto = null;
		}
		orderList = null;
		System.gc();
		return dtoList;
	}

	private OrderDTO getOrderDTOFromOrder(final Order order) {

		Invoice invoice = invoiceDAO.getInvoice(order.getCheckId());
		if(invoice == null)
			return null;

		OrderDTO dto = new OrderDTO();

		dto.id = order.getOrderId().toString();
		dto.checkId = order.getCheckId().toString();
		//dto.orderType = CheckType.Delivery.name();
		dto.deliveryTime = invoice.getDeliveryDateTime();
		dto.orderTime = order.getCreatedTime();
		
		dto.lastModified = order.getModifiedTime();
		dto.deliveryAddress = invoice.getDeliveryAddress();
		dto.fulfillmentCenterId = invoice.getScreenId();
		dto.changeAmount = new Float(order.getMoneyOut()).longValue();
		if(order.getDeliveryAgent()!=null){
		if(Character.isDigit(order.getDeliveryAgent().charAt(0))){
			Integer agentId = Integer.parseInt(order.getDeliveryAgent());
			User deliveryBoy= userService.getUser(agentId);
		if(deliveryBoy!=null){
			dto.deliveryAgent = deliveryBoy.getFirstName()+" "+deliveryBoy.getLastName()+" "+" ("+deliveryBoy.getContact()+")";
			dto.deliveryAgentId = deliveryBoy.getUserId();
		}
		}else {
			dto.deliveryAgent = order.getDeliveryAgent();
		}
		}
		dto.instructions = invoice.getDeliveryInst();
		dto.taxJsonObj = invoice.getTaxJsonObject();
		dto.discountList = getDiscountList(invoice.getDiscount_Charge());
		//dto.deliveryCharges = Math.round(check.getOutCircleDeliveryCharges());

		dto.customerName = invoice.getName();
		//dto.customerMobNo = invoice.getPhone();
		dto.customerEmail=invoice.getEmail();
		dto.customerId = invoice.getCustomerId();
		dto.orderSource = invoice.getOrderSource();
		dto.status = order.getStatus().name();
		dto.invoiceId = invoice.getInvoiceId();
		dto.paymentStatus = invoice.getStatus();
		dto.orderAmount = invoice.getRoundOffTotal();
		dto.isfirstOrder=invoice.isFirstOrder();
		dto.tokenAmount=invoice.getTokenAmount();
		dto.allowEdit=true;
		if(Status.CANCELLED==order.getStatus() || invoice.getStatus()==com.emanage.enums.check.Status.Cancel){
			dto.isEdited = false;
		}else if(invoice.getEditOrderRemark()!=null){
			dto.isEdited = true;
		}
		dto.creditBalance = invoice.getCreditBalance();
		if(order.getPaymentStatus() != null)
			dto.paymentMethod = order.getPaymentStatus();
		else
			dto.paymentMethod = "Unknown";

		if(invoice.getCoupon_Applied()!=null){
			List<Coupon> coupon = new ArrayList<>();
			for(Coupon coup : invoice.getCoupon_Applied()){
				coupon.add(couponService.getCouponById(coup.getCoupanId()));
			}
			dto.couponApplied = coupon;
			}
		Customer customer = customerDAO.getCustomer(invoice.getCustomerId());
		
		if(customer !=null){
		dto.customerEmail = customer.getEmail();
		}
		else {
			logger.info("Customer is Null "+customer +" for check id :" +invoice.getInvoiceNo());
		}
		List list1 = getItemsDTOFromOrder(order.getOrderDishes());
		invoice = null;
		dto.items = list1;
		list1 = null;
		return dto;
	}

	private List<DiscountDTO> getDiscountList(List<Order_DCList> discount_charge) {
		List<DiscountDTO> discountDTOList = new ArrayList<DiscountDTO>();

		for(Order_DCList dc : discount_charge){
			DiscountDTO dto = new DiscountDTO();
			dto.id = dc.getDcId();
			dto.name = dc.getName();
			dto.type = dc.getType();
			dto.value = dc.getValue();
			discountDTOList.add(dto);
		}
		return discountDTOList;
	}

	private List<OrderDishDTO> getItemsDTOFromOrder(List<OrderDish> orderList ){
		List<OrderDishDTO> list = new ArrayList();
		for (OrderDish dish : orderList ){
			OrderDishDTO dishDTO = new OrderDishDTO();
			dishDTO.name = dish.getName();
			dishDTO.menuId = dish.getDishId();
			dishDTO.quantity = dish.getQuantity();
			dishDTO.itemType = dish.getDishType();
			dishDTO.instructions = dish.getInstructions();
			dishDTO.price = dish.getPrice();
			ObjectMapper mapper = new ObjectMapper();
			Order_SectionDTO[] sections;
			try {
				if(dish.getSectionsJson()!=null) {
				sections = mapper.readValue(dish.getSectionsJson(),Order_SectionDTO[].class);
				dishDTO.sections=sections;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			dishDTO.dishSizeName = dish.getDishSize();
			if(dish.getDishSize()!=null && !("".equalsIgnoreCase(dish.getDishSize()))){
				List<Dish_Size> sizeList = dishTypeService.getDish_SizeListbyDishId(dish.getDishId());
				for(Dish_Size size :sizeList ){
				if(size.getName().equalsIgnoreCase(dish.getDishSize())){
					dishDTO.dishSizeId=size.getDishSizeId();
				}
				}
				
			}
			
			List<JsonAddOn> list1 = getAddonDTOFromOrder(dish.getOrderAddOn());
			dishDTO.addOn = list1;
			list.add(dishDTO);
			list1 = null;
			dishDTO = null;
		}
		return list;
	}

	private List<JsonAddOn> getAddonDTOFromOrder(List<OrderAddOn> addOnList ){
		List<JsonAddOn> list = new ArrayList();

		for (OrderAddOn addOn : addOnList ){
			JsonAddOn ja =  new JsonAddOn();
			OrderDishDTO dishDTO = new OrderDishDTO();
			ja.itemId = addOn.getAddOnId();
			ja.name = addOn.getName();
			ja.price = addOn.getPrice();
			//ja.smallImageUrl = addOn.getSmallImageUrl();
			list.add(ja);
		}
		return list;
	}

	@Override
	@Transactional
	public List<OrderDTO> getDispatchedCancelOrders(Integer restaurantId,List<String> orderType, String ordersOfDay) {
		Map<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("restaurantId", restaurantId);

		List<Status> orderStatusList = new ArrayList<>();
		for(String status : orderType)
			orderStatusList.add(Enum.valueOf(Status.class, status.toUpperCase()));

		if(orderStatusList.size() > 0 )
			queryMap.put("status",orderStatusList);

		Restaurant restaurant  =  restaurantServices.getRestaurant(restaurantId);
		Date now = null;
		Date fromDate =null;
		Date toDate =null;

		if(ordersOfDay.equalsIgnoreCase("future")){
			 now = DateUtil.nowInSpecifiedTimeZone(restaurant.getTimeZone());
			 now = DateUtil.addToDate(now,1,0,0,restaurant.getTimeZone());
			 fromDate = DateUtil.getStartOfDay(now,restaurant.getTimeZone());
			 toDate = DateUtil.getEndOfDay(now,restaurant.getTimeZone());
		}else{
			now = DateUtil.yesterdaySpecifiedTimeZone(restaurant.getTimeZone());
			fromDate = DateUtil.getStartOfDay(now,restaurant.getTimeZone());
			now = DateUtil.nowInSpecifiedTimeZone(restaurant.getTimeZone());
			toDate = DateUtil.getEndOfDay(now,restaurant.getTimeZone());
		}
		
		queryMap.put("fromDate", fromDate);
		queryMap.put("toDate", toDate);
		
		

		List<Order> orderList= orderDAO.getOpenDispatchedCancelOrders(queryMap);
		List<OrderDTO> dtoList = new ArrayList<OrderDTO>();
		for(Order order : orderList){
			OrderDTO dto = getOrderDTOFromOrder(order);
			if(dto != null)
				dtoList.add(dto);
			dto = null;
		}
		orderList = null;
		System.gc();
		return dtoList;
	}
	
	@Override
	@Transactional
	public void sendDemoEnrollmentEmail(HttpServletRequest request,DemoRegDTO regd,Customer customer) throws MessagingException {
		final Context ctx = new Context(request.getLocale());
		String templateName="demoEnrollment";
		
		
		Vendor vendor = vendorService.getVendor(regd.vendorId);
		SectionDishes sCourse = null;
		Platter platter = null;
		
		if(regd.platterId!=null) {
			platter = vendorPlatterService.getPlatter(regd.platterId);
			ctx.setVariable("course", platter);
		}else if(regd.courseId!=null) {
			sCourse = vendorPlatterService.getSectionDish(regd.courseId);
			ctx.setVariable("course", sCourse);
		}
		
		
		Restaurant rest = restaurantServices.getRestaurant(vendor.getCountryId());
		ctx.setVariable("customer", customer);
		ctx.setVariable("vendor", vendor);
		ctx.setVariable("regd", regd);
		ctx.setVariable("rest", rest);
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
		DateFormat formatter1;
		formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		formatter1.setTimeZone(cal.getTimeZone());
		ctx.setVariable("checkDate", formatter1.format(cal.getTime()));
		
		final MimeMessage mimeMessage = mailSender.createMimeMessage();
	    final MimeMessageHelper message =
	        new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

	    message.setSubject("Course demo notification");
	    if (rest != null) {

	    	String senderEmail=rest.getMailUsername();
	    	InternetAddress restaurantEmailAddress = null;
			try {
				restaurantEmailAddress = new InternetAddress(senderEmail, rest.getBussinessName());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	message.setFrom(restaurantEmailAddress);
	    	message.setReplyTo(restaurantEmailAddress);
	    }
	    message.setTo(customer.getEmail());
	    if(vendor!=null && vendor.getAlertMail()!=null) {
	    		message.setBcc(vendor.getAlertMail());
	    }
	    // Create the HTML body using Thymeleaf

	    final String htmlContent = templateEngine.process(templateName, ctx);
	    message.setText(htmlContent, true); // true = isHtml
	    
	    if (!StringUtility.isNullOrEmpty(rest.getMailUsername()) && !StringUtility.isNullOrEmpty(rest.getMailPassword())) {
        
	    	mailSender.setUsername(rest.getMailUsername());
	    	mailSender.setPassword(rest.getMailPassword());
	   
	    	mailSender.setHost(rest.getMailHost());
	    	mailSender.setProtocol(rest.getMailProtocol());
	    	mailSender.setPort(rest.getMailPort());
	    }
	    mailSender.send(mimeMessage);
	}
	
	public String emailCreditCheckFromServer(HttpServletRequest request,Invoice check,int customerId, String emailAddr, Status status,String refund, String subject, String sender,String reason,float extraAmount,String h) throws MessagingException, UnsupportedEncodingException{
 		// Prepare the evaluation context
 	    final Context ctx = new Context(request.getLocale());
 	    if(subject==null){
 	    	subject = "Your Credit Info";
 	    }
 	    
 	    String templateName="emailCreditBill";
 		logger.info("***********************sending check***********************************");
 		logger.info("Customer Credit Check emailling in process.. : check Id: "+check.getInvoiceId());
 		logger.info("Email Address: " + emailAddr);
 		User user = null;
 		Restaurant org=null;
 		//CreditBill creditBill=null;
 		Customer customer=null;
 			if (check != null) {
 				customer  = customerService.getCustomer(customerId);
 				String description  = setDescription(status,customer,check,refund,reason,extraAmount);
 				org=restaurantServices.getRestaurant(customer.getOrgId());
 				CustomerCreditBill customerCreditBill = new CustomerCreditBill(customer,check,description);
 				ctx.setVariable("customerCreditBill", customerCreditBill);
 				ctx.setVariable("user", org);
 				Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(org.getTimeZone()));
 				cal.setTime(customerCreditBill.getDate());
 				DateFormat formatter1;
 				formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
 				formatter1.setTimeZone(cal.getTimeZone());
 				ctx.setVariable("creditBillDate", formatter1.format(cal.getTime()));
 				
 	    // Prepare message using a Spring helper
 	    final MimeMessage mimeMessage = mailSender.createMimeMessage();
 	    final MimeMessageHelper message =
 	        new MimeMessageHelper(mimeMessage, false, "UTF-8"); // true = multipart

 	    message.setSubject(subject);
 	    if (org != null) {
 	    	//String senderEmail = StringUtility.isNullOrEmpty(rest.getMailUsername()) ? user.getUsername() : rest.getMailUsername();
 	    	String senderEmail=org.getMailUsername();
 	    	InternetAddress restaurantEmailAddress = new InternetAddress(senderEmail, org.getBussinessName());
 	    	message.setFrom(restaurantEmailAddress);
 	    	message.setReplyTo(restaurantEmailAddress);
 	    }
 	    message.setTo(emailAddr);
 	    // Create the HTML body using Thymeleaf
 	    
 	    final String htmlContent = templateEngine.process(templateName, ctx);
 	    message.setText(htmlContent, true); // true = isHtml
 	    String oldUsername = null;
 	    String oldPassword = null;
 	    String oldHost = null;
 	    String oldProtocol = null;
 	    Integer oldPort = -1;
 	    if (!StringUtility.isNullOrEmpty(org.getMailUsername()) && !StringUtility.isNullOrEmpty(org.getMailPassword())) {
 	    	oldUsername = mailSender.getUsername(); 
 	    	oldPassword = mailSender.getPassword();
 	    	oldHost = mailSender.getHost();
 	    	oldProtocol = mailSender.getProtocol();
 	    	oldPort = mailSender.getPort();
 	    	if(sender!=null){
 	    		MailerUtility mu = new MailerUtility();
 	    		mailSender.setUsername(mu.username);
 		    	mailSender.setPassword(mu.password);
 	    	}else{
 	    	mailSender.setUsername(org.getMailUsername());
 	    	mailSender.setPassword(org.getMailPassword());
 	    	}
 	    	mailSender.setHost(org.getMailHost());
 	    	mailSender.setProtocol(org.getMailProtocol());
 	    	mailSender.setPort(org.getMailPort());
 	    }
 	    
 	    mailSender.send(mimeMessage);
         logger.info("emmail sent for email Id :" + customer.getEmail());
 	    if (!StringUtility.isNullOrEmpty(oldUsername) && !StringUtility.isNullOrEmpty(oldPassword)) {
 	    	mailSender.setUsername(oldUsername);
 	    	mailSender.setPassword(oldPassword);
 	    	mailSender.setHost(oldHost);
 	    	mailSender.setProtocol(oldProtocol);
 	    	mailSender.setPort(oldPort);
 	    }
 		} else {
 				logger.info("No check Found for "+emailAddr);
 				return "No check found";
 		}
 	    logger.info("***********************sending end***********************************");
 	    return "Email Sent Successfully";
 	}
    String setDescription(Status status,Customer customer,Invoice check,String refund,String reason,float extraAmount) {
    	if(customer.getCredit().getStatus()==CustomerCreditAccountStatus.ACTIVE){
    		Restaurant restaurant =  restaurantServices.getRestaurant(check.getCountryId());
	    	if(status==Status.CANCELLED && "CREDIT".equalsIgnoreCase(refund)){
	    		return "Your order No. "+check.getOrderId()+" (of  Total Rs."+check.getRoundOffTotal()+") has been cancelled. We owe you  Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
	    				+ " Which will be automatically deducted from your next order payment. "
	    				+ "Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    	}else if(status==Status.CANCELLED && "CASH".equalsIgnoreCase(refund)){
	    		return "Your order with order No. "+check.getOrderId()+" has been cancelled. We have initiated the refund as CASH. One of our delivery boy will hand over it to you soon.";
	    	}else if("COMPLETED".equalsIgnoreCase(refund)&& "DELIVERED".equalsIgnoreCase(reason)){
	    		return "Your order No. "+check.getOrderId()+" (of  Total Rs."+check.getRoundOffTotal()+") has been DELIVERED. Your current balance with "+restaurant.getRestaurantName()+" is "+Math.abs(customer.getCredit().getCreditBalance())+"."
	    				+"Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    	}else if("UPDATE".equalsIgnoreCase(refund) && "DISCOUNT".equalsIgnoreCase(reason)){
	    		return "Your order No. "+check.getOrderId()+" has been discounted by Rs "+extraAmount+". We owe you  Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
	    				+ " Which Will automatically deducted from your next order payment. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    	}else if("UPDATE".equalsIgnoreCase(refund) && "ADDED".equalsIgnoreCase(reason)){
	    		return "Your order No. "+check.getOrderId()+" has been discounted by Rs "+extraAmount+". You owe us  Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
	    				+ " Which will automatically added to your next order payment. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    	}else if("UPDATE".equalsIgnoreCase(refund) && "CHARGED".equalsIgnoreCase(reason)){
	    		return "Your order No. "+check.getOrderId()+" has been discounted by Rs "+extraAmount+". You owe us  Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
	    				+ " Which will automatically added to your next order payment. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    	}else if("PAYMENT_TYPE_CHANGED".equalsIgnoreCase(reason)){
	    		if(customer.getCredit().getCreditBalance()<0){
	    			return "Your order No. "+check.getOrderId()+" has been changed to "+check.getOrders().get(0).getPaymentStatus()+" from "+refund+". We owe you Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
		    				+ " Which will automatically deducted from your next order payment. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    		}else{
	    			return "Your order No. "+check.getOrderId()+" has been changed to "+check.getOrders().get(0).getPaymentStatus()+" from "+refund+". You owe us Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
		    				+ " Which will automatically added to your next order payment. Request you to make this payment at earliest. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    		}
      		} else if(status==Status.CANCELLED && refund==null){
      			if(customer.getCredit().getCreditBalance()<0){
      				return "Your order No. "+check.getOrderId()+" (of  Total Rs."+check.getRoundOffTotal()+") has been cancelled. We owe you Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
      						+ " Which will automatically deducted from your next order payment. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
      			}else if(customer.getCredit().getCreditBalance()>0){
      				return "Your order No. "+check.getOrderId()+" (of  Total Rs."+check.getRoundOffTotal()+") has been cancelled. You owe us Rs "+Math.abs(customer.getCredit().getCreditBalance())+"."
      						+ " Which will automatically added to your next order payment. Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";

      			}
	    	}
    		else if(refund==null){
	    		return "Your current balance with "+restaurant.getRestaurantName()+" is "+Math.abs(customer.getCredit().getCreditBalance())+"."
	    				+"Should there be any concern pls call at "+restaurant.getBussinessPhoneNo()+".  Assuming you of our best, always.";
	    	}
    }
		return null;
     }

	@Override
	@Transactional
	public Invoice placeRazorPayOrder(Invoice invoice) throws JSONException {
		// TODO Auto-generated method stub
		try {
			Properties prop = new Properties();
			String propFileName = "razorpay.properties";
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				try {
					prop.load(inputStream);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			Float tokenAmountInPaisa = invoice.getTokenAmount()*100;// amount * 100 converting it to paisa
			
			String secret_keyId = prop.getProperty("key_id");
			String secret_key = prop.getProperty("key_secret");
			logger.info("Registring order with razor pay :"+ tokenAmountInPaisa +" paisa");
			logger.info("secret keyId : "+secret_keyId);
			logger.info("secret key : "+secret_key);
			
			RazorpayClient razorpayClient = new RazorpayClient(secret_keyId,secret_key);
			
			JSONObject options = new JSONObject();
			options.put("amount", tokenAmountInPaisa);
			options.put("currency", "INR");
			options.put("receipt",invoice.getInvoiceId());
			options.put("payment_capture", false);
			com.razorpay.Order order = razorpayClient.Orders.create(options);
			String orderId = order.get("id");
			
			invoice.setRazorPayId(orderId);
			invoiceDAO.addInvoice(invoice);
			System.out.println("------- rAZOR PAY -----");
			System.out.println(orderId);
			
		} catch (RazorpayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return invoice;
	}

}