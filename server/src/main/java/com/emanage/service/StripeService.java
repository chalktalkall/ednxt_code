package com.emanage.service;


import java.io.IOException;

import com.emanage.utility.ChargeRequest;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

public interface StripeService {
	Charge charge(ChargeRequest chargeRequest) throws StripeException, IOException;
}