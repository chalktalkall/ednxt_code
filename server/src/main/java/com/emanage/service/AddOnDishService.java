/**
 * 
 */
package com.emanage.service;

import java.util.List;

import com.emanage.domain.AddOnDish;

/**
 * @author rahul
 *
 */
public interface AddOnDishService {

	public void addDish(AddOnDish dish);
	public void updateMenuModificationTime(Integer dishId);
	public List<AddOnDish> listDish();
	public List<AddOnDish> listDishByRestaurant(Integer restId);
	public void removeDish(Integer id) throws Exception;
	public List<AddOnDish> getDishes(Integer[] ids);
	public AddOnDish getDish(Integer id);
}
