package com.emanage.service;

import java.util.Date;
import java.util.List;

import com.emanage.domain.BookedPlatter;
import com.emanage.domain.CuisineMenu;
import com.emanage.domain.Platter;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.Sections;
import com.emanage.dto.LocationLatLong;
import com.emanage.enums.Status;

public interface VendorPlatterService {

	public void addPlatter(Platter menu);
	public List<Platter> listPlattersByVendorId(Integer vendorId);
	public List<Platter> allPlattersByStatus(Integer vendorId, Status status);
	public Platter getPlatter(Integer id);
	public void removePlatter(Integer id);
	public Integer getVendorIdUsingPlatterId(Integer id);
	
	public List<Platter> listPlatters();
	public List<Platter> allPosPlatters(Integer countryId, Status status,boolean posStatus);
	public List<Platter> listPlattersByCountry(Integer countryId);
	public Platter getPlatterByPlatterName(String name, Integer restaurantId);
	
	public void removeSections(List<Integer> sectionIds);
	public void addSection(Sections section); 
	
	public List<Platter> getPlatterByLatlong(double latitud, double longitude, Date date, Integer plateCount);
	
	public List<CuisineMenu>  getCoursesByLatlong(LocationLatLong locationLatLong);
	
	public SectionDishes getSectionDish(Integer id);
	public void saveSectionDish(SectionDishes sectionDish);
	
	public void addPlatterBooking(BookedPlatter menu);
	public List<BookedPlatter> listBookedPlattersByVendorIdAndDate(Integer vendorId,Date date);
	
	public Integer getBookedPlatterCountByDateandId(Integer vendorId, Date date) ;
	
	public CuisineMenu  getPlatterByPlatterId(Integer platterId);
	
	public  List<CuisineMenu> getCoursesByInstituteId(Integer instituteId) ;
	public  List<CuisineMenu> getPopularCourses() ;
	public  List<CuisineMenu> getFeatureCourses() ;
	public List<CuisineMenu> getAllCourseByCriteria(LocationLatLong locationLatLong);
	
	public  List<CuisineMenu> getActiveWebinars() ;
	List<CuisineMenu> getFeatureTrainings();
	List<CuisineMenu> getAllCourseByCriteriaNew(LocationLatLong locationLatLong);
	List<CuisineMenu> getFeaturedCanvasCourses(Integer universityId);
	CuisineMenu getCourseById(Integer courseId);
	
	
	
}
