package com.emanage.service;

import java.util.List;

import com.emanage.domain.FulfillmentCenter;

public interface FulfillmentCenterService {
	
	public void addKitchenScreen(FulfillmentCenter kitchenScreen);
	public void removeKitchenScreen(int id);
	public List<FulfillmentCenter> getKitchenScreens(int restaurantId);
	public FulfillmentCenter getKitchenScreen(int id);
	
}
