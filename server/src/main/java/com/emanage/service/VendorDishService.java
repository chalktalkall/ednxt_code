package com.emanage.service;

import java.util.List;

import com.emanage.domain.VendorDish;

public interface VendorDishService {

	public VendorDish addVendorDish(VendorDish dish);
	public void updateMenuModificationTime(Integer dishId);
	public List<VendorDish> listVendorDishByVendorId(Integer vandorId);
	public void removeVendorDish(Integer id) throws Exception;
	public List<VendorDish> getVendorDishes(Integer[] ids);
	public VendorDish getVendorDish(Integer id);
}
