/**
 * 
 */
package com.emanage.service;

import java.util.List;

/**
 * @author rahul
 *
 */
public interface OrderDishService {

	public void removeOrderDishes(List<Integer> orderDishIds);
}
