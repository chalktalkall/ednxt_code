/**
 * 
 */
package com.emanage.service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.naming.directory.InvalidAttributesException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;

import com.emanage.domain.CreditType;
import com.emanage.domain.Customer;
import com.emanage.domain.CustomerAddress;
import com.emanage.domain.Customers;
import com.emanage.domain.Restaurant;
import com.emanage.dto.CustomerAppRegisterDTO;
import com.emanage.dto.CustomerDataDTO;
import com.emanage.dto.CustomerRegisterationDTO;
import com.emanage.dto.FetchOTPDTO;
import com.emanage.dto.ResponseDTO;
import com.emanage.dto.VerifyWebCustomer;
import com.emanage.dto.WebCustomerRegisterDTO;
import com.emanage.dto.credit.AddCreditToCustomerAccountDTO;
import com.emanage.dto.credit.CreditDTO;
import com.emanage.dto.credit.CreditStatementDTO;
import com.emanage.dto.credit.CreditTransactionDTO;
import com.emanage.dto.credit.CreditTypeDTO;
import com.emanage.dto.credit.CustomerCreditDTO;

/**
 * @author rahul
 *
 */
public interface CustomerService {

    void addCustomer(Customer customer);

    void removeCustomer(Integer id) throws Exception;

    Customer getCustomer(Integer id);

    List<Customer> getCustomerById(Integer id);

    List<Customer> getCustomerByParams(Integer custId, String email, String phone, Integer orgId);

    CustomerAddress getCustomerAddressById(int id);

    void addCustomerAddress(CustomerAddress customerAddress);

    void updateCustomerAddress(CustomerAddress customerAddress);

    void removeCustomerAddress(Integer customerId);

    List<CustomerAddress> getCustomerAddress(Integer customerId);

    List<Customer> getCustomerByDate(Integer orgId, Integer restaurantId, Date startDate, Date endDate);

    String registerCustomer(CustomerRegisterationDTO customer);

    Customer authenticate(String phoneNumber, String simNumber, Integer orgId);

    boolean fetchNewOTP(FetchOTPDTO fetchOtp);

    boolean sendSMS(String mobileNumber, String content, String priority);

    Customer verifyOTP(String email, String simNumber, String otp, Integer orgId);

    boolean setAccountStatus(String mobileNumber, int value, Integer orgId);

    boolean setForcedLogin(String mobileNumber,Integer orgId);

    Customer isCustomerFacebookIdExist(String facebookId);

    boolean isCustomerAuthentic(String facebookId, Integer orgId);

    String signUp(Customer customer);

    boolean loginCustomer(String mobileNumber, Integer orgId);

    boolean verifyOTP(String email, String otp);

    boolean emailOTP(String mobileNumber, Integer orgId);

    List<Customer> listCustomerInRestaurant(int restaurantId);

    List<Customer> listCustomerByMobile(int restaurantId, List<String> mobileNo);

    @PreAuthorize("hasAnyRole('manager','admin')")
    ResponseDTO sendTestOTP(String otpDetails);

    ResponseDTO enableCustomerCredit(CustomerCreditDTO customerCreditDTO, Integer orgId) throws Exception;

    @PreAuthorize("hasAnyRole('manager','admin')")
    ResponseDTO updateCustomerCredit(CustomerCreditDTO customerCredit, Integer orgId) throws Exception;

    @PreAuthorize("hasAnyRole('manager','admin')")
    ResponseDTO removeCustomerCredit(Integer customerId, Integer orgId) throws InvalidAttributesException;

    @PreAuthorize("hasAnyRole('manager','admin')")
    ResponseDTO addCustomerCreditType(CreditTypeDTO creditDTO) throws Exception;
   // public ResponseDTO updateCustomerCreditType(CreditType credit);
   List<CreditType> listCustomerCreditType(int orgId);

    CreditType getCreditType(Integer creditId);

    List<CreditDTO> listCustomerCredit(int orgId);

    ResponseDTO creatTransaction(AddCreditToCustomerAccountDTO creditAddDTO, Integer orgId, Integer userId) throws InvalidAttributesException;

    @PreAuthorize("hasAnyRole('manager','admin')")
    ResponseDTO editCustomerCreditType(CreditType creditType) throws Exception;

    List<CreditTransactionDTO> listCustomerCreditTransactions(int customerId, String fromDate, String toDate, String format) throws Exception;

    @PreAuthorize("hasAnyRole('manager','admin')")
    ResponseDTO deleteCustomerCreditType(int creditTypeId, int parseInt);



	/**************************************************************************************************/

    ResponseDTO registerWebCustomer(WebCustomerRegisterDTO customerDTO);

    Customer verifyCustomerOTP(VerifyWebCustomer customer) throws Exception;

    ResponseDTO generateNewOTP(FetchOTPDTO fetchOtp);

    Customer login(String mobileNo, int orgId, String device, String appId) throws Exception;

    ResponseDTO registerCustomerApp(CustomerAppRegisterDTO customerAppDTO);

    CustomerDataDTO getCustomerData(String phone, Integer orgId);

    ResponseDTO autoEmailCreditBillFromServer(CreditStatementDTO creditStatementDTO,Restaurant org) throws MessagingException, UnsupportedEncodingException;

    void autoEmailCreditBillbyList(List<String> statementIdList);


    Customer getCustomerByPhone(String mobileNoOfCustomer, int orgId);

    void deleteCustomerCreditAccount(Integer customerId);

    void deleteCustomer(Integer customerId);

    Customer getCustomerFronInvoiceId(String invoiceId);
    
    Customers getCustomerInfoJSON(String phone,String custId,String restaurantId, HttpServletRequest request, HttpServletResponse response) throws Exception;

	ResponseDTO generateEmailOTP(FetchOTPDTO fetchOtp);

	Customers getCustomerInfoByEmail(String email, String restaurantId, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
}
