/**
 * 
 */
package com.emanage.service;

import java.util.List;

import com.emanage.domain.AddOnDishType;

/**
 * @author rahul
 *
 */
public interface AddOnDishTypeService {
	public void addDishType(AddOnDishType dishType);
	public List<AddOnDishType> listDishTypes();
	public List<AddOnDishType> listDishTypesByRestaurant(Integer restaurantId);
	public void removeDishType(Integer id);
	public AddOnDishType getDishType(Integer id);
	
}
