/**
 * 
 */
package com.emanage.service;

import java.util.List;

import com.emanage.domain.Section;

/**
 * @author rahul
 *
 */
public interface SectionService {

	public void addSection(Section section);
	public void removeSections(List<Integer> sectionIds);
}
