package com.emanage.service;

import java.util.List;

import com.emanage.domain.Vendor;

public interface VendorService {

	public Vendor addVendor(Vendor vendor);
	public List<Vendor> listVendor();
	public List<Vendor> listVendorByCountryId(Integer countryId);
	public List<Vendor> listVendorByOrgId(Integer orgId);
	public void removeVenor(Integer vendorId);
	public Vendor getVendor(Integer vendorId);
	public void saveVendor(Vendor vendor);
	List<Vendor> listActiveVendorById(Integer countryId);
}
