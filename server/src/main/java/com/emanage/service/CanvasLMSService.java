/**
 * 
 */
package com.emanage.service;

import java.util.List;

import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.Vendor;

/**
 * @author rahul
 *
 */

public interface CanvasLMSService {

	Object getCourse(Integer courseId,Integer vendorId);
	Canvas_Courses getCourseById(Integer course_id);
	
	List<Object> listActiveCourses();
	
	List<Object> getCourseByCollegeCampus(Integer vendorId);
	List<Canvas_Courses> listCoursesByCollegeId(Integer vendorId);
	List<Object> createUser(Integer vendorId,Integer customerId);
	
	Object getUserDetails(Integer customerId, Integer vendorId);
	
	List<Object> enrollUser(Integer customerId, Integer vendorId,Integer courseId);
	
	//Canvas_Courses getCourseByCanvasId(Integer vendorId,Integer canvasCourseId);
	void updateLMSCourse(Canvas_Courses course);
	void deleteLMSCourse(Canvas_Courses course);
	
	List<Canvas_Courses> getAllCourseList(Integer vendorId);
	List<Canvas_Courses> getAllCourseListByUniv(Integer vendorId);
	Canvas_Courses getCourseByCanvasId(Vendor vendor, Integer id);
	
}
