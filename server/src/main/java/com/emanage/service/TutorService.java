package com.emanage.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.emanage.domain.TutorRequest;
import com.emanage.dto.ResponseDTO;
 
public interface TutorService {

	ResponseDTO saveTutorRequest(TutorRequest tutorRequest,HttpServletRequest request, HttpServletResponse response,String restaurantId);
}
