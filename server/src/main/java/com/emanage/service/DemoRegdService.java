package com.emanage.service;

import java.util.List;

import com.emanage.domain.DemoRegd;
import com.emanage.dto.RegResultDTO;

public interface DemoRegdService {

	public void addDemoRegd(DemoRegd demoRegd);
	public List<DemoRegd> listDemoRegd();
	public List<DemoRegd> listDemoRegd(Integer vendorId);
	public void removeDemoRegd(Integer id) throws Exception;
	public List<DemoRegd> getDemoRegd(Integer[] ids);
	public DemoRegd getDemoRegd(Integer id);
	public List<RegResultDTO> listDemoByUserId(Integer customerId);
}
