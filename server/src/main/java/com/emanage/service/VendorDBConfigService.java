package com.emanage.service;

import java.util.List;

import com.emanage.domain.DBConfig;

public interface VendorDBConfigService {

	 DBConfig addDBConfig(DBConfig dbConfig);
	 DBConfig getDBConfig(Integer dbConfig);
	 List<DBConfig> listDBConfigByCountryId(Integer countryId);
	 void removeDBConfig(Integer dbConfigId);
	 DBConfig getCampusDBConfig(Integer campusId);
		
}
