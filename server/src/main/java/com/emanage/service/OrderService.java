/**
 * 
 */
package com.emanage.service;

import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;

import com.emanage.domain.Customer;
import com.emanage.domain.Invoice;
import com.emanage.domain.Order;
import com.emanage.dto.DemoRegDTO;
import com.emanage.dto.OrderDTO;

/**
 * @author rahul
 *
 */
public interface OrderService {

	public List<Order> listOrders(Map<String, Object> queryMap);
	public void addOrder(Order order);
	public void removeOrder(Integer id) throws Exception;
	public Order getOrder(Integer id);
	public List<Integer> getAllOpenOrderCheckIds(Integer restaurantId);
	/*public List<Order> getDailyDeliveryBoyInvoice(Integer restaurantId, Date startDate, Date endDate);*/
	public List<OrderDTO> getOrders(Integer restaurantId, List<String> orderType,String ordersOfDay);
	public List<OrderDTO> getDispatchedCancelOrders(Integer restaurantId, List<String> orderType,String ordersOfDay);
	//public String emailCreditCheckFromServer(HttpServletRequest request,Invoice check,int customerId, String emailAddr, Status status,String refund, String subject, String sender,String reason,float extraAmount,String h) throws MessagingException, UnsupportedEncodingException;
	public void sendDemoEnrollmentEmail(HttpServletRequest request,DemoRegDTO regd,Customer cust) throws MessagingException;
	public Invoice placeRazorPayOrder(Invoice invoice) throws JSONException;
}
