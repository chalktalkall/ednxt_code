package com.emanage.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emanage.domain.CountryDishType;
import com.emanage.domain.CountrySectionType;
import com.emanage.domain.CuisineType;
import com.emanage.service.CuisineNDishService;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Rahul
 *
 */
@Controller
@ApiIgnore
@RequestMapping("/cuisineDishType")
public class CuisineNDishTypeController {

	@Autowired
	CuisineNDishService cuisineNDishTypeService;

	@RequestMapping("/")
	public String listCuisineTypes(Map<String, Object> map, HttpServletRequest request) {
		map.put("cuisineType", new CuisineType());
		map.put("cuisineTypeList", cuisineNDishTypeService
				.listCuisineTypesByCountryId((Integer) request.getSession().getAttribute("countryId")));
		return "countryCuisineType";
	}

	@RequestMapping("/editCuisineType/{cousineId}")
	public String editCuisineType(Map<String, Object> map, HttpServletRequest request,
			@PathVariable("cousineId") Integer cousineId) {
		CuisineType cuisineType = cuisineNDishTypeService.getCuisineType(cousineId);
		map.put("cuisineType", cuisineType);
		map.put("cuisineTypeList", cuisineNDishTypeService.listCuisineTypesByCountryId((Integer) request.getSession().getAttribute("countryId")));
		return "countryCuisineType";
	}

	@RequestMapping(value = "/addCuisineType", method = RequestMethod.POST)
	public String addCuisineType(@ModelAttribute("cuisineType") CuisineType cuisineType, BindingResult result) {
		cuisineNDishTypeService.addCuisineType(cuisineType);
		return "redirect:/cuisineDishType/";
	}

	@RequestMapping("/deleteCuisine/{cousineId}")
	public String deleteCuisineType(@PathVariable("cousineId") Integer cousineId) {
		cuisineNDishTypeService.removeDishType(cousineId);
		return "redirect:/cuisineDishType/";
	}

	// Dish Type module started

	@RequestMapping("/listDishType")
	public String listDishTypes(Map<String, Object> map, HttpServletRequest request) {
		map.put("dishType", new CountryDishType());
		map.put("dishTypeList", cuisineNDishTypeService
				.listDishTypesByCountryId(((Integer) request.getSession().getAttribute("countryId"))));
		return "countryDishType";
	}

	@RequestMapping("/editDishType/{dishTypeId}")
	public String editDishType(Map<String, Object> map, HttpServletRequest request,
			@PathVariable("dishTypeId") Integer dishTypeId) {

		CountryDishType dishType = cuisineNDishTypeService.getDishType(dishTypeId);
		map.put("dishType", dishType);
		map.put("dishTypeList", cuisineNDishTypeService
				.listDishTypesByCountryId(((Integer) request.getSession().getAttribute("restaurantId"))));
		return "countryDishType";
	}

	@RequestMapping(value = "/addDishType", method = RequestMethod.POST)
	public String addDishType(@ModelAttribute("dishType") CountryDishType dishType, BindingResult result,
			HttpServletRequest request) {
		cuisineNDishTypeService.addDishType(dishType);
		return "redirect:/cuisineDishType/listDishType/";
	}

	@RequestMapping("/deleteDishType/{dishTypeId}")
	public String deleteDishType(@PathVariable("dishTypeId") Integer dishTypeId) {
		cuisineNDishTypeService.removeDishType(dishTypeId);
		return "redirect:/cuisineDishType/listDishType/";
	}
	
	
	// Section Type module started

		@RequestMapping("/listSectionType")
		public String listSectionTypes(Map<String, Object> map, HttpServletRequest request) {
			map.put("sectionType", new CountrySectionType());
			map.put("sectionTypeList", cuisineNDishTypeService
					.listSectionTypesByCountryId(((Integer) request.getSession().getAttribute("countryId"))));
			return "countrySectionType";
		}

		@RequestMapping("/editSectionType/{sectionTypeId}")
		public String editSectionType(Map<String, Object> map, HttpServletRequest request,
				@PathVariable("sectionTypeId") Integer sectionTypeId) {

			CountrySectionType sectionType = cuisineNDishTypeService.getSectionType(sectionTypeId);
			map.put("sectionType", sectionType);
			map.put("sectionTypeList", cuisineNDishTypeService
					.listSectionTypesByCountryId(((Integer) request.getSession().getAttribute("countryId"))));
			return "countrySectionType";
		}

		@RequestMapping(value = "/addSectionType", method = RequestMethod.POST)
		public String addSectionType(@ModelAttribute("sectionType") CountrySectionType sectionType, BindingResult result,
				HttpServletRequest request) {
			cuisineNDishTypeService.addSectionType(sectionType);
			return "redirect:/cuisineDishType/listSectionType/";
		}

		@RequestMapping("/deleteSectionType/{sectionTypeId}")
		public String deleteSectionType(@PathVariable("sectionTypeId") Integer sectionTypeId) {
			cuisineNDishTypeService.removeSectionType(sectionTypeId);
			return "redirect:/cuisineDishType/listSectionType/";
		}
		
		@RequestMapping(value="/getCourseTypes/{universityId}",method=RequestMethod.GET)
		public @ResponseBody List<CuisineType> getCuisineTypes(HttpServletRequest request, @PathVariable("universityId") Integer universityId) {
			
			List<CuisineType> cuisineType = cuisineNDishTypeService.listCuisineTypes(universityId);
			return cuisineType;
		}

}
