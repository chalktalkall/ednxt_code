/**
 * 
 */
package com.emanage.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanage.domain.TaxType;
import com.emanage.enums.restaurant.ChargesType;
import com.emanage.service.DishTypeService;
import com.emanage.service.RestaurantService;
import com.emanage.service.TaxTypeService;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @author rahul
 *
 */
@Controller
@RequestMapping("/taxTypes")
@ApiIgnore
public class TaxTypeController {

	@Autowired
	TaxTypeService taxTypeService;
	 
	@Autowired
	DishTypeService dishTypeService;
	
	@Autowired
	RestaurantService restService;
	
	/*@RequestMapping("/getTaxes/{restaurantId}")
	public @ResponseBody List<TaxType> getlistTaxTypes(Map<String, Object> map, HttpServletRequest request, @PathVariable("restaurantId") Integer restId) {
		
		List<TaxType> taxTypeList =  taxTypeService.listTaxTypesByRestaurantId(restId);
		
		
		return taxTypeList;
	}*/
	
	@RequestMapping("/")
	@ApiIgnore
	public String listTaxTypes(Map<String, Object> map, HttpServletRequest request) {
		map.put("taxType", new TaxType());
		map.put("taxTypeList", taxTypeService.listTaxTypesByRestaurantId((Integer) request.getSession().getAttribute("restaurantId")));
		map.put("chargeTypes", ChargesType.values());
		map.put("dishType",dishTypeService.listDishTypesByRestaurantId((Integer) request.getSession().getAttribute("restaurantId")));
		return "taxType";
	}

	@RequestMapping("/edit/{taxTypeId}")
	@ApiIgnore
	public String editTaxType(Map<String, Object> map, HttpServletRequest request, @PathVariable("taxTypeId")	Integer taxTypeId) {

		TaxType taxType = taxTypeService.getTaxType(taxTypeId);
		map.put("taxType", taxType);
		map.put("chargeTypes", ChargesType.values());
		map.put("taxTypeList", taxTypeService.listTaxTypesByRestaurantId((Integer) request.getSession().getAttribute("restaurantId")));
		map.put("dishType",dishTypeService.listDishTypesByRestaurantId((Integer) request.getSession().getAttribute("restaurantId")));
		return "taxType";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiIgnore
	public String addTaxType(@ModelAttribute("taxType")
	TaxType taxType, BindingResult result,HttpServletRequest request) {
		System.out.println(taxType.getRestaurantId());
		taxTypeService.addTaxType(taxType);
		return "redirect:/taxTypes/";
	}

	@RequestMapping(value = "/addOver", method = RequestMethod.POST)
	@ApiIgnore
	public void addOverrideTaxType(@ModelAttribute("taxType")
	TaxType taxType, BindingResult result,HttpServletRequest request) {
		taxTypeService.addTaxType(taxType);
	}
	@RequestMapping("/delete/{taxTypeId}")
	@ApiIgnore
	public String deleteTaxType(@PathVariable("taxTypeId")
	Integer taxTypeId,HttpServletRequest request) {
		TaxType taxType = taxTypeService.getTaxType(taxTypeId);
		taxTypeService.removeTaxType(taxTypeId);
		return "redirect:/taxTypes/";
	}
}
