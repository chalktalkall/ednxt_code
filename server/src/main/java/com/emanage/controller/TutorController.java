package com.emanage.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.emanage.domain.TutorRequest;
import com.emanage.dto.ResponseDTO;
import com.emanage.service.TutorService;

@RequestMapping("/tutor")
@RestController
public class TutorController {

	@Autowired
	private TutorService tutorService;
	
	@RequestMapping(value = "/requestTutor", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseDTO requestTutor(@RequestBody TutorRequest tutorRequest, HttpServletRequest request, HttpServletResponse response) {
		return tutorService.saveTutorRequest(tutorRequest,request,response,"11");
	}
}
