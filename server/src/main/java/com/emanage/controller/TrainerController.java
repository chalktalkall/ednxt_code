package com.emanage.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.emanage.domain.Trainer;
import com.emanage.dto.ResponseDTO;
import com.emanage.service.TrainerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(description="Trainer REST API's")
@RequestMapping("trainer")
public class TrainerController {
	
	@Autowired
	TrainerService trainerService;

	@ApiOperation(value = "Update Save Presentation.")
	@RequestMapping(value = "/savePresn", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseDTO savePresn(@RequestBody Trainer trainer,HttpServletRequest request) {
		return trainerService.saveTrainer(trainer,request);
	}
	
	@RequestMapping(value = "/getActiveTrainers", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Trainer> getAllActivePresn() {
		return trainerService.getAllActiveTrainer();
	}
	
	@RequestMapping(value = "/getAllTrainers", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Trainer> getAllPresn() {
		return trainerService.getAllTrainer();
	}
	
	@RequestMapping(value = "/deleteTrainer", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseDTO deletePresentation(@RequestParam(required = true) int id) {
		return trainerService.deleteTrainer(id);
	}
	
	@RequestMapping(value = "/getTrainerById", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Trainer getPresentationById(@RequestParam(required = true) int id) {
		return trainerService.getTrainer(id);
	}
	
	@RequestMapping(value = "/editPresn", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseDTO editPresn(@RequestBody Trainer presn) {
		return null;
	}
	
	
	
}
