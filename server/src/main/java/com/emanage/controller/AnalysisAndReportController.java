package com.emanage.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.emanage.enums.report.ReportFormat;
import com.emanage.service.BirtReportService;

import io.swagger.annotations.Api;

/**
 * Created by rahul on 3/1/2017.
 */
@Controller
@RequestMapping("/analysisAndReport")
@Api(description="Analysis and Report REST API's")
public class AnalysisAndReportController {

    final static Logger logger = Logger.getLogger(AnalysisAndReportController.class);

    @Autowired
    private BirtReportService birtReportService;

    @RequestMapping(value = "/orderSourcesReportOrg/{orgId}/{format}", method = RequestMethod.GET)
    @ResponseBody
    public void listReports(@PathVariable("orgId") int orgId, @PathVariable("format") ReportFormat format, HttpServletRequest request, HttpServletResponse response) throws Exception {
        birtReportService.orderSourcesReportOrg(orgId, format, request, response);
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        logger.error("Request: " + req.getRequestURL() + " raised " + ex);
        ModelAndView mav = new ModelAndView("reportingError");
        mav.addObject("errMsg", ex.getLocalizedMessage());
        mav.addObject("url", req.getRequestURL());
        return mav;
    }
}
