package com.emanage.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.emanage.config.CSConstants;
import com.emanage.constants.PlatterType;
import com.emanage.domain.CuisineMenu;
import com.emanage.domain.MainMenus;
import com.emanage.domain.Platter;
import com.emanage.domain.PlatterMenuWrapper;
import com.emanage.domain.Restaurant;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.Sections;
import com.emanage.domain.VendorDish;
import com.emanage.dto.GetCourseRequestDTO;
import com.emanage.dto.LocationLatLong;
import com.emanage.dto.RegResultDTO;
import com.emanage.enums.ClassMode;
import com.emanage.enums.Status;
import com.emanage.service.CuisineNDishService;
import com.emanage.service.DemoRegdService;
import com.emanage.service.RestaurantService;
import com.emanage.service.VendorDishService;
import com.emanage.service.VendorPlatterService;
import com.emanage.utility.StringUtility;

import io.swagger.annotations.Api;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@Component
@RequestMapping("/vendorPlatterMenu")
@Api(description = "Vendor Plater Menu REST API's")
public class VendorPlatterController {

	@Autowired
	private VendorPlatterService vendorPlatterService;

	@Autowired
	private VendorDishService vendorDishSerivce;

	@Autowired
	private CuisineNDishService cuisineNDishService;

	@Autowired
	private RestaurantService restService;
	
	@Autowired
	private DemoRegdService DemoRegdService;
	

	@RequestMapping("/")
	@ApiIgnore
	public String listMenus(Map<String, Object> map, HttpServletRequest request) {

		map.put("menu", new Platter());
		List<Platter> platter = vendorPlatterService
				.listPlattersByVendorId((Integer) request.getSession().getAttribute("vendorId"));
		map.put("plattersList", platter);
		// map.put("dishList", vendorDishSerivce.listVendorDishByVendorId((Integer)
		// request.getSession().getAttribute("vendorId")));
		return "manageVendorSystem";
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@ApiIgnore
	public String createMenu(Map<String, Object> map, HttpServletRequest request) {

		/* Here we're only creating menu not saving sections,items to it */
		map.put("menu", new Platter());
		map.put("statusTypes", Status.values());
		map.put("classMode", ClassMode.values());
		map.put("platterTypeList", PlatterType.values());
		map.put("sectionTypeList", cuisineNDishService
				.listSectionTypesByCountryId((Integer) request.getSession().getAttribute("countryId")));
		map.put("cuisineTypeList", cuisineNDishService
				.listCuisineTypesByCountryId((Integer) request.getSession().getAttribute("countryId")));
		map.put("dishList",
				vendorDishSerivce.listVendorDishByVendorId((Integer) request.getSession().getAttribute("vendorId")));
		map.put("tagList", cuisineNDishService.listCuisineTypes((Integer) request.getSession().getAttribute("countryId")));
		
		
		return "newPlatter";
	}

	@RequestMapping("/edit/{platterId}")
	@ApiIgnore
	public String editMenu(Map<String, Object> map, HttpServletRequest request,
			@PathVariable("platterId") Integer platterId) {

		/* Here we're populating menu to edit */
		Platter platter = vendorPlatterService.getPlatter(platterId);
		map.put("menu", platter);
		map.put("statusTypes", Status.values());
		map.put("classMode", ClassMode.values());
		map.put("platterTypeList", PlatterType.values());
		map.put("sectionTypeList", cuisineNDishService
				.listSectionTypesByCountryId((Integer) request.getSession().getAttribute("countryId")));
		map.put("cuisineTypeList", cuisineNDishService
				.listCuisineTypesByCountryId((Integer) request.getSession().getAttribute("countryId")));
		map.put("dishList",
				vendorDishSerivce.listVendorDishByVendorId((Integer) request.getSession().getAttribute("vendorId")));
		return "newPlatter";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiIgnore
	public String addMenu(@ModelAttribute("menu") Platter menu, BindingResult result,
			@RequestParam("dishIds") Integer[] dishIds, HttpServletRequest request) {

		ArrayList<VendorDish> dishes = new ArrayList<VendorDish>();
		if (dishIds != null && dishIds.length > 0) {
			List<VendorDish> dishesArr = vendorDishSerivce.getVendorDishes(dishIds);
			HashMap<Integer, VendorDish> dishesMap = new HashMap<Integer, VendorDish>();

			if (dishesArr != null) {
				for (VendorDish dish : dishesArr) {
					dishesMap.put(dish.getDishId(), dish);
				}
			}
			for (Integer dishId : dishIds) {
				VendorDish dish = dishesMap.get(dishId);
				if (dish != null) {
					dishes.add(dish);
				}
			}
		}
		vendorPlatterService.addPlatter(menu);

		return "redirect:/vendorPlatterMenu/";
	}

	@RequestMapping(value = "/addNew", method = RequestMethod.POST)
	@ApiIgnore
	public String addNewMenu(@ModelAttribute("menu") Platter menu, BindingResult result,
			@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		FileOutputStream fos = null;
		String fileUrl = menu.getImageUrl();
		if(menu.getPlatterId()!=null && menu.getTagList().trim().length()==0) {
			Platter plat = 	vendorPlatterService.getPlatter(menu.getPlatterId());
				if(plat.getTagList()!=null && plat.getTagList().trim().length()>0) {
					menu.setTagList(plat.getTagList());
				}
				
		}
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				String fileDir = File.separator + "static" + File.separator + menu.getVendorId() + File.separator;
				fileUrl = fileDir + menu.getVendorId() + "_menu_"
						+ file.getOriginalFilename().replaceAll("[^a-zA-Z0-9_.]", "_");
				File dir = new File("webapps" + fileDir);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				File outfile = new File("webapps" + fileUrl);
				fos = new FileOutputStream(outfile);
				fos.write(bytes);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} 
		
		if (!fileUrl.equals(menu.getImageUrl()) && menu.getImageUrl().startsWith("/")) {
			File oldFile = new File("webapps" + menu.getImageUrl());
			if (oldFile.exists()) {
				oldFile.delete();
			}
		}
		menu.setImageUrl(fileUrl);
		menu.setVendorId(menu.getVendorId());
		menu.setCountryId(menu.getCountryId());
		
		Set<String> dishIds = new HashSet<String>();
		List<Sections> menuSections = menu.getSections();
		TreeMap<Integer, Sections> sectionTree = new TreeMap<Integer, Sections>();
		List<Integer> removedSections = new ArrayList<Integer>();
		if (menuSections != null && menuSections.size() > 0) {
			for (Sections menuSection : menuSections) {
				if (menuSection.isValid() && !StringUtility.isNullOrEmpty(menuSection.getDishIds())) {
					sectionTree.put(menuSection.getPosition(), menuSection);
					String[] dishIdsStrArr = menuSection.getDishIds().split(CSConstants.COMMA);

					if (dishIdsStrArr != null) {
						dishIds.addAll(Arrays.asList(dishIdsStrArr));
					}
				}
				if (!menuSection.isValid() && menuSection.getSectionId() != null && menuSection.getSectionId() > 0) {
					removedSections.add(menuSection.getSectionId());
				}
			}
		}
		Integer[] dishIdsArr = new Integer[dishIds.size()];

		HashMap<Integer, SectionDishes> dishMap = new HashMap<Integer, SectionDishes>();
		HashMap<Integer, String> dishAddOnPrice = new HashMap<Integer, String>();
		HashMap<Integer, String> dishDisplayPrice = new HashMap<Integer, String>();
		HashMap<Integer, String> dishIsReplaceable = new HashMap<Integer, String>();
		HashMap<Integer, String> replacements = new HashMap<Integer, String>();
		int dishCounter = 0;
		for (String dishId : dishIds) {
			if (dishId.equalsIgnoreCase("")) {
				continue;
			}
			String[] dishDeatils = dishId.split(CSConstants.HYPHEN);

			Integer dishID = Integer.parseInt(dishDeatils[0]);
			if (!("false".equals(dishDeatils[1]))) {
				dishIsReplaceable.put(dishID, dishDeatils[1]);
			}
			if (!("NA".equalsIgnoreCase(dishDeatils[2]))) {
				dishAddOnPrice.put(dishID, dishDeatils[2]);
			}
			if (!("NA".equalsIgnoreCase(dishDeatils[3]))) {
				replacements.put(dishID, dishDeatils[3].toString());
			}
			if (!("NA".equalsIgnoreCase(dishDeatils[4]))) {
				dishDisplayPrice.put(dishID, dishDeatils[4].toString());
			}

			dishIdsArr[dishCounter++] = dishID;
		}
		
		if (dishIds.size() > 0) {
			List<VendorDish> dishes = vendorDishSerivce.getVendorDishes(dishIdsArr);
			for (VendorDish dish : dishes) {
				SectionDishes sectionDish = new SectionDishes();

				if (dishIsReplaceable.containsKey(dish.getDishId())) {
					sectionDish.setReplaceable(Boolean.parseBoolean(dishIsReplaceable.get(dish.getDishId())));
				} else {
					sectionDish.setReplaceable(false);
				}

				if (replacements.containsKey(dish.getDishId())) {
					sectionDish.setReplacements(replacements.get(dish.getDishId()).substring(0,
							replacements.get(dish.getDishId()).length()));// (Float.parseFloat(dishAddOnPrice.get(dish.getDishId())));
				}

				if (dishAddOnPrice.containsKey(dish.getDishId())) {
					sectionDish.setPrice(Float.parseFloat(dishAddOnPrice.get(dish.getDishId())));
				}
				if (dishDisplayPrice.containsKey(dish.getDishId())) {
					sectionDish.setDisplayPrice(Float.parseFloat(dishDisplayPrice.get(dish.getDishId())));
				}

				sectionDish.setDishId(dish.getDishId());
				sectionDish.setAlcoholic(dish.getAlcoholic());
				sectionDish.setContents(dish.getContents());
				sectionDish.setCountryId(dish.getCountryId());
				sectionDish.setDescription(dish.getDescription());
				sectionDish.setDisabled(dish.getDisabled());
				sectionDish.setDishTypeId(dish.getDishTypeId());
				sectionDish.setImageUrl(dish.getImageUrl());
				sectionDish.setName(dish.getName());
				sectionDish.setQuantityInG(dish.getQuantityInG());
				sectionDish.setRectangularImageUrl(dish.getRectangularImageUrl());
				sectionDish.setShortDescription(dish.getShortDescription());
				sectionDish.setVegetarian(dish.getVegetarian());
				sectionDish.setVendorId(dish.getVendorId());
				sectionDish.setCouseDetailsURL(dish.getCourseDetailsURL());
				sectionDish.setCourseDuration(dish.getCourseDuration());
				sectionDish.setCourseDurationInHr(dish.getCourseDurationInHr());
				dishMap.put(dish.getDishId(), sectionDish);
			}
		}

		ArrayList<Sections> finalSections = new ArrayList<Sections>();
		for (Integer key : sectionTree.keySet()) {
			finalSections.add(sectionTree.get(key));
		}
		for (Sections section : finalSections) {
			ArrayList<SectionDishes> finalDishes = null;
			if (!StringUtility.isNullOrEmpty(section.getDishIds())) {
				String[] dishIdsStrArr = section.getDishIds().split(CSConstants.COMMA);
				if (dishIdsStrArr != null) {
					finalDishes = new ArrayList<SectionDishes>();
					for (int i = 0; i < dishIdsStrArr.length; i++) {
						if (dishIdsStrArr[i].equalsIgnoreCase("")) {
							continue;
						}
						String[] dishArr = dishIdsStrArr[i].split("-");
						finalDishes.add(dishMap.get(Integer.parseInt(dishArr[0])));
					}
				}
			}

			section.setDishes(finalDishes);
		}

		menu.setSections(finalSections);
		menu.setModifiedTime(new Date());

		
		if (fileUrl != null && fileUrl.contains("null_")) {
			String newFileUrl = renameFileToHaveMenuId(fileUrl, menu.getPlatterId());
			menu.setImageUrl(newFileUrl);
			vendorPlatterService.addPlatter(menu);
		}else {
			vendorPlatterService.addPlatter(menu);
		}
		if (removedSections.size() > 0) {
			vendorPlatterService.removeSections(removedSections);
		}

		return "redirect:/vendorPlatterMenu/";
	}

	private String renameFileToHaveMenuId(String fileUrl, Integer menuId) {
		File oldFile = new File("webapps" + fileUrl);
		String newFileUrl = fileUrl.replace("null_", menuId + "_");
		File newFile = new File("webapps" + newFileUrl);
		oldFile.renameTo(newFile);
		return newFileUrl;
	}

	@RequestMapping("/delete/{platterId}")
	@ApiIgnore
	public String deleteMenu(@PathVariable("platterId") Integer platterId, HttpServletRequest request) {

		Platter menu = vendorPlatterService.getPlatter(platterId);
		if (menu != null) {
			String menuImageUrl = menu.getImageUrl();
			if (!StringUtility.isNullOrEmpty(menuImageUrl) && menuImageUrl.startsWith("/")) {
				File image = new File("webapps" + menuImageUrl);
				if (image.exists()) {
					image.delete();
				}
			}
			vendorPlatterService.removePlatter(platterId);

		}

		return "redirect:/vendorPlatterMenu/";
	}

	@RequestMapping("/show/{menuId}")
	@ApiIgnore
	public String showMenu(Map<String, Object> map, @PathVariable("menuId") Integer menuId) {
		Platter menu = vendorPlatterService.getPlatter(menuId);
		map.put("platter", menu);
		return "showNewMenu";
	}

	@RequestMapping(value = "/getPlattersByLocation", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public MainMenus getAllPlattersByLocation(@RequestBody LocationLatLong locationLatLong,
			HttpServletRequest request) {
		MainMenus menus = new MainMenus();
		if (locationLatLong != null) {
			double latitude = locationLatLong.latitude;
			double longitude = locationLatLong.longnitude;

			List<Platter> platterList = vendorPlatterService.getPlatterByLatlong(latitude, longitude,
					locationLatLong.getDate(), locationLatLong.plateCount);
			List<PlatterMenuWrapper> menuWrappers = new ArrayList<PlatterMenuWrapper>();
			if (platterList != null && platterList.size() > 0) {
				Restaurant country = restService.getRestaurant(platterList.get(0).getCountryId());
				for (Platter menu : platterList) {
					menuWrappers.add(PlatterMenuWrapper.getMenuWrapper(menu, country.getTimeZone()));
				}
			}

			menus.setMenus(menuWrappers);
			return menus;
		}
		return menus;
	}

	@RequestMapping(value = "/getCuisinesByLocation", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<CuisineMenu> getAllCuisinesByLocation(@RequestBody LocationLatLong locationLatLong,
			HttpServletRequest request) {
		return vendorPlatterService.getCoursesByLatlong(locationLatLong);
	}
	
	@RequestMapping(value = "/getUserDemoRegd", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<RegResultDTO> getDemoRegdByUserId(@RequestParam(required = true) Integer customerId,
			HttpServletRequest request) {
		return DemoRegdService.listDemoByUserId(customerId);
	}
	

	@RequestMapping(value = "/getPlatterById", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody CuisineMenu getPlatterByPlatterId(@RequestBody  GetCourseRequestDTO courseReq) {
		if(courseReq.courseId!=null && courseReq.courseId>0) {
			return vendorPlatterService.getCourseById(courseReq.courseId);
		}else if(courseReq.eventId!=null && courseReq.eventId>0) {
			return vendorPlatterService.getPlatterByPlatterId(courseReq.eventId);
		}else {
			return null;
		}
	}
	
	@RequestMapping("/getCoursesByInstituteId/{instituteId}")
	@ResponseBody
	public List<CuisineMenu> getAllCuisinesByVendorId(@PathVariable("instituteId") Integer instituteId) {
		return vendorPlatterService.getCoursesByInstituteId(instituteId);
	}
	
	@RequestMapping("/featureCourses/{universityId}")
	@ResponseBody
	public List<CuisineMenu> getActiveFeatureCoursesByOrgId(@PathVariable("universityId") Integer universityId) {
		return vendorPlatterService.getFeaturedCanvasCourses(universityId);
	}
	
	@RequestMapping("/featureTrainings")
	@ResponseBody
	public List<CuisineMenu> getFeatureTrainings() {
		return vendorPlatterService.getFeatureTrainings();
	}
	
	@RequestMapping("/allActiveWebinars")
	@ResponseBody
	public List<CuisineMenu> getActiveWebinars() {
		return vendorPlatterService.getActiveWebinars();
	}
	
	@RequestMapping(value = "/allCoursesByCriteria", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public List<CuisineMenu> getAllCourseByCriteria(@RequestBody LocationLatLong locationLatLong,
			HttpServletRequest request) {
			return vendorPlatterService.getAllCourseByCriteriaNew(locationLatLong);
	}
	
	@RequestMapping("/getPopularCourses")
	@ResponseBody
	public List<CuisineMenu> getPopularCourses() {
		return vendorPlatterService.getPopularCourses();
	}
		
}
