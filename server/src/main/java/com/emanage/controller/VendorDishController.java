package com.emanage.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.VendorDish;
import com.emanage.enums.Status;
import com.emanage.service.CanvasLMSService;
import com.emanage.service.CuisineNDishService;
import com.emanage.service.VendorDishService;
import com.emanage.utility.ImageUtility;
import com.emanage.utility.StringUtility;

import io.swagger.annotations.Api;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@Component
@RequestMapping("/vendorDish")
@Api(description = "Coupon REST API's")
public class VendorDishController {

	@Autowired
	private VendorDishService vendorDishService;
	
	@Autowired
	private CuisineNDishService cuisineNDishService;
	
	@Autowired
	private CanvasLMSService canvasLMSService;
	
	
	private static int MAXFILESIZE=5;
	
	@RequestMapping("/")
	@ApiIgnore
	public String listVendorDishes(Map<String, Object> map, HttpServletRequest request) {

		VendorDish  vendorDish = new VendorDish();
		map.put("vendorDish", vendorDish);
		Integer vendorId = (Integer) request.getSession().getAttribute("vendorId");
		map.put("vendorDishList", vendorDishService.listVendorDishByVendorId(vendorId));
		Integer countryId = (Integer) request.getSession().getAttribute("countryId");
		map.put("dishTypes", cuisineNDishService.listDishTypesByCountryId(countryId));
		map.put("cuisineTypes", cuisineNDishService.listCuisineTypesByCountryId(countryId));
		return "vendorDish";
	}
	
	@RequestMapping("/edit/{dishId}")
	@ApiIgnore
	public String editVendorDish(Map<String, Object> map, HttpServletRequest request, @PathVariable("dishId") Integer dishId) {
		VendorDish vendorDish = vendorDishService.getVendorDish(dishId);
		
		map.put("vendorDish", vendorDish);
		Integer vendorId = (Integer) request.getSession().getAttribute("vendorId");
		Integer countryId = (Integer) request.getSession().getAttribute("countryId");
		map.put("vendorDishList", vendorDishService.listVendorDishByVendorId(vendorId));
		map.put("dishTypes", cuisineNDishService.listDishTypesByCountryId(countryId));
		map.put("cuisineTypes", cuisineNDishService.listCuisineTypesByCountryId(countryId));
		return "vendorDish";
	} 
	
	@RequestMapping(value = "/getLMSCourses/{vendorId}", method = RequestMethod.GET)
	@ApiIgnore
	public String getLMSDataFromDB(Map<String, Object> map,
			@PathVariable("vendorId") Integer vendorId) {

		if (vendorId != null) {
			map.put("courses", canvasLMSService.getAllCourseList(vendorId));
		}
		return "listLMSCourse";
	}
	
	@RequestMapping(value = "/deleteLMSCourse/{courseId}", method = RequestMethod.GET)
	@ApiIgnore
	public String deleteLMSCourse(Map<String, Object> map,@PathVariable("courseId") Integer courseId) {
		Canvas_Courses course=null;
		if (courseId != null) {
			course = canvasLMSService.getCourseById(courseId);
			canvasLMSService.deleteLMSCourse(course);
		}
		return "redirect:/vendorDish/getLMSCourses/"+course.getVendorId();
	}
	
	@RequestMapping(value = "/deactivateLMSCourse/{courseId}", method = RequestMethod.GET)
	@ApiIgnore
	public String updatesLMSCourse(Map<String, Object> map,
			@PathVariable("courseId") Integer courseId) {
		Canvas_Courses course=null;
		if (courseId != null) {
			course = canvasLMSService.getCourseById(courseId);
			if(course.getStatus()==Status.ACTIVE) {
				course.setStatus(Status.INACTIVE);
			}else {
				course.setStatus(Status.ACTIVE);
			}
			canvasLMSService.updateLMSCourse(course);
		}
		return "redirect:/vendorDish/getLMSCourses/"+course.getVendorId();
	}
	
	@RequestMapping("/saveLMSCourse/{courseId}")
	@ApiIgnore
	public String editLMSViewCourse(Map<String, Object> map, HttpServletRequest request, @PathVariable("courseId") Integer courseId) {
		
		Canvas_Courses courseLMS =canvasLMSService.getCourseById(courseId);
		map.put("courseLMS", courseLMS);
		map.put("statusTypes", com.emanage.enums.Status.values());
		Integer vendorId = (Integer) request.getSession().getAttribute("vendorId");
		map.put("courses", canvasLMSService.getAllCourseList(vendorId));
		
		return "listLMSCourse";
	}
	
	@RequestMapping(value = "/addDish", method = RequestMethod.POST)
	@ApiIgnore
	public String addDish(Map<String, Object> map, @ModelAttribute("dish")
	VendorDish vendorDish, BindingResult result, @RequestParam("file") MultipartFile squareImage, @RequestParam("file[1]") MultipartFile rectangularImage,@RequestParam("file[2]") MultipartFile courseDetailsURL, HttpServletRequest request) throws Exception {
		FileOutputStream fos = null;
		//String fileUrl = dish.getImageUrl();
		String outFileUrl = null;
		String imageUrls= null;
//		if(vendorDish!=null && vendorDish.getDishType()!=null) {
//			vendorDish = vendorDishService.addVendorDish(vendorDish);	//why??
//		}
		ArrayList<MultipartFile> files = new ArrayList<MultipartFile>();
		//List<Dish_Size> dSize= new ArrayList<Dish_Size>();
		files.add(squareImage);
		files.add(rectangularImage);
		files.add(courseDetailsURL);
		//int id = vendorDish.getDishId();
		
		if (files != null && files.size() == 3) {
		//	String[] fileUrls = new String[2];
			int iter = 0;
			for (MultipartFile file : files) {
				
				String fileUrl = null;
				if (iter==0) {
					fileUrl = vendorDish.getImageUrl();
				} else if (iter==1) {
					fileUrl = vendorDish.getRectangularImageUrl();
				}else if (iter==2) {
					fileUrl = vendorDish.getCourseDetailsURL();
				}
				
				if (!file.isEmpty()) {
					if (file.getSize() > MAXFILESIZE*1000*1000) {
						result.rejectValue("imageUrl", "error.upload.sizeExceeded", "You cannot upload the file of more than " + MAXFILESIZE + " MB");
						map.put("dish", vendorDish);
						map.put("dishList", vendorDishService.listVendorDishByVendorId(vendorDish.getVendorId()));
						return "dish";
					}
		            try {
						byte[] bytes = file.getBytes();
						String fileDir = File.separator + "static" + File.separator + vendorDish.getVendorId() + File.separator ;
						
						if (iter == 0)
							fileUrl = fileDir + vendorDish.getDishId() + file.getOriginalFilename().replaceAll("[^a-zA-Z0-9_.]", "_");
						 else if (iter == 1)
							 fileUrl = fileDir + "Rect_"+vendorDish.getDishId() + file.getOriginalFilename().replaceAll("[^a-zA-Z0-9_.]", "_");
						 else if (iter == 2)
							 fileUrl = fileDir + "doc_"+vendorDish.getDishId() + file.getOriginalFilename().replaceAll("[^a-zA-Z0-9_.]", "_");
						
						File dir = new File("webapps" + fileDir);
						if (!dir.exists()) { 
							dir.mkdirs();
						}
						outFileUrl = "webapps" + fileUrl;
						File outfile = new File(outFileUrl); 
						fos = new FileOutputStream(outfile);
						fos.write(bytes);
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						if (fos != null) {
							try {
								fos.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
		            if (iter==0){
		            	if (!StringUtility.isNullOrEmpty(outFileUrl))
		            	ImageUtility.resizeImage(outFileUrl, ImageUtility.getSmallImageUrl(outFileUrl, 200, 200), ImageUtility.getFileFormat(outFileUrl)/*"jpg"*/, 200, 200);
		            }
		            // store the bytes somewhere
		           //return "uploadSuccess";
		       } else {
		           //return "uploadFailure";
		       }
				
				if (iter == 0)
					imageUrls  = vendorDish.getImageUrl();
				 else if (iter == 1)
					imageUrls   = "Rect_"+vendorDish.getRectangularImageUrl();
				 else if (iter == 2)
						imageUrls   = "doc_"+vendorDish.getCourseDetailsURL();
			
				if (imageUrls!=null && !fileUrl.equals(imageUrls) && imageUrls.startsWith("/")) {
					File oldFile = new File("webapps" + vendorDish.getImageUrl());
					if (oldFile.exists()) {
						oldFile.delete();
					}
					File oldSmallFile = new File("webapps" + ImageUtility.getSmallImageUrl(vendorDish.getImageUrl(), 200, 200) );
					if (oldSmallFile.exists()) {
						oldSmallFile.delete();
					}
				}
				if (iter == 0)
					vendorDish.setImageUrl(fileUrl);
				else if (iter == 1) 
					vendorDish.setRectangularImageUrl(fileUrl);
				else if (iter == 2) 
					vendorDish.setCourseDetailsURL(fileUrl);
				
					if (fileUrl.contains("null_")) {
						String newFileUrl = renameFileToHaveDishId(fileUrl, vendorDish.getDishId());
						if (iter == 0) 
							vendorDish.setImageUrl(newFileUrl);
						else if (iter == 1) 
							vendorDish.setRectangularImageUrl(newFileUrl);
						else if (iter == 2) 
							vendorDish.setCourseDetailsURL(newFileUrl);
					//	vendorDishService.addVendorDish(vendorDish); //will check it again
						String smallFileOldUrl = ImageUtility.getSmallImageUrl(fileUrl, 200, 200);
						renameFileToHaveDishId(smallFileOldUrl, vendorDish.getDishId());
					}
				iter++;
		}
		
			vendorDishService.addVendorDish(vendorDish);
			if ( vendorDish.getDishId() != null &&  vendorDish.getDishId() > 0) {
			//	vendorDishService.updateMenuModificationTime(vendorDish.getDishId());
			} 
		}
		return "redirect:/vendorDish/";
	}

	private String renameFileToHaveDishId(String fileUrl, Integer dishId) {
		File oldFile = new File("webapps" + fileUrl);
		String newFileUrl = fileUrl.replace("null_", dishId + "_");
		File newFile = new File("webapps" + newFileUrl);
		oldFile.renameTo(newFile);
		return newFileUrl;
	}

	@RequestMapping("/delete/{dishId}")
	@ApiIgnore
	public String deleteDish(Map<String, Object> map, HttpServletRequest request, @PathVariable("dishId") Integer dishId) {

		try {
			VendorDish dish = vendorDishService.getVendorDish(dishId);
			if (dish != null) {
				String dishImageUrl = dish.getImageUrl();
				vendorDishService.removeVendorDish(dishId);
				if (!StringUtility.isNullOrEmpty(dishImageUrl) && dishImageUrl.startsWith("/")) {
					File image = new File("webapps" + dishImageUrl);
					if (image.exists()) {
						image.delete();
					}
					File smallImage = new File("webapps" + ImageUtility.getSmallImageUrl(dishImageUrl, 200, 200));
					if (smallImage.exists()) {
						smallImage.delete();
					}
				}
			}
		} catch (DataIntegrityViolationException exp) {
			map.put("errorMsg", "Sorry, this dish is associated with some id and could not be deleted");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMsg", "Sorry, something went wrong and we could not delete this dish.");
		}
		return listVendorDishes(map, request);
	}

	
}
