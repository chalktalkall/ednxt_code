package com.emanage.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.emanage.config.CSConstants;
import com.emanage.domain.Restaurant;
import com.emanage.domain.Vendor;
import com.emanage.dto.VendorListRequest;
import com.emanage.enums.LMS_NAME;
import com.emanage.enums.Status;
import com.emanage.enums.restaurant.ChargesType;
import com.emanage.service.CanvasLMSService;
import com.emanage.service.RestaurantService;
import com.emanage.service.VendorPlatterService;
import com.emanage.service.VendorService;
import com.emanage.utility.StringUtility;

import io.swagger.annotations.Api;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@Component
@RequestMapping("/vendor")
@Api(description = "Coupon REST API's")
public class VendorController {

	final static Logger logger = Logger.getLogger(VendorController.class);
	private static int MAXFILESIZE = 5;

	@Autowired
	private VendorService vendorService;

	@Autowired
	private VendorPlatterService vendorPlatterService;

	@Autowired
	private CanvasLMSService canvasLMSService;
	
	@Autowired
	RestaurantService restaurantService;

	@RequestMapping("/createVendor")
	@ApiIgnore
	public String createVendor(Map<String, Object> map, HttpServletRequest request) {
		map.put("vendor", new Vendor());
		map.put("chargeTypes", ChargesType.values());
		map.put("timeZones", CSConstants.timeZoneIds);
		map.put("openFlag", CSConstants.openFlag);
		map.put("statusTypes", com.emanage.enums.Status.values());
		map.put("lmsList", LMS_NAME.values());
		ArrayList<String> countryName = new ArrayList<String>();
		String[] locales = Locale.getISOCountries();
		for (String countryCode : locales) {
			Locale obj = new Locale("", countryCode);
			countryName.add(obj.getDisplayCountry(Locale.ENGLISH));
		}
		map.put("countryList", countryName);
		// map.put("vendorList", vendorService.listVendorById((Integer)
		// request.getSession().getAttribute("countryId")));
		return "addVendor";
	}

	@RequestMapping("/listVendor")
	@ApiIgnore
	public String listVendor(Map<String, Object> map, HttpServletRequest request) {
		List<Vendor> vendorList = vendorService
				.listVendorByCountryId((Integer) request.getSession().getAttribute("restaurantId"));
		map.put("vendorList", vendorList);
		return "listVendor";
	}

	@RequestMapping(value = "/edit/{vendorId}", method = RequestMethod.GET)
	@ApiIgnore
	public String editVendor(Map<String, Object> map, 
			@PathVariable("vendorId") Integer vendorId) {

		Vendor vendor = vendorService.getVendor((Integer) vendorId);
		if (vendorId != null) {
			map.put("vendor", vendor);
			map.put("chargeTypes", ChargesType.values());
			map.put("timeZones", CSConstants.timeZoneIds);
			map.put("openFlag", CSConstants.openFlag);
			map.put("statusTypes", com.emanage.enums.Status.values());
			map.put("lmsList", LMS_NAME.values());
			ArrayList<String> countryName = new ArrayList<String>();
			String[] locales = Locale.getISOCountries();
			for (String countryCode : locales) {
				Locale obj = new Locale("", countryCode);
				countryName.add(obj.getDisplayCountry(Locale.ENGLISH));
			}
			map.put("countryList", countryName);
			return "addVendor";
		}
		return "listVendor";
	}

	@RequestMapping(value = "/fetchLMSData/{vendorId}", method = RequestMethod.GET)
	@ApiIgnore
	public String fetchLMSData(@PathVariable("vendorId") Integer vendorId) {

		if (vendorId != null) {
			canvasLMSService.getCourseByCollegeCampus(vendorId);
		}
		return "redirect:/vendor/listVendor";
	}


	@RequestMapping("/disableEnable/{vendorId}")
	@ApiIgnore
	public String disableEnableCoupon(Map<String, Object> map, HttpServletRequest request,
			@PathVariable("vendorId") Integer vendorId, RedirectAttributes redirectAttributes) {
		try {
			Vendor vendor = vendorService.getVendor(vendorId);
			if (vendor != null) {
				if (vendor.getStatus() == Status.ACTIVE) {
					vendor.setStatus(Status.INACTIVE);
				} else if (vendor.getStatus() == Status.INACTIVE) {
					vendor.setStatus(Status.ACTIVE);
				}
				vendorService.saveVendor(vendor);
			}
		} catch (Exception e) {
			map.put("errorMsg", "Sorry, something went wrong and we could not disable/enable this AddOn.");
		}

		// redirecting to api call to function (not jsp page)
		return "redirect:/vendor/listVendor";// createCoupon(map, request);
	}

	@RequestMapping(value = "/addUpdateVendor")
	@ApiIgnore
	public String updateUser(Map<String, Object> map, @ModelAttribute("user") Vendor vendor, BindingResult result,
			@RequestParam("files[0]") MultipartFile portraitImage,
			@RequestParam("files[1]") MultipartFile landscapeImage, @RequestParam("files[2]") MultipartFile image1,
			@RequestParam("files[3]") MultipartFile image2, @RequestParam("files[4]") MultipartFile image3,
			@RequestParam("files[5]") MultipartFile image4, @RequestParam("files[6]") MultipartFile image5,
			HttpServletRequest request) {
		FileOutputStream fos = null;
		ArrayList<MultipartFile> files = new ArrayList<MultipartFile>();
		files.add(portraitImage);
		files.add(landscapeImage);
		files.add(image1);
		files.add(image2);
		files.add(image3);
		files.add(image4);
		files.add(image5);

		if (vendor.getVendorId() == null) {
			vendor = vendorService.addVendor(vendor);
			
		}

		if (files != null && files.size() == 7) {
			String[] fileUrls = new String[11];
			int iter = 0;
			for (MultipartFile file : files) {
				String fileUrl = null;
				if (iter == 0) {
					fileUrl = vendor.getBusinessPortraitImageUrl();
				} else if (iter == 1) {
					fileUrl = vendor.getBusinessLandscapeImageUrl();
				} else if (iter == 2) {
					fileUrl = vendor.getInstituteImageURLF();
				} else if (iter == 3) {
					fileUrl = vendor.getInstituteImageURLS();
				} else if (iter == 4) {
					fileUrl = vendor.getInstituteImageURLT();
				} else if (iter == 5) {
					fileUrl = vendor.getInstituteImageURLFO();
				} else if (iter == 6) {
					fileUrl = vendor.getInstituteImageURLFI();
				}

				if (!file.isEmpty()) {
					if (file.getSize() > MAXFILESIZE * 1000 * 1000) {
						String rejectValueName = null;
						if (iter == 0) {
							rejectValueName = "businessPortraitImageUrl";
						} else if (iter == 1) {
							rejectValueName = "businessLandscapeImageUrl";
						} else if (iter == 2) {
							rejectValueName = "packagingImageUR";
						}

						result.rejectValue(rejectValueName, "error.upload.sizeExceeded",
								"You cannot upload the file of more than " + MAXFILESIZE + " MB");
						map.put("vendor", vendor);
						return "editVendor";
					}
					try {
						byte[] bytes = file.getBytes();
						String fileDir = File.separator + "static" + File.separator + vendor.getVendorId()
								+ File.separator;
						String filePrefix = null;
						if (iter == 0) {
							filePrefix = "portrait";
						} else if (iter == 1) {
							filePrefix = "landscape";
						} else if (iter == 2) {
							filePrefix = "packagingImage";
						} else if (iter == 2) {
							filePrefix = "image1";
						} else if (iter == 3) {
							filePrefix = "image2";
						} else if (iter == 4) {
							filePrefix = "image3";
						} else if (iter == 5) {
							filePrefix = "image5";
						} else if (iter == 6) {
							filePrefix = "image6";
						} else {
							filePrefix = "button";
						}

						fileUrl = fileDir + filePrefix + "_"
								+ file.getOriginalFilename().replaceAll("[^a-zA-Z0-9_.]", "_");
						fileUrls[iter] = fileUrl;
						File dir = new File("webapps" + fileDir);
						if (!dir.exists()) {
							dir.mkdirs();
						}
						File outfile = new File("webapps" + fileUrl);
						fos = new FileOutputStream(outfile);
						fos.write(bytes);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.info("Exception mail sent");
					} finally {
						if (fos != null) {
							try {
								fos.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								logger.info("Exception mail sent");
								e.printStackTrace();
							}
						}
					}
				}
				iter++;
			}

			for (iter = 0; iter < 11; iter++) {
				String existingImageUrl = null;
				if (iter == 0) {
					existingImageUrl = vendor.getBusinessPortraitImageUrl();
				} else if (iter == 1) {
					existingImageUrl = vendor.getBusinessLandscapeImageUrl();
				} else if (iter == 2) {
					existingImageUrl = vendor.getInstituteImageURLF();
				} else if (iter == 3) {
					existingImageUrl = vendor.getInstituteImageURLS();
				} else if (iter == 4) {
					existingImageUrl = vendor.getInstituteImageURLT();
				} else if (iter == 5) {
					existingImageUrl = vendor.getInstituteImageURLFO();
				} else if (iter == 6) {
					existingImageUrl = vendor.getInstituteImageURLFI();
				}

				String fileUrl = fileUrls[iter];
				if (!StringUtility.isNullOrEmpty(fileUrl)) {
					if (!fileUrl.equals(existingImageUrl) && !StringUtility.isNullOrEmpty(existingImageUrl)
							&& existingImageUrl.startsWith("/")) {
						File oldFile = new File("webapps" + existingImageUrl);
						if (oldFile.exists()) {
							oldFile.delete();
						}
					}
					if (iter == 0) {
						vendor.setBusinessPortraitImageUrl(fileUrl);
					} else if (iter == 1) {
						vendor.setBusinessLandscapeImageUrl(fileUrl);
					} else if (iter == 2) {
						vendor.setInstituteImageURLF(fileUrl);
					} else if (iter == 3) {
						vendor.setInstituteImageURLS(fileUrl);
					} else if (iter == 4) {
						vendor.setInstituteImageURLT(fileUrl);
					} else if (iter == 5) {
						vendor.setInstituteImageURLFO(fileUrl);
					} else if (iter == 6) {
						vendor.setInstituteImageURLFI(fileUrl);
					}

				}
			}
		}
		Restaurant rest =  restaurantService.getRestaurant(vendor.getCountryId());
		vendor.setOrgId(rest.getParentRestaurantId());
		vendorService.addVendor(vendor);
		return "redirect:/vendor/listVendor";
	}

	@RequestMapping(value = "/removeImage", method = RequestMethod.GET)
	@ApiIgnore
	public String removeCloseImage(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String parameter = request.getParameter("parameter");
		Integer vendorId = Integer.parseInt(request.getParameter("vendorId"));

		Vendor vendor = vendorService.getVendor(vendorId);
		if (vendor != null) {
			if (parameter.equalsIgnoreCase("businessLandscapeImageUrl")) {
				vendor.setBusinessLandscapeImageUrl("");
			} else if (parameter.equalsIgnoreCase("businessPortraitImageUrl")) {
				vendor.setBusinessPortraitImageUrl("");
			} else if (parameter.equalsIgnoreCase("instituteImageURLF")) {
				vendor.setInstituteImageURLF("");
			} else if (parameter.equalsIgnoreCase("instituteImageURLS")) {
				vendor.setInstituteImageURLS("");
			} else if (parameter.equalsIgnoreCase("instituteImageURLT")) {
				vendor.setInstituteImageURLT("");
			} else if (parameter.equalsIgnoreCase("instituteImageURLFO")) {
				vendor.setInstituteImageURLFO("");
			} else if (parameter.equalsIgnoreCase("instituteImageURLFI")) {
				vendor.setInstituteImageURLFI("");
			}

			vendorService.addVendor(vendor);
		}
		return "redirect:/vendor/edit/" + vendor.getVendorId();
	}

	@RequestMapping("/view/{vendorId}")
	@ApiIgnore
	public String list(Map<String, Object> map, HttpServletRequest request,
			@PathVariable("vendorId") Integer vendorId) {
		Vendor vendor = vendorService.getVendor(vendorId);
		TreeMap<String, String> map1 = new TreeMap<String, String>();
		if (vendor.getVendorId() != null) {
			request.getSession().setAttribute("vendorId", vendor.getVendorId());
			request.getSession().setAttribute("countryId", vendor.getCountryId());
			request.getSession().setAttribute("vendorName", vendor.getBusinessName());
			map1.put("vendorPlatterMenu/", "Manage Menu");
			map1.put("vendorDish/", "Create Courses/Events");
			map.put("plattersList", vendorPlatterService.listPlattersByVendorId(vendorId));

		}
		request.getSession().setAttribute("vendorPops", map1);

		return "manageVendorSystem";
	}

	@RequestMapping(value = "/listVendorsById", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody List<Vendor> getPlatterByPlatterId(@RequestBody VendorListRequest vendorListRequest,
			HttpServletRequest request) {
		if (vendorListRequest.orgId != null && vendorListRequest.orgId>0 ) {
			return vendorService.listVendorByOrgId(vendorListRequest.orgId);
		} else {
			return vendorService.listActiveVendorById(vendorListRequest.universityId);
		}

	}
}
