package com.emanage.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanage.domain.DBConfig;
import com.emanage.domain.Vendor;
import com.emanage.enums.DataSourceType;
import com.emanage.service.VendorDBConfigService;
import com.emanage.service.VendorService;

import io.swagger.annotations.Api;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@Component
@RequestMapping("/vendorDBConfig")
@Api(description = "Vendor DB Menu REST API's")
public class VendorDBConfigController {

	@Autowired
	private VendorDBConfigService vendorDBConfigService;
	
	@Autowired
	VendorService vendorService;
	
	@RequestMapping("/")
	@ApiIgnore
	public String listDB(Map<String, Object> map, HttpServletRequest request) {

		Integer univID=(Integer) request.getSession().getAttribute("countryId");
		List<Vendor> vendorList= vendorService.listActiveVendorById(univID);
		List<DBConfig> dbConfigList = vendorDBConfigService.listDBConfigByCountryId(univID);
		
		map.put("dbConfig", new DBConfig());
		map.put("dbType", DataSourceType.values());
		map.put("campusList", vendorList);
		map.put("dbConfigList", dbConfigList);
	
		return "dbconfig";
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@ApiIgnore
	public String createMenu(Map<String, Object> map, HttpServletRequest request) {

		Integer univID=(Integer) request.getSession().getAttribute("countryId");
		List<Vendor> vendorList= vendorService.listActiveVendorById(univID);
		List<DBConfig> dbConfigList = vendorDBConfigService.listDBConfigByCountryId(univID);
		
		map.put("dbConfig", new DBConfig());
		map.put("dbType", DataSourceType.values());
		map.put("campusList", vendorList);
		map.put("dbConfigList", dbConfigList);
		
		return "dbconfig";
	}

	@RequestMapping("/edit/{configId}")
	@ApiIgnore
	public String editMenu(Map<String, Object> map, HttpServletRequest request,
			@PathVariable("configId") Integer configId) {

		Integer univID=(Integer) request.getSession().getAttribute("countryId");
		List<Vendor> vendorList= vendorService.listActiveVendorById(univID);
		List<DBConfig> dbConfigList = vendorDBConfigService.listDBConfigByCountryId(univID);
		
		DBConfig dbConfig = vendorDBConfigService.getDBConfig(configId);
		map.put("dbConfig", dbConfig);
		map.put("campusList", vendorList);
		map.put("dbType", DataSourceType.values());
		map.put("dbConfigList", dbConfigList);
	
		return "dbconfig";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiIgnore
	public String addMenu(@ModelAttribute("dbConfig") DBConfig dbConfig, BindingResult result,
			 HttpServletRequest request) {

		vendorDBConfigService.addDBConfig(dbConfig);

		return "redirect:/vendorDBConfig/";
	}

	@RequestMapping("/delete/{configId}")
	@ApiIgnore
	public String deleteMenu(@PathVariable("configId") Integer configId, HttpServletRequest request) {
	
		vendorDBConfigService.removeDBConfig(configId);
		return "redirect:/vendorDBConfig/";
	}



	
		
}
