package com.emanage.controller;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emanage.domain.BookedPlatter;
import com.emanage.domain.Canvas_Courses;
import com.emanage.domain.CheckDishResponse;
import com.emanage.domain.Coupon;
import com.emanage.domain.CouponFlatRules;
import com.emanage.domain.CouponResponse;
import com.emanage.domain.CreditTransactions;
import com.emanage.domain.CreditType;
import com.emanage.domain.Customer;
import com.emanage.domain.Customers;
import com.emanage.domain.DCJson;
import com.emanage.domain.DemoRegd;
import com.emanage.domain.EditInvoices;
import com.emanage.domain.Invoice;
import com.emanage.domain.InvoiceResponse;
import com.emanage.domain.JsonAddOn;
import com.emanage.domain.JsonCouponInfo;
import com.emanage.domain.JsonDish;
import com.emanage.domain.JsonOrder;
import com.emanage.domain.Order;
import com.emanage.domain.OrderAddOn;
import com.emanage.domain.OrderDish;
import com.emanage.domain.OrderResponse;
import com.emanage.domain.Order_DCList;
import com.emanage.domain.PaymentType;
import com.emanage.domain.Platter;
import com.emanage.domain.PrintCheckFromDB;
import com.emanage.domain.Restaurant;
import com.emanage.domain.Role;
import com.emanage.domain.SectionDishes;
import com.emanage.domain.TaxType;
import com.emanage.domain.User;
import com.emanage.domain.Vendor;
import com.emanage.dto.DemoRegDTO;
import com.emanage.dto.DiscountDTO;
import com.emanage.dto.OrderDTO;
import com.emanage.dto.OrderPlatterDTO;
import com.emanage.dto.PlaceOrderDTO;
import com.emanage.dto.ResponseDTO;
import com.emanage.dto.credit.AddCreditToCustomerAccountDTO;
import com.emanage.dto.credit.CustomerCreditDTO;
import com.emanage.dto.saleRegister.TillCashUpdateDTO;
import com.emanage.dto.saleRegister.TillDTO;
import com.emanage.dto.saleRegister.TransactionDTO;
import com.emanage.enums.LMS_NAME;
import com.emanage.enums.check.BasePaymentType;
import com.emanage.enums.check.CheckType;
import com.emanage.enums.check.CustomPaymentType;
import com.emanage.enums.check.OrderSource;
import com.emanage.enums.check.PaymentMode;
import com.emanage.enums.credit.BilligCycle;
import com.emanage.enums.credit.CreditTransactionStatus;
import com.emanage.enums.credit.CustomerCreditAccountStatus;
import com.emanage.enums.order.DestinationType;
import com.emanage.enums.order.SourceType;
import com.emanage.enums.order.Status;
import com.emanage.enums.till.TillTransaction;
import com.emanage.enums.till.TillTransactionStatus;
import com.emanage.enums.till.TransactionCategory;
import com.emanage.service.AsyncService;
import com.emanage.service.CanvasLMSService;
import com.emanage.service.CashRegisterService;
import com.emanage.service.CouponService;
import com.emanage.service.CustomerCreditService;
import com.emanage.service.CustomerService;
import com.emanage.service.DemoRegdService;
import com.emanage.service.InvoiceService;
import com.emanage.service.OrderService;
import com.emanage.service.RestaurantService;
import com.emanage.service.SQLDataSource;
import com.emanage.service.TaxTypeService;
import com.emanage.service.UserService;
import com.emanage.service.VendorPlatterService;
import com.emanage.service.VendorService;
import com.emanage.utility.StringUtility;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/order")
@Api(description = "Place Order REST API's")
public class OrderControllerNew {

	final static Logger logger = Logger.getLogger(OrderControllerNew.class);

	@Autowired
	private RestaurantService restService;

	@Autowired
	private TaxTypeService taxTypeService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private VendorService vendorService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private UserService userService;
	
	@Autowired
	@Qualifier("customerCreditAutomatedBilling")
	private CustomerCreditService customerCreditService;

	@Autowired
	private CashRegisterController cashRegisterController;

	@Autowired
	private CashRegisterService cashRegisterService;

	@Autowired
	private CustomerController customerController;

	@Autowired
	private DemoRegdService demoRegdService;

	@Autowired
	private VendorPlatterService vendorPlatterService;
	
	@Autowired
	private CanvasLMSService canvasLMSService;
	
	@Autowired
	private SQLDataSource sqlDataSource;

	public HttpServletRequest REQUEST;

	@Autowired
	@Lazy(value = true)
	AsyncService asyncService;

	@RequestMapping(value = "/getDispatchedCancelOrders", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<OrderDTO> getDispatchedCancelOrders(HttpServletRequest request,
			HttpServletResponse response, @RequestParam(required = false) String restaurantId,
			@RequestParam(required = false) String[] orderType, @RequestParam(required = false) String ordersOfDay)
			throws Exception {

		Integer restaurantID;
		// String restIdStr = request.getParameter("restaurantId");

		if (restaurantId != null && !restaurantId.equals(""))
			restaurantID = Integer.parseInt(restaurantId);
		else if (request.getSession().getAttribute("restaurantId") != null)
			restaurantID = (Integer) request.getSession().getAttribute("restaurantId");
		else
			throw new Exception("RestaurantId not found!!");

		List<String> orderTypeList = Arrays.asList(orderType);
		// String ordersOfDay = request.getParameter("ordersOfDay");
		if (ordersOfDay == null || ordersOfDay == "")
			ordersOfDay = "Today";
		return orderService.getDispatchedCancelOrders(restaurantID, orderTypeList, ordersOfDay);
	}

	@RequestMapping(value = "/allDeliveryBoy.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody @JsonRawValue Map<String, List<Map>> getAllDeliveryBoy(HttpServletRequest request,
			HttpServletResponse response, @RequestParam(required = false) String restaurantId) throws JSONException {
		Integer roleId = 0;
		List<User> deliveryBoyList = null;
		// Integer restaurantId =
		// Integer.parseInt(request.getParameter("restaurantId"));
		Integer fulfillmentCenterId;
		String fulId = restaurantId;
		if (fulId != null)
			fulfillmentCenterId = Integer.parseInt(fulId);
		else
			fulfillmentCenterId = (Integer) request.getSession().getAttribute("restaurantId");
		Map<String, List<Map>> json = new HashMap<String, List<Map>>();
		List<Role> roles = userService.getUserRole();
		if (roles.size() > 0) {
			for (Role roleVal : roles) {
				if ("deliveryBoy".equalsIgnoreCase(roleVal.getRole())) {
					roleId = roleVal.getId();
				}
			}
			deliveryBoyList = userService.listUserByRole(fulfillmentCenterId, roleId);
			/*
			 * Collections.sort(deliveryBoyList, new Comparator<User>() { public int
			 * compare(User v1,User v2) { return
			 * v1.getUserName().compareTo(v2.getUserName()); } });
			 */
		}

		List<Map> kd = new ArrayList<Map>();
		for (User us : deliveryBoyList) {
			Map<String, Object> d = new HashMap<String, Object>();
			d.put("name", us.getFirstName() + " " + us.getLastName());
			d.put("id", us.getUserId().toString());
			d.put("fulfillmentCenterId", us.getKitchenId().toArray());
			kd.add(d);
		}

		json.put("deliveryBoy", kd);
		String data = String.format(json.toString());
		return json;
	}

	@RequestMapping(value = "/placeOrder", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody Map<String, Object> placeOrder(@RequestBody PlaceOrderDTO orderBody,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			Restaurant rest = restService.getRestaurant(orderBody.countryId);

			Customer customer = getCustomer(orderBody);
			Customer cust = customerService.getCustomer(customer.getCustomerId());

			if (rest.isEnableCustCredit() && "CUSTOMER CREDIT".equalsIgnoreCase(orderBody.order.paymentMethod)) {
				if (cust.getCredit() != null) {
					if (cust.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF
							&& cust.getCredit().getCreditBalance() > 0) {
						m.put("status", "error");
						m.put("error", "You can't place CUSTOMER CREDIT order for CREDIT Type ONE_OFF");
						return m;
					}
				}
			}
			customer.setRestaurantId(orderBody.countryId);
			customerController.setCustomerInfoJSON(customer, request);

			Invoice invoice = getCheck(customer.getCustomerId(), orderBody.countryId);
			invoice.setName(customer.getFirstName() + " " + customer.getLastName());
			//invoice.setPhone(customer.getPhone());
			invoice.setEmail(customer.getEmail());
			invoice.setClassMode(orderBody.order.classMode);
			invoice.setDeliveryAddress(orderBody.customer.address);
			List<Order_DCList> discount_Charge = getDCValue(orderBody);
			if (discount_Charge != null)
				invoice.setDiscount_Charge(discount_Charge);
			invoice.setInvoiceType(CheckType.Delivery);

			//======>
			//Integer vendorId = vendorPlatterService.getVendorIdUsingPlatterId(orderBody.order.items.get(0).menuId);
			
			invoiceService.addInvoice(invoice);
			JsonOrder jsonOrder = getJsonOrder(orderBody, customer, invoice);
			if(!orderBody.order.items.isEmpty()) {
				Vendor vendor =  vendorService.getVendor(invoice.getVendorId());
				if(vendor.getLms_name()==LMS_NAME.CANVAS) {
					Canvas_Courses canvasCourse = canvasLMSService.getCourseByCanvasId(vendor, orderBody.order.items.get(0).lms_course_id);
					if(canvasCourse == null) {
						m.put("status", "error");
						m.put("error", "We're very sorry, this course is no longer available");
						return m;
					}	
				}if(vendor.getLms_name()==LMS_NAME.DB_VIEW) {
				Canvas_Courses canvasCourse	=sqlDataSource.getViewDataCourse(vendor, orderBody.order.items.get(0).lms_course_id);
				if(canvasCourse == null) {
					m.put("status", "error");
					m.put("error", "We're very sorry, this course is no longer available");
					return m;
				}
				}
			}
			
			if (orderBody.order.couponCode != null) {
				jsonOrder.setCouponCode(orderBody.order.couponCode);
			}
			jsonOrder.setCheckType(CheckType.Delivery);

			OrderResponse orderResponse =addToInvoiceJSON(orderBody, jsonOrder, request, response);// addToCheckJSON(orderBody, jsonOrder, request, response);

			if ("Failed".equalsIgnoreCase(orderResponse.getStatus())) {
				if (orderResponse.getInvoiceNo() > 0) {
					invoiceService.removeInvoice(orderResponse.getInvoiceNo());
				}
				m.put("status", "error");
				m.put("error", orderResponse.getError());
			} else {

				//addPlatterBooking(orderBody, invoice.getVendorId());
				m.put("status", "success");
				m.put("Id", invoice.getInvoiceNo());
				m.put("paymentType", orderResponse.getStatus());
			}
		} catch (Exception e) {
			response.setStatus(500);
			m.put("status", "error");
			m.put("error", "Failure occur");
			e.printStackTrace();
			asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
			logger.info("Exception mail sent");
			e.printStackTrace();
		}

		return m;
	}

	public void addPlatterBooking(PlaceOrderDTO orderBody, Integer vendorId) throws ParseException {
		if (orderBody.order.noOfPeople > 0) {
			BookedPlatter booking = new BookedPlatter();

			String format = "yyyy-MM-dd HH:mm";
			SimpleDateFormat formatterD = new SimpleDateFormat(format);
			//Date deliveryTimeD = formatterD.parse(orderBody.order.deliveryDateTime);

			booking.setCount(orderBody.order.noOfPeople);
			booking.setPlatterId(orderBody.order.items.get(0).menuId);
			//booking.setDate(deliveryTimeD);
			booking.setVendorId(vendorId);
			vendorPlatterService.addPlatterBooking(booking);

		}
	}

	@RequestMapping(value = "/generateCheckForPrint", method = RequestMethod.GET)
	public String generateCheckFor (Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response, @RequestParam String checkId) {
		String billTemplateName = "defaultbill";
		Integer checkID = Integer.parseInt(checkId);
		Invoice check = invoiceService.getInvoice(checkID);
		Vendor vendor = vendorService.getVendor(check.getVendorId());
		if (check != null) {
			Restaurant rest = restService.getRestaurant(check.getCountryId());
			map.put("restaurant", rest);
			map.put("vendor", vendor);
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(rest.getTimeZone()));
			cal.setTime(check.getOpenTime());
			DateFormat formatter1;
			formatter1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			formatter1.setTimeZone(cal.getTimeZone());
			map.put("checkDate", formatter1.format(cal.getTime()));
			Float waveOff = null;

			PrintCheckFromDB checkResponse = new PrintCheckFromDB(check, taxTypeService, waveOff, rest);
			if (rest.isRoundOffAmount()) {
				checkResponse.setRoundedOffTotal(Math.round(checkResponse.getRoundedOffTotal()));
			}
			map.put("checkRespone", checkResponse);
			billTemplateName = "defaultbill";
			if (check.getCustomerId() > 0) {
				Customer customer = customerService.getCustomer(check.getCustomerId());

				map.put("customer", customer);
			}

			Map<String, JsonDish> itemsMap = new TreeMap<String, JsonDish>();
			List<CheckDishResponse> items = checkResponse.getItems();
			List<JsonAddOn> jsonAdd = new ArrayList<JsonAddOn>();
			for (CheckDishResponse item : items) {

				if (itemsMap.containsKey(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""))) {
					JsonDish jsonDish = itemsMap.get(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""));
					jsonDish.setPrice(jsonDish.getPrice() + item.getPrice());
					jsonDish.setQuantity(jsonDish.getQuantity() + 1);
				} else {
					JsonDish jsonDish = new JsonDish();
					jsonDish.setQuantity(1);
					jsonDish.setName(item.getName());
					jsonDish.setId(item.getDishId());
					jsonDish.setPrice(item.getPrice());
					if (item.getSections() != null) {
						jsonDish.setSection(Arrays.asList(item.getSections()));
					}
					// jsonDish.setDishSize(item.getDishSize());
					List<OrderAddOn> orderAddOn = item.getAddOnresponse();
					if (orderAddOn != null) {
						for (OrderAddOn oad : orderAddOn) {
							JsonAddOn jsonAddOn = new JsonAddOn();
							jsonAddOn.setItemId(oad.getAddOnId());
							jsonAddOn.setDishId(item.getDishId());
							jsonAddOn.setName(oad.getName());
							jsonAddOn.setPrice(oad.getPrice());
							jsonAddOn.setQuantity(oad.getQuantity());
							jsonAddOn.setDishSize(oad.getDishSize());
							jsonAdd.add(jsonAddOn);
							jsonDish.setAddOns(jsonAdd);
						}
					}
					itemsMap.put(item.getDishId() + "" + item.getDishSize().replaceAll("\\s", ""), jsonDish);
				}
			}
			map.put("itemsMap", itemsMap);
		}

		return "custom/" + billTemplateName;
	}

	public @ResponseBody OrderResponse addToCheckJSON(PlaceOrderDTO orderBody, JsonOrder order,
			HttpServletRequest request, HttpServletResponse reponse) throws Exception {
		Invoice invoice = null;
		String error = "";

		boolean editedOrder = orderBody.order.isEdited;
		boolean allowDiscount = false;
		String orderSource = "";
		if (orderBody.order.orderSource == null || orderBody.order.orderSource.equals(""))
			orderSource = "Website";
		else {
			orderSource = orderBody.order.orderSource;
		}

		String paymentStatus = orderBody.order.paymentMethod;
		String deliveryInst = orderBody.order.instructions;
		// String paymentThirdParty = orderBody.order.paymentMethod;

		Restaurant restaurant = null;
		Integer countryId = null;

		String paidStatus = (String) request.getAttribute("paidStatus");
		// String emailbill = request.getParameter("emailbill");

		boolean bFirstOrder = false;

		if (order.getInvoiceNo() > 0) {
			invoice = invoiceService.getInvoice(order.getInvoiceNo());
		}

		Customer customer = null;
		if (invoice == null) {
			if (order.getCustId() > 0) {
				customer = customerService.getCustomer(order.getCustId());
				if (customer != null) {
					countryId = customer.getRestaurantId();
					invoice = invoiceService.getInvoiceByCustId(countryId, customer.getCustomerId());
				}
			}
		}
		if (invoice != null) {
			countryId = invoice.getCountryId();
		} else {
			if (order.getCustId() > 0 && customer == null) {
				error = "No customer was found";
			} else {
				invoice = new Invoice();
				bFirstOrder = true;
				// invoice.setDeliveryAddress(order.getDeliveryAddress());
			}
		}

		Order targetOrder = null;
		OrderResponse orderResp = new OrderResponse();
		double caloriesCount = 0;
		
		
		if (StringUtility.isNullOrEmpty(error)) {

			List<JsonDish> jsonDishes = order.getItems();
			HashSet<Integer> hs = new HashSet<Integer>();
			List<Integer> li = new ArrayList<Integer>();
			for (JsonDish jsonDish : jsonDishes) {
				Platter platter = vendorPlatterService.getPlatter(jsonDish.getId());

				if (platter != null) {
					int microScreenId = 0;
					hs.add(microScreenId);
					li.add(microScreenId);
				} else {
					orderResp.setError("Sorry! this  is not a  valid dish");
					orderResp.setStatus("Failed");
					return orderResp;
				}
			}
			for (int fixSceenId : hs) {
				targetOrder = new Order();
				targetOrder.setMicroKitchenId(fixSceenId);
				targetOrder.setRestaurantId(countryId);
				targetOrder.setCheckId(invoice.getInvoiceNo());
				targetOrder.setCreatedTime(new Date());
				if (invoice.getCustomerId() > 0) {
					targetOrder.setSourceType(SourceType.COUNTER);
					targetOrder.setSourceId(order.getCustId());
					targetOrder.setDestinationType(DestinationType.COUNTER);
					targetOrder.setDestinationId(order.getCustId());

					if (paymentStatus != null) {
						if ("Online".equalsIgnoreCase(paymentStatus)
								|| "OnlinePayPal".equalsIgnoreCase(paymentStatus)) {
							targetOrder.setPaymentStatus("PG_PENDING");
						} else if ("MobiKwikWallet".equals(paymentStatus)) {
							targetOrder.setPaymentStatus("WALLET_PENDING");
						} else if ("payTm".equals(paymentStatus)) {
							targetOrder.setPaymentStatus("PAYTM_PENDING");
						} else {
							targetOrder.setPaymentStatus(paymentStatus);
						}
					} else {
						targetOrder.setPaymentStatus("COD");
					}
				}
				if (editedOrder) {
					Status status = (Status) request.getAttribute("editOrderStatus");
					String deliveryBoy = (String) request.getAttribute("editOrderDeliverBoy");
					Float moneyIn = (Float) request.getAttribute("editOrderMoneyIn");
					Float moneyOut = (Float) request.getAttribute("editOrderMoneyOut");
					int microId = (int) request.getAttribute("editOrderMicroKitchen");

					if (invoice.getStatus() != com.emanage.enums.check.Status.Paid) {
						invoice.setStatus(com.emanage.enums.check.Status.Unpaid);
					}
					// logger.info(" edit order status " + status);
					targetOrder.setStatus(status);
					if ("Paid".equalsIgnoreCase(paidStatus)) {
						if (PaymentMode.PAYTM_PENDING.toString().equalsIgnoreCase(targetOrder.getPaymentStatus())) {
							targetOrder.setPaymentStatus(PaymentMode.PAYTM.toString());
						} else {
							targetOrder.setPaymentStatus("PG");
						}
					}
					targetOrder.setDeliveryAgent(deliveryBoy);
					targetOrder.setMoneyIn(moneyIn);
					targetOrder.setMoneyOut(moneyOut);
					targetOrder.setMicroKitchenId(microId);

				} else {
					targetOrder.setStatus(Status.NEW);
				}
				// logger.info("15>Setting order status :" + Status.READY);
				Float addOnBill = 0.0f;
				Float bill = 0.0f;
				HashMap<String, OrderDish> orderDishMap = new HashMap<String, OrderDish>();
				customer = customerService.getCustomer(invoice.getCustomerId());
				if (orderSource == null || "".equals(orderSource)) {
					invoice.setOrderSource("Website");
				} else {
					invoice.setOrderSource(orderSource);
				}

				float dishPrice = 0.0f;
				for (JsonDish jsonDish : jsonDishes) {

					Platter platter = vendorPlatterService.getPlatter(jsonDish.getId());
					if (platter != null) {
						if(platter.getVendorMargin()!=null && platter.getVendorMargin()==0){
							Vendor vendor  = vendorService.getVendor(platter.getVendorId());
							invoice.setVedorMargin(vendor.getVendorMargin());
						}else {
							invoice.setVedorMargin(platter.getVendorMargin());
						}
						restaurant = restService.getRestaurant(invoice.getCountryId());
						List<com.emanage.domain.OrderSource> os = restService
								.listOrderSourcesByOrgId(restaurant.getParentRestaurantId());
						for (com.emanage.domain.OrderSource orderS : os) {
							if (orderS.getName().equalsIgnoreCase(invoice.getOrderSource())) {
							}
						}
						int microId = 0;

						if (microId == fixSceenId) {
							addOnBill = 0.0f;
							StringBuffer addOnId = new StringBuffer();
							addOnId.append("");
							String instStr = "";
							if (jsonDish.getInstructions() != null) {
								instStr = jsonDish.getInstructions().replaceAll("\\s+", "");
							}
							if (jsonDish.getAddOns() != null) {
								for (JsonAddOn jsonAddon : jsonDish.getAddOns()) {
									addOnId.append(jsonAddon.getDishId());
								}
							}

							if (orderDishMap.get(jsonDish.getId() + "" + instStr + "" + addOnId) != null) {
								orderDishMap.get(jsonDish.getId() + "" + instStr + "" + addOnId)
										.addMore(jsonDish.getQuantity());
							} else {
								HashMap<String, OrderAddOn> orderAddOnDishMap = new HashMap<String, OrderAddOn>();
								OrderDish orderDish = new OrderDish();
								List<JsonAddOn> jsonAddOns = jsonDish.getAddOns();
								if (jsonAddOns != null) {
									float addOnPrice = 0.0f;
									for (JsonAddOn jsonAddOn : jsonAddOns) {
										OrderAddOn orderAddOn = new OrderAddOn();
										SectionDishes addon = vendorPlatterService
												.getSectionDish(jsonAddOn.getItemId());
										if (addon != null) {
											orderAddOn.setAddOnId(jsonAddOn.getItemId());
											orderAddOn.setDishId(jsonDish.getId());
											orderAddOn.setName(addon.getName());
											orderAddOn.setPrice(addon.getPrice());
											orderAddOn.setQuantity(jsonAddOn.getQuantity());
											orderAddOn.setSmallImageUrl(addon.getImageUrl());
											orderAddOn.setDishType(addon.getDishType().getName());
											orderAddOnDishMap.put(jsonAddOn.getItemId() + "", orderAddOn);
											addOnPrice = addon.getPrice();
											orderAddOn.setPrice(addOnPrice);
											orderAddOn.setDishSize("");
											addOnBill += addOnPrice * jsonAddOn.getQuantity();
										}
									}
								}

								orderDish.setDishId(jsonDish.getId());
								orderDish.setQuantity(jsonDish.getQuantity());
								orderDish.setName(jsonDish.getName());

								dishPrice = jsonDish.getPrice();// dish.getPriceByHappyHour(restaurant.getTimeZone());
								orderDish.setPrice(jsonDish.getPrice());

								ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
								String json = ow.writeValueAsString(jsonDish.getSection());
								orderDish.setSectionsJson(json);
								orderDish.setDishSize("");
								// orderDish.setDishType(StringUtility.isNullOrEmpty(jsonDish.getCuisineType())
								// ? platter.getCuisineType().getName()
								// : jsonDish.getCuisineType());
								orderDish.setInstructions(jsonDish.getInstructions());
								orderDish.setOrderAddOn(new ArrayList<OrderAddOn>(orderAddOnDishMap.values()));

								orderDishMap.put(orderDish.getDishId() + "" + instStr + "" + addOnId, orderDish);
							}
							bill += (dishPrice * jsonDish.getQuantity()) + addOnBill;
						}
					}
				}
				
				targetOrder.setBill(bill);
				if (orderDishMap.size() > 0) {
					targetOrder.setOrderDishes(new ArrayList<OrderDish>(orderDishMap.values()));
				}

				if (invoice != null) {
					// logger.debug("17>If invoice status is !=null then");
					List<Order> orders = invoice.getOrders();
					orders.add(targetOrder);
					Float checkBill = invoice.getBill() + targetOrder.getBill();

					invoice.setBill(checkBill);
					invoice.setRewards(caloriesCount);

					customer.setRewardPoints(customer.getRewardPoints() + caloriesCount);

					if (order.getCheckType() != null) {
						invoice.setInvoiceType(order.getCheckType());
					}

					// validating Delivery Area
					logger.info("Delivery Time :" + orderBody.order.deliveryDateTime);

					invoice.setDeliveryInst(deliveryInst);

					String format = "yyyy-MM-dd HH:mm";
					SimpleDateFormat formatterD = new SimpleDateFormat(format);
					TimeZone tzD = TimeZone.getTimeZone(restaurant.getTimeZone());
					formatterD.setTimeZone(tzD);
					Date deliveryTimeD = formatterD.parse(orderBody.order.deliveryDateTime);

					invoice.setDeliveryDateTime(deliveryTimeD);

					if ("Paid".equalsIgnoreCase(paidStatus)) {
						invoice.setStatus(com.emanage.enums.check.Status.Paid);
					}

					customerService.addCustomer(customer);
					bill = invoice.getBill();
					logger.info("20>check database updated by customer :" + customer.getPhone());
					invoiceService.addInvoice(invoice);

					if (bFirstOrder) {
						Order orderToUpdate = invoice.getOrders().get(0);
						orderToUpdate.setCheckId(invoice.getInvoiceNo());
						orderService.addOrder(orderToUpdate);
						targetOrder = orderToUpdate;
					}
				}
			}

			if (targetOrder == null || targetOrder.getOrderId() == null) {
				orderResp.setStatus("Failed");
				orderResp.setError("No order was created");
				logger.info("23>No order was created");
			} else {
				logger.info("23> else part where order has been created ");
				orderResp.setOrderId(targetOrder.getOrderId());
				invoice.setOrderId(targetOrder.getOrderId());
				orderResp.setCountryId(countryId);
				orderResp.setInvoiceNo(invoice.getInvoiceNo());

				Float waiveOff = null;

				/* Coupon Management */
				List<Coupon> couponApplied = new ArrayList<Coupon>();
				logger.info(order.getCouponCode() + " : coupon code list  : ");
				if (order.getCouponCode() != null && order.getCouponCode().size() > 0) {
					InvoiceResponse checkRespons = new InvoiceResponse(invoice, taxTypeService, 0.0f, restaurant);
					// Logic for Coupon Management
					invoice.setRoundOffTotal(checkRespons.getRoundedOffTotal());

					if (editedOrder) {
						if (invoice.getCoupon_Applied() != null && invoice.getCoupon_Applied().size() > 0) {
							List<Coupon> couponList = new ArrayList<Coupon>();
							for (String coup : order.getCouponCode()) {
								couponList.add(couponService.getEnabledCouponByCode(coup, invoice.getCountryId()));
							}
							invoice.setCoupon_Applied(couponList);
						} else if (invoice.getCoupon_Applied() == null || invoice.getCoupon_Applied().size() == 0) {
							
							couponApplied = validateCoupon(invoice, order, paymentStatus);
							if (couponApplied == null || couponApplied.size() == 0) {
								orderResp.setError("Either coupon has been expired or criteria doesn't match");
								orderResp.setStatus("Failed");
								orderResp.setInvoiceNo(invoice.getInvoiceNo());
								return orderResp;
							} else {
								invoice.setCoupon_Applied(couponApplied);
							}
						}
					} else {
						couponApplied = validateCoupon(invoice, order, paymentStatus);
						if (couponApplied == null || couponApplied.size() == 0) {
							orderResp.setError("Either coupon has been expired or criteria doesn't match");
							orderResp.setStatus("Failed");
							orderResp.setInvoiceNo(invoice.getInvoiceNo());
							return orderResp;
						} else {
							invoice.setCoupon_Applied(couponApplied);
						}
					}
				} else if (editedOrder && order.getCouponCode() == null && invoice.getCoupon_Applied() != null) {
					invoice.setCoupon_Applied(couponApplied);
				}
				InvoiceResponse checkRespons = new InvoiceResponse(invoice, taxTypeService, waiveOff, restaurant);
				invoice.setTokenAmount(checkRespons.getTokenAmount());
				SortedMap<String, Float> val = checkRespons.getTaxDetails();
				Set<String> keys = val.keySet();
				JSONObject obj = new JSONObject();
				for (String iterate : keys) {
					if (iterate != null) {
						TaxType taxType = taxTypeService.getTaxTypeByName(iterate, invoice.getCountryId());
						if (taxType != null) {
							try {
								obj.put(taxType.getTaxTypeId().toString(), val.get(iterate));
							} catch (JSONException e) {
								e.printStackTrace();
								asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
								logger.info("Exception mail sent");
							}
						}
					}
				}

				if (restaurant != null) {
					if (restaurant.isRoundOffAmount()) {
						invoice.setRoundOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
						checkRespons.setRoundedOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
					} else {
						invoice.setRoundOffTotal(checkRespons.getRoundedOffTotal());
					}
				}

				invoice.setTaxJsonObject(obj.toString());
				logger.info("setting json tax object" + checkRespons.getRoundedOffTotal());
				logger.info("setting Invoice rounndOffTotal." + checkRespons.getRoundedOffTotal());
				if (restaurant != null) {
					if (restaurant.isRoundOffAmount()) {
						invoice.setRoundOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
						checkRespons.setRoundedOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
					} else {
						invoice.setRoundOffTotal(checkRespons.getRoundedOffTotal());
					}
				}
				if (restaurant.getServiceTaxText() != null && (!restaurant.getServiceTaxText().equalsIgnoreCase(""))) {
					// check.setAdditionalChargesName1(restaurant.getServiceTaxText());
					// check.setAdditionalChargesValue1(restaurant.getServiceTaxValue());
				}
				/**
				 * @comment= Here we are validating customer's CREDIT Account for new order.
				 */
				if (!editedOrder) {
					if ("CUSTOMER CREDIT".equalsIgnoreCase(targetOrder.getPaymentStatus())) {
						Restaurant org = restService.getRestaurant(restaurant.getParentRestaurantId());
						if (org.isEnableCustCredit()) {
							orderResp = validateCustomerCredit(targetOrder, invoice, customer);
							if ("Failure".equalsIgnoreCase(orderResp.getStatus())) {
								invoiceService.removeInvoice(invoice.getInvoiceNo());
								return orderResp;
							}
						}
					} else {
						if (customer.getCredit() != null) {
							if (customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE
									&& (invoice.getOrderSource().equalsIgnoreCase(OrderSource.Website.toString())
											|| invoice.getOrderSource().equalsIgnoreCase(OrderSource.IOS.toString())
											|| invoice.getOrderSource().equalsIgnoreCase(OrderSource.Android.toString())
											|| invoice.getOrderSource().equalsIgnoreCase(OrderSource.POS.toString()))) {
								if (customer.getCredit().getCreditBalance() < 0) {
									if (Math.abs(customer.getCredit().getCreditBalance()) <= invoice
											.getRoundOffTotal()) {
										boolean saveData = customerCreditService.createBillRecoveryTransaction(
												customer.getCustomerId(), "CREDIT", invoice.getInvoiceId(),
												customer.getCredit().getCreditBalance(),
												"Creating pending transaction");
										if (saveData) {
											invoice.setCreditBalance(customer.getCredit().getCreditBalance());
										}
									} else {
										boolean saveData = customerCreditService.createBillRecoveryTransaction(
												customer.getCustomerId(), "CREDIT", invoice.getInvoiceId(),
												(float) invoice.getRoundOffTotal(), "Creating pending transaction");
										if (saveData) {
											invoice.setCreditBalance(-(float) invoice.getRoundOffTotal());
										}
									}
								} else if (customer.getCredit().getCreditBalance() > 0) {
									if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
										boolean saveData = customerCreditService.createBillRecoveryTransaction(
												customer.getCustomerId(), "CREDIT", invoice.getInvoiceId(),
												customer.getCredit().getCreditBalance(),
												"Creating pending transaction");
										if (saveData) {
											invoice.setCreditBalance(customer.getCredit().getCreditBalance());
										}
									}
								}
							}
						}
					}
				}

				String lastPaymentType = (String) request.getAttribute("editOrderPaymentStatus");
				float amountCD = 0.0f;
				String update = null;
				String reason = null;
				ResponseDTO responseDTO = null;
				/*
				 * Condition where current payment type is CUSTOMER CREDIT and calculating
				 * things for edited and non edited orders
				 **/
				if (targetOrder.getPaymentStatus().equalsIgnoreCase("CUSTOMER CREDIT")) {
					amountCD = 0.0f;
					if (customer.getCredit() == null
							&& !editedOrder) {/* if order is not edited and customer don't have CC account */
						responseDTO = openDefaultCreditAccount(restaurant.getParentRestaurantId(), invoice,
								TransactionCategory.DEBIT, null);
						if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
							reason = "ADDED";
							amountCD = (float) invoice.getRoundOffTotal();
						}
					} else if (customer.getCredit() != null
							&& !editedOrder) { /* if order is not edited and customer have CC account */
						amountCD = (float) invoice.getRoundOffTotal();
						responseDTO = creditDebitAmountToAccount(customer, invoice, restaurant.getParentRestaurantId(),
								TransactionCategory.DEBIT, amountCD, true);
						if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
							reason = "ADDED";
							amountCD = (float) invoice.getRoundOffTotal();
						}
					} else if (editedOrder) {/* If order is edited */
						Float lastcheckBill = (Float) request.getAttribute("editCheckBill");
						if (invoice.getLastInvoiceAmount() == 0) {
							invoice.setLastInvoiceAmount(lastcheckBill);
						}
						if (lastcheckBill != null) {
							if (lastPaymentType.equalsIgnoreCase(
									"CUSTOMER CREDIT")) {/* If last and current payment type is CUSTOMER CREDIT */
								if (lastcheckBill > (float) invoice.getRoundOffTotal()) {
									amountCD = lastcheckBill - (float) invoice.getRoundOffTotal();
									responseDTO = creditDebitAmountToAccount(customer, invoice,
											restaurant.getParentRestaurantId(), TransactionCategory.CREDIT, amountCD,
											true);
									if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
										update = "UPDATE";
										reason = "ADDED";
									}
								} else if (lastcheckBill < (float) invoice.getRoundOffTotal()) {
									orderResp = validateCustomerCredit(targetOrder, invoice, customer);
									if ("Failure".equalsIgnoreCase(orderResp.getStatus())) {
										return orderResp;
									} else {
										amountCD = (float) invoice.getRoundOffTotal() - lastcheckBill;
										responseDTO = creditDebitAmountToAccount(customer, invoice,
												restaurant.getParentRestaurantId(), TransactionCategory.DEBIT, amountCD,
												true);
										if (customer.getCredit().getCreditType()
												.getBillingCycle() == BilligCycle.ONE_OFF) {
											reason = "CHARGED";
											update = "UPDATE";
										}
									}
								}
							} else { /* If last payment type is not CUSTOMR CREDIT */
								if (customer.getCredit() == null) {
									responseDTO = openDefaultCreditAccount(restaurant.getParentRestaurantId(), invoice,
											TransactionCategory.DEBIT, null);
									customer = customerService.getCustomer(customer.getCustomerId());
									if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
										update = "UPDATE";
										reason = "ADDED";
										amountCD = (float) invoice.getRoundOffTotal();
									}
								} else {
									orderResp = validateCustomerCredit(targetOrder, invoice, customer);
									if ("Failure".equalsIgnoreCase(orderResp.getStatus())) {
										// revertEditedOrder(invoice);
										return orderResp;
									} else {
										amountCD = (float) invoice.getRoundOffTotal();
										responseDTO = creditDebitAmountToAccount(customer, invoice,
												restaurant.getParentRestaurantId(), TransactionCategory.DEBIT, amountCD,
												true);
										if (customer.getCredit().getCreditType()
												.getBillingCycle() == BilligCycle.ONE_OFF) {
											update = "UPDATE";
											reason = "ADDED";
										}
									}
								}
							}
						}
					}
				}
				/*
				 * Condition where last payment type was CUSTOMER CREDIT but current payment
				 * type is not CUSTOMER CREDIT
				 */
				else if ("CUSTOMER CREDIT".equalsIgnoreCase(lastPaymentType)
						&& (!"CUSTOMER CREDIT".equalsIgnoreCase(targetOrder.getPaymentStatus()))) {
					Float checkBill = (Float) request.getAttribute("editCheckBill");
					responseDTO = creditDebitAmountToAccount(customer, invoice, restaurant.getParentRestaurantId(),
							TransactionCategory.CREDIT, checkBill, true);
					if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
						update = lastPaymentType;
						reason = "PAYMENT_TYPE_CHANGED";
						amountCD = 0;
					}
				} else if (editedOrder && targetOrder.getPaymentStatus() != null) {/*
																					 * Credit Card calculation for all
																					 * use cases except CUSTOMER CREDIT
																					 */
					// ResponseDTO responseDTO =null;
					Float lastcheckBill = (Float) request.getAttribute("editCheckBill");
					if (invoice.getLastInvoiceAmount() == 0) {
						invoice.setLastInvoiceAmount(lastcheckBill);
					}
					if (lastcheckBill > (float) invoice.getRoundOffTotal()
							&& invoice.getStatus() == com.emanage.enums.check.Status.Paid) {
						amountCD = lastcheckBill - (float) invoice.getRoundOffTotal();
						if (customer.getCredit() == null) {
							responseDTO = openDefaultCreditAccount(restaurant.getParentRestaurantId(), invoice,
									TransactionCategory.CREDIT, amountCD);
							if (responseDTO.result.equalsIgnoreCase("SUCCESS")) {
								customer = customerService.getCustomer(customer.getCustomerId());
								if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
									update = "UPDATE";
									reason = "ADDED";
									// amountCD=(float)check.getRoundOffTotal();
								}

							}
						} else {
							responseDTO = creditDebitAmountToAccount(customer, invoice,
									restaurant.getParentRestaurantId(), TransactionCategory.CREDIT, amountCD, true);
							if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
								update = "UPDATE";
								reason = "ADDED";
							}
						}
					} else if (lastcheckBill < (float) invoice.getRoundOffTotal()
							&& invoice.getStatus() == com.emanage.enums.check.Status.Paid) {
						amountCD = (float) invoice.getRoundOffTotal() - lastcheckBill;
						if (customer.getCredit() == null) {
							responseDTO = openDefaultCreditAccount(restaurant.getParentRestaurantId(), invoice,
									TransactionCategory.DEBIT, amountCD);
							customer = customerService.getCustomer(customer.getCustomerId());
							if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
								update = "UPDATE";
								reason = "CHARGED";
							}
						} else {
							responseDTO = creditDebitAmountToAccount(customer, invoice,
									restaurant.getParentRestaurantId(), TransactionCategory.DEBIT, amountCD, true);
							if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
								update = "UPDATE";
								reason = "CHARGED";
							}
						}
					}
				}
				if (responseDTO != null) {
					if ("ERROR".equalsIgnoreCase(responseDTO.result)) {
						orderResp.setError(responseDTO.message);
						orderResp.setStatus("Failed");
						orderResp.setInvoiceNo(invoice.getInvoiceNo());
						return orderResp;
					}
				}
				// logger.info("status :" + paymentStatus);
				// *** here I have added a piece of code for UPDATE transaction
				if (editedOrder) {
					Float lastorderBill = (Float) request.getAttribute("editBill");
					Float lastInvoiceBill = (Float) request.getAttribute("editCheckBill");
					if (lastInvoiceBill > 0) {
						if (invoice.getLastInvoiceAmount() == 0) {
							invoice.setLastInvoiceAmount(lastInvoiceBill);
						}
					}
					if (targetOrder.getStatus() == Status.OUTDELIVERY || targetOrder.getStatus() == Status.DELIVERED) {
						TransactionDTO tillList = cashRegisterService.fetchTransactionsByCheck(invoice.getCountryId(),
								invoice.getInvoiceNo());
						String tillId = tillList.tillDetails.tillId;
						Float checkBill = (Float) request.getAttribute("editCheckBill");

						if (allowDiscount || checkBill > invoice.getRoundOffTotal()) {
							update = "UPDATE";
							reason = "DISCOUNT";
							amountCD = (float) (Math.abs(checkBill) - Math.abs(invoice.getRoundOffTotal()));
							cashRegisterService.applyDiscount((Integer) request.getSession().getAttribute("userId"),
									(float) (Math.abs(checkBill) - Math.abs(invoice.getRoundOffTotal())),
									invoice.getInvoiceNo(), "giving discount", targetOrder.getPaymentStatus());
							try {
								restService
										.emailCheckFromServerNew(request, invoice, restaurant.getAlertMail(),
												restaurant, "defaultemailbill",
												"Special discount has been applied to this invoice. Status: "
														+ targetOrder.getStatus().toString(),
												"cs", update, reason, amountCD);
							} catch (Exception e) {
								logger.info("email not sent");
								asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
								logger.info("Exception mail sent");
							}
							invoice.setEditOrderRemark("Special discount has been applied to this invoice: status: "
									+ targetOrder.getStatus().toString() + " edited by :"
									+ request.getSession().getAttribute("username"));
						}
						if (lastorderBill != null) {
							String updatePaymentType = (String) request.getAttribute("editOrderPaymentStatus");
							if (lastorderBill != targetOrder.getBill()) {
								logger.info(updatePaymentType + "---" + tillId
										+ "creating transaction for edited order with updated bill :"
										+ targetOrder.getPaymentStatus());
								try {
									List<PaymentType> paymentList = restService
											.listPaymentTypeByOrgId(restaurant.getParentRestaurantId());
									if (!(targetOrder.getPaymentStatus().equalsIgnoreCase(updatePaymentType))) {
										if (invoice.getCreditBalance() > 0) {
											for (PaymentType pt : paymentList) {
												if (pt.getName().equalsIgnoreCase(targetOrder.getPaymentStatus())) {
													if (pt.getType()
															.equalsIgnoreCase(BasePaymentType.CASH.toString())) {
														updateCash(TillTransaction.UPDATE.toString(),
																(float) invoice.getCreditBalance(), invoice, request,
																true, updatePaymentType,
																"Edit order transaction checkId="
																		+ invoice.getInvoiceNo(),
																tillId, targetOrder.getPaymentStatus());
													} else if (pt.getType()
															.equalsIgnoreCase(BasePaymentType.CREDIT.toString())
															&& !("CUSTOMER CREDIT".equalsIgnoreCase(
																	targetOrder.getPaymentStatus()))) {
														updateCash(TillTransaction.UPDATE.toString(),
																(float) invoice.getCreditBalance(), invoice, request,
																true, updatePaymentType,
																"Edit order transaction checkId="
																		+ invoice.getInvoiceNo(),
																tillId, targetOrder.getPaymentStatus());
													} else if ("CUSTOMER CREDIT"
															.equalsIgnoreCase(targetOrder.getPaymentStatus())) {
														updateCash(TillTransaction.CANCEL.toString(),
																(float) invoice.getCreditBalance(), invoice, request,
																true, updatePaymentType,
																" Order with orderId=" + targetOrder.getOrderId(),
																tillId, targetOrder.getPaymentStatus());
														customerCreditService.updateBillRecoveryTransaction("FAILED",
																customer.getCustomerId(), invoice.getCreditBalance(),
																"CREDIT", "Setting Status Failed");
														invoice.setCreditBalance(0);

													}
												}
											}
										}
									}
									invoice.setEditOrderRemark(
											"This order has been edited. Status: " + targetOrder.getStatus().toString()
													+ " edited by :" + request.getSession().getAttribute("username"));
									asyncService.emailCheckFromServerNew(request, invoice, restaurant.getAlertMail(),
											restaurant, "defaultemailbill",
											"This order has been edited. Status: " + targetOrder.getStatus().toString(),
											"cs", update, reason, amountCD);
								} catch (Exception e) {
									logger.info("email not sent");
									asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
									logger.info("Exception mail sent");
								}
							} else if (!(targetOrder.getPaymentStatus().equalsIgnoreCase(updatePaymentType))) {
								updateCash(TillTransaction.UPDATE.toString(), (float) invoice.getRoundOffTotal(),
										invoice, request, false, updatePaymentType,
										"Edit order transaction checkId=" + invoice.getInvoiceNo(), tillId,
										targetOrder.getPaymentStatus());
								List<PaymentType> paymentList = restService
										.listPaymentTypeByOrgId(restaurant.getParentRestaurantId());
								if (invoice.getCreditBalance() > 0) {
									for (PaymentType pt : paymentList) {
										if (pt.getName().equalsIgnoreCase(targetOrder.getPaymentStatus())) {
											if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
												updateCash(TillTransaction.UPDATE.toString(),
														(float) invoice.getCreditBalance(), invoice, request, true,
														updatePaymentType,
														"Edit order transaction checkId=" + invoice.getInvoiceNo(),
														tillId, targetOrder.getPaymentStatus());
											} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())
													&& !("CUSTOMER CREDIT"
															.equalsIgnoreCase(targetOrder.getPaymentStatus()))) {
												updateCash(TillTransaction.UPDATE.toString(),
														(float) invoice.getCreditBalance(), invoice, request, true,
														updatePaymentType,
														"Edit order transaction checkId=" + invoice.getInvoiceNo(),
														tillId, targetOrder.getPaymentStatus());
											} else if ("CUSTOMER CREDIT"
													.equalsIgnoreCase(targetOrder.getPaymentStatus())) {
												updateCash(TillTransaction.CANCEL.toString(),
														(float) invoice.getCreditBalance(), invoice, request, true,
														updatePaymentType,
														" Order with orderId=" + targetOrder.getOrderId(), tillId,
														targetOrder.getPaymentStatus());
												customerCreditService.updateBillRecoveryTransaction("FAILED",
														customer.getCustomerId(), invoice.getCreditBalance(), "CREDIT",
														"Setting Status Failed");
												invoice.setCreditBalance(0);

											}
										}
									}
								}

								try {
									invoice.setEditOrderRemark(
											"This order has been edited. Status: " + targetOrder.getStatus().toString()
													+ " edited by :" + request.getSession().getAttribute("username"));
									asyncService.emailCheckFromServerNew(request, invoice, restaurant.getAlertMail(),
											restaurant, "defaultemailbill",
											"This order has been edited. Status: " + targetOrder.getStatus().toString(),
											"cs", update, reason, amountCD);
								} catch (Exception e) {
									logger.info("email not sent");
									asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
									logger.info("Exception mail sent");
								}
							}
						}
					} else {
						invoice.setEditOrderRemark(
								"This order has been edited. Status: " + targetOrder.getStatus().toString()
										+ " edited by :" + request.getSession().getAttribute("username"));
					}
				}
				request.setAttribute("phone", invoice.getEmail());
				request.setAttribute("orgId", customer.getOrgId().toString());
				request.setAttribute("orderLimit", "1");
//				OrderHistory orderHistory = customerController.getLatestOrders(request, invoice.getEmail(),
//						customer.getCustomerId().toString(), customer.getOrgId().toString(), customer.getRestaurantId().toString(),"1", "false");
//				if (orderHistory != null) {
//					if (orderHistory.totalOrders == 1) {
//						invoice.setFirstOrder(true);
//					}
//				}

				// InvoiceResponse checkResponse = new InvoiceResponse(invoice, taxTypeService,
				// waiveOff, restaurant);

				invoiceService.addInvoice(invoice);

				if (paymentStatus.equalsIgnoreCase("Online")) {
					orderResp.setStatus("Online");
					try {
						invoice = orderService.placeRazorPayOrder(invoice);
					} catch (JSONException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					logger.info("23>setting status Online ref.. Success");
				} else if (paymentStatus.equalsIgnoreCase("MobiKwikWallet")) {
					orderResp.setStatus("MobiKwikWallet");
				} else if (paymentStatus.equals("payTm")) {
					orderResp.setStatus("payTm");
				} else if (paymentStatus.equalsIgnoreCase("OnlinePayPal")) {
					orderResp.setStatus("OnlinePayPal");
				} else if (paymentStatus.equalsIgnoreCase("Subscription")) {
					orderResp.setStatus("COD");

					try {
						if (editedOrder == true) {
							asyncService.emailCheckFromServerNew(request, invoice, customer.getEmail(), restaurant, "",
									(editedOrder == true ? "Hopprz : Edited Order" : null), null, update, reason,
									amountCD);
						}
					} catch (Exception e) {
						logger.info("Email sent fail");
						e.printStackTrace();
						asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
					}

					logger.info("23>SUBSCRIPTION Success");
				} else {
					orderResp.setStatus("COD");
					try {
						asyncService.emailCheckFromServerNew(request, invoice, customer.getEmail(), restaurant,
								"defaultemailbill", null, null, null, null, 0);
						if (editedOrder == true) {
							asyncService.emailCheckFromServerNew(request, invoice, customer.getEmail(), restaurant, "",
									(editedOrder == true ? "Hopprz : Edited Order" : null), null, update, reason,
									amountCD);
						}
					} catch (Exception e) {
						logger.info("Email sent fail");
						e.printStackTrace();
						try {
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						} catch (Exception e1) {
						}

					}
					logger.info("23>COD Success");
				}
			}
		} else {
			orderResp.setStatus("Failed");
			logger.info("23>setting status Failed");
			orderResp.setError(error);
		}
		return orderResp;
	}

	public @ResponseBody OrderResponse addToInvoiceJSON(PlaceOrderDTO orderBody, JsonOrder order,
			HttpServletRequest request, HttpServletResponse reponse) throws Exception {
		Invoice invoice = null;
		String error = "";

		boolean editedOrder = orderBody.order.isEdited;
		String orderSource = "";
		if (orderBody.order.orderSource == null || orderBody.order.orderSource.equals(""))
			orderSource = "Website";
		else {
			orderSource = orderBody.order.orderSource;
		}

		String paymentStatus = orderBody.order.paymentMethod;
		String deliveryInst = orderBody.order.instructions;

		Restaurant restaurant = null;
		Integer countryId = null;
		Customer customer = null;
		String paidStatus = (String) request.getAttribute("paidStatus");

		boolean bFirstOrder = false;

		if (order.getInvoiceNo() > 0) {
			invoice = invoiceService.getInvoice(order.getInvoiceNo());
		}

		customer = customerService.getCustomer(order.getCustId());
		if (invoice != null) {
			countryId = invoice.getCountryId();
		} else {
			if (order.getCustId() > 0 && customer == null) {
				error = "No customer was found";
			} else {
				invoice = new Invoice();
				bFirstOrder = true;
				// invoice.setDeliveryAddress(order.getDeliveryAddress());
			}
		}

		Order targetOrder = null;
		OrderResponse orderResp = new OrderResponse();
		//double caloriesCount = 0;
		//int activeDeliveryAreaID = 0;
		if (StringUtility.isNullOrEmpty(error)) {

			List<JsonDish> jsonDishes = order.getItems();
				targetOrder = new Order();
				//targetOrder.setMicroKitchenId(fixSceenId);
				targetOrder.setRestaurantId(countryId);
				targetOrder.setCheckId(invoice.getInvoiceNo());
				targetOrder.setCreatedTime(new Date());
				if (invoice.getCustomerId() > 0) {
					//targetOrder.setSourceType(SourceType.COUNTER);
					targetOrder.setSourceId(order.getCustId());
					//targetOrder.setDestinationType(DestinationType.COUNTER);
					targetOrder.setDestinationId(order.getCustId());

					if (paymentStatus != null) {
						if ("Online".equalsIgnoreCase(paymentStatus)
								|| "OnlinePayPal".equalsIgnoreCase(paymentStatus)) {
							targetOrder.setPaymentStatus("PG_PENDING");
						} else if ("MobiKwikWallet".equals(paymentStatus)) {
							targetOrder.setPaymentStatus("WALLET_PENDING");
						} else if ("payTm".equals(paymentStatus)) {
							targetOrder.setPaymentStatus("PAYTM_PENDING");
						} else {
							targetOrder.setPaymentStatus(paymentStatus);
						}
					} else {
						targetOrder.setPaymentStatus("COD");
					}
				}
				targetOrder.setStatus(Status.NEW);
				
				// logger.info("15>Setting order status :" + Status.READY);
				Float addOnBill = 0.0f;
				Float bill = 0.0f;
				HashMap<String, OrderDish> orderDishMap = new HashMap<String, OrderDish>();
				//customer = customerService.getCustomer(invoice.getCustomerId());
				if (orderSource == null || "".equals(orderSource)) {
					invoice.setOrderSource("Website");
				} else {
					invoice.setOrderSource(orderSource);
				}

				float dishPrice = 0.0f;
				for (JsonDish jsonDish : jsonDishes) {

					Canvas_Courses course= canvasLMSService.getCourseById(jsonDish.getId());
					//Platter platter = vendorPlatterService.getPlatter(jsonDish.getId());
					if (course != null) {
						
							Vendor vendor  = vendorService.getVendor(course.getVendorId());
							invoice.setVedorMargin(vendor.getVendorMargin());
							invoice.setCourseSource(vendor.getLms_name().name());
					}
						restaurant = restService.getRestaurant(invoice.getCountryId());
						List<com.emanage.domain.OrderSource> os = restService
								.listOrderSourcesByOrgId(restaurant.getParentRestaurantId());
						for (com.emanage.domain.OrderSource orderS : os) {
							if (orderS.getName().equalsIgnoreCase(invoice.getOrderSource())) {
							}
						}
							addOnBill = 0.0f;
							StringBuffer addOnId = new StringBuffer();
							addOnId.append("");
							String instStr = "";
							if (jsonDish.getInstructions() != null) {
								instStr = jsonDish.getInstructions().replaceAll("\\s+", "");
							}
							if (jsonDish.getAddOns() != null) {
								for (JsonAddOn jsonAddon : jsonDish.getAddOns()) {
									addOnId.append(jsonAddon.getDishId());
								}
							}

							if (orderDishMap.get(jsonDish.getId() + "" + instStr + "" + addOnId) != null) {
								orderDishMap.get(jsonDish.getId() + "" + instStr + "" + addOnId)
										.addMore(jsonDish.getQuantity());
							} else {
								HashMap<String, OrderAddOn> orderAddOnDishMap = new HashMap<String, OrderAddOn>();
								OrderDish orderDish = new OrderDish();
								List<JsonAddOn> jsonAddOns = jsonDish.getAddOns();
								if (jsonAddOns != null) {
									float addOnPrice = 0.0f;
									for (JsonAddOn jsonAddOn : jsonAddOns) {
										OrderAddOn orderAddOn = new OrderAddOn();
										SectionDishes addon = vendorPlatterService
												.getSectionDish(jsonAddOn.getItemId());
										if (addon != null) {
											orderAddOn.setAddOnId(jsonAddOn.getItemId());
											orderAddOn.setDishId(jsonDish.getId());
											orderAddOn.setName(addon.getName());
											orderAddOn.setPrice(addon.getPrice());
											orderAddOn.setQuantity(jsonAddOn.getQuantity());
											orderAddOn.setSmallImageUrl(addon.getImageUrl());
											orderAddOn.setDishType(addon.getDishType().getName());
											orderAddOnDishMap.put(jsonAddOn.getItemId() + "", orderAddOn);
											addOnPrice = addon.getPrice();
											orderAddOn.setPrice(addOnPrice);
											orderAddOn.setDishSize("");
											addOnBill += addOnPrice * jsonAddOn.getQuantity();
										}
									}
								}

								orderDish.setDishId(jsonDish.getId());
								orderDish.setQuantity(jsonDish.getQuantity());
								orderDish.setName(jsonDish.getName());

								dishPrice = jsonDish.getPrice();// dish.getPriceByHappyHour(restaurant.getTimeZone());
								orderDish.setPrice(jsonDish.getPrice());

								ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
								String json = ow.writeValueAsString(jsonDish.getSection());
								orderDish.setSectionsJson(json);
								orderDish.setDishSize("");
								
								orderDish.setInstructions(jsonDish.getInstructions());
								orderDish.setOrderAddOn(new ArrayList<OrderAddOn>(orderAddOnDishMap.values()));

								orderDishMap.put(orderDish.getDishId() + "" + instStr + "" + addOnId, orderDish);
							}
							bill += (dishPrice * jsonDish.getQuantity()) + addOnBill;
						//}
					}
				//}
				
				targetOrder.setBill(bill);
				if (orderDishMap.size() > 0) {
					targetOrder.setOrderDishes(new ArrayList<OrderDish>(orderDishMap.values()));
				}

				if (invoice != null) {
					List<Order> orders = invoice.getOrders();
					orders.add(targetOrder);
					Float checkBill = invoice.getBill() + targetOrder.getBill();

					invoice.setBill(checkBill);

				invoice.setDeliveryInst(deliveryInst);

					String format = "yyyy-MM-dd HH:mm";
					SimpleDateFormat formatterD = new SimpleDateFormat(format);
					TimeZone tzD = TimeZone.getTimeZone(restaurant.getTimeZone());
					formatterD.setTimeZone(tzD);
					if ("Paid".equalsIgnoreCase(paidStatus)) {
						invoice.setStatus(com.emanage.enums.check.Status.Paid);
					}

					customerService.addCustomer(customer);
					bill = invoice.getBill();
					logger.info("20>check database updated by customer :" + customer.getPhone());
					invoiceService.addInvoice(invoice);

					if (bFirstOrder) {
						Order orderToUpdate = invoice.getOrders().get(0);
						orderToUpdate.setCheckId(invoice.getInvoiceNo());
						orderService.addOrder(orderToUpdate);
						targetOrder = orderToUpdate;
					}
				}
			

			if (targetOrder == null || targetOrder.getOrderId() == null) {
				orderResp.setStatus("Failed");
				orderResp.setError("No order was created");
				logger.info("23>No order was created");
			} else {
				logger.info("23> else part where order has been created ");
				orderResp.setOrderId(targetOrder.getOrderId());
				invoice.setOrderId(targetOrder.getOrderId());
				orderResp.setCountryId(countryId);
				orderResp.setInvoiceNo(invoice.getInvoiceNo());

				Float waiveOff = null;

				/* Coupon Management */
				List<Coupon> couponApplied = new ArrayList<Coupon>();
				logger.info(order.getCouponCode() + " : coupon code list  : ");
				if (order.getCouponCode() != null && order.getCouponCode().size() > 0) {
					InvoiceResponse checkRespons = new InvoiceResponse(invoice, taxTypeService, 0.0f, restaurant);
					// Logic for Coupon Management
					invoice.setRoundOffTotal(checkRespons.getRoundedOffTotal());

					if (editedOrder) {
						if (invoice.getCoupon_Applied() != null && invoice.getCoupon_Applied().size() > 0) {
							List<Coupon> couponList = new ArrayList<Coupon>();
							for (String coup : order.getCouponCode()) {
								couponList.add(couponService.getEnabledCouponByCode(coup, invoice.getCountryId()));
							}
							invoice.setCoupon_Applied(couponList);
						} else if (invoice.getCoupon_Applied() == null || invoice.getCoupon_Applied().size() == 0) {
							couponApplied = validateCoupon(invoice, order, paymentStatus);
							if (couponApplied == null || couponApplied.size() == 0) {
								orderResp.setError("Either coupon has been expired or criteria doesn't match");
								orderResp.setStatus("Failed");
								orderResp.setInvoiceNo(invoice.getInvoiceNo());
								return orderResp;
							} else {
								invoice.setCoupon_Applied(couponApplied);
							}
						}
					} else {
						couponApplied = validateCoupon(invoice, order, paymentStatus);
						if (couponApplied == null || couponApplied.size() == 0) {
							orderResp.setError("Either coupon has been expired or criteria doesn't match");
							orderResp.setStatus("Failed");
							orderResp.setInvoiceNo(invoice.getInvoiceNo());
							return orderResp;
						} else {
							invoice.setCoupon_Applied(couponApplied);
						}
					}
				} else if (editedOrder && order.getCouponCode() == null && invoice.getCoupon_Applied() != null) {
					invoice.setCoupon_Applied(couponApplied);
				}
				
				InvoiceResponse checkRespons = new InvoiceResponse(invoice, taxTypeService, waiveOff, restaurant);
				//invoice.setTokenAmount(checkRespons.getTokenAmount());
				Map<Integer,TaxType>  val = checkRespons.getTaxTypeArray();
				Map<String, Float>  Taxsum = checkRespons.getTaxDetails();
				Set<Integer> keys = val.keySet();
				JSONObject obj = new JSONObject();
				for (Integer iterate : keys) {
					if (iterate != null) {
						TaxType taxType = val.get(iterate);
						if (taxType != null) {
							try {
								obj.put(taxType.getTaxTypeId().toString(), Taxsum.get(taxType.getName()));
							} catch (JSONException e) {
								e.printStackTrace();
								asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
								logger.info("Exception mail sent");
							}
						}
					}
				}

				if (restaurant != null) {
					if (restaurant.isRoundOffAmount()) {
						invoice.setRoundOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
						checkRespons.setRoundedOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
					} else {
						invoice.setRoundOffTotal(checkRespons.getRoundedOffTotal());
					}
				}

				invoice.setTaxJsonObject(obj.toString());
				logger.info("setting json tax object" + checkRespons.getRoundedOffTotal());
				logger.info("setting Invoice rounndOffTotal." + checkRespons.getRoundedOffTotal());
				if (restaurant != null) {
					if (restaurant.isRoundOffAmount()) {
						invoice.setRoundOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
						checkRespons.setRoundedOffTotal(Math.round(checkRespons.getRoundedOffTotal()));
					} else {
						invoice.setRoundOffTotal(checkRespons.getRoundedOffTotal());
					}
				}

				request.setAttribute("phone", invoice.getEmail());
				request.setAttribute("orgId", customer.getOrgId().toString());
				request.setAttribute("orderLimit", "1");
//				OrderHistory orderHistory = customerController.getLatestOrders(request, invoice.getEmail(),
//						customer.getCustomerId().toString(), customer.getOrgId().toString(), customer.getRestaurantId().toString(),"1", "false");
//				if (orderHistory != null) {
//					if (orderHistory.totalOrders == 1) {
//						invoice.setFirstOrder(true);
//					}
//				}


				invoiceService.addInvoice(invoice);

				if (paymentStatus.equalsIgnoreCase("Online")) {
					orderResp.setStatus("Online");
					logger.info("23>setting status Online ref.. Success");
				} else if (paymentStatus.equalsIgnoreCase("MobiKwikWallet")) {
					orderResp.setStatus("MobiKwikWallet");
				} else if (paymentStatus.equals("payTm")) {
					orderResp.setStatus("payTm");
				} else if (paymentStatus.equalsIgnoreCase("OnlinePayPal")) {
					orderResp.setStatus("OnlinePayPal");
				} else if (paymentStatus.equalsIgnoreCase("Subscription")) {
					orderResp.setStatus("COD");

				} else {
					orderResp.setStatus("COD");
					try {
						asyncService.emailCheckFromServerNew(request, invoice, customer.getEmail(), restaurant,
								"defaultemailbill", null, null, null, null, 0);
					} catch (Exception e) {
						logger.info("Email sent fail");
						e.printStackTrace();
						try {
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						} catch (Exception e1) {
						}

					}
					logger.info("23>COD Success");
				}
			}
		} else {
			orderResp.setStatus("Failed");
			logger.info("23>setting status Failed");
			orderResp.setError(error);
		}
		return orderResp;
	}

	@RequestMapping(value = "/setOrderPaymentType")
	@ApiOperation(value = "This API requires user login session, because we're getting some data from session")
	public @ResponseBody Map<String, Object> setOrderPaymentType(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String orderId, @RequestParam(required = false) String tillId,
			@RequestParam(required = false) String remarks, @RequestParam String paymentType)
			throws UnsupportedEncodingException, MessagingException {
		Map<String, Object> map = new TreeMap<String, Object>();
		REQUEST = request;
		Integer orderID = Integer.parseInt(orderId);
		// String statusStr = request.getParameter("status");
		// String tillId = request.getParameter("tillId");
		// zString remarks = request.getParameter("remarks");
		String role = (String) request.getSession().getAttribute("role");
		String userName = (String) request.getSession().getAttribute("username");
		Order order = orderService.getOrder(orderID);
		Invoice check = invoiceService.getInvoice(order.getCheckId());
		Restaurant rest = restService.getRestaurant(check.getCountryId());
		List<PaymentType> listPy = restService.listPaymentTypeByOrgId(rest.getParentRestaurantId());
		// String paymentType = request.getParameter("paymentType");
		boolean sendEmail = true;
		// boolean isPartial=false;
		if (request.getAttribute("sendEmail") != null) {
			sendEmail = (boolean) request.getAttribute("sendEmail");
		}
		if (request.getAttribute("partialPaymentType") != null) {
			paymentType = (String) request.getAttribute("partialPaymentType");
			tillId = (String) request.getAttribute("tillId");
			// isPartial=true;
		}
		boolean isValid = false;
		PaymentType lastOrderPyObj = null;
		// String lastOrderPaymentType=null;
		if (paymentType != null && !(paymentType.equalsIgnoreCase("undefined"))) {
			for (PaymentType py : listPy) {
				if (paymentType.equalsIgnoreCase(py.getName())) {
					isValid = true;
				}
			}
		} else {
			map.put("status", "error");
			map.put("message", "Invalid Payment Type");
			return map;
		}

		if (order.getPaymentStatus().equalsIgnoreCase(paymentType)) {
			map.put("status", "error");
			map.put("message", "It's already marked as : " + paymentType);
			return map;
		}

		logger.info(order.getPaymentStatus() + " and updated payment type:" + paymentType + ": order id :" + orderId
				+ " System User Role : " + role + " username :" + userName);
		if (isValid) {
			ResponseDTO responseDTO = null;
			if (!StringUtility.isNullOrEmpty(paymentType)) {
				Order or = orderService.getOrder(orderID);
				or.setPaymentStatus(paymentType);
				// lastOrderPaymentType = order.getPaymentStatus();
				if (!(order.getPaymentStatus().toString().equalsIgnoreCase("CUSTOMER CREDIT"))
						&& paymentType.equalsIgnoreCase("CUSTOMER CREDIT")) {
					Customer customer = customerService.getCustomer(check.getCustomerId());
					if (customer.getCredit() == null) {
						if (check.getCreditBalance() < 0) {
							responseDTO = openDefaultCreditAccount(rest.getParentRestaurantId(), check,
									TransactionCategory.DEBIT,
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()));
						} else {
							responseDTO = openDefaultCreditAccount(rest.getParentRestaurantId(), check,
									TransactionCategory.DEBIT, null);
						}
					} else {
						if (check.getCreditBalance() < 0) {
							responseDTO = creditDebitAmountToAccount(customer, check, rest.getParentRestaurantId(),
									TransactionCategory.DEBIT,
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), false);
						} else {
							responseDTO = creditDebitAmountToAccount(customer, check, rest.getParentRestaurantId(),
									TransactionCategory.DEBIT, (float) (check.getRoundOffTotal()), false);
						}
					}
				} else if (order.getPaymentStatus().toString().equalsIgnoreCase("CUSTOMER CREDIT")
						&& (!paymentType.equalsIgnoreCase("CUSTOMER CREDIT"))) {
					Customer customer = customerService.getCustomer(check.getCustomerId());
					responseDTO = creditDebitAmountToAccount(customer, check, rest.getParentRestaurantId(),
							TransactionCategory.CREDIT, (float) check.getRoundOffTotal(), true);
				}
				if (responseDTO != null) {
					if ("ERROR".equalsIgnoreCase(responseDTO.result)) {
						map.put("status", "error");
						map.put("message", responseDTO.message);
						return map;
					}
				}
				orderService.addOrder(or);
				if (!paymentType.equalsIgnoreCase(order.getPaymentStatus())) {
					Customer cust = customerService.getCustomer(check.getCustomerId());
					String lastPaymentStatus = order.getPaymentStatus();
					order.setPaymentStatus(paymentType);
					try {

						if (check.getCreditBalance() < 0) {
							updateCash(TillTransaction.UPDATE.toString(),
									(float) check.getRoundOffTotal() + check.getCreditBalance(), check, request, false,
									lastPaymentStatus, "Edit order transaction checkId=" + check.getInvoiceNo(), tillId,
									paymentType);
						} else {
							updateCash(TillTransaction.UPDATE.toString(), (float) check.getRoundOffTotal(), check,
									request, false, lastPaymentStatus,
									"Edit order transaction checkId=" + check.getInvoiceNo(), tillId, paymentType);
						}

						List<PaymentType> paymentList = restService
								.listPaymentTypeByOrgId(rest.getParentRestaurantId());
						for (PaymentType pt : paymentList) {
							if (pt.getName().equalsIgnoreCase(lastPaymentStatus)) {
								lastOrderPyObj = pt;
							}
						}
						if (check.getCreditBalance() != 0) {
							for (PaymentType pt : paymentList) {
								if (pt.getName().equalsIgnoreCase(paymentType)) {
									if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
										updateCash(TillTransaction.UPDATE.toString(), (float) check.getCreditBalance(),
												check, request, true, lastPaymentStatus,
												"Edit order transaction checkId=" + check.getInvoiceNo(), tillId,
												paymentType);
									} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())
											&& !("CUSTOMER CREDIT".equalsIgnoreCase(paymentType))) {
										updateCash(TillTransaction.UPDATE.toString(), (float) check.getCreditBalance(),
												check, request, true, lastPaymentStatus,
												"Edit order transaction checkId=" + check.getInvoiceNo(), tillId,
												paymentType);
									} else if ("CUSTOMER CREDIT".equalsIgnoreCase(paymentType)) {
										if (lastOrderPyObj != null) {
											// if(lastOrderPyObj.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())){
											updateCash(TillTransaction.CANCEL.toString(),
													(float) check.getCreditBalance(), check, request, true,
													lastPaymentStatus, " Order with orderId=" + order.getOrderId(),
													tillId, paymentType);

										}
										if (check.getCreditBalance() < 0) {
											customerCreditService.updateBillRecoveryTransaction("SUCCESS",
													cust.getCustomerId(), check.getCreditBalance(), "CREDIT",
													"Setting Status Failed");
										} else {
											customerCreditService.updateBillRecoveryTransaction("FAILED",
													cust.getCustomerId(), check.getCreditBalance(), "CREDIT",
													"Setting Status Failed");
										}
										check.setCreditBalance(0);
									}
								}
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						map.put("status", "error");
						map.put("message", "Exception arrived while creating transaction. Hence operation failed!.");
						// asyncService.emailException(ExceptionUtils.getStackTrace(e),request);
						logger.info("Exception mail sent");
						return map;
					}
					if (remarks == null) {
						remarks = order.getStatus().toString() + " Payment type of this Invoice has been changed to "
								+ paymentType + ": edited by :" + request.getSession().getAttribute("username");
					}
					check.setEditOrderRemark(remarks);
					invoiceService.addInvoice(check);
					orderService.addOrder(order);
					try {
						if (sendEmail) {
							asyncService.emailCheckFromServerNew(request, check, rest.getAlertMail(), rest,
									"defaultemailbill", "Payment type of this Invoice has been changed to "
											+ paymentType + " from " + lastPaymentStatus + " Remarks: " + remarks,
									"cs", null, null, 0);
							asyncService.emailCheckFromServerNew(request, check, cust.getEmail(), rest,
									"defaultemailbill", "Salad Days : Edited Payment Mode", null, lastPaymentStatus,
									"PAYMENT_TYPE_CHANGED", 0);
						}
					} catch (Exception e) {
						e.printStackTrace();
						// asyncService.emailException(ExceptionUtils.getStackTrace(e),request);
						logger.info("Exception mail sent");
					}
					map.put("status", "success");
				} else {
					map.put("status", "error");
					map.put("message", "Payment type  already exist");
				}
			}
		} else {
			map.put("status", "error");
			map.put("message", "Payment Type does not exist");
			return map;
		}
		return map;

	}

	public ResponseDTO validateRefundForCustomerCredit(HttpServletRequest request, Status status, Order order,
			Customer customer, Invoice check, Restaurant rest, String refund) {
		ResponseDTO responseDTO = null;
		REQUEST = request;
		List<com.emanage.domain.OrderSource> orderSource = new ArrayList<>();
		orderSource = restService.listOrderSourcesByOrgId(rest.getParentRestaurantId());
		for (com.emanage.domain.OrderSource os : orderSource) {
			if (os.getName().equalsIgnoreCase(check.getOrderSource()) && os.getStatus().equalsIgnoreCase("Active")) {
				logger.info("We're avoiding opening credit account for orderSource : " + os.getName()
						+ " and  check Id/invoiceId : " + check.getInvoiceNo() + "/" + check.getInvoiceId());
				logger.info("Customer :" + customer.getPhone() + " & " + customer.getCustomerId());
				return responseDTO;
			}
		}

		if ("CUSTOMER CREDIT".equalsIgnoreCase(order.getPaymentStatus()) && status == Status.CANCELLED) {
			float amount = (float) check.getRoundOffTotal();
			if (customer.getCredit() != null
					&& customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE) {
				responseDTO = creditDebitAmountToAccount(customer, check, rest.getParentRestaurantId(),
						TransactionCategory.CREDIT, amount, false);
			}
			if ("ERROR".equalsIgnoreCase(responseDTO.result)) {
				return responseDTO;
			}
			try {
				CreditTransactions ct = customerCreditService.getLastPendingTransaction(customer.getCustomerId());
				if (ct != null) {
					if (ct.getStatus() == CreditTransactionStatus.PENDING) {
						Invoice ck = invoiceService.getInvoiceByInvoiceId(ct.getInvoiceId()).get(0);
						try {
							customerCreditService.updateBillRecoveryTransaction("FAILED", customer.getCustomerId(),
									ck.getCreditBalance(), "CREDIT",
									"Setting status failed of existing pending transaction");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
							logger.info("Exception mail sent");
						}
						ck.setCreditBalance(0);
						ck.setStatus(com.emanage.enums.check.Status.Cancel);
						invoiceService.addInvoice(ck);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
				} catch (UnsupportedEncodingException | MessagingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.info("Exception mail sent");
			}
		} else if ("PG".equalsIgnoreCase(order.getPaymentStatus()) && status == Status.CANCELLED) {
			if ("CREDIT".equalsIgnoreCase(refund)) {
				if (customer.getCredit() == null) {
					responseDTO = openDefaultCreditAccount(rest.getParentRestaurantId(), check,
							TransactionCategory.CREDIT, null);
				} else if (customer.getCredit() != null
						&& customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE) {
					responseDTO = creditDebitAmountToAccount(customer, check, rest.getParentRestaurantId(),
							TransactionCategory.CREDIT, (float) check.getRoundOffTotal(), true);
				}
				if ("ERROR".equalsIgnoreCase(responseDTO.result)) {
					return responseDTO;
				}
			} else if ("CASH".equalsIgnoreCase(refund)) {

			}
			CreditTransactions ct = customerCreditService.getLastPendingTransaction(customer.getCustomerId());
			if (ct != null) {
				if (ct.getStatus() == CreditTransactionStatus.PENDING) {
					Invoice ck = invoiceService.getInvoiceByInvoiceId(ct.getInvoiceId()).get(0);

					try {
						customerCreditService.updateBillRecoveryTransaction("SUCCESS", customer.getCustomerId(),
								ck.getCreditBalance(), "CREDIT",
								"Setting status success of existing pending transaction");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						try {
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						} catch (UnsupportedEncodingException | MessagingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						logger.info("Exception mail sent");
					}
					ck.setCreditBalance(0);
					ck.setStatus(com.emanage.enums.check.Status.Cancel);
					invoiceService.addInvoice(ck);
				}
			}
		} else if (status == Status.CANCELLED && check.getStatus() == com.emanage.enums.check.Status.Paid) {

			if ("CREDIT".equalsIgnoreCase(refund)) {
				if (customer.getCredit() == null) {
					responseDTO = openDefaultCreditAccount(rest.getParentRestaurantId(), check,
							TransactionCategory.CREDIT, null);
				} else if (customer.getCredit() != null
						&& customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE) {
					responseDTO = creditDebitAmountToAccount(customer, check, rest.getParentRestaurantId(),
							TransactionCategory.CREDIT, (float) check.getRoundOffTotal(), true);
				}
				if ("ERROR".equalsIgnoreCase(responseDTO.result)) {
					return responseDTO;
				}
			} else if ("CASH".equalsIgnoreCase(refund)) {

			}
			CreditTransactions ct = customerCreditService.getLastPendingTransaction(customer.getCustomerId());
			if (ct != null) {
				if (ct.getStatus() == CreditTransactionStatus.PENDING) {
					Invoice ck = invoiceService.getInvoiceByInvoiceId(ct.getInvoiceId()).get(0);
					try {
						customerCreditService.updateBillRecoveryTransaction("SUCCESS", customer.getCustomerId(),
								ck.getCreditBalance(), "CREDIT",
								"Setting status success of existing pending transaction");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						try {
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						} catch (UnsupportedEncodingException | MessagingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						logger.info("Exception mail sent");
					}
					ck.setCreditBalance(0);
					ck.setStatus(com.emanage.enums.check.Status.Cancel);
					invoiceService.addInvoice(ck);
				}
			}
		}
		return responseDTO;
	}

	public void creditCancellationAndEmail(HttpServletRequest request, Invoice check, Status status, Status oldStatus,
			Restaurant rest, String refund, String remarks) {

		if (status == Status.CANCELLED) {
			Customer cust = customerService.getCustomer(check.getCustomerId());
			// if(check.getZomatoOrderId()!=null){
			// /*Trying to reject zomato order. */
			// try{
			// zomatoService.zomatoOrderReject(check,request);
			// }catch(Exception e){
			// logger.info("Can't cancel zomato order");
			// }
			// }
			if (oldStatus == Status.OUTDELIVERY) {
				// String remarks = request.getParameter("remarks");
				if (cust != null)
					try {
						asyncService.emailCheckFromServerNew(request, check, cust.getEmail(), rest,
								"saladdaysCancelEmail",
								"Salad Days : Order No. " + check.getOrderId() + " has been cancelled.", null, refund,
								"CANCELLED", 0);
					} catch (Exception e) {
						logger.info("Email exception for checkID/InvoiceId :" + check.getInvoiceId() + "/"
								+ check.getInvoiceId());
						try {
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						} catch (UnsupportedEncodingException | MessagingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						logger.info("Exception mail sent");
					}
				if (remarks == null) {
					remarks = "No remarks";
				}
				check.setEditOrderRemark(remarks);
				try {
					asyncService.emailCheckFromServerNew(request, check, rest.getAlertMail(), rest, "defaultemailbill",
							"This invoice has been Cancelled. Remarks: " + remarks, "cs", refund, "CANCELLED", 0);
				} catch (Exception e) {
					logger.info("Email exception for checkID/InvoiceId :" + check.getInvoiceId() + "/"
							+ check.getInvoiceId());
					try {
						asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
					} catch (UnsupportedEncodingException | MessagingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					logger.info("Exception mail sent");
				}
			} else if (oldStatus == Status.DELIVERED) {
				if (cust != null)
					try {
						asyncService.emailCheckFromServerNew(request, check, cust.getEmail(), rest,
								"saladdaysCancelEmail",
								"Salad Days : Order No. " + check.getOrderId() + " has been cancelled.", null, refund,
								"CANCELLED", 0);
						asyncService.emailCheckFromServerNew(request, check, rest.getAlertMail(), rest,
								"defaultemailbill", "This invoice has been Cancelled after marking it DELIVERED .",
								"cs", refund, "CANCELLED", 0);
					} catch (Exception e) {
						logger.info("Email exception for checkID/InvoiceId :" + check.getInvoiceId() + "/"
								+ check.getInvoiceId());
						try {
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						} catch (UnsupportedEncodingException | MessagingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						logger.info("Exception mail sent");
					}
			} else {
				if (cust != null && (!check.getOrders().get(0).getPaymentStatus().equalsIgnoreCase("PG_PENDING"))
						&& (!check.getOrders().get(0).getPaymentStatus().equalsIgnoreCase("WALLET_PENDING"))
						&& (!check.getOrders().get(0).getPaymentStatus().equalsIgnoreCase("PAYTM_PENDING"))
						&& (!check.getOrders().get(0).getPaymentStatus().equalsIgnoreCase("PENDING"))) {
					try {
						asyncService.emailCheckFromServerNew(request, check, cust.getEmail(), rest,
								"saladdaysCancelEmail",
								"Salad Days : Order No. " + check.getOrderId() + " has been cancelled.", null, refund,
								"CANCELLED", 0);
					} catch (Exception e) {
						logger.info("Email exception for checkID/InvoiceId :" + check.getInvoiceId() + "/"
								+ check.getInvoiceId());
						try {
							asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						} catch (UnsupportedEncodingException | MessagingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						logger.info("Exception mail sent");
					}
				}
			}
			if (check.getCreditBalance() != 0) {
				try {
					customerCreditService.updateBillRecoveryTransaction("FAILED", cust.getCustomerId(),
							check.getCreditBalance(), "CREDIT", "Setting Status Failed");
				} catch (Exception e) {
					try {
						asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
					} catch (UnsupportedEncodingException | MessagingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					logger.info("Exception mail sent");
				}
			}
		}
	}

	public ResponseDTO updateTillTransactionForPartialPayment(Invoice check, Order order, String paymentType,
			String updateToPaymentType, HttpServletRequest request, String tillId, Restaurant rest,
			float orderAmountRecieved) throws Exception {
		ResponseDTO responseDTO = null;

		List<PaymentType> paymentList = restService.listPaymentTypeByOrgId(rest.getParentRestaurantId());
		for (PaymentType pt : paymentList) {
			if (pt.getName().equalsIgnoreCase(paymentType)) {
				if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
					responseDTO = updateCash(TillTransaction.NEW.toString(), (float) orderAmountRecieved, check,
							request, true, paymentType,
							"New* Transaction CREDIT_BILL_IN_CASH for OrderId=" + order.getOrderId(), tillId,
							updateToPaymentType);
					if ("SUCCESS".equalsIgnoreCase(responseDTO.result)) {
						responseDTO = updateCash(TillTransaction.SUCCESS.toString(), (float) orderAmountRecieved, check,
								request, true, paymentType,
								"Success* Transaction CREDIT_BILL_IN_CASH for OrderId=" + order.getOrderId(), tillId,
								updateToPaymentType);
					}
				} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())
						&& !("CUSTOMER CREDIT".equalsIgnoreCase(paymentType))) {
					responseDTO = updateCash(TillTransaction.NEW.toString(), (float) orderAmountRecieved, check,
							request, true, paymentType,
							"New* Transaction CREDIT_BILL_IN_CREDIT for OrderId=" + order.getOrderId(), tillId,
							updateToPaymentType);
					if ("SUCCESS".equalsIgnoreCase(responseDTO.result)) {
						responseDTO = updateCash(TillTransaction.SUCCESS.toString(), (float) orderAmountRecieved, check,
								request, true, paymentType,
								"Success* Transaction CREDIT_BILL_IN_CREDIT for OrderId=" + order.getOrderId(), tillId,
								updateToPaymentType);
					}
				}
			}
		}

		return responseDTO;
	}

	ResponseDTO vaidateRefundForPartialPayment(HttpServletRequest request, Status status, Order order, Customer cust,
			Invoice check, Restaurant rest, String refund, float orderAmountRecieved) {
		REQUEST = request;
		ResponseDTO responseDTO = null;
		Customer customer = customerService.getCustomer(cust.getCustomerId());
		if (customer != null) {
			if (customer.getCredit() == null) {
				responseDTO = openDefaultCreditAccount(rest.getParentRestaurantId(), check, TransactionCategory.CREDIT,
						(float) orderAmountRecieved);
			} else if (customer.getCredit() != null
					&& customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE) {
				responseDTO = creditDebitAmountToAccount(customer, check, rest.getParentRestaurantId(),
						TransactionCategory.CREDIT, (float) orderAmountRecieved, true);
			}
			if ("ERROR".equalsIgnoreCase(responseDTO.result)) {
				return responseDTO;
			}

		}
		return responseDTO;
	}

	@RequestMapping(value = "/setOrderStatus", method = RequestMethod.GET)
	@ApiOperation(value = "This API requires user login session, because we're getting some data from session")
	public @ResponseBody Map<String, Object> setOrderStatus(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String orderId, @RequestParam(required = false) String status,
			@RequestParam(required = false) String tillId,
			@ApiParam(value = "This parameter required when"
					+ " you cancle the order and want to refund the amount, refund value will be ( CREDIT or CASH ). "
					+ "CASH if you want to return cash or CREDIT in case you want to add that amount to his CUSTOMER CREDIT."
					+ "right now we are only doing CREDIT internally.") @RequestParam(required = false) String refund,
			@ApiParam(value = "If you want to mark PENDING order paid, send 'Paid' value as paramater.") @RequestParam(required = false) String paidStatus,
			@ApiParam(value = "Money collected on cash collection/ money sent with delivery boy") @RequestParam(required = false) String money,
			@ApiParam(value = "Payment type of order") @RequestParam(required = false) String paymentType,
			@ApiParam(value = "Optionl, in case you want to send reason, why order has been edited") @RequestParam(required = false) String remarks,
			@ApiParam(value = "DeliveryBoy name who took the order for delivery") @RequestParam(required = false) String deliveryBoy)
			throws UnsupportedEncodingException, MessagingException {

		Map<String, Object> map = new TreeMap<String, Object>();
		Integer orderID = Integer.parseInt(orderId);

		String role = (String) request.getSession().getAttribute("role");
		String userName = (String) request.getSession().getAttribute("username");
		Float moneyIn = Float.parseFloat(money);
		status = status.toUpperCase();
		Status orderStatus = Status.valueOf(Status.class, status);
		Order order = orderService.getOrder(orderID);
		Status oldStatus = order.getStatus();
		Invoice check = invoiceService.getInvoice(order.getCheckId());
		Restaurant rest = restService.getRestaurant(check.getCountryId());
		logger.info("1>Setting order Status " + status + " for orderId : " + orderID);
		boolean isPartialPayment = false;
		logger.info("Previous order status : " + oldStatus + "   : current status requested " + status + " : "
				+ paymentType + ": order id :" + orderId + " System User Role : " + role + " username :" + userName);
		String updateToPaymentType = "";
		if (order.getPaymentStatus() != null && orderStatus == Status.OUTDELIVERY) {
			String[] pyType = order.getPaymentStatus().split("_");
			if (pyType.length > 0) {
				if (order.getPaymentStatus().equalsIgnoreCase(pyType[0] + "_PENDING")
						|| "WALLET_PENDING".equalsIgnoreCase(order.getPaymentStatus())) {
					map.put("Error", "You can't dispatch order with payment type: " + order.getPaymentStatus()
							+ ". Please resolve its status");
					logger.info("returning order Id" + order.getOrderId());
					return map;
				}
			}
		}
		if (oldStatus == Status.CANCELLED && order.getMoneyOut() == 0) {
			map.put("Error", "Order is already: " + Status.CANCELLED.toString());
			logger.info("returning order Id" + order.getOrderId());
			return map;
		} else if (orderStatus == oldStatus) {
			map.put("Error", "Order is already marked as: " + status.toString() + ". Please refresh you screen");
			logger.info("returning order Id" + order.getOrderId());
			return map;
		} else if (oldStatus == Status.OUTDELIVERY
				&& (orderStatus == Status.READY || orderStatus == Status.PENDING || orderStatus == Status.NEW)) {
			map.put("Error", "Order is already marked as: " + oldStatus.toString() + ". Please refresh you screen");
			logger.info(status.toString() + " returning order Id" + order.getOrderId());
			return map;
		}

		if (oldStatus == Status.CANCELLED && orderStatus != Status.DELIVERED) {
			map.put("Error", "Order is already marked as: " + oldStatus.toString() + ".");
			logger.info(status.toString() + " returning order Id" + order.getOrderId());
			return map;
		}

		if (oldStatus != Status.CANCELLED && orderStatus == Status.CANCELLED) {
			try {
				updatePlatterBooking(check.getDeliveryDateTime(), order.getOrderDishes().get(0).getQuantity(),
						order.getOrderDishes().get(0).getDishId());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		order.setStatus(orderStatus);
		orderService.addOrder(order);

		Customer customer = customerService.getCustomer(check.getCustomerId());

		/**
		 * Partial Payment on construction
		 */
		if (moneyIn > 0) {
			if (orderStatus.equals(Status.DELIVERED)
					&& moneyIn < (order.getMoneyOut() + check.getRoundOffTotal() + check.getCreditBalance())) {
				try {
					if (moneyIn > Math.abs(check.getRoundOffTotal() + check.getCreditBalance() + order.getMoneyOut())) {
						map.put("status", "error");
						map.put("message", "Partial amount should not be greater then order amount.");
						return map;
					}
					// ResponseDTO responseDTO=null;
					// float moneyOut= order.getMoneyOut();
					float orderAmountRecieved = Math.abs(moneyIn - order.getMoneyOut());
					if (orderAmountRecieved > 0) {
						logger.info("entered into partial payment: amount " + orderAmountRecieved);
						request.setAttribute("tillId", tillId);
						request.setAttribute("sendEmail", false);
						request.setAttribute("partialPaymentType", "CUSTOMER CREDIT");
						Map<String, Object> mapResult = setOrderPaymentType(request, response,
								order.getOrderId().toString(), tillId, "Partial payment process", "CUSTOMER CREDIT");
						if ("success".equalsIgnoreCase((String) mapResult.get("status"))) {
							updateTillTransactionForPartialPayment(check, order, paymentType, updateToPaymentType,
									request, tillId, rest, orderAmountRecieved);
							vaidateRefundForPartialPayment(request, orderStatus, order, customer, check, rest, refund,
									orderAmountRecieved);
							paymentType = "CUSTOMER CREDIT";
							isPartialPayment = true;
							if (check.getCreditBalance() == 0) {
								// check.setCreditBalance(-orderAmountRecieved);
							} else if (check.getCreditBalance() > 0) {
								// check.setCreditBalance(Math.abs(check.getCreditBalance()+(-orderAmountRecieved)));
							} else if (check.getCreditBalance() < 0) {
								// check.setCreditBalance(check.getCreditBalance()-orderAmountRecieved);
							}
							order.setPaymentStatus(paymentType);
						}
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					// asyncService.emailException(ExceptionUtils.getStackTrace(e1),request);
					logger.info("Exception mail sent");
				}

			}
		}
		/** Ends here */

		/** Validating and initiating refund for order cancellation */
		ResponseDTO responseDTO = validateRefundForCustomerCredit(request, orderStatus, order, customer, check, rest,
				refund);
		/** Validating and initiating refund ends here */
		if (responseDTO != null) {
			if ("ERROR".equalsIgnoreCase(responseDTO.result)) {
				map.put("status", "error");
				map.put("message", responseDTO.message);
				return map;
			}
		}
		try {
			if (!StringUtility.isNullOrEmpty(paymentType)) {
				/** updating payment type and till transaction as well */
				if (!paymentType.equalsIgnoreCase(order.getPaymentStatus())) {
					String lastPaymentStatus = order.getPaymentStatus();
					order.setPaymentStatus(paymentType);
					updateCash(TillTransaction.UPDATE.toString(), (float) check.getRoundOffTotal(), check, request,
							false, lastPaymentStatus, "Edit order transaction checkId=" + check.getInvoiceId(), tillId,
							paymentType);
					List<PaymentType> paymentList = restService.listPaymentTypeByOrgId(rest.getParentRestaurantId());
					if (check.getCreditBalance() > 0) {
						for (PaymentType pt : paymentList) {
							if (pt.getName().equalsIgnoreCase(paymentType)) {
								if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
									updateCash(TillTransaction.UPDATE.toString(), (float) check.getCreditBalance(),
											check, request, true, lastPaymentStatus,
											"Edit order transaction checkId=" + check.getInvoiceId(), tillId,
											paymentType);
								} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())
										&& !("CUSTOMER CREDIT".equalsIgnoreCase(paymentType))) {
									updateCash(TillTransaction.UPDATE.toString(), (float) check.getCreditBalance(),
											check, request, true, lastPaymentStatus,
											"Edit order transaction checkId=" + check.getInvoiceId(), tillId,
											paymentType);
								} else if ("CUSTOMER CREDIT".equalsIgnoreCase(paymentType)) {
									updateCash(TillTransaction.CANCEL.toString(), (float) check.getCreditBalance(),
											check, request, true, lastPaymentStatus,
											" Order with orderId=" + order.getOrderId(), tillId, paymentType);
									invoiceService.addInvoice(check);
									Customer cust = customerService.getCustomer(check.getCustomerId());
									customerCreditService.updateBillRecoveryTransaction("FAILED", cust.getCustomerId(),
											check.getCreditBalance(), "CREDIT", "Setting Status Failed");
									check.setCreditBalance(0);
								}
							}
						}
					}

					try {
						asyncService.emailCheckFromServerNew(request, check, rest.getAlertMail(), rest,
								"defaultemailbill", "Payment type of this Invoice has been changed to " + paymentType,
								"cs", paymentType, "PAYMENT_TYPE_CHANGED", 0);
					} catch (Exception e) {
						logger.info("Email exception for checkID/InvoiceId :" + check.getInvoiceId() + "/"
								+ check.getInvoiceId());
						asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
						logger.info("Exception mail sent");
					}
					// String remarks = request.getParameter("remarks");
					if (remarks != null) {
						check.setEditOrderRemark(remarks);
					}
					if (rest.isDeliveryManagerEdit()) {
						// check.setAllowEdit(true);
					}
				} else {
					order.setPaymentStatus(paymentType);
				}
				/** updating payment type and till transaction ends here */
			}
			if (role != null) {
				creditCancellationAndEmail(request, check, orderStatus, oldStatus, rest, refund, remarks);
			}
			if (paidStatus != null) {
				markOrderPG(paidStatus, check, order);
			}
			/** Till and order status management */
			map = tillAndStatusManagementForOrders(request, status, check, order, rest, paymentType, orderStatus,
					oldStatus, moneyIn, tillId, updateToPaymentType, isPartialPayment, deliveryBoy);
			if (map != null) {
				return map;
			}
			/** Till and order status management end */

			if (status.equals("EDITMONEYOUT") || status.equals("EDITMONEYIN")) {
				if (status.equals("EDITMONEYOUT"))
					order.setModifiedTime(new Date());
				orderService.addOrder(order);
				return map;
			} else {
				if (oldStatus != Status.CANCELLED) {
					order.setStatus(orderStatus);
				}
				order.setModifiedTime(new Date());
				orderService.addOrder(order);
				return map;
			}
		} catch (Exception e) {
			map.put("Error", e.getMessage());
			asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
			logger.info("Exception mail sent");
			return map;
		}
	}

	public void updatePlatterBooking(Date deliveryDateTime, Integer noOfPeople, Integer menuId) throws ParseException {
		if (noOfPeople > 0) {
			BookedPlatter booking = new BookedPlatter();
			// String format = "yyyy-MM-dd HH:mm";
			// SimpleDateFormat formatterD = new SimpleDateFormat(format);
			// Date deliveryTimeD = formatterD.parse(deliveryDateTime);

			booking.setCount(-noOfPeople);
			booking.setPlatterId(menuId);
			booking.setDate(deliveryDateTime);
			Integer vendorId = vendorPlatterService.getVendorIdUsingPlatterId(menuId);
			booking.setVendorId(vendorId);
			vendorPlatterService.addPlatterBooking(booking);
		}
	}

	public void markOrderPG(String paidStatus, Invoice check, Order order) {
		if ("Paid".equalsIgnoreCase(paidStatus)) {
			logger.info("Marking order paid  and setting PG:" + order.getOrderId());
			check.setStatus(com.emanage.enums.check.Status.Paid);
			// check.setPayment(PaymentMode.PG);
			order.setPaymentStatus("PG");
			invoiceService.addInvoice(check);
		}
	}

	public Map<String, Object> tillAndStatusManagementForOrders(HttpServletRequest request, String statusStr,
			Invoice check, Order order, Restaurant rest, String paymentType, Status status, Status oldStatus,
			float money, String tillId, String updateToPaymentType, boolean isPartialPayment, String deliveryBoy)
			throws Exception {
		Map<String, Object> map = new TreeMap<String, Object>();
		if (oldStatus == Status.CANCELLED) {
			status = Status.CANCELLED;
		}
		if (statusStr.equalsIgnoreCase("OUTDELIVERY") || statusStr.equalsIgnoreCase("EDITMONEYOUT")) {
			// String deliveryBoy = request.getParameter("deliveryBoy");
			/* Zomato api calling for status update */
			/*
			 * if(check.getZomatoOrderId()!=null){ try{
			 * zomatoService.zomatoOrderOutForDelivery(check,request); }catch(Exception e){
			 * logger.info("Zomto update order out status failed:  exception: "+e.getCause()
			 * ); } }
			 */

			logger.info("2>Setting status " + status + " for payment Type " + paymentType + " for orderId :"
					+ order.getOrderId());
			List<PaymentType> paymentList = restService.listPaymentTypeByOrgId(rest.getParentRestaurantId());
			for (PaymentType pt : paymentList) {
				if (pt.getName().equalsIgnoreCase(order.getPaymentStatus())) {
					if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
						if (money > 0) {
							ResponseDTO resp = updateCash(TillTransaction.NEW.toString(), money, check, request, false,
									CustomPaymentType.TRANSACTION_CASH.toString(),
									"Transaction Cash Out To Address Order orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
							if (resp.result.equalsIgnoreCase("Error")) {
								map.put("Error", resp.message);
								return map;
							}
						}
						if (check.getCreditBalance() > 0) {
							updateCash(TillTransaction.NEW.toString(), (float) check.getRoundOffTotal(), check, request,
									false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
							updateCash(TillTransaction.NEW.toString(), (float) check.getCreditBalance(), check, request,
									true, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else if (check.getCreditBalance() < 0) {
							updateCash(TillTransaction.NEW.toString(),
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
									request, false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else {
							updateCash(TillTransaction.NEW.toString(), (float) check.getRoundOffTotal(), check, request,
									false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						}
						check.setStatus(com.emanage.enums.check.Status.Unpaid);
					} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())) {
						if (check.getCreditBalance() > 0) {
							updateCash(TillTransaction.NEW.toString(), (float) check.getRoundOffTotal(), check, request,
									false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
							updateCash(TillTransaction.NEW.toString(), (float) check.getCreditBalance(), check, request,
									true, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else if (check.getCreditBalance() < 0) {
							updateCash(TillTransaction.NEW.toString(),
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
									request, false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else {
							updateCash(TillTransaction.NEW.toString(), (float) check.getRoundOffTotal(), check, request,
									false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						}
						// check.setStatus(com.emanage.enums.check.Status.Paid);
					} else if (pt.getType().equalsIgnoreCase(BasePaymentType.PREPAID.toString())) {
						if (check.getCreditBalance() > 0) {
							updateCash(TillTransaction.NEW.toString(), (float) check.getRoundOffTotal(), check, request,
									false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
							updateCash(TillTransaction.NEW.toString(), (float) check.getCreditBalance(), check, request,
									false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else if (check.getCreditBalance() < 0) {
							updateCash(TillTransaction.NEW.toString(),
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
									request, false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else {
							updateCash(TillTransaction.NEW.toString(), (float) check.getRoundOffTotal(), check, request,
									false, paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						}
						// check.setStatus(com.emanage.enums.check.Status.Paid);
					} else if (pt.getType().equalsIgnoreCase(BasePaymentType.DNC.toString())) {
						updateCash(TillTransaction.NEW.toString(),
								(float) check.getRoundOffTotal() + check.getCreditBalance(), check, request, false,
								paymentType, "New Order with orderId=" + order.getOrderId(), tillId,
								updateToPaymentType);
					}
				}
				/* } */
				invoiceService.addInvoice(check);
			}
			order.setMoneyOut(money);
			logger.info("money out : " + money);
			order.setDeliveryAgent(deliveryBoy);
		} else if (statusStr.equalsIgnoreCase("CONFIRMDELIVERY") || statusStr.equalsIgnoreCase("EDITMONEYIN")) {
			logger.info("money In : " + money);
			order.setMoneyIn(money);
		}
		if (status == Status.CANCELLED) {
			try {
				restoreStock(check, 0);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
				logger.info("Exception mail sent");
			}
			if (oldStatus == Status.OUTDELIVERY) {
				TransactionDTO tillList = cashRegisterService.fetchTransactionsByCheck(check.getCountryId(),
						check.getInvoiceNo());
				// List<SaleTransaction> listTill = tillList.transationList;
				tillId = tillList.tillDetails.tillId;
				List<PaymentType> paymentList = restService.listPaymentTypeByOrgId(rest.getParentRestaurantId());
				for (PaymentType pt : paymentList) {
					if (pt.getName().equalsIgnoreCase(order.getPaymentStatus())) {
						if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
							if (check.getCreditBalance() > 0) {
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getRoundOffTotal(), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with orderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getCreditBalance(), check,
										request, true, order.getPaymentStatus(),
										" Order with orderId=" + order.getOrderId(), tillId, updateToPaymentType);
							} else if (check.getCreditBalance() < 0) {
								updateCash(TillTransaction.CANCEL.toString(),
										(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with orderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
							} else {
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getRoundOffTotal(), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with orderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
							}
						} else if (pt.getType().equalsIgnoreCase(BasePaymentType.PREPAID.toString())) {
							if (check.getCreditBalance() > 0) {
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getRoundOffTotal(), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getCreditBalance(), check,
										request, true, order.getPaymentStatus(),
										"New Order with orderId=" + order.getOrderId(), tillId, updateToPaymentType);
							} else if (check.getCreditBalance() < 0) {
								updateCash(TillTransaction.CANCEL.toString(),
										(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
							} else {
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getRoundOffTotal(), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
							}
						} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())) {
							if (check.getCreditBalance() > 0) {
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getRoundOffTotal(), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getCreditBalance(), check,
										request, true, order.getPaymentStatus(),
										"New Order with orderId=" + order.getOrderId(), tillId, updateToPaymentType);
							} else if (check.getCreditBalance() < 0) {
								updateCash(TillTransaction.CANCEL.toString(),
										(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
							} else {
								updateCash(TillTransaction.CANCEL.toString(), (float) check.getRoundOffTotal(), check,
										request, false, order.getPaymentStatus(),
										order.getPaymentStatus() + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
							}
						} else if (pt.getType().equalsIgnoreCase(BasePaymentType.DNC.toString())) {
							updateCash(TillTransaction.CANCEL.toString(),
									(float) check.getRoundOffTotal() + check.getCreditBalance(), check, request, false,
									order.getPaymentStatus(),
									order.getPaymentStatus() + " Order with OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						}
					}
				}
			} else if (oldStatus == Status.CANCELLED || (status == Status.CANCELLED && oldStatus == Status.DELIVERED)) {
				paymentType = order.getPaymentStatus();
				List<PaymentType> paymentList = restService.listPaymentTypeByOrgId(rest.getParentRestaurantId());
				for (PaymentType pt : paymentList) {
					if (pt.getName().equalsIgnoreCase(order.getPaymentStatus())) {
						if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
							if (oldStatus == Status.DELIVERED) {
								String userName = (String) request.getSession().getAttribute("username");
								User user = null;
								if (userName != null) {
									user = userService.getUserByUsername(userName);
								}
								try {
									ResponseDTO respDTO = cashRegisterService.cancelOrderTransaction(order.getCheckId(),
											paymentType, TillTransactionStatus.SUCCESS, true, user.getUserId());
									logger.info("Response DTO for cancelled order: " + respDTO.message + ""
											+ respDTO.message);
								} catch (Exception e) {
									e.printStackTrace();
									asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
									logger.info("Exception mail sent");
								}
								// updateCash(TillTransaction.CANCEL.toString(), (float)
								// check.getRoundOffTotal(), check, request, paymentType, "Cancelling after
								// deivered OrderId=" + order.getOrderId(), tillId, updateToPaymentType);
								/*
								 * }else if(check.getCreditBalance()<0){ //
								 * updateCash(TillTransaction.CANCEL.toString(),
								 * (float)Math.abs(check.getRoundOffTotal()+check.getCreditBalance()), check,
								 * request, paymentType, "Cancelling after deivered OrderId=" +
								 * order.getOrderId(), tillId, updateToPaymentType); }else{ //
								 * updateCash(TillTransaction.CANCEL.toString(), (float)
								 * check.getRoundOffTotal(), check, request, paymentType,
								 * "Transaction Cash In To from OrderId=" + order.getOrderId(), tillId,
								 * updateToPaymentType); }
								 */
							} else {
								if (order.getMoneyOut() > 0) {
									updateCash(TillTransaction.SUCCESS.toString(), order.getMoneyOut(), check, request,
											false, CustomPaymentType.TRANSACTION_CASH.toString(),
											"Transaction Cash In To Address Order orderId=" + order.getOrderId(),
											tillId, updateToPaymentType);
									order.setMoneyIn(order.getMoneyOut());
								}
							}

						} else if (pt.getType().equalsIgnoreCase(BasePaymentType.PREPAID.toString())) {
							if (oldStatus == Status.DELIVERED) {
								String userName = (String) request.getSession().getAttribute("username");
								User user = null;
								if (userName != null) {
									user = userService.getUserByUsername(userName);
								}
								try {
									ResponseDTO respDTO = cashRegisterService.cancelOrderTransaction(order.getCheckId(),
											paymentType, TillTransactionStatus.SUCCESS, true, user.getUserId());
									logger.info("Response DTO for cancelled order: " + respDTO.message + ""
											+ respDTO.message);
								} catch (Exception e) {
									e.printStackTrace();
									asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
									logger.info("Exception mail sent");
								}
								/*
								 * if(check.getCreditBalance()>0){ updateCash(TillTransaction.CANCEL.toString(),
								 * (float)check.getRoundOffTotal(), check, request, paymentType,
								 * paymentType+" Order with OrderId="+order.getOrderId(),tillId,
								 * updateToPaymentType); }else if(check.getCreditBalance()<0){
								 * updateCash(TillTransaction.CANCEL.toString(),
								 * (float)Math.abs(check.getRoundOffTotal()+check.getCreditBalance()), check,
								 * request, paymentType,
								 * paymentType+" Order Cancelled with OrderId="+order.getOrderId(),tillId,
								 * updateToPaymentType); }else{ updateCash(TillTransaction.CANCEL.toString(),
								 * (float)check.getRoundOffTotal(), check, request, paymentType,
								 * paymentType+" Order Cancelled with OrderId="+order.getOrderId(),tillId,
								 * updateToPaymentType); }
								 */
							} else {
								if (order.getMoneyOut() > 0) {
									updateCash(TillTransaction.SUCCESS.toString(), order.getMoneyOut(), check, request,
											false, CustomPaymentType.TRANSACTION_CASH.toString(),
											"Transaction Cash In To Address Order orderId=" + order.getOrderId(),
											tillId, updateToPaymentType);
									order.setMoneyIn(order.getMoneyOut());
								}
							}

						} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())) {
							if (oldStatus == Status.DELIVERED) {
								String userName = (String) request.getSession().getAttribute("username");
								User user = null;
								if (userName != null) {
									user = userService.getUserByUsername(userName);
								}
								ResponseDTO respDTO = null;
								if ("CUSTOMER CREDIT".equalsIgnoreCase(updateToPaymentType)) {
									try {
										respDTO = cashRegisterService.cancelOrderTransaction(order.getCheckId(),
												paymentType, TillTransactionStatus.SUCCESS, false, user.getUserId());
									} catch (Exception e) {
										e.printStackTrace();
										asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
										logger.info("Exception mail sent");
									}
								} else {
									try {
										respDTO = cashRegisterService.cancelOrderTransaction(order.getCheckId(),
												paymentType, TillTransactionStatus.SUCCESS, true, user.getUserId());
									} catch (Exception e) {
										e.printStackTrace();
										asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
										logger.info("Exception mail sent");
									}
								}
								logger.info(
										"Response DTO for cancelled order: " + respDTO.message + "" + respDTO.message);
								/*
								 * if(check.getCreditBalance()>0){ updateCash(TillTransaction.CANCEL.toString(),
								 * (float)check.getRoundOffTotal(), check, request, paymentType,
								 * paymentType+" Order with OrderId="+order.getOrderId(),tillId,
								 * updateToPaymentType); }else if(check.getCreditBalance()<0){
								 * updateCash(TillTransaction.CANCEL.toString(),
								 * (float)Math.abs(check.getRoundOffTotal()+check.getCreditBalance()), check,
								 * request, paymentType,
								 * paymentType+" Order with OrderId="+order.getOrderId(),tillId,
								 * updateToPaymentType); }else{ updateCash(TillTransaction.CANCEL.toString(),
								 * (float)check.getRoundOffTotal(), check, request, paymentType,
								 * paymentType+" Order with OrderId="+order.getOrderId(),tillId,
								 * updateToPaymentType); }
								 */
							} else {
								if (order.getMoneyOut() > 0) {
									updateCash(TillTransaction.SUCCESS.toString(), order.getMoneyOut(), check, request,
											false, CustomPaymentType.TRANSACTION_CASH.toString(),
											"Transaction Cash In To Address Order orderId=" + order.getOrderId(),
											tillId, updateToPaymentType);
									order.setMoneyIn(order.getMoneyOut());
								}
							}
						} else if (pt.getType().equalsIgnoreCase(BasePaymentType.DNC.toString())) {
							if (oldStatus == Status.DELIVERED) {
								String userName = (String) request.getSession().getAttribute("username");
								User user = null;
								if (userName != null) {
									user = userService.getUserByUsername(userName);
								}
								try {
									ResponseDTO respDTO = cashRegisterService.cancelOrderTransaction(order.getCheckId(),
											updateToPaymentType, TillTransactionStatus.SUCCESS, true, user.getUserId());
									logger.info("Response DTO for cancelled order: " + respDTO.message + ""
											+ respDTO.message);
								} catch (Exception e) {
									e.printStackTrace();
									asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
									logger.info("Exception mail sent");
								}
								// updateCash(TillTransaction.CANCEL.toString(),
								// (float)check.getRoundOffTotal()+check.getCreditBalance(), check, request,
								// paymentType, paymentType+" Order with
								// OrderId="+order.getOrderId(),tillId,updateToPaymentType);
							} else {
								if (order.getMoneyOut() > 0) {
									updateCash(TillTransaction.SUCCESS.toString(), order.getMoneyOut(), check, request,
											false, CustomPaymentType.TRANSACTION_CASH.toString(),
											"Transaction Cash In To Address Order orderId=" + order.getOrderId(),
											tillId, updateToPaymentType);
									order.setMoneyIn(order.getMoneyOut());
								}
							}
						}
					}
				}
			}

			check.setStatus(com.emanage.enums.check.Status.Cancel);
			check.setCreditBalance(0);
			invoiceService.addInvoice(check);
			logger.info("0000> Invoice Id " + check.getInvoiceId() + " : Order Id " + order.getOrderId() + " :Status :"
					+ status + " user Name : " + request.getAttribute("username"));
			updateCheckForCancelOrder(order);
		}
		if (status == Status.DELIVERED) {

			List<PaymentType> paymentList = restService.listPaymentTypeByOrgId(rest.getParentRestaurantId());
			for (PaymentType pt : paymentList) {
				if (pt.getName().equalsIgnoreCase(order.getPaymentStatus())) {
					if (pt.getType().equalsIgnoreCase(BasePaymentType.CASH.toString())) {
						float moneyIn = Math
								.abs((float) (money - (check.getRoundOffTotal() + check.getCreditBalance())));
						if (check.getCreditBalance() > 0) {
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getRoundOffTotal(), check,
									request, false, paymentType,
									"Transaction Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getCreditBalance(), check,
									request, true, paymentType,
									"Transaction Credit Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else if (check.getCreditBalance() < 0) {
							updateCash(TillTransaction.SUCCESS.toString(),
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
									request, false, paymentType,
									"Transaction Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else {
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getRoundOffTotal(), check,
									request, false, paymentType,
									"Transaction Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						}
						if (moneyIn > 0) {
							updateCash(TillTransaction.SUCCESS.toString(), moneyIn, check, request, false,
									CustomPaymentType.TRANSACTION_CASH.toString(),
									"Transaction Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						}
					} else if (pt.getType().equalsIgnoreCase(BasePaymentType.CREDIT.toString())) {
						if (order.getMoneyOut() > 0)
							updateCash(TillTransaction.SUCCESS.toString(), order.getMoneyOut(), check, request, false,
									CustomPaymentType.TRANSACTION_CASH.toString(),
									"Transaction Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						if (check.getCreditBalance() > 0 && !isPartialPayment) {
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getRoundOffTotal(), check,
									request, false, paymentType,
									paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getCreditBalance(), check,
									request, true, paymentType,
									"Transaction Credit Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else if (check.getCreditBalance() < 0 && !isPartialPayment) {
							updateCash(TillTransaction.SUCCESS.toString(),
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
									request, false, paymentType,
									paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else {
							if (check.getCreditBalance() < 0) {
								updateCash(TillTransaction.SUCCESS.toString(),
										(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
										request, false, paymentType,
										paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);
							} else {
								updateCash(TillTransaction.SUCCESS.toString(), (float) check.getRoundOffTotal(), check,
										request, false, paymentType,
										paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
										updateToPaymentType);

							}
						}
					} else if (pt.getType().equalsIgnoreCase(BasePaymentType.PREPAID.toString())) {
						if (order.getMoneyOut() > 0)
							updateCash(TillTransaction.SUCCESS.toString(), order.getMoneyOut(), check, request, false,
									CustomPaymentType.TRANSACTION_CASH.toString(),
									"Transaction Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						if (check.getCreditBalance() > 0) {
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getRoundOffTotal(), check,
									request, false, paymentType,
									paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getCreditBalance(), check,
									request, true, paymentType,
									"Transaction Credit Cash In To from OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else if (check.getCreditBalance() < 0) {
							updateCash(TillTransaction.SUCCESS.toString(),
									(float) Math.abs(check.getRoundOffTotal() + check.getCreditBalance()), check,
									request, false, paymentType,
									paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						} else {
							updateCash(TillTransaction.SUCCESS.toString(), (float) check.getRoundOffTotal(), check,
									request, false, paymentType,
									paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
									updateToPaymentType);
						}
					} else if (pt.getType().equalsIgnoreCase(BasePaymentType.DNC.toString()))
						updateCash(TillTransaction.SUCCESS.toString(),
								(float) check.getRoundOffTotal() + check.getCreditBalance(), check, request, false,
								paymentType, paymentType + " Order with OrderId=" + order.getOrderId(), tillId,
								updateToPaymentType);

				}
			}

			if (check.getCreditBalance() != 0 && !("CUSTOMER CREDIT".equalsIgnoreCase(paymentType))) {
				Customer cust = customerService.getCustomer(check.getCustomerId());
				customerCreditService.updateBillRecoveryTransaction("SUCCESS", cust.getCustomerId(),
						check.getCreditBalance(), "CREDIT", "Setting Status Succes");

			} else if (check.getCreditBalance() < 0 && "CUSTOMER CREDIT".equalsIgnoreCase(paymentType)) {
				Customer cust = customerService.getCustomer(check.getCustomerId());
				try {
					customerCreditService.updateBillRecoveryTransaction("SUCCESS", cust.getCustomerId(),
							check.getCreditBalance(), "CREDIT", "Setting Status Succes");
				} catch (Exception e) {
					e.printStackTrace();
					asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
					logger.info("Exception mail sent");
				}
			} else if (check.getCreditBalance() > 0 && "CUSTOMER CREDIT".equalsIgnoreCase(paymentType)) {
				Customer cust = customerService.getCustomer(check.getCustomerId());
				try {
					customerCreditService.updateBillRecoveryTransaction("FAILED", cust.getCustomerId(),
							check.getCreditBalance(), "CREDIT", "Setting Status Failed");
				} catch (Exception e) {
					e.printStackTrace();
					// asyncService.emailException(ExceptionUtils.getStackTrace(e),request);
					logger.info("Exception mail sent");
				}
				if (!isPartialPayment) {
					check.setCreditBalance(0);
				}
			}
			logger.info("money In : " + money);
			order.setMoneyIn(money);
			check.setStatus(com.emanage.enums.check.Status.Paid);
			invoiceService.addInvoice(check);
		}
		return null;
	}

	public void restoreStock(Invoice check, int restoreCount) throws ParseException {

	}

	public void updateCheckForCancelOrder(Order order) {
		// Fetch the check from order
		// Update check with calculations of removal of bill values etc.
		logger.info("UpdateChekForcancelOrder api called" + order.getCheckId());
		Invoice check = invoiceService.getInvoice(order.getCheckId());
		float checkBill = check.getBill() - order.getBill();
		// check.setOutCircleDeliveryCharges(0);
		if (checkBill <= 0) {
			checkBill = 0;
		}

		check.setBill(checkBill);
		invoiceService.addInvoice(check);
	}
	// private ResponseDTO updateCash(String transactionType,Float amount,Check
	// check,HttpServletRequest request,boolean isCredit,String paymentType,String
	// remark,String tillId,String updateToPaymentType) throws Exception{
	// TillCashUpdateDTO updateDTO = new TillCashUpdateDTO();
	// updateDTO.amount = amount;
	// updateDTO.checkId = check.getCheckId();
	// updateDTO.paymentType=paymentType;
	// updateDTO.ffcId=check.getKitchenScreenId();
	// updateDTO.remarks=remark;
	// updateDTO.tillTransactionType=transactionType;
	// updateDTO.updatedToPaymentType="";
	// updateDTO.tillId = tillId;
	// updateDTO.updatedToPaymentType=updateToPaymentType;
	// updateDTO.isCredit = isCredit;
	// ResponseDTO response=null;
	// synchronized(this){
	// System.out.println("called this API");
	// response = cashRegisterController.updateCash(updateDTO, request);
	// if(response.result.equals("Error"))
	// try{
	// throw new Exception(response.message);
	// }catch(Exception e){
	// e.printStackTrace();
	// //asyncService.emailException(ExceptionUtils.getStackTrace(e),request);
	// logger.info("Exception: "+response.message);
	// return response;
	// }
	// }
	// return response;
	// }

	public OrderResponse validateCustomerCredit(Order targetOrder, Invoice check, Customer customer) {
		OrderResponse orderResp = new OrderResponse();
		if ("CUSTOMER CREDIT".equalsIgnoreCase(targetOrder.getPaymentStatus())) {
			if (customer.getCredit() != null) {
				if (customer.getCredit().getStatus() == CustomerCreditAccountStatus.ACTIVE) {
					if (customer.getCredit().getCreditBalance() + check.getRoundOffTotal() > customer.getCredit()
							.getMaxLimit()) {
						orderResp.setStatus("Failed");
						logger.info("23>setting status Failed");
						orderResp.setError("Your Customer Credit limit is over. Remaining amount is :"
								+ (customer.getCredit().getMaxLimit() - customer.getCredit().getCreditBalance())
								+ " and your bill amount is: " + check.getRoundOffTotal());
						return orderResp;

					} else {
						orderResp.setStatus("Success");
						return orderResp;
					}
				}
			}
		}
		return orderResp;
	}

	public ResponseDTO openDefaultCreditAccount(Integer orgId, Invoice check, TransactionCategory transactionType,
			Float withDefaultAmount) {
		Restaurant org = restService.getRestaurant(orgId);
		ResponseDTO responseDto = null;
		if (org.isEnableCustCredit()) {
			CreditType creditType = customerService.getCreditType(org.getDefaultCreditType());
			if (creditType != null) {
				CustomerCreditDTO customerCreditDTO = new CustomerCreditDTO();
				customerCreditDTO.creditBalance = 0;
				customerCreditDTO.creditTypeId = creditType.getId();
				customerCreditDTO.customerId = check.getCustomerId();
				customerCreditDTO.maxLimit = creditType.getMaxLimit();
				// customerCreditDTO.ffcId = check.getKitchenScreenId();
				customerCreditDTO.billingAddress = "";
				try {
					responseDto = customerService.enableCustomerCredit(customerCreditDTO, orgId);

					if (responseDto.result.equalsIgnoreCase("SUCCESS")) {
						logger.info("Account has been created  status: " + responseDto.result);
						AddCreditToCustomerAccountDTO creditAddDTO = new AddCreditToCustomerAccountDTO();
						creditAddDTO.customerId = check.getCustomerId();
						creditAddDTO.invoiceId = check.getInvoiceId();
						if (withDefaultAmount != null) {
							creditAddDTO.amount = withDefaultAmount;
						} else {
							creditAddDTO.amount = (float) check.getRoundOffTotal();
						}
						creditAddDTO.orgId = orgId;
						creditAddDTO.remark = "Created a default account " + creditType.getName()
								+ " and crediting to it";
						creditAddDTO.transactionType = transactionType;
						responseDto = customerService.creatTransaction(creditAddDTO, orgId, check.getInvoiceNo());
						logger.info(" Transaction status : " + responseDto.result);
					} else {
						logger.info("Account status: " + responseDto.result + " Message : " + responseDto.message);
					}
				} catch (Exception e) {
					logger.info("Failed to open Default account for customer ");
					try {
						asyncService.emailException(ExceptionUtils.getStackTrace(e), REQUEST);
					} catch (UnsupportedEncodingException | MessagingException e1) {
						e1.printStackTrace();
					}
					logger.info("Exception mail sent");
				}
			}
		}
		return responseDto;
	}

	@RequestMapping(value = "/sendEmailUsingAPI", method = RequestMethod.GET)
	public @ResponseBody ResponseDTO sendEmailUsingAPI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) String checkId) {

		ResponseDTO resp = new ResponseDTO();
		if (checkId != null) {
			Invoice check = invoiceService.getInvoice(Integer.parseInt(checkId));
			if (check != null) {
				Customer customer = customerService.getCustomer(check.getCustomerId());
				Restaurant restaurant = restService.getRestaurant(check.getCountryId());
				try {
					asyncService.emailCheckFromServerNew(request, check, customer.getEmail(), restaurant, "", null,
							null, null, null, (float) 0.0);
					resp.message = "Email sent.";
					resp.result = "Success";
				} catch (UnsupportedEncodingException | MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			resp.message = "Invalid invoice Id.";
			resp.result = "Error";
		}
		return resp;
	}

	@RequestMapping(value = "/getOrdersByType", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<OrderDTO> getOrdersByType(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) String restaurantId, @RequestParam(required = false) String[] orderType,
			@RequestParam(required = false) String ordersOfDay) throws Exception {

		Integer restaurantID;
		// String restIdStr = request.getParameter("restaurantId");

		if (restaurantId != null && !restaurantId.equals(""))
			restaurantID = Integer.parseInt(restaurantId);
		else if (request.getSession().getAttribute("restaurantId") != null)
			restaurantID = (Integer) request.getSession().getAttribute("restaurantId");
		else
			throw new Exception("vendor not found!!");

		List<String> orderTypeList = Arrays.asList(orderType);
		// String ordersOfDay = request.getParameter("ordersOfDay");
		if (ordersOfDay == null || ordersOfDay == "")
			ordersOfDay = "Today";
		return orderService.getOrders(restaurantID, orderTypeList, ordersOfDay);
	}

	@RequestMapping(value = "/editOrder", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody Map<String, Object> editOrder(@RequestBody PlaceOrderDTO orderBody, Model model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		REQUEST = request;
		Invoice invoice = invoiceService.getInvoice(Integer.parseInt(orderBody.order.checkId));
		Invoice tempInvoice = new Invoice();
		if (invoice == null) {
			throw new Exception("Order doesn't already exists");
		}
		double lastSavedTotal = invoice.getRoundOffTotal();
		Gson gson = new Gson();
		if (invoice != null) {
			String json = gson.toJson(invoice);
			EditInvoices editCheck = new EditInvoices();
			editCheck.setInvoiceNo(invoice.getInvoiceNo());
			editCheck.setInvoiceJson(json);
			// editCheck.setOpenTime(check.getModifiedTime());
			invoiceService.addEditInvoice(editCheck);
		}
		tempInvoice = copyCheck(invoice);

		logger.info("Entered into EDIT POS");

		Map<String, Object> m = new HashMap<String, Object>();
		request.setAttribute("editOrderFlag", true);
		request.setAttribute("lastSavedTotal", lastSavedTotal);
		String username = (String) request.getSession().getAttribute("username");
		try {
			Integer countryID;
			// String restIdStr = request.getParameter("restaurantId");

			if (orderBody.countryId != null && orderBody.countryId > 0)
				countryID = orderBody.countryId;
			else if (request.getSession().getAttribute("restaurantId") != null)
				countryID = (Integer) request.getSession().getAttribute("restaurantId");
			else
				throw new Exception("countryID not found!!");

			Restaurant restaurant = restService.getRestaurant(countryID);
			String role = (String) request.getSession().getAttribute("role");
			Order lastOrder = orderService.getOrder(Integer.parseInt(orderBody.order.id));
			if (lastOrder.getStatus() == Status.OUTDELIVERY) {

				m.put("status", "error");
				m.put("error",
						"This order has been dispatched.So you can't make any change.Please contact Restaurant Manager");
				return m;
			} else if (lastOrder.getStatus() == Status.DELIVERED) {
				boolean allow = false;
				if (orderBody.order.discountAmount > 0 || allow) {
					request.setAttribute("allowDiscount", true);
				}
			}
			logger.info("Check id : " + orderBody.order.checkId + "/" + invoice.getInvoiceId() + "  System user Role : "
					+ role + " user :" + username);

			double calories = 0;// removeCalories(invoice.getOrders(),restaurant);
			Customer customer = getCustomer(orderBody);
			customer.setDeliveryTime(orderBody.order.deliveryDateTime);
			customer.setRestaurantId(countryID);

			if (restaurant != null)
				customer.setOrgId(restaurant.getParentRestaurantId());
			else
				customer.setOrgId(countryID);

			customerController.setCustomerInfoJSON(customer, request);
			request.setAttribute("restaurantId", countryID.toString());
			request.setAttribute("custId", customer.getCustomerId().toString());

			List<Order_DCList> discount_Charge = getDCValue(orderBody);
			if (discount_Charge != null)
				invoice.setDiscount_Charge(discount_Charge);

			// CheckType checkType = CheckType.valueOf(CheckType.class, "Delivery");
			// check.setCheckType(CheckType.Delivery);
			request.setAttribute("checkType", "Delivery");
			invoice.setBill(0.0f);
			invoice.setRoundOffTotal(0);
			invoice.setRewards(Math.abs(invoice.getRewards() - calories));
			invoiceService.addInvoice(invoice);
			Status status = lastOrder.getStatus();
			String deliveryBoy = lastOrder.getDeliveryAgent();
			request.setAttribute("editOrderDeliverBoy", deliveryBoy);
			request.setAttribute("editOrderMoneyIn", lastOrder.getMoneyIn());
			request.setAttribute("editOrderMoneyOut", lastOrder.getMoneyOut());
			request.setAttribute("editOrderMicroKitchen", lastOrder.getMicroKitchenId());
			request.setAttribute("editOrderPaymentStatus", lastOrder.getPaymentStatus());
			request.setAttribute("editBill", lastOrder.getBill());
			request.setAttribute("editCheckBill", (float) tempInvoice.getRoundOffTotal());

			if (orderBody.order.paidStatus != null) {
				request.setAttribute("paidStatus", "Paid");
			}
			// this.lastOrder=lastOrder;
			orderService.removeOrder(Integer.parseInt(orderBody.order.id));

			JsonOrder jsonOrder = getJsonOrder(orderBody, customer, invoice);
			//request.setAttribute("editOrderStatus", status);
			request.setAttribute("paymentStatus", orderBody.order.paymentMethod);

			request.setAttribute("paymentThirdParty", orderBody.order.paymentMethod);
			// if(orderBody.order.deliveryCharges==0){
			// request.setAttribute("deliveryCharge","Zero");
			// }
			if (orderBody.order.orderSource == null || orderBody.order.orderSource.equals(""))
				request.setAttribute("orderSource", "Website");
			else {
				request.setAttribute("orderSource", orderBody.order.orderSource);
			}

			request.setAttribute("deliveryInst", orderBody.order.instructions);
			if (orderBody.order.couponCode != null) {
				if (orderBody.order.couponCode.size() > 0) {
					jsonOrder.setCouponCode(orderBody.order.couponCode);
				}
			}
			OrderResponse orderResponse = addToCheckJSON(orderBody, jsonOrder, request, response);
			if ("Failed".equalsIgnoreCase(orderResponse.getStatus())
					|| "Failure".equalsIgnoreCase(orderResponse.getStatus())) {
				revertEditedOrder(tempInvoice, lastOrder);
				m.put("status", "error");
				m.put("error", orderResponse.getError());
			} else {
				m.put("status", "success");
				m.put("Id", invoice.getInvoiceNo());
			}
		} catch (Exception e) {
			response.setStatus(500);
			m.put("status", "error");
			asyncService.emailException(ExceptionUtils.getStackTrace(e), request);
			logger.info("Exception mail sent");
			e.printStackTrace();
		}
		return m;
	}

	@RequestMapping(value = "/validateTillAccess", method = RequestMethod.POST)
	@ResponseBody
	Map<String, String> validateTillAccess(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Integer checkId, @RequestParam String username) {
		// String checkId = request.getParameter("checkId");
		Map<String, String> map = new HashMap<String, String>();
		Invoice invoice = invoiceService.getInvoice(checkId);
		if (invoice != null) {
			// String username = request.getParameter("username");
			Order order = orderService.getOrder(invoice.getOrderId());
			if (order.getStatus() == Status.OUTDELIVERY) {
				if (username != null) {
					User user = userService.getUserByUsername(username);
					if (user != null) {
						TransactionDTO transaction = cashRegisterService.fetchTransactionsByCheck(user.getUserId(),
								invoice.getInvoiceNo());
						TillDTO till = transaction.tillDetails;
						boolean isValid = cashRegisterService.validateTillAccess(till.tillId, user.getUserId());
						if (isValid) {
							map.put("status", "true");
							return map;
						} else {
							map.put("status", "false");
							map.put("message",
									"You don't have access to update this order. The person who has opened the sale register can edit this order");
							return map;
						}
					}
				} else {
					map.put("status", "false");
					map.put("message", "This user doesn't exist.");
					return map;
				}
			} else {
				map.put("status", "true");
				return map;
			}
		} else {
			map.put("status", "false");
			map.put("message", "This check is not present in our database.");
			return map;
		}
		return map;
	}

	public Invoice copyCheck(Invoice invoice) {
		Invoice invoces = new Invoice();
		invoces.setBill(invoice.getBill());
		invoces.setEmail(invoice.getEmail());
		invoces.setCountryId(invoice.getCountryId());
		invoces.setInvoiceId(invoice.getInvoiceId());
		invoces.setInvoiceNo(invoice.getInvoiceNo());
		invoces.setInvoiceType(invoice.getInvoiceType());
		invoces.setCloseTime(invoice.getCloseTime());
		invoces.setCustomerId(invoice.getCustomerId());
		invoces.setDeliveryAddress(invoice.getDeliveryAddress());
		invoces.setDeliveryInst(invoice.getDeliveryInst());
		invoces.setDeliveryDateTime(invoice.getDeliveryDateTime());
		invoces.setDiscount_Charge(invoice.getDiscount_Charge());

		invoces.setEditOrderRemark(invoice.getEditOrderRemark());
		invoces.setFirstOrder(invoice.isFirstOrder());
		invoces.setScreenId(invoice.getScreenId());
		invoces.setName(invoice.getName());
		invoces.setOpenTime(invoice.getOpenTime());
		invoces.setOrderSource(invoice.getOrderSource());

		invoces.setPaymentMode(invoice.getPaymentMode());
		invoces.setResponseCode(invoice.getResponseCode());
		invoces.setRewards(invoice.getRewards());
		invoces.setRoundOffTotal(invoice.getRoundOffTotal());
		invoces.setStatus(invoice.getStatus());

		invoces.setTaxJsonObject(invoice.getTaxJsonObject());
		invoces.setTransactionId(invoice.getTransactionId());
		invoces.setTransactionStatus(invoice.getTransactionStatus());
		return invoces;
	}

	public boolean revertEditedOrder(Invoice tempCheck, Order lastOrder) {
		if (lastOrder != null) {
			Order order = new Order();
			order.setBill(lastOrder.getBill());
			order.setCheckId(lastOrder.getCheckId());
			order.setCreatedTime(lastOrder.getCreatedTime());
			order.setDeliveryAgent(lastOrder.getDeliveryAgent());
			order.setDestinationId(lastOrder.getDestinationId());
			order.setMicroKitchenId(lastOrder.getMicroKitchenId());
			order.setModifiedTime(lastOrder.getModifiedTime());
			order.setMoneyIn(lastOrder.getMoneyIn());
			order.setMoneyOut(lastOrder.getMoneyOut());
			List<OrderDish> orderDishList = new ArrayList<OrderDish>();
			for (OrderDish orderDish : lastOrder.getOrderDishes()) {
				OrderDish orderD = new OrderDish();
				orderD.setDishId(orderDish.getDishId());
				orderD.setDishSize(orderDish.getDishSize());
				orderD.setDishType(orderDish.getDishType());
				orderD.setInstructions(orderDish.getInstructions());
				orderD.setName(orderDish.getName());
				List<OrderAddOn> orderAddOnList = new ArrayList<OrderAddOn>();
				for (OrderAddOn orderAdd : orderDish.getOrderAddOn()) {
					OrderAddOn orderAddOn = new OrderAddOn();
					orderAddOn.setDishId(orderAdd.getDishId());
					orderAddOn.setDishSize(orderAdd.getDishSize());
					orderAddOn.setDishType(orderAdd.getDishType());
					orderAddOn.setName(orderAdd.getName());
					orderAddOn.setOrderDishId(orderAdd.getOrderDishId());
					orderAddOn.setPrice(orderAdd.getPrice());
					orderAddOn.setQuantity(orderAdd.getQuantity());
					orderAddOn.setSmallImageUrl(orderAdd.getSmallImageUrl());
					orderAddOnList.add(orderAddOn);
				}
				orderD.setOrderAddOn(orderAddOnList);
				orderD.setPrice(orderDish.getPrice());
				orderD.setQuantity(orderDish.getQuantity());
				orderDishList.add(orderD);
			}
			order.setOrderDishes(orderDishList);
			order.setPaid(lastOrder.getPaid());
			order.setPaymentStatus(lastOrder.getPaymentStatus());
			order.setRestaurantId(lastOrder.getRestaurantId());
			order.setSourceId(lastOrder.getSourceId());
			order.setSourceType(lastOrder.getSourceType());
			order.setStatus(lastOrder.getStatus());
			order.setUserId(lastOrder.getUserId());

			tempCheck.setOrderId(order.getOrderId());
			List<Order> orderList = new ArrayList<>();
			orderList.add(order);
			tempCheck.setOrders(orderList);
			invoiceService.addInvoice(tempCheck);
			return true;
		}
		return false;
	}

	public ResponseDTO creditDebitAmountToAccount(Customer customer, Invoice check, Integer orgId,
			TransactionCategory transactionType, float amount, boolean saveCreditAmount) {
		Restaurant org = restService.getRestaurant(orgId);
		ResponseDTO responseDto = null;
		if (org.isEnableCustCredit()) {
			try {
				AddCreditToCustomerAccountDTO creditAddDTO = new AddCreditToCustomerAccountDTO();
				creditAddDTO.customerId = check.getCustomerId();
				creditAddDTO.invoiceId = check.getInvoiceId();
				creditAddDTO.amount = amount;
				creditAddDTO.orgId = orgId;
				creditAddDTO.remark = transactionType.toString() + " Amount from/to  "
						+ customer.getCredit().getCreditType().getName();
				creditAddDTO.transactionType = transactionType;
				responseDto = customerService.creatTransaction(creditAddDTO, orgId, check.getInvoiceNo());
				logger.info(" Transaction status : " + responseDto.result + " transaction message : "
						+ responseDto.message);
				if ("SUCCESS".equalsIgnoreCase(responseDto.result)) {
					if (customer.getCredit().getCreditType().getBillingCycle() == BilligCycle.ONE_OFF) {
						if (transactionType == TransactionCategory.DEBIT) {
							// Customer cust = customerService.getCustomer(customer.getCustomerId());
							if (customer.getCredit().getCreditBalance() < 0) {
								if (Math.abs(customer.getCredit().getCreditBalance()) <= check.getRoundOffTotal()) {
									// check.setCreditBalance(customer.getCredit().getCreditBalance());
								} else if (Math.abs(customer.getCredit().getCreditBalance()) > check
										.getRoundOffTotal()) {
									// check.setCreditBalance(-(float)check.getRoundOffTotal());
								}
							} else {
								if (saveCreditAmount) {
									// check.setCreditBalance(cust.getCredit().getCreditBalance());
								}
							}
						} else if (transactionType == TransactionCategory.CREDIT) {
							// Customer cust = customerService.getCustomer(customer.getCustomerId());
							// check.setCreditBalance(cust.getCredit().getCreditBalance());
						}
						// checkService.addCheck(check);
					}
				}

			} catch (Exception e) {
				logger.info("Exception arrived during  transaction");
				try {
					asyncService.emailException(ExceptionUtils.getStackTrace(e), REQUEST);
				} catch (UnsupportedEncodingException | MessagingException e1) {
					e1.printStackTrace();
				}
				logger.info("Exception mail sent");
			}
		}
		return responseDto;
	}

	private ResponseDTO updateCash(String transactionType, Float amount, Invoice check, HttpServletRequest request,
			boolean isCredit, String paymentType, String remark, String tillId, String updateToPaymentType)
			throws Exception {
		TillCashUpdateDTO updateDTO = new TillCashUpdateDTO();
		updateDTO.amount = amount;
		updateDTO.checkId = check.getInvoiceNo();
		updateDTO.paymentType = paymentType;
		// updateDTO.ffcId = check.getKitchenScreenId();
		updateDTO.remarks = remark;
		updateDTO.tillTransactionType = transactionType;
		updateDTO.updatedToPaymentType = "";
		updateDTO.tillId = tillId;
		updateDTO.updatedToPaymentType = updateToPaymentType;
		updateDTO.isCredit = isCredit;
		ResponseDTO response = null;
		synchronized (this) {
			response = cashRegisterController.updateCash(updateDTO, request);
			if (response.result.equals("Error"))
				try {
					throw new Exception(response.message);
				} catch (Exception e) {
					e.printStackTrace();
					// asyncService.emailException(ExceptionUtils.getStackTrace(e),request);
					logger.info("Exception: " + response.message);
					return response;
				}
		}
		return response;
	}

	public List<Coupon> validateCoupon(Invoice check, JsonOrder order, String paymentStatus) {
		List<Coupon> applied_couponList = new ArrayList<>();
		List<String> couponCodeList= order.getCouponCode();
		int courseId = order.getItems().get(0).getId();
		if(courseId<=0) {
			return null;
		}

		for (String code : couponCodeList) {
			JsonCouponInfo jsonCoupon = new JsonCouponInfo();
			// jsonCoupon.setCheckId(check.getCheckId());
			jsonCoupon.setCouponCode(code);
			jsonCoupon.setCustomerId(check.getCustomerId());
			jsonCoupon.setRestaurantID(check.getCountryId());
			jsonCoupon.setOrderAmount(check.getBill());
			jsonCoupon.setCourseId(courseId);

			if (paymentStatus != null) {
				if (check.getOrders().get(0) != null) {
					jsonCoupon.setPaymentMode(check.getOrders().get(0).getPaymentStatus());
				}
			}
			jsonCoupon.setOrderSource(check.getOrderSource());
			jsonCoupon.setOrderAmount(check.getRoundOffTotal());

			CouponResponse couponResponse = couponService.getCouponDef(jsonCoupon);
			logger.info(check.getRoundOffTotal() + "Amount and " + check.getInvoiceNo() + "/" + check.getInvoiceId()
					+ " checkId/InvoiceId  is getting error message : " + couponResponse.getError());
			if (couponResponse != null && couponResponse.getIsValid()) {
				if (couponResponse.isIsCouponApplicable()) {
					CouponFlatRules couponRules = couponResponse.getRules();
					if (check.getRoundOffTotal() >= couponRules.getMinOrderPayment()) {
						Coupon coupon = couponService.getEnabledCouponByCode(jsonCoupon.getCouponCode(),
								check.getCountryId());
						applied_couponList.add(coupon);
						/*
						 * if(couponRules.getIsForSelectedCustomer()){
						 * 
						 * }
						 */
						if (!couponRules.getIsUsedOncePerCustomer()) {
							// Coupon coupon =
							// couponService.getCouponByCode(jsonCoupon.getCouponCode(),check.getRestaurantId());
							// applied_couponList.add(coupon);

						}
					}
				}
			}
		}
		return applied_couponList;

	}

	private JsonOrder getJsonOrder(PlaceOrderDTO orderBody, Customer customer, Invoice invoice) {

		List<JsonDish> jsonItems = new ArrayList<>();
		for (OrderPlatterDTO dto : orderBody.order.items) {
			for (int i = 0; i < dto.quantity; i++) {
				JsonDish dish = new JsonDish();	
				if(dto.menuId!=null && dto.lms_course_id!=null) {
					System.out.println(dto.menuId);
					Canvas_Courses getDish =canvasLMSService.getCourseById(dto.menuId);
					System.out.println(getDish);
					if(getDish!=null) {
						dish.setId(dto.menuId);
						dish.setInstructions(dto.instructions);
						dish.setName(getDish.getName());
						dish.setPrice(getDish.getPrice());
						dish.setSection(dto.sections);
						dish.setAddOns(dto.addOn);
						invoice.setVendorId(getDish.getVendorId());
						jsonItems.add(dish);
					}
					
				}else {
					System.out.println("else");
					Platter getDish=vendorPlatterService.getPlatter(dto.menuId);
					if(getDish!=null) {
						dish.setId(dto.menuId);
						dish.setInstructions(dto.instructions);
						dish.setName(getDish.getName());
						dish.setPrice(getDish.getPrice());
						dish.setSection(dto.sections);
						dish.setAddOns(dto.addOn);
						invoice.setVendorId(getDish.getVendorId());
						jsonItems.add(dish);
					}
				}
				
			}
		}
		JsonOrder jsonOrder = new JsonOrder();
		jsonOrder.setInvoiceNo(invoice.getInvoiceNo());
		jsonOrder.setCustId(customer.getCustomerId());
		jsonOrder.setItems(jsonItems);
		invoiceService.addInvoice(invoice);
		return jsonOrder;
	}

	private Customer getCustomer(PlaceOrderDTO order) throws Exception {
		Customer customer = new Customer();
		customer.setAddress(order.customer.address);
		customer.setCity(order.customer.city);
		customer.setCustomerId(order.customer.id);
		customer.setDeliveryArea(order.customer.deliveryArea);
		customer.setDeliveryTime(order.order.deliveryDateTime);
		customer.setLatitude(order.customer.latitude);
		customer.setLongitude(order.customer.longitude);
		customer.setEmail(order.customer.email);
		List<String> names = new LinkedList<String>(Arrays.asList(order.customer.name.split(" ")));
		if (names.size() > 0) {
			customer.setFirstName(names.get(0));
			if (names.size() > 1) {
				names.remove(0);
				customer.setLastName("");
				for (String name : names)
					customer.setLastName(customer.getLastName() + " " + name);
				customer.setLastName(customer.getLastName().trim());
			}
		}
		//customer.setPhone(order.customer.phone.toString());
		return customer;
	}

	private Invoice getCheck(Integer custId, Integer countryId) {

		Invoice invoice = new Invoice();
		invoice.setCountryId(countryId);
		invoice.setOpenTime(new Date());
		invoice.setStatus(com.emanage.enums.check.Status.Unpaid);
		invoice.setCustomerId(custId);
	
		return invoice;
	}

	private List<Order_DCList> getDCValue(PlaceOrderDTO orderBody) {

		Order_DCList dcd = new Order_DCList();
		List<Order_DCList> dc = new ArrayList<Order_DCList>();
		if (orderBody.order.discountList != null) {
			for (DiscountDTO dto : orderBody.order.discountList) {
				DCJson dcV = new DCJson(dto);
				dcd.setCategory(dcV.getCategory());
				dcd.setDcId(dcV.getDcId());
				dcd.setName(dcV.getName());
				dcd.setType(dcV.getType());
				dcd.setValue(dcV.getValue());
				dc.add(dcd);
				dcd = new Order_DCList();
			}
		}
		return dc;
	}

	@RequestMapping(value = "/demoRegd", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody Map<String, Object> getDemoRegd(@RequestBody DemoRegDTO demoRegd, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			Customers customer = customerController.getCustomerInfoJSON(demoRegd.phone, null,
					demoRegd.countryId.toString(), request, response);
			if (customer != null && customer.getCustomers() != null && customer.getCustomers().size() > 0) {
				DemoRegd registration = new DemoRegd();
				Customer cust = customer.getCustomers().get(0);
				cust.setFirstName(demoRegd.fullName);
				cust.setEmail(demoRegd.email);

				registration.setCourseId(demoRegd.courseId);
				registration.setCustomerId(cust.getCustomerId());
				if(demoRegd.getDate()!=null) {
					registration.setDemoDate(demoRegd.getDate());
				}
				
				if(demoRegd.attendeesCount==null) {
					registration.setAttendeesCount(1);
				}else {
					registration.setAttendeesCount(demoRegd.attendeesCount);
				}
				registration.setVendorId(demoRegd.vendorId);
				demoRegdService.addDemoRegd(registration);
				asyncService.sendDemoEnrollmentEmail(request, demoRegd, cust);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
