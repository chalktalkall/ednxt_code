package com.emanage.utility;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by rahul on 1/10/2016.
 */
public class DateUtil {

	public static Date convertStringDateToGMTDate(String dateTimeString, String format, String inputDateTimeZone)
			throws ParseException {

		TimeZone inputTimeZone = TimeZone.getTimeZone(inputDateTimeZone);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(inputTimeZone);
		Date dateInInputTimeZone = sdf.parse(dateTimeString);

		Calendar cal = new GregorianCalendar(inputTimeZone);
		cal.setTime(dateInInputTimeZone);
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		return cal.getTime();
	}

	public static Long findDateDifference(String start_date, String end_date) {

		// SimpleDateFormat converts the
		// string format to date object
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Long difference_In_Days=null;
		// Try Block
		try {

			// parse method is used to parse
			// the text from a string to
			// produce the date
			Date d1 = sdf.parse(start_date);
			Date d2 = sdf.parse(end_date);

			// Calucalte time difference
			// in milliseconds
			long difference_In_Time = d2.getTime() - d1.getTime();

			// Calucalte time difference in
			// seconds, minutes, hours, years,
			// and days
//			long difference_In_Seconds = (difference_In_Time / 1000) % 60;
//
//			long difference_In_Minutes = (difference_In_Time / (1000 * 60)) % 60;
//
//			long difference_In_Hours = (difference_In_Time / (1000 * 60 * 60)) % 24;
//
//			long difference_In_Years = (difference_In_Time / (1000l * 60 * 60 * 24 * 365));

			difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;
		}

		// Catch the Exception
		catch (ParseException e) {
			e.printStackTrace();
		}
		return difference_In_Days;
	}

	public static Date convertDateToGMTDate(Date inputDate, String inputDateTimeZone) {
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(inputDateTimeZone));
		cal.setTime(inputDate);

		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		return cal.getTime();
	}

	public static Date getStartOfDay(Date date, String timeZone) {
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		return c.getTime();
	}

	public static Date getEndOfDay(Date date, String timeZone) {
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);

		return c.getTime();
	}

	public static Date nowInGMT() {
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		return cal.getTime();
	}

	public static Date nowInSpecifiedTimeZone(String timeZone) {
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		return cal.getTime();
	}

	public static Date yesterdaySpecifiedTimeZone(String timeZone) {
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		cal.add(cal.DATE, -1);
		return cal.getTime();
	}

	public static Date getLastDayDate(String timeZone) {
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		return cal.getTime();
	}

	public static Date addToDate(Date date, int numDays, int numMonths, int numYears, String timeZone) {
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, numDays);
		cal.add(Calendar.MONTH, numMonths);
		cal.add(Calendar.YEAR, numYears);
		return cal.getTime();
	}

	public static Date addToTime(Date date, int numHours, int numMins, int numSeconds, String timeZone) {
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, numHours);
		cal.add(Calendar.MINUTE, numMins);
		cal.add(Calendar.SECOND, numSeconds);
		return cal.getTime();
	}

	public static Timestamp getCurrentTimestampInGMT() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		return Timestamp.valueOf(fmt.format(timestamp));
	}

	public static Timestamp convertTimeInMilliSecToTimestampInGMT(long timeInMilliSec) {
		Timestamp timestamp = new Timestamp(timeInMilliSec);
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		return Timestamp.valueOf(fmt.format(timestamp));
	}

	public static Timestamp convertTimeInMilliSecToTimestampInIST(long timeInMilliSec) {
		Timestamp timestamp = new Timestamp(timeInMilliSec);
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		return Timestamp.valueOf(fmt.format(timestamp));
	}

	public static Date getMonthStartEndDate(String filter, String timeZone, String date, String format,
			boolean startEnd) throws ParseException {
		Calendar cal = GregorianCalendar.getInstance(TimeZone.getTimeZone(timeZone));
		if (!StringUtility.isNullOrEmpty(date) && !StringUtility.isNullOrEmpty(format)) {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
			cal.setTime(sdf.parse(date));
		} else
			startEnd = true;
		int dateCount = cal.getActualMinimum(Calendar.DATE);
		if ("END".equalsIgnoreCase(filter))
			dateCount = cal.getActualMaximum(Calendar.DATE);
		if (startEnd)
			cal.set(Calendar.DATE, dateCount);
		if ("END".equalsIgnoreCase(filter)) {
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
		} else {
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
		}
		cal.getTime();
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		return cal.getTime();
	}

	public static Timestamp convertDateStringIntoTimeStamp(String date, String timeZone) {
		Timestamp timestamp = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			Date parsedDate = dateFormat.parse(date);
			timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (Exception e) {
		}
		return timestamp;
	}

	public static String getFormatedDateTimeyyyyMMddhhmmss(Date date) {
		String format = "yyyy-MM-dd HH:mm";
		SimpleDateFormat formatterD = new SimpleDateFormat(format);
		String deliveryTimeD = formatterD.format(date);
		return deliveryTimeD;
	}

	public String getFormatedDateTimeyyyyMMdd(Date date) {
		String format = "yyyy-MM-dd";
		SimpleDateFormat formatterD = new SimpleDateFormat(format);
		String deliveryTimeD = formatterD.format(date);
		return deliveryTimeD;
	}

	public static Timestamp convertDateStringIntoTimeStampGMT(String date, String format, String timeZone) {
		Timestamp timestamp = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			Date parsedDate = dateFormat.parse(date);
			timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (Exception e) {
		}
		return convertTimeInMilliSecToTimestampInGMT(timestamp.getTime());
	}

	public static boolean compareDate(String date1, String date2) throws ParseException {
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = sdformat.parse(date1);
		Date d2 = sdformat.parse(date2);
		if (d1.compareTo(d2) > 0) {
			return true;
		} else if (d1.compareTo(d2) < 0) {
			return false;
		} else if (d1.compareTo(d2) == 0) {
			return true;
		}
		return false;
	}

}