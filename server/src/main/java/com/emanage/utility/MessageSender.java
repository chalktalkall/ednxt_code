package com.emanage.utility;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.emanage.eXoTel.ExOTel;
import com.emanage.eXoTel.SendSMS;


/**
 * @author rahul 
 *
 */
@SuppressWarnings("deprecation")
public class MessageSender {
	final static Logger logger = Logger.getLogger(MessageSender.class);

	public static Boolean sendMessage(String mobileNumber, String content, String priority){
		
		boolean isDelivered = false;
		SendSMS s  = new SendSMS();
		String jsoResponse=s.sendSms(mobileNumber,content);
		
		// Response from TextLocal
			/*"test_mode":true,
			 * "balance":9,
			 * "batch_id":99,
			 * "cost":1,"num_messages":1,
			 * "message":{"num_parts":1,"sender":"TXTLCL","content":"This is a test message being sent using Exotel with a (rahul) and (9540095277)"},
			 *  "receipt_url":"","custom":"","messages":[{"id":1,"recipient":919540095277}],
			 *  "status":"success"}
			
			*/		
		 
		try {
			JSONObject result = new JSONObject(jsoResponse);
			String status=   result.getString("status");
			System.out.println("status :"+status);
			if("SUCCESS".equalsIgnoreCase(status)) {
				isDelivered=true;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return isDelivered;
	}
	
	public static String sendTestMessage(String mobileNumber, String content, String priority){
		String responseString=null; 
		try {
			System.out.println("----------------------------");
			    @SuppressWarnings("resource")
				HttpClient client = new DefaultHttpClient();
	            HttpResponse response = client.execute(createPostRequest(getPostParameters(mobileNumber, content, priority)));
	            HttpEntity entity = (HttpEntity) response.getEntity();
	            if (entity != null) {
	            	responseString=EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
	             }
	            responseString=responseString+"\n\n Status on SMS sent and Response code="+response.getStatusLine().getStatusCode();
	            logger.info("Status on SMS sent to using exotel is "+entity+" and Response code="+response.getStatusLine().getStatusCode());
	        } catch (Exception e) {
	            logger.info("Could not send OTP. Exception="+e);
	            responseString="Could not send OTP. Exception="+e;
		 }
		return responseString;
	}

	private static Integer sendRequest(HttpPost post) {
		int httpStatusCode=400;
		System.out.println("iside");
		 try {
			    HttpClient client = new DefaultHttpClient();
	            HttpResponse response = client.execute(post);
	            System.out.println(response.getParams());
	            HttpEntity entity = (HttpEntity) response.getEntity();
//	            if (entity != null) {
//	                 String retSrc = EntityUtils.toString(entity); 
//	                 JSONObject result = new JSONObject(retSrc);
//	                 JSONArray tokenList = result.getJSONArray("names");
//	                 JSONObject oj = tokenList.getJSONObject(0);
//	                 String token = oj.getString("name"); 
//	             }
	            logger.info("Status on SMS sent to using exotel is "+entity.getContent());
	            httpStatusCode = response.getStatusLine().getStatusCode();
	            System.out.println("reason :"+response.getStatusLine().getReasonPhrase());
	        } catch (Exception e) {
			 e.printStackTrace();
		 }
		return httpStatusCode;	
	}
	
	private static HttpPost createPostRequest(ArrayList<NameValuePair> postParameters){
		ExOTel eXoTel=new ExOTel();
		
        HttpPost post = new HttpPost(eXoTel.getUrl());
        post.setHeader("Authorization", "Basic " + eXoTel.getAuthStringEnc());
        try {
            post.setEntity(new UrlEncodedFormEntity(postParameters));
            System.out.println(post.getParams());
            try {
				System.out.println(post.getEntity().getContent().toString());
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
		return post;
	}
	
	private static ArrayList<NameValuePair> getPostParameters(String mobileNumber, String content, String priority){
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("From", "+919540095277"));
        postParameters.add(new BasicNameValuePair("To", mobileNumber)); 
       postParameters.add(new BasicNameValuePair("Priority", priority));
        String out = null;
        String staMs="";
		try {
			 staMs="This is a test message being sent using Exotel with a (rahul) and (9540095277). If this is being abused, report to 08088919888";
			out = new String(staMs.getBytes("UTF-8"), "ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        postParameters.add(new BasicNameValuePair("Body", staMs));
		return postParameters;    
	}
}
