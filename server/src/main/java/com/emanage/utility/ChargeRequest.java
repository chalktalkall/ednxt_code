package com.emanage.utility;

public class ChargeRequest {

    public enum Currency {
        EUR, USD;
    }
    private String description;
    private long amount; // cents
    private String currency;
    private String stripeEmail;
	private String stripeToken;
	
    public void setAmount(long amount) {
		this.amount = amount;
	}
	public void setStripeEmail(String stripeEmail) {
		this.stripeEmail = stripeEmail;
	}
	public void setStripeToken(String stripeToken) {
		this.stripeToken = stripeToken;
	}
    
    public String getDescription() {
        return description;
    }
    public long getAmount() {
        return amount;
    }
    public String getCurrency() {
        return currency;
    }
    public String getStripeEmail() {
        return stripeEmail;
    }
    public String getStripeToken() {
        return stripeToken;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    
}