package com.emanage.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emanage.enums.Status;

@Entity
@Table(name="CANVAS_COURSES")
public class Canvas_Courses implements Serializable {
	
    private static final long serialVersionUID = 1L;
   
    @Id
	@Column(name="courseId")
	@GeneratedValue
    private Integer courseId;
   
	@Column(name="id")
    private Integer id;
    
    @Column(name="vendorId")
	private Integer vendorId;
    
    @Column(name="root_account_id")
    private Integer root_account_id;
    
    @Column(name="account_id")
    private Integer account_id;
    
    @Column(name="name")
    private String name;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="start_at")
    private Date start_at;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="end_at")
    private Date end_at;
    
    @Column(name="is_public")
    private boolean is_public;
    
    @Column(name="course_code")
    private String course_code;
    
    @Column(name="default_view")
    private String default_view;
    
    @Column(name="enrollment_term_id")
    private Integer enrollment_term_id;
    
    @Column(name="term")
    private String term; //json string
    
    @Column(name="time_zone")
    private String time_zone;
    
    @Column(name="syllabus_body")
    private String syllabus_body; // html string
    
    @Column(name="sections")
    private String sections; //json string
    
    @Column(name="image_download_url")
    private String image_download_url;
    
    @Column(name="public_description")
    private String public_description;
    
    @Column(name="course_format")
    private String course_format;
   
    @Column(name="price")
    private Float price;
    
    @Column(name="tagList")
    private String tagList;
    
    @Column(name="isFeatured")
    private boolean isFeatured;
    
    @Column(name="status")
    private Status status;
    
    
    @Column(name="days_offered")
    private String days_offered;	
    
    @Column(name="start_time")
    private String start_time;	
    
    
    @Column(name="end_time")
    private String end_time;	
    
    @Column(name="non_regular_times")
    private String non_regular_times;	
    
    @Column(name="session")
    private String session;	

    
    @Column(name="max_enrollement")
    private Integer max_enrollement;	
    
    
    @Column(name="total_enrollment")
    private Integer total_enrollment;	
    
    
    @Column(name="course_level")
    private String course_level;	
    
    @Column(name="instructor")
    private String instructor;
    
    @Column(name="campus")
    private String campus;	
    
    
    @Column(name="course_material")
    private String course_material;	
    
    @Column(name="credits")
    private Float credits;	
   
    @Column(name="course_dept")
    private String course_dept;	
   
    public String getCourse_dept() {
		return course_dept;
	}
	public void setCourse_dept(String course_dept) {
		this.course_dept = course_dept;
	}
	public String getDays_offered() {
		return days_offered;
	}
	public void setDays_offered(String days_offered) {
		this.days_offered = days_offered;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getNon_regular_times() {
		return non_regular_times;
	}
	public void setNon_regular_times(String non_regular_times) {
		this.non_regular_times = non_regular_times;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public Integer getMax_enrollement() {
		return max_enrollement;
	}
	public void setMax_enrollement(Integer max_enrollement) {
		this.max_enrollement = max_enrollement;
	}
	public Integer getTotal_enrollment() {
		return total_enrollment;
	}
	public void setTotal_enrollment(Integer total_enrollment) {
		this.total_enrollment = total_enrollment;
	}
	public String getCourse_level() {
		return course_level;
	}
	public void setCourse_level(String course_level) {
		this.course_level = course_level;
	}
	public String getInstructor() {
		return instructor;
	}
	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}
	public String getCampus() {
		return campus;
	}
	public void setCampus(String campus) {
		this.campus = campus;
	}
	public String getCourse_material() {
		return course_material;
	}
	public void setCourse_material(String course_material) {
		this.course_material = course_material;
	}
	public Float getCredits() {
		return credits;
	}
	public void setCredits(Float credits) {
		this.credits = credits;
	}
	public Status getStatus() {
		return status;
	}
	public Integer getCourseId() {
		return courseId;
	}
	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
    
//    public Status isStatus() {
//		return status;
//	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public boolean isFeatured() {
		return isFeatured;
	}
	public void setFeatured(boolean isFeatured) {
		this.isFeatured = isFeatured;
	}
	public String getTagList() {
		return tagList;
	}
	public void setTagList(String tagList) {
		this.tagList = tagList;
	}
	public String getCourse_format() {
		return course_format;
	}
	public void setCourse_format(String course_format) {
		this.course_format = course_format;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	public Integer getRoot_account_id() {
		return root_account_id;
	}
	public void setRoot_account_id(Integer root_account_id) {
		this.root_account_id = root_account_id;
	}
	public Integer getAccount_id() {
		return account_id;
	}
	public void setAccount_id(Integer account_id) {
		this.account_id = account_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStart_at() {
		return start_at;
	}
	public void setStart_at(Date start_at) {
		this.start_at = start_at;
	}
	public Date getEnd_at() {
		return end_at;
	}
	public void setEnd_at(Date end_at) {
		this.end_at = end_at;
	}
	public boolean isIs_public() {
		return is_public;
	}
	public void setIs_public(boolean is_public) {
		this.is_public = is_public;
	}
	public String getCourse_code() {
		return course_code;
	}
	public void setCourse_code(String course_code) {
		this.course_code = course_code;
	}
	public String getDefault_view() {
		return default_view;
	}
	public void setDefault_view(String default_view) {
		this.default_view = default_view;
	}
	public Integer getEnrollment_term_id() {
		return enrollment_term_id;
	}
	public void setEnrollment_term_id(Integer enrollment_term_id) {
		this.enrollment_term_id = enrollment_term_id;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public String getSyllabus_body() {
		return syllabus_body;
	}
	public void setSyllabus_body(String syllabus_body) {
		this.syllabus_body = syllabus_body;
	}
//	public String getEnrollments() {
//		return enrollments;
//	}
//	public void setEnrollments(String enrollments) {
//		this.enrollments = enrollments;
//	}
//	public String getCourse_progress() {
//		return course_progress;
//	}
//	public void setCourse_progress(String course_progress) {
//		this.course_progress = course_progress;
//	}
	public String getSections() {
		return sections;
	}
	public void setSections(String sections) {
		this.sections = sections;
	}
	public String getImage_download_url() {
		return image_download_url;
	}
	public void setImage_download_url(String image_download_url) {
		this.image_download_url = image_download_url;
	}
	public String getPublic_description() {
		return public_description;
	}
	public void setPublic_description(String public_description) {
		this.public_description = public_description;
	}

	
}
