/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.emanage.domain.JsonDish;
import com.emanage.enums.check.CheckType;

/**
 * @author rahul
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonOrder implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	public int invoiceNo;
	public int number;
	public int custId;
	public float price;
	public CheckType checkType;
 // Added just for delivery orders. as there is only one order per check
	public String deliveryAddress;
	public List<JsonDish> items;
	public List<String> couponCode;
	
	public List<String> getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(List<String> couponCode) {
		this.couponCode = couponCode;
	}
	public CheckType getCheckType() {
		return checkType;
	}
	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public List<JsonDish> getItems() {
		return items;
	}
	public void setItems(List<JsonDish> items) {
		this.items = items;
	}
	
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	
	public int getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(int invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
}
