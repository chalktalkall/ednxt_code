/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;

import com.emanage.utility.ImageUtility;


/**
 * @author Rahul
 *
 */
public class VendorDishWrapper implements Serializable {
    private static final long serialVersionUID = 1L;
	
	private Integer itemId;
	
	private Integer countryId;
	
	private Integer vendorId;
	
	private Integer dishId;

	private String name;
	
	private String description;
	
	private String shortDescription;
	
	private String rectangularImage;
	
	private String imageUrl;
	
	private String smallImageUrl;
	
	private Float price;

	private Float displayPrice;

	private String itemType;
	
	private String contents;

	private String quantityInG;

	private Boolean vegetarian;
	
	private Boolean alcoholic;

	private Boolean disabled;
	
	private Boolean isDishReplaceable;
	
	private  String [] replacementsList;
	
	private String courseDetialsURL;

	private boolean online;
	
	private Integer courseDuration;
	
	private Float courseDurationInHr;

	public Float getCourseDurationInHr() {
		return courseDurationInHr;
	}

	public void setCourseDurationInHr(Float courseDurationInHr) {
		this.courseDurationInHr = courseDurationInHr;
	}

	public Integer getCourseDuration() {
		return courseDuration;
	}

	public void setCourseDuration(Integer courseDuration) {
		this.courseDuration = courseDuration;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public String getCourseDetialsURL() {
		return courseDetialsURL;
	}

	public void setCourseDetialsURL(String courseDetialsURL) {
		this.courseDetialsURL = courseDetialsURL;
	}

	public String[] getReplacementsList() {
		return replacementsList;
	}

	public void setReplacementsList(String[] replacementsList) {
		this.replacementsList = replacementsList;
	}

	public Boolean getIsDishReplaceable() {
		return isDishReplaceable;
	}

	public void setIsDishReplaceable(Boolean isDishReplaceable) {
		this.isDishReplaceable = isDishReplaceable;
	}

	

	public static VendorDishWrapper getDishWrapper(SectionDishes dish,String timeZone) throws NullPointerException {
		VendorDishWrapper dishWrapper = new VendorDishWrapper();
		dishWrapper.setItemId(dish.getSectionDishId());
		dishWrapper.setCountryId(dish.getCountryId());
		dishWrapper.setVendorId(dish.getVendorId());
		dishWrapper.setIsDishReplaceable(dish.isReplaceable());
		dishWrapper.setName(dish.getName());
		dishWrapper.setDescription(dish.getDescription()==null?"":dish.getDescription().replaceAll("'", "&#39;"));
		dishWrapper.setShortDescription(dish.getShortDescription()==null?"":dish.getShortDescription().replaceAll("'", "&#39;"));
		dishWrapper.setImageUrl(dish.getImageUrl());
		dishWrapper.setSmallImageUrl(ImageUtility.getSmallImageUrl(dish.getImageUrl(), 200, 200));
		dishWrapper.setRectangularImage(dish.getRectangularImageUrl());
		dishWrapper.setContents(dish.getContents());
		dishWrapper.setQuantityInG(dish.getQuantityInG());
		dishWrapper.setItemType(dish.getDishType().getName());
		dishWrapper.setVegetarian(dish.getVegetarian());
		dishWrapper.setAlcoholic(dish.getAlcoholic());
		dishWrapper.setDisabled(dish.getDisabled());
		dishWrapper.setDishId(dish.getDishId());
		dishWrapper.setPrice(dish.getPrice());
		dishWrapper.setDisplayPrice(dish.getDisplayPrice());
		dishWrapper.setReplacementsList(dish.getReplacementList());
		dishWrapper.setCourseDetialsURL(dish.getCouseDetailsURL());
		dishWrapper.setOnline(dish.isOnline());
		dishWrapper.setCourseDuration(dish.getCourseDuration());
		dishWrapper.setCourseDurationInHr(dish.getCourseDurationInHr());
		return dishWrapper;
	}

	public String getContents() {
		return contents;
	}


	public void setContents(String contents) {
		this.contents = contents;
	}


	public String getQuantityInG() {
		return quantityInG;
	}


	public void setQuantityInG(String quantityInG) {
		this.quantityInG = quantityInG;
	}
	
	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public Float getDisplayPrice() {
		
		return displayPrice;
	}

	public void setDisplayPrice(Float displayPrice) {
		
		this.displayPrice = displayPrice;
	}

	public String getRectangularImage() {
		
		return rectangularImage;
	}

	public void setRectangularImage(String rectangularImage) {
		this.rectangularImage = rectangularImage;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public Boolean getVegetarian() {
		return vegetarian;
	}

	public void setVegetarian(Boolean vegetarian) {
		this.vegetarian = vegetarian;
	}

	public Boolean getAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(Boolean alcoholic) {
		this.alcoholic = alcoholic;
	}


	public String getSmallImageUrl() {
		return smallImageUrl;
	}


	public void setSmallImageUrl(String smallImageUrl) {
		this.smallImageUrl = smallImageUrl;
	}


	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	
	public Integer getDishId() {
		return dishId;
	}

	public void setDishId(Integer dishId) {
		this.dishId = dishId;
	}
}
