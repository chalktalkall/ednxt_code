/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author Rahul
 *
 */
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="COUNTRY_SECTIONTYPES")
public class CountrySectionType implements Serializable {
    private static final long serialVersionUID = 1L;

	@Id
	@Column(name="sectionTypeId")
	@GeneratedValue
	private Integer sectionTypeId;

	@Column(name="countryId")
	private Integer countryId;

	@Column(name="name")
	private String name;

	
	public Integer getSectionTypeId() {
		return sectionTypeId;
	}

	public void setSectionTypeId(Integer sectionTypeId) {
		this.sectionTypeId = sectionTypeId;
	}
	
	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
