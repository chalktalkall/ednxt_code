/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @author rahul
 *
 */
public class MainMenus implements Serializable {
    private static final long serialVersionUID = 1L;

	List<PlatterMenuWrapper> menus;
	Integer countryId;
//	Status status;
//	String portraitImageUrl;
//	String landscapeImageUrl;
//	String appCacheIconUrl;
//	String buttonIconUrl;
//	String currency;
	
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	
	public List<PlatterMenuWrapper> getMenus() {
		return menus;
	}
	public void setMenus(List<PlatterMenuWrapper> menus) {
		this.menus = menus;
	}
//	
//	public Status getStatus() {
//		return status;
//	}
//	public void setStatus(Status status) {
//		this.status = status;
//	}
//
//	public String getPortraitImageUrl() {
//		return portraitImageUrl;
//	}
//	public void setPortraitImageUrl(String portraitImageUrl) {
//		this.portraitImageUrl = portraitImageUrl;
//	}
//	public String getLandscapeImageUrl() {
//		return landscapeImageUrl;
//	}
//	public void setLandscapeImageUrl(String landscapeImageUrl) {
//		this.landscapeImageUrl = landscapeImageUrl;
//	}
//	public String getAppCacheIconUrl() {
//		return appCacheIconUrl;
//	}
//	public void setAppCacheIconUrl(String appCacheIconUrl) {
//		this.appCacheIconUrl = appCacheIconUrl;
//	}
//	public String getButtonIconUrl() {
//		return buttonIconUrl;
//	}
//	public void setButtonIconUrl(String buttonIconUrl) {
//		this.buttonIconUrl = buttonIconUrl;
//	}
//	public String getCurrency() {
//		return currency;
//	}
//	public void setCurrency(String currency) {
//		this.currency = currency;
//	}
}
