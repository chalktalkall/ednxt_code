package com.emanage.domain;

import java.io.Serializable;
import java.util.Date;

import com.emanage.enums.order.Status;

public class CheckSummery implements Serializable {
    private static final long serialVersionUID = 1L;
	
	private Date openTime;
	
	private Date pickupTime;

	private Integer invoiceNo;

	private String deliveryAddress;
	
	private double invoiceAmount;
	
	private Status status;
	
	private String pickUpBoy;
	
	private Double rewardPoints;
	
	
	public Date getPickupTime() {
		return pickupTime;
	}

	public void setPickupTime(Date pickupTime) {
		this.pickupTime = pickupTime;
	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getPickUpBoy() {
		return pickUpBoy;
	}

	public void setPickUpBoy(String pickUpBoy) {
		this.pickUpBoy = pickUpBoy;
	}


	public Date getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Double getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	public Integer getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(Integer invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
}
