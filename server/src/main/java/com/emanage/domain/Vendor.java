package com.emanage.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
/**
 * @author rahul
 *
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.emanage.config.TimeZoneConstants;
import com.emanage.enums.LMS_NAME;
import com.emanage.enums.Status;

/**
 * @author Rahul
 *
 */

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="VENDOR")
public class Vendor  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="vendorId")
	@GeneratedValue
	private Integer vendorId;
	
	@Column(name="vendorName")
	private String vendorName;
	
	@Column(name="businessName")
	private String businessName;

	@Column(name="businessContact")
	private String businessContact;
	
	@Column(name="secondaryContact")
	private String secondaryContact;
	
	@Column(name="vendorMargin")
	private Float vendorMargin;

	@Column(name="orderCancellationTime")
	private Integer orderCancellationTime;
	
	@Column(name="instituteImageURLFI")
	private String instituteImageURLFI;
	
	@Column(name="instituteImageURLFO")
	private String instituteImageURLFO;
	
	@Column(name="instituteImageURLT")
	private String instituteImageURLT;
	
	@Column(name="instituteImageURLS")
	private String instituteImageURLS;
	
	@Column(name="instituteImageURLF")
	private String instituteImageURLF;

	@Column(name="BUSINESSPORTRAITIMAGEURL")
	private String businessPortraitImageUrl;
	
	@Column(name="BUSINESSLANDSCAPEIMAGEURL")
	private String businessLandscapeImageUrl;
	
	@Column(name="STATUS")
	private Status status = Status.ACTIVE;
	
	@Column(name="isDeliveryActive")
	private Status isDeliveryActive = Status.ACTIVE;
	
	@Column(name="ADDRESS")
	private String address;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="COUNTRY")
	private String country;
	
	@Column(name="ZIP")
	private String zip;
	
	@Column(name="CURRENCY")
	private String currency;
	
	@Column(name="roundOffAmount")
	private boolean roundOffAmount=false;
	
	@Column(name="orgId")
	private Integer orgId;
	
	@Column(name="countryId")
	private Integer countryId;
	
	@Column(name="longitude")
	private Float longitude;
	
	@Column(name="latitude")
	private Float latitude;
	
	@Column(name="servingDistance")
	private Float servingDistance;
	
	@Column(name="description")
	private String description;

	@Column(name="introduction")
	private String introduction;

	@Column(name="certifiedTrainer")
	private boolean certifiedTrainer=false;

	@Column(name="certificate")
	private boolean certificate=false;
	
	@Column(name="assessmentKit")
	private boolean assessmentKit=false;
	
	@Column(name="ebook")
	private boolean ebook=false;

	@Column(name="placementAssistance")
	private boolean placementAssistance=false;
	
	@Column(name="metroStation")
	private boolean metroStation=false;
	
	@Column(name="rating")
	private Float rating;

	@Column
	private String alertMail;
	
	@Column(name="lms_key")
	private String lms_key;

	@Column(name="lms_name")
	private LMS_NAME lms_name;

	@Column(name="root_account_id")
	private Integer root_Account_id;

	@Transient
	private String timeZoneUnicode;
	
	@Transient
	private List<TaxType> taxList;
	
	@Transient
	private List<OrderSource> orderSource;
	
	@Transient
	private List<PaymentType> paymentType;
	
	public Integer getRoot_Account_id() {
		return root_Account_id;
	}

	public void setRoot_Account_id(Integer root_Account_id) {
		this.root_Account_id = root_Account_id;
	}
	
	public LMS_NAME getLms_name() {
		return lms_name;
	}

	public void setLms_name(LMS_NAME lms_name) {
		this.lms_name = lms_name;
	}
	
	public String getLms_key() {
		return lms_key;
	}

	public void setLms_key(String lms_key) {
		this.lms_key = lms_key;
	}
	
	public boolean isAssessmentKit() {
		return assessmentKit;
	}

	public void setAssessmentKit(boolean assessmentKit) {
		this.assessmentKit = assessmentKit;
	}

	public boolean isEbook() {
		return ebook;
	}

	public void setEbook(boolean ebook) {
		this.ebook = ebook;
	}
	
	public Float getVendorMargin() {
		return vendorMargin;
	}

	public void setVendorMargin(Float vendorMargin) {
		this.vendorMargin = vendorMargin;
	}
	
	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessContact() {
		return businessContact;
	}

	public void setBusinessContact(String businessContact) {
		this.businessContact = businessContact;
	}

	public String getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(String secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

//	public Integer getMinOrderQuantity() {
//		return minOrderQuantity;
//	}
//
//	public void setMinOrderQuantity(Integer minOrderQuantity) {
//		this.minOrderQuantity = minOrderQuantity;
//	}
//
//	public Integer getMaxPlatesCount() {
//		return maxPlatesCount;
//	}
//
//	public void setMaxPlatesCount(Integer maxPlatesCount) {
//		this.maxPlatesCount = maxPlatesCount;
//	}
//
//	public Integer getServingPeopleCount() {
//		return servingPeopleCount;
//	}
//
//	public void setServingPeopleCount(Integer servingPeopleCount) {
//		this.servingPeopleCount = servingPeopleCount;
//	}
//
//	public Integer getTotalCookingStaff() {
//		return totalCookingStaff;
//	}
//
//	public void setTotalCookingStaff(Integer totalCookingStaff) {
//		this.totalCookingStaff = totalCookingStaff;
//	}

//	public Integer getTimeForPrepartion() {
//		return timeForPrepartion;
//	}
//
//	public void setTimeForPrepartion(Integer timeForPrepartion) {
//		this.timeForPrepartion = timeForPrepartion;
//	}

	public Integer getOrderCancellationTime() {
		return orderCancellationTime;
	}

	public void setOrderCancellationTime(Integer orderCancellationTime) {
		this.orderCancellationTime = orderCancellationTime;
	}

	public String getBusinessPortraitImageUrl() {
		return businessPortraitImageUrl;
	}

	public void setBusinessPortraitImageUrl(String businessPortraitImageUrl) {
		this.businessPortraitImageUrl = businessPortraitImageUrl;
	}

	public String getBusinessLandscapeImageUrl() {
		return businessLandscapeImageUrl;
	}

	public void setBusinessLandscapeImageUrl(String businessLandscapeImageUrl) {
		this.businessLandscapeImageUrl = businessLandscapeImageUrl;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Status getIsDeliveryActive() {
		return isDeliveryActive;
	}

	public void setIsDeliveryActive(Status isDeliveryActive) {
		this.isDeliveryActive = isDeliveryActive;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public boolean isRoundOffAmount() {
		return roundOffAmount;
	}

	public void setRoundOffAmount(boolean roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}

	public String getAlertMail() {
		return alertMail;
	}

	public void setAlertMail(String alertMail) {
		this.alertMail = alertMail;
	}
	
	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getTimeZoneUnicode() {
		
		TimeZoneConstants tz = new TimeZoneConstants();
		 final Map<String,String> map = tz.currHexCode;
		timeZoneUnicode = map.get(getCurrency());
		return timeZoneUnicode;
	}

	public void setTimeZoneUnicode(String timeZoneUnicode) {
		this.timeZoneUnicode = timeZoneUnicode;
	}

	public List<TaxType> getTaxList() {
		return taxList;
	}

	public void setTaxList(List<TaxType> taxList) {
		this.taxList = taxList;
	}

	public List<OrderSource> getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(List<OrderSource> orderSource) {
		this.orderSource = orderSource;
	}

	public List<PaymentType> getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(List<PaymentType> paymentType) {
		this.paymentType = paymentType;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Float getServingDistance() {
		return servingDistance;
	}

	public void setServingDistance(Float servingDistance) {
		this.servingDistance = servingDistance;
	}
	public String getInstituteImageURLFI() {
		return instituteImageURLFI;
	}

	public void setInstituteImageURLFI(String instituteImageURLFI) {
		this.instituteImageURLFI = instituteImageURLFI;
	}

	public String getInstituteImageURLFO() {
		return instituteImageURLFO;
	}

	public void setInstituteImageURLFO(String instituteImageURLFO) {
		this.instituteImageURLFO = instituteImageURLFO;
	}

	public String getInstituteImageURLT() {
		return instituteImageURLT;
	}

	public void setInstituteImageURLT(String instituteImageURLT) {
		this.instituteImageURLT = instituteImageURLT;
	}

	public String getInstituteImageURLS() {
		return instituteImageURLS;
	}

	public void setInstituteImageURLS(String instituteImageURLS) {
		this.instituteImageURLS = instituteImageURLS;
	}

	public String getInstituteImageURLF() {
		return instituteImageURLF;
	}

	public void setInstituteImageURLF(String instituteImageURLF) {
		this.instituteImageURLF = instituteImageURLF;
	}
	

	public boolean isCertifiedTrainer() {
		return certifiedTrainer;
	}

	public void setCertifiedTrainer(boolean certifiedTrainer) {
		this.certifiedTrainer = certifiedTrainer;
	}

	public boolean isCertificate() {
		return certificate;
	}

	public void setCertificate(boolean certificate) {
		this.certificate = certificate;
	}

	public boolean isPlacementAssistance() {
		return placementAssistance;
	}

	public void setPlacementAssistance(boolean placementAssistance) {
		this.placementAssistance = placementAssistance;
	}

	public boolean isMetroStation() {
		return metroStation;
	}

	public void setMetroStation(boolean metroStation) {
		this.metroStation = metroStation;
	}
	
	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

}
