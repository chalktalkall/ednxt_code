package com.emanage.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author Rahul
 *
 */

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name = "VENDOR_DISHES")
public class VendorDish implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DISHID")
	@GeneratedValue
	private Integer dishId;

	@Column(name = "VENDORID")
	private Integer vendorId;

	@Column(name = "COUNTRYID")
	private Integer countryId;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "SHORTDESCRIPTION")
	private String shortDescription;

	@Column(name = "IMAGEURL")
	private String imageUrl;

	@Column(name = "rectangularImageUrl")
	private String rectangularImageUrl;

	@Column(name = "PRICE")
	private Float price;

	@Column(name = "DISPLAYPRICE")
	private Float displayPrice;

	@Column(name = "DISHTYPEID")
	private int dishTypeId;

	@Column(name = "CONTENTS")
	private String contents;

	@Column(name = "QUANTITYING")
	private String quantityInG;

	@Column(name = "VEGETARIAN")
	private boolean vegetarian;

	@Column(name = "ALCOHOLIC")
	private boolean alcoholic;

	@Column(name = "DISABLED")
	private boolean disabled;

	@Column(name = "RECOMMENDED")
	private boolean recommended;
	
	@Column(name="courseDetailsURL")
	private String courseDetailsURL;
	
	@Column(name="online")
	private boolean online;
	
	@Column(name="courseDuration")
	private Integer courseDuration=0;
	
	@Column(name="courseDurationInHr")
	private Float courseDurationInHr=0.0f;

	@JoinColumn(name = "dishTypeId", unique = true, insertable=false, updatable=false)
    @OneToOne(cascade = CascadeType.REFRESH)
	private CountryDishType dishType;
	
	public Float getCourseDurationInHr() {
		return courseDurationInHr;
	}

	public void setCourseDurationInHr(Float courseDurationInHr) {
		this.courseDurationInHr = courseDurationInHr;
	}

	public Integer getCourseDuration() {
		return courseDuration;
	}

	public void setCourseDuration(Integer courseDuration) {
		this.courseDuration = courseDuration;
	}
	
	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}
	
	public String getCourseDetailsURL() {
		return courseDetailsURL;
	}

	public void setCourseDetailsURL(String courseDetailsURL) {
		this.courseDetailsURL = courseDetailsURL;
	}

	
	public CountryDishType getDishType() {
		return dishType;
	}

	public void setDishType(CountryDishType dishType) {
		this.dishType = dishType;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getRectangularImageUrl() {
		return rectangularImageUrl;
	}

	public void setRectangularImageUrl(String rectangularImageUrl) {
		this.rectangularImageUrl = rectangularImageUrl;
	}

	public Integer getDishId() {
		return dishId;
	}

	public void setDishId(Integer dishId) {
		this.dishId = dishId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
	public boolean getVegetarian() {
		return vegetarian;
	}

	public void setVegetarian(boolean vegetarian) {
		this.vegetarian = vegetarian;
	}

	public boolean getAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	public boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Float getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(Float displayPrice) {
		this.displayPrice = displayPrice;
	}

	public boolean isRecommended() {
		return recommended;
	}

	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	
	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getQuantityInG() {
		return quantityInG;
	}

	public void setQuantityInG(String quantityInG) {
		this.quantityInG = quantityInG;
	} 
	

	public int getDishTypeId() {
		return dishTypeId;
	}

	public void setDishTypeId(int dishTypeId) {
		this.dishTypeId = dishTypeId;
	}
}
