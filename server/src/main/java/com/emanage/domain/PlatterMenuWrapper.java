/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.emanage.constants.FoodType;
import com.emanage.constants.PlatterType;
import com.emanage.enums.ClassMode;
import com.emanage.enums.Status;

/**
 * @author rahul
 *
 */
public class PlatterMenuWrapper implements Serializable {
    private static final long serialVersionUID = 1L;

	private Integer menuId;
	
	private Integer countryId;

	private String name;
	
	private String description;

	private Status status;
	
	private boolean posVisible;
	
	private String imageUrl;
	
	private String businessPotrateImageURL;
	
	private String businessDescription;
	
	private String businessIntro;

	private Double distance;

	private Float price;
	
	private Float displayprice;

	private FoodType foodType;

	private List<SectionWrapper> sections;
	
	private Integer vendorId;
	
	private List<String> instituteImages;
	
	private boolean certifiedTrainer=false;

	private boolean certificate=false;
	
	private boolean placementAssistance=false;
	
	private boolean metroStation=false;
	
	private String instituteName;
	
	private String validity;
	
	private String demoUrl;
	
	private String redirectURL;

	private ClassMode classMode;
	
	private Float vendorMargin;
	
	private Float rating;
	
	private boolean assessmentKit;
	
	private boolean ebook;
	
	private PlatterType type;

	
	public PlatterType getType() {
		return type;
	}

	public void setType(PlatterType type) {
		this.type = type;
	}

	public boolean isAssessmentKit() {
		return assessmentKit;
	}

	public void setAssessmentKit(boolean assessmentKit) {
		this.assessmentKit = assessmentKit;
	}

	public boolean isEbook() {
		return ebook;
	}

	public void setEbook(boolean ebook) {
		this.ebook = ebook;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}
	
	public String getDemoUrl() {
		return demoUrl;
	}

	public void setDemoUrl(String demoUrl) {
		this.demoUrl = demoUrl;
	}
	
	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	public Float getVendorMargin() {
		return vendorMargin;
	}

	public void setVendorMargin(Float vendorMargin) {
		this.vendorMargin = vendorMargin;
	}

	public ClassMode getClassMode() {
		return classMode;
	}

	public void setClassMode(ClassMode classMode) {
		this.classMode = classMode;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String date) {
		this.validity = date;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public boolean isCertifiedTrainer() {
		return certifiedTrainer;
	}

	public void setCertifiedTrainer(boolean certifiedTrainer) {
		this.certifiedTrainer = certifiedTrainer;
	}

	public boolean isCertificate() {
		return certificate;
	}

	public void setCertificate(boolean certificate) {
		this.certificate = certificate;
	}

	public boolean isPlacementAssistance() {
		return placementAssistance;
	}

	public void setPlacementAssistance(boolean placementAssistance) {
		this.placementAssistance = placementAssistance;
	}

	public boolean isMetroStation() {
		return metroStation;
	}

	public void setMetroStation(boolean metroStation) {
		this.metroStation = metroStation;
	}

	public  List<String> getInstituteImages() {
		return instituteImages;
	}

	public void setInstituteImages(List<String> instituteImages) {
		this.instituteImages = instituteImages;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	
	public String getBusinessIntro() {
		return businessIntro;
	}

	public void setBusinessIntro(String businessIntro) {
		this.businessIntro = businessIntro;
	}
	
	public static PlatterMenuWrapper getMenuWrapper(Platter menu,String timeZone ) {
		
		PlatterMenuWrapper menuWrapper = new PlatterMenuWrapper();
		menuWrapper.setMenuId(menu.getPlatterId());
		menuWrapper.setCountryId(menu.getCountryId());
		menuWrapper.setName(menu.getName());
		menuWrapper.setDescription(menu.getDescription()!=null?menu.getDescription().replaceAll("'", "&#39;"):"");
		menuWrapper.setStatus(menu.getStatus());
		menuWrapper.setImageUrl(menu.getImageUrl());
		menuWrapper.setPosVisible(menu.isPosVisible());
		menuWrapper.setPrice(menu.getPrice());
//		menuWrapper.setFoodType(menu.getFoodType());
		menuWrapper.setBusinessPotrateImageURL(menu.getBusinessPotrateImageURL());
		menuWrapper.setBusinessDescription(menu.getBusinessDescription());
		menuWrapper.setDistance(menu.getDistance());
		menuWrapper.setVendorId(menu.getVendorId());
		menuWrapper.setInstituteImages(menu.getInstituteImages());
		menuWrapper.setCertificate(menu.isCertificate());
		menuWrapper.setCertifiedTrainer(menu.isCertifiedTrainer());
		menuWrapper.setMetroStation(menu.isMetroStation());
		menuWrapper.setPlacementAssistance(menu.isPlacementAssistance());
		menuWrapper.setInstituteName(menu.getInstituteName());
		menuWrapper.setDisplayprice(menu.getDisplayPrice());
		menuWrapper.setBusinessIntro(menu.getBusinessIntro());
		menuWrapper.setValidity(menu.getValidity());
		menuWrapper.setClassMode(menu.getClassMode());
		menuWrapper.setVendorMargin(menu.getVendorMargin());
		menuWrapper.setRating(menu.getRating());
		menuWrapper.setDemoUrl(menu.getDemoUrl());
		menuWrapper.setRedirectURL(menu.getRedirectURL());
		menuWrapper.setAssessmentKit(menu.isAssessmentKit());
		menuWrapper.setEbook(menu.isEbook());
		menuWrapper.setType(menu.getPlatterType());
		List<Sections> existingSections = menu.getSections();
		if (existingSections != null) {
			menuWrapper.sections = new ArrayList<SectionWrapper>();
			for (Sections section : existingSections) {
				menuWrapper.sections.add(SectionWrapper.getSectionWrapper(section,timeZone));
			}
		}
		return menuWrapper;
	}
	
	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public Float getPrice() {
		return price;
	}
	
	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	
	public boolean isPosVisible() {
		return posVisible;
	}

	public void setPosVisible(boolean posVisible) {
		this.posVisible = posVisible;
	}
	
	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<SectionWrapper> getSections() {
		return sections;
	}

	public void setSections(List<SectionWrapper> sections) {
		this.sections = sections;
	}

	public FoodType getFoodType() {
		return foodType;
	}

	public void setFoodType(FoodType foodType) {
		this.foodType = foodType;
	}
	
	public String getBusinessPotrateImageURL() {
		return businessPotrateImageURL;
	}

	public void setBusinessPotrateImageURL(String businessPotrateImageURL) {
		this.businessPotrateImageURL = businessPotrateImageURL;
	}
	
	public String getBusinessDescription() {
		return businessDescription;
	}

	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}
	
	public Float getDisplayprice() {
		return displayprice;
	}

	public void setDisplayprice(Float displayprice) {
		this.displayprice = displayprice;
	}
}
