/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.emanage.constants.PlatterType;

/**
 * @author Rahul
 *
 */
public class SectionWrapper implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer sectionId;

	private String name;

	private String shortName;

	private String description;

	private Float price;

	private PlatterType platterSectionType;

	private List<VendorDishWrapper> items;

	public static long compareTo(java.util.Date date1, java.util.Date date2) {
		return date1.getTime() - date2.getTime();
	}

	public static SectionWrapper getSectionWrapper(Sections section, String timeZone) {
		SectionWrapper sectionWrapper = new SectionWrapper();
		sectionWrapper.setSectionId(section.getSectionId());
		sectionWrapper.setName(section.getName());
		sectionWrapper.setDescription(section.getDescription().replaceAll("'", "&#39;"));
		sectionWrapper.setPrice(section.getPrice());
		sectionWrapper.setPlatterSectionType(section.getPlatterSectionType());
		List<SectionDishes> dishes = section.getDishes();
		if (dishes != null) {
			sectionWrapper.items = new ArrayList<VendorDishWrapper>();
			for (SectionDishes dish : dishes) {
				if (dish != null && !dish.getDisabled()) {
					sectionWrapper.items.add(VendorDishWrapper.getDishWrapper(dish, timeZone));

				}
			}
		}
		return sectionWrapper;
	}

	public PlatterType getPlatterSectionType() {
		return platterSectionType;
	}

	public void setPlatterSectionType(PlatterType platterSectionType) {
		this.platterSectionType = platterSectionType;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public List<VendorDishWrapper> getItems() {
		return items;
	}

	public void setItems(List<VendorDishWrapper> items) {
		this.items = items;
	}

}
