/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * @author rahul
 * 
 */
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="DELIVERYAREAS")
public class DeliveryArea implements Serializable {
    private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@GeneratedValue
	private Integer id;
	
	@Column(name="restaurantId")
	private Integer restaurantId;
	
	@Column(name="name")
	private String name;

	
	@Column(name="City")
	private String City;
	
	@Column(name="State")
	private String State;
	
	@Column(name="Country")
	private String Country;
	
    
    @Column(name="deliveryTimeInterval")
    private int deliveryTimeInterval =15;
    
	@Column(name="minDeliveryTime")
    private int  minDeliveryTime = 15;

	
	
	@Transient
	@com.fasterxml.jackson.annotation.JsonRawValue
	private String todayTimeJson=null;
	
	@Transient
	@com.fasterxml.jackson.annotation.JsonRawValue
	private String tomorrowTimeJson=null;
	
	
	
	public String getTodayTimeJson() {
		return todayTimeJson;
	}

	public void setTodayTimeJson(String todayTimeJson) {
		this.todayTimeJson = todayTimeJson;
	}

	public String getTomorrowTimeJson() {
		return tomorrowTimeJson;
	}

	public void setTomorrowTimeJson(String tomorrowTimeJson) {
		this.tomorrowTimeJson = tomorrowTimeJson;
	}
	
	@JsonIgnore
	public int getDeliveryTimeInterval() {
		return deliveryTimeInterval;
	}

	
	public void setDeliveryTimeInterval(int deliveryTimeInterval) {
		this.deliveryTimeInterval = deliveryTimeInterval;
	}

	@JsonIgnore
	public int getMinDeliveryTime() {
		return minDeliveryTime;
	}

	public void setMinDeliveryTime(int minDeliveryTime) {
		this.minDeliveryTime = minDeliveryTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
