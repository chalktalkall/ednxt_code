/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;

/**
 * @author rahul
 *
 */
public class OrderResponse implements Serializable {
    private static final long serialVersionUID = 1L;
	
	int orderId;
	int invoiceNo;
	int countryId;	
	String error;
	String status;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(int invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
