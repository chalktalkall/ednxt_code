/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.util.List;

import com.emanage.dto.Order_SectionDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author rahul
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonDish implements Serializable {
    private static final long serialVersionUID = 1L;
    
	public int id;
	public String name;
	public float price;
	public int quantity;
	public String cuisineType;
    public List<Order_SectionDTO> section; 
	 
	 
	public List<Order_SectionDTO> getSection() {
		return section;
	}
	public void setSection(List<Order_SectionDTO> section) {
		this.section = section;
	}
	public String instructions;
	public List<JsonAddOn> addOns;
	
	public String getCuisineType() {
		return cuisineType;
	}
	public void setCuisineType(String cuisineType) {
		this.cuisineType = cuisineType;
	}
	public List<JsonAddOn> getAddOns() {
		return addOns;
	}
	public void setAddOns(List<JsonAddOn> addOns) {
		this.addOns = addOns;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getQuantity() {
		if (quantity == 0)
			return 1;
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions= instructions;
	}
	
	
}
