package com.emanage.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="EDIT_INVOICES")
public class EditInvoices {

	@Id
	@Column(name="id")
	@GeneratedValue
	Integer id;
	
	@Column(name="INVOICEJSON")
	String invoiceJson;

	@Column(name="INVOICENO")
	Integer InvoiceNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPENTIME")
	private Date openTime;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getOpenTime() {
		return openTime;
	}

	public Integer getInvoiceNo() {
		return InvoiceNo;
	}

	public void setInvoiceNo(Integer invoiceNo) {
		InvoiceNo = invoiceNo;
	}
	
	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}
	
	public String getInvoiceJson() {
		return invoiceJson;
	}

	public void setInvoiceJson(String invoiceJson) {
		this.invoiceJson = invoiceJson;
	}

	
	
}
