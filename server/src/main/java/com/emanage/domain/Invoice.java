/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.emanage.domain.Coupon;
import com.emanage.domain.Order;
import com.emanage.domain.Order_DCList;
import com.emanage.enums.check.CheckType;
import com.emanage.enums.check.PaymentMode;
import com.emanage.enums.check.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRawValue;

/**
 * @author rahul
 *
 */
 
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="INVOICES")
public class Invoice implements Serializable {
    private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID")
	@GeneratedValue
	private Integer invoiceNo;
	
	@Column(name="COUNTRYID")
	private Integer countryId;
	
	@Column(name="CUSTOMERID")
	private Integer customerId;

	@Column(name="PAYMENT")
	private PaymentMode paymentMode;
	
	@Column(name="STATUS")
	private Status status;
	
	@Column(name="CHECKTYPE")
	private CheckType invoiceType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPENTIME")
	private Date openTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CLOSETIME",nullable=true)
	private Date closeTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFIEDTIME",nullable=true)
	private Date modifiedTime = new Date();
	
	@Column(name="BILL")
	private float bill;
	
	@Column(name="responseCode")
	private String responseCode;

	@Column(name ="deliveryDateTime")
	private Date deliveryDateTime;
	
	@Column(name="deliveryAddress")
	private String deliveryAddress;

	@Column(name="transactionId")
	private String transactionId;
	
	@Column(name="transactionStatus")
	private String transactionStatus;
	
	@Column(name="invoiceId")
	private String invoiceId;
	
	@Column(name="orderId")
	private Integer orderId;
	
	@Column(name="screenId")
	private int screenId;

	@Column(name="roundOffTotal")
	private double roundOffTotal;
	
	@Column(name="taxJsonObject")
	private String taxJsonObject;
	
//	@Column(name="paymentType")
//	private String paymentType;

	@Column(name="orderSource")
	private String orderSource;
	
	@Column(name="deliveryInst")
	private String deliveryInst;
	
	@Column(name="rewards")
	private Double rewards;
	
	@Column(name="editOrderRemark")
	private String editOrderRemark;
	
	@Column(name="firstOrder")
	private boolean firstOrder;
	
	@Column(name="creditBalance")
	private float creditBalance;
	
	@Column(name="lastInvoiceAmount")
	private float lastInvoiceAmount;
	
	@Column(name="name")
	private String name;

	@Column(name="email")
	private String  email;
	
	@Column(name="vendorId")
	private Integer vendorId;
	
	@Column(name="razorPayId")
	private String razorPayId;
	
	@Column(name="tokenAmount")
	private Float tokenAmount;
	
	@Column(name="vendorMargin")
	private Float vedorMargin;
	
	@Column(name="classMode")
	private String classMode;
	
	@Column(name="courseSource")
	private String courseSource;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(orphanRemoval=true)
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.REMOVE})
	@JoinTable(name="CHECK_ORDER", 
	                joinColumns={@JoinColumn(name="CHECKID")},
	                inverseJoinColumns={@JoinColumn(name="ORDERID")})
	private List<Order> orders = new ArrayList<Order>();
	
	@OneToMany(fetch=FetchType.EAGER, orphanRemoval=true)
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.REMOVE})
	@JoinTable(name="CHECK_DCLIST", 
	                joinColumns={@JoinColumn(name="CHECKID")}, 
	                inverseJoinColumns={@JoinColumn(name="ID")})
	private List<Order_DCList> discount_Charge = new ArrayList<Order_DCList>();
	
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany()
	@Cascade({org.hibernate.annotations.CascadeType.REFRESH})	
	@JoinTable(name="CHECK_COUPON_LIST", 
	                joinColumns={@JoinColumn(name="CHECKID")}, 
	                inverseJoinColumns={@JoinColumn(name="COUPONID")})	
	@JsonIgnoreProperties("check_used")
	private List<Coupon> coupon_Applied = new ArrayList<Coupon>();
	
	public String getCourseSource() {
		return courseSource;
	}

	public void setCourseSource(String courseSource) {
		this.courseSource = courseSource;
	}
	
	public String getClassMode() {
		return classMode;
	}

	public void setClassMode(String classMode) {
		this.classMode = classMode;
	}
	
	public Float getTokenAmount() {
		return tokenAmount;
	}

	public void setTokenAmount(Float tokenAmount) {
		this.tokenAmount = tokenAmount;
	}

	public Float getVedorMargin() {
		return vedorMargin;
	}

	public void setVedorMargin(Float vedorMargin) {
		this.vedorMargin = vedorMargin;
	}
	
	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<Coupon> getCoupon_Applied() {
		return coupon_Applied;
	}

	public void setCoupon_Applied(List<Coupon> coupon_Applied) {
		this.coupon_Applied.clear();
		this.coupon_Applied.addAll(coupon_Applied);
		//this.coupon_Applied = coupon_Applied;
	}
	
	public boolean isFirstOrder() {
		return firstOrder;
	}

	public void setFirstOrder(boolean firstOrder) {
		this.firstOrder = firstOrder;
	}
	
	public String getEditOrderRemark() {
		return editOrderRemark;
	}

	public void setEditOrderRemark(String editOrderRemark) {
		this.editOrderRemark = editOrderRemark;
	}
	
	public Double getRewards() {
		return rewards;
	}

	public void setRewards(Double rewards) {
		this.rewards = rewards;
	}

	public String getDeliveryInst() {
		return deliveryInst;
	}

	public void setDeliveryInst(String deliveryInst) {
		this.deliveryInst = deliveryInst;
	}

	public String getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(String thirdParty) {
		this.orderSource = thirdParty;
	}
	
	
	public List<Order_DCList> getDiscount_Charge() {
		return discount_Charge;
	}

	public void setDiscount_Charge(List<Order_DCList> discount_Charge) {
		this.discount_Charge = discount_Charge;
	}


//	@JsonIgnore
//	public String getPaymentType() {
//		return paymentType;
//	}
//
//	public void setPaymentType(String paymentType) {
//		this.paymentType = paymentType;
//	}

	
	@JsonIgnore
	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	@JsonRawValue
	public String getTaxJsonObject() {
		return taxJsonObject;
	}

	@JsonRawValue
	public void setTaxJsonObject(String taxJsonObject) {
		this.taxJsonObject = taxJsonObject;
	}
	
	public double getRoundOffTotal() {
		return roundOffTotal;
	}

	public void setRoundOffTotal(double roundOffTotal) {
		this.roundOffTotal = roundOffTotal;
	}
	
	public Integer getOrderId() {
		
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		
		this.orderId = orderId;
	}

	

	public Integer getCustomerId() {
		return customerId == null ? -1: customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	public int getScreenId() {
		return screenId;
	}

	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}

//	@JsonIgnore
//	public PaymentMode getPayment() {
//		return payment;
//	}
//
//	public void setPayment(PaymentMode payment) {
//		this.payment = payment;
//	}

	public Status getStatus() {
		return status;
	}

	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public Date getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	public float getBill() {
		return bill;
	}

	public void setBill(float bill) {
		this.bill = bill;
	}

	public CheckType getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(CheckType checkType) {
		this.invoiceType = checkType;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Integer getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(Integer invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	public float getCreditBalance() {
		return creditBalance;
	}

	public void setCreditBalance(float creditBalance) {
		this.creditBalance = creditBalance;
	}
	
	public float getLastInvoiceAmount() {
		return lastInvoiceAmount;
	}

	public void setLastInvoiceAmount(float lastInvoiceAmount) {
		this.lastInvoiceAmount = lastInvoiceAmount;
	}
	
	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public Date getDeliveryDateTime() {
		return deliveryDateTime;
	}

	public void setDeliveryDateTime(Date deliveryDateTime) {
		this.deliveryDateTime = deliveryDateTime;
	}

	public String getRazorPayId() {
		return razorPayId;
	}

	public void setRazorPayId(String razorPayId) {
		this.razorPayId = razorPayId;
	}
}
