package com.emanage.domain;

import java.io.Serializable;

import javax.persistence.Column;
/**
 * @author rahul
 *
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Rahul
 *
 */

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="TUTOR_REQUEST")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TutorRequest  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Id")
	@GeneratedValue
	private Integer Id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="email")
	private String email;
	
	@Column(name="course")
	private String course;
	
	@Column(name="address")
	private String address;
	
	@Column(name="instruction")
	private String instruction;
	
//	@Column(name="longitude")
//	private Float longitude;
//	
//	@Column(name="latitude")
//	private Float latitude;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

//	public Float getLongitude() {
//		return longitude;
//	}
//
//	public void setLongitude(Float longitude) {
//		this.longitude = longitude;
//	}
//
//	public Float getLatitude() {
//		return latitude;
//	}
//
//	public void setLatitude(Float latitude) {
//		this.latitude = latitude;
//	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
	
	
