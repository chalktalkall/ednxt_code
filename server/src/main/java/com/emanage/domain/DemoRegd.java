package com.emanage.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="DEMO_REGD")
public class DemoRegd {
	
	@Id
	@GeneratedValue	
	@Column(name="id")
	private int id;
	
	@Column(name="customerId")
	private Integer customerId;
	
	@Column(name="courseId")
	private Integer courseId;
	
	@Column(name="demoDate")
	private Date demoDate;
	
	@Column(name="vendorId")
	private Integer vendorId;
	
	@Column(name="attendeesCount")
	private Integer attendeesCount;
	
	public Integer getAttendeesCount() {
		return attendeesCount;
	}
	public void setAttendeesCount(Integer attendeesCount) {
		this.attendeesCount = attendeesCount;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getCourseId() {
		return courseId;
	}
	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
	public Date getDemoDate() {
		return demoDate;
	}
	public void setDemoDate(Date demoDate) {
		this.demoDate = demoDate;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

}
