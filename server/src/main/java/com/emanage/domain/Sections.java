/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.IndexColumn;

import com.emanage.constants.PlatterType;

/**
 * @author Rahul
 *
 */
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="PLATTER_SECTIONS")
public class Sections implements Serializable {
    private static final long serialVersionUID = 1L;
    
	@Id
	@Column(name="SECTIONID")
	@GeneratedValue
	private Integer sectionId;
	
//	@Column(name="VENDORID")
//	private Integer vendorId;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="shortName")
	private String shortName;
	
	@Column(name="sectionTypeId")
	private Integer sectionTypeId;

	@JoinColumn(name = "sectionTypeId", insertable=false, updatable=false )
    @OneToOne(cascade = CascadeType.REFRESH,orphanRemoval=false)
	private CountrySectionType sectionType;

	@Column(name="DESCRIPTION")
	private String description;
	
//	@Column(name="HEADER")
//	private String header;
//	
//	@Column(name="FOOTER")
//	private String footer;
	
	@Column(name="PRICE")
	private Float price;
	
	@Column(name="platterType")
	private PlatterType platterSectionType;
	
	@Column(name="parentPlatterId")
	private Integer parentPlatterId;
	
	@Transient
	private boolean valid = true;
	
	@Transient
	private String dishIds = "";
	
	@Transient
	private int position;

	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@OneToMany(fetch=FetchType.EAGER,cascade = CascadeType.ALL, orphanRemoval = true)
	@IndexColumn(name="DISHPOSITION")
    @JoinTable(name="SECTION_DISH", 
                joinColumns={@JoinColumn(name="SECTIONID")}, 
                inverseJoinColumns={@JoinColumn(name="SECTIONDISHID")})
    private List<SectionDishes> dishes = new ArrayList<SectionDishes>();

	public CountrySectionType getSectionType() {
		return sectionType;
	}

	public void setSectionType(CountrySectionType sectionType) {
		this.sectionType = sectionType;
	}
	
	public PlatterType getPlatterSectionType() {
		return platterSectionType;
	}

	public void setPlatterSectionType(PlatterType platterSectionType) {
		this.platterSectionType = platterSectionType;
	}
	
	public Integer getParentPlatterId() {
		return parentPlatterId;
	}

	public void setParentPlatterId(Integer parentPlatterId) {
		this.parentPlatterId = parentPlatterId;
	}
	
	public Integer getSectionTypeId() {
		return sectionTypeId;
	}

	public void setSectionTypeId(Integer sectionTypeId) {
		this.sectionTypeId = sectionTypeId;
	}
	
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	public Integer getSectionId() {
		return sectionId;
	}

	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
//
//	public String getHeader() {
//		return header;
//	}
//
//	public void setHeader(String header) {
//		this.header = header;
//	}

//	public String getFooter() {
//		return footer;
//	}
//
//	public void setFooter(String footer) {
//		this.footer = footer;
//	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public List<SectionDishes> getDishes() {
		return dishes;
	}

	public void setDishes(ArrayList<SectionDishes> dishes) {
		this.dishes = dishes;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getDishIds() {
		return dishIds;
	}

	public void setDishIds(String dishIds) {
		this.dishIds = dishIds;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

//	public Integer getVendorId() {
//		return vendorId;
//	}
//
//	public void setVendorId(Integer vendorId) {
//		this.vendorId = vendorId;
//	}
	
}
