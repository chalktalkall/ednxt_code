package com.emanage.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.emanage.enums.DataSourceType;

@Entity
@Table(name="DB_CONFIG")
public class DBConfig implements Serializable {
    private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@GeneratedValue
	private int id;
	
	@Column(name="campusId")
	private Integer campusId;
	
	@Column(name="univId")
	private Integer univId;

	@Column(name="name")
	private String name;
	
	@Column(name="dbType")
	private DataSourceType dbType;
	
	@Column(name="dbUrl")
	private String dbUrl;
	
	@Column(name="dbPort")
	private Integer dbPort;

	@Column(name="username")
	private String username;

	@Column(name="dbView")
	private String dbView;
	
	@Column(name="dbInvoiceTable")
	private String dbInvoiceTable;
	
	@Column(name="dbPasssword")
	private String  password;
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getCampusId() {
		return campusId;
	}

	public void setCampusId(Integer campusId) {
		this.campusId = campusId;
	}

	public Integer getUnivId() {
		return univId;
	}

	public void setUnivId(Integer univId) {
		this.univId = univId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDbView() {
		return dbView;
	}

	public void setDbView(String dbView) {
		this.dbView = dbView;
	}

	public String getDbInvoiceTable() {
		return dbInvoiceTable;
	}

	public void setDbInvoiceTable(String dbInvoiceTable) {
		this.dbInvoiceTable = dbInvoiceTable;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DataSourceType getDbType() {
		return dbType;
	}

	public void setDbType(DataSourceType dbType) {
		this.dbType = dbType;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public Integer getDbPort() {
		return dbPort;
	}

	public void setDbPort(Integer dbPort) {
		this.dbPort = dbPort;
	}


}
