/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.emanage.enums.Status;
import com.emanage.enums.restaurant.ChargesType;

/**
 * @author Rahul	
 *
 */
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="TAXTYPES")
public class TaxType implements Serializable {
    private static final long serialVersionUID = 1L;
    
	@Id
	@Column(name="id")
	@GeneratedValue
	private Integer taxTypeId;
	
	@Column(name="restaurantId")
	private Integer restaurantId;
	
	@Column(name="name")
	private String name;

	@Column(name="chargeType")
	private ChargesType chargeType;

	@Column(name="taxValue")
	private Float taxValue;
	
	@Column(name="min_value")
	private Float minValue;
	
	@Column(name="max_value")
	private Float maxValue;

	@Column(name = "dishType")
	private String dishType="Default";
	
	@Column(name="overridden")
	private int overridden =1;
	
	@Column(name="STATUS")
	private Status status = Status.ACTIVE;
	
	public Float getMinValue() {
		return minValue;
	}

	public void setMinValue(Float minValue) {
		this.minValue = minValue;
	}

	public Float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Float maxValue) {
		this.maxValue = maxValue;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getOverridden() {
		return overridden;
	}

	public void setOverridden(int overridden) {
		this.overridden = overridden;
	}

	public ChargesType getChargeType() {
		return chargeType;
	}

	public void setChargeType(ChargesType chargeType) {
		this.chargeType = chargeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getTaxTypeId() {
		return taxTypeId;
	}

	public void setTaxTypeId(Integer taxTypeId) {
		this.taxTypeId = taxTypeId;
	}

	public Float getTaxValue() {
	
		return taxValue;
	}
	
	public double getTaxCharge(double bill, ChargesType chargeType, float additionalChargeValue) {
		double retVal = 0;
		if (chargeType == ChargesType.ABSOLUTE) {
			retVal = additionalChargeValue;
		} else if (chargeType == ChargesType.PERCENTAGE) {
			retVal = bill * additionalChargeValue / 100;
		}
		
		return retVal;
	}

	public void setTaxValue(Float taxValue) {
		this.taxValue = taxValue;
	}

	public String getDishType() {
		return dishType;
	}

	public void setDishType(String dishType) {
		this.dishType = dishType;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
}