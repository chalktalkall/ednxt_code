package com.emanage.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.emanage.enums.LMS_NAME;

@Entity
@Table(name = "USER_LMS")
public class User_LMS implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name="id")
	@GeneratedValue
	@Id
	private Integer id;

	@Column(name="lms_user_id")
	private Integer lms_user_id; //lms user_id

	@Column(name="customer_id")
	private Integer customer_id;

	@Column(name="lms")
	private LMS_NAME lms;

	@Column(name="root_account_id")
	private Integer root_account_id;
	
	public Integer getRoot_account_id() {
		return root_account_id;
	}

	public void setRoot_account_id(Integer root_account_id) {
		this.root_account_id = root_account_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	

	public LMS_NAME getLms() {
		return lms;
	}

	public void setLms(LMS_NAME lms) {
		this.lms = lms;
	}

	public Integer getLms_user_id() {
		return lms_user_id;
	}

	public void setLms_user_id(Integer lms_user_id) {
		this.lms_user_id = lms_user_id;
	}
	
	public Integer getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}


}
