/**
 * 
 */
package com.emanage.domain;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.IndexColumn;

import com.emanage.constants.PlatterType;
import com.emanage.enums.ClassMode;
import com.emanage.enums.Status;

/**
 * @author rahul
 *
 */
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="PLATTERS")
public class Platter implements Serializable {
    private static final long serialVersionUID = 1L;
	@Id
	@Column(name="PLATTERID")
	@GeneratedValue
	private Integer platterId;
	
	@Column(name="COUNTRYID")
	private Integer countryId;
	
	@Column(name="VENDORID")
	private Integer vendorId;

	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="DEMO_URL")
	private 	String demoUrl;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFIEDTIME")
	private Date modifiedTime;
	
	@Column(name="PRICE")
	private Float price=0.0f;
	
	@Column(name="displayPrice")
	private Float displayPrice=0.0f;

	@Column(name="STATUS")
	private Status status = Status.ACTIVE;
	
	@Column(name="posVisible")
	private boolean posVisible=false;
	
	@Column(name="IMAGEURL")
	private String imageUrl;	 
	
	@Column(name="CUISINETYPEID")
	private  Integer cuisineTypeId;
	
	@Column(name="MAXAVAIL")
	private Integer maxAvail;

	@Column(name="MINAVAIL")
	private Integer minAvail;

	@Column(name="timeForPrepartion")
	private Float timeForPrepartion;
	
	@Column(name="classMode")
	private ClassMode classMode;

	@JoinColumn(name = "cuisineTypeId", insertable=false, updatable=false)
	@OneToOne(cascade = CascadeType.REFRESH,orphanRemoval=false)
	private CuisineType cuisineType;
	
	@Column(name="validity")
	private String validity;
	
	@Column(name="vendorMargin")
	private Float vendorMargin;
	
	@Column(name="featureCourse")
	private boolean featureCourse; 
	
	@Column(name="redirectURL")
	private String redirectURL;

	@Column(name="platterType")
	private PlatterType platterType =PlatterType.COURSE;

	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, })
	@OneToMany(fetch=FetchType.EAGER, orphanRemoval=true, cascade=CascadeType.ALL)
	@IndexColumn(name="SECTIONPOSITION")
    @JoinTable(name="PLATTER_SECTION", 
                joinColumns={@JoinColumn(name="PLATTERID")}, 
                inverseJoinColumns={@JoinColumn(name="SECTIONID")})
    private List<Sections> sections = new ArrayList<Sections>();

	@Column(name="tagList")
	private String tagList;
	

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}
	
	public boolean isFeatureCourse() {
		return featureCourse;
	}

	public void setFeatureCourse(boolean featureCourse) {
		this.featureCourse = featureCourse;
	}

	public String getDemoUrl() {
		return demoUrl;
	}

	public void setDemoUrl(String demoUrl) {
		this.demoUrl = demoUrl;
	}
	
	public Float getVendorMargin() {
		return vendorMargin;
	}

	public void setVendorMargin(Float vendorMargin) {
		this.vendorMargin = vendorMargin;
	}
	
	public String getValidity() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String sDate=null;
		try {
			if(this.validity!=null && this.validity.trim().length()>1){
			java.util.Date date = formatter.parse(this.validity);
			sDate=formatter.format(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return sDate;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public ClassMode getClassMode() {
		return classMode;
	}

	public void setClassMode(ClassMode classMode) {
		this.classMode = classMode;
	}
	
	public String getTagList() {
		return tagList;
	}

	public void setTagList(String tagList) {
		this.tagList = tagList;
	}

	@Transient
	private String businessPotrateImageURL;

	@Transient
	private String businessDescription;
	
	@Transient
	private String businessIntro;

	@Transient
	private Double distance;
	
	@Transient
	private List<String> instituteImages;
	
	@Transient
	private boolean certifiedTrainer=false;

	@Transient
	private boolean certificate=false;
	
	@Transient
	private boolean placementAssistance=false;
	
	@Transient
	private boolean metroStation=false;
	
	@Transient
	private String instituteName; 
	
	@Transient
	private Float rating;
	
	@Transient
	private boolean assessmentKit;
	
	@Transient
	private boolean ebook;;

	
	public boolean isAssessmentKit() {
		return assessmentKit;
	}

	public void setAssessmentKit(boolean assessmentKit) {
		this.assessmentKit = assessmentKit;
	}

	public boolean isEbook() {
		return ebook;
	}

	public void setEbook(boolean ebook) {
		this.ebook = ebook;
	}

	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	public String getBusinessIntro() {
		return businessIntro;
	}

	public void setBusinessIntro(String businessIntro) {
		this.businessIntro = businessIntro;
	}
	
	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public boolean isCertifiedTrainer() {
		return certifiedTrainer;
	}

	public void setCertifiedTrainer(boolean certifiedTrainer) {
		this.certifiedTrainer = certifiedTrainer;
	}

	public boolean isCertificate() {
		return certificate;
	}

	public void setCertificate(boolean certificate) {
		this.certificate = certificate;
	}

	public boolean isPlacementAssistance() {
		return placementAssistance;
	}

	public void setPlacementAssistance(boolean placementAssistance) {
		this.placementAssistance = placementAssistance;
	}

	public boolean isMetroStation() {
		return metroStation;
	}

	public void setMetroStation(boolean metroStation) {
		this.metroStation = metroStation;
	}

	public  List<String> getInstituteImages() {
		return instituteImages;
	}

	public void setInstituteImages(List<String> instituteImages) {
		this.instituteImages = instituteImages;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getBusinessDescription() {
		return businessDescription;
	}

	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}

	public String getBusinessPotrateImageURL() {
		return businessPotrateImageURL;
	}

	public void setBusinessPotrateImageURL(String businessPotrateImageURL) {
		this.businessPotrateImageURL = businessPotrateImageURL;
	}

	public Integer getCuisineTypeId() {
		return cuisineTypeId;
	}

	public void setCuisineTypeId(Integer cuisineTypeId) {
		this.cuisineTypeId = cuisineTypeId;
	}
	
	public CuisineType getCuisineType() {
		return cuisineType;
	}

	public void setCuisineType(CuisineType cuisineType) {
		this.cuisineType = cuisineType;
	}
	
	public boolean isPosVisible() {
		return posVisible;
	}

	public void setPosVisible(boolean posVisible) {
		this.posVisible = posVisible;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public List<Sections> getSections() {
		return sections;
	}

	public void setSections(ArrayList<Sections> sections) {
		this.sections = sections;
	}

	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getPlatterId() {
		return platterId;
	}

	public void setPlatterId(Integer platterId) {
		this.platterId = platterId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public void setSections(List<Sections> sections) {
		this.sections = sections;
	}
	
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
//	public FoodType getFoodType() {
//		return foodType;
//	}
//
//	public void setFoodType(FoodType foodType) {
//		this.foodType = foodType;
//	}
	public Integer getMaxAvail() {
		return maxAvail;
	}

	public void setMaxAvail(Integer maxAvail) {
		this.maxAvail = maxAvail;
	}

	public Integer getMinAvail() {
		return minAvail;
	}

	public void setMinAvail(Integer minAvail) {
		this.minAvail = minAvail;
	}
	
	public Float getTimeForPrepartion() {
		return timeForPrepartion;
	}

	public void setTimeForPrepartion(Float timeForPrepartion) {
		this.timeForPrepartion = timeForPrepartion;
	}

	public Float getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(Float displayPrice) {
		this.displayPrice = displayPrice;
	}
	
	public PlatterType getPlatterType() {
		return platterType;
	}

	public void setPlatterType(PlatterType platterType) {
		this.platterType = platterType;
	}
}
