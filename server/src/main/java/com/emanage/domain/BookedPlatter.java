package com.emanage.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name="BOOKED_PLATTER")
public class BookedPlatter {

	@Id
	@Column(name="id")
	@GeneratedValue
	Integer id;
	
	@Column(name="vendorId")
	private Integer vendorId;
	
	@Column(name="platterId")
	private Integer platterId;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="count")
	private Integer count;
	 
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	public Integer getPlatterId() {
		return platterId;
	}
	public void setPlatterId(Integer platterId) {
		this.platterId = platterId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
