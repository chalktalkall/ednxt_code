package com.emanage.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Entity
@Table(name="USER_ENROLLMENT")
public class User_Enrollment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="enrollemntId")
	@GeneratedValue
	private Integer enrollemntId; // db auto generated
	
	@Column(name="id")
	private Integer id; // lms id
	
	@Column(name="course_id")
	private Integer course_id;
	
	@Column(name="customer_id")
	private Integer customer_id; // application userId
	
	@Column(name="type")
	private String type;
	
	@Column(name="self_enrolled")
	private boolean self_enrolled;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date created_at;
	
	@Column(name="enrollmennt_state")
	private String enrollmennt_state;
	
	@Column(name="time_zone")
	private String time_zone;
	
	public String getTime_zone() {
		return time_zone;
	}
	
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCourse_id() {
		return course_id;
	}
	public void setCourse_id(Integer course_id) {
		this.course_id = course_id;
	}
//	public Integer getUser_id() {
//		return user_id;
//	}
//	public void setUser_id(Integer user_id) {
//		this.user_id = user_id;
//	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isSelf_enrolled() {
		return self_enrolled;
	}
	public void setSelf_enrolled(boolean self_enrolled) {
		this.self_enrolled = self_enrolled;
	}
	public String getEnrollmennt_state() {
		return enrollmennt_state;
	}
	public void setEnrollmennt_state(String enrollmennt_state) {
		this.enrollmennt_state = enrollmennt_state;
	}
}
