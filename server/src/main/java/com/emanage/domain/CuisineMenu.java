package com.emanage.domain;

import java.io.Serializable;
import java.util.List;

import com.emanage.dto.CourseWrapper;

public class CuisineMenu  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String  name;
	
	private List<PlatterMenuWrapper> menus;
	
	private List<CourseWrapper> courses;
	
	private Integer countryId;
	
	
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public List<CourseWrapper> getCourses() {
		return courses;
	}
	public void setCourses(List<CourseWrapper> courses) {
		this.courses = courses;
	}
	public List<PlatterMenuWrapper> getMenus() {
		return menus;
	}
	public void setMenus(List<PlatterMenuWrapper> menus) {
		this.menus = menus;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
