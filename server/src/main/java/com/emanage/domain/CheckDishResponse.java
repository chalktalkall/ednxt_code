/**
 * 
 */
package com.emanage.domain;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.emanage.dto.Order_SectionDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author rahul
 *
 */
public class CheckDishResponse implements Serializable {
    private static final long serialVersionUID = 1L;

	private Integer dishId;
	private String name;
	private float price;
	private String dishType;
	private String dishSize;
	private Order_SectionDTO[] sections;

	List<OrderAddOn> addOnresponse =  new ArrayList<OrderAddOn>(); 
	
	public List<OrderAddOn> getAddOnresponse() {
		return addOnresponse;
	}

	public void setAddOnresponse(List<OrderAddOn> addOnresponse) {
		this.addOnresponse = addOnresponse;
	}

	public Order_SectionDTO[] getSections() {
		
		return sections;
	}

	public void setSections(String section) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			if(section !=null) {
			 this.sections = mapper.readValue(section,Order_SectionDTO[].class);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.sections = sections;
	}
	
	public CheckDishResponse(OrderDish orderDish) {
		this.dishId = orderDish.getDishId();
		this.name = orderDish.getName();
		this.price = orderDish.getPrice();
		this.dishType = orderDish.getDishType();
		this.addOnresponse = orderDish.getOrderAddOn();
		this.dishSize = orderDish.getDishSize();
		this.setSections(orderDish.getSectionsJson());
	}
	
	public String getDishSize() {
		return dishSize;
	}

	public void setDishSize(String dishSize) {
		this.dishSize = dishSize;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public Integer getDishId() {
		return dishId;
	}
	public void setDishId(Integer dishId) {
		this.dishId = dishId;
	}
	public String getDishType() {
		return dishType;
	}
	public void setDishType(String dishType) {
		this.dishType = dishType;
	}
	
}
