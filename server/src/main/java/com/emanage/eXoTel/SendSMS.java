package com.emanage.eXoTel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
 
public class SendSMS {
	
	public String sendSms(String phone,String msz) {
		try {
			// Construct data text local
			String apiKey = "apikey=" + "2ik9xwuUKxo-pSXlsQKKOv4DeQ4hDwQoOsyoFKDWsj";
			String message = "&message=" + msz;
			String sender = "&sender=" + "LTCHTK";
			String numbers = "&numbers=" + phone;
			String test="&test="+ "false";
			
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
			String data = apiKey + numbers + message + sender + test;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			return stringBuffer.toString();
		} catch (Exception e) {
			System.out.println("Error SMS "+e);
			return "Error "+e;
		}
	}
}