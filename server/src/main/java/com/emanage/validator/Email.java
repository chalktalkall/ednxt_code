package com.emanage.validator;

public @interface Email {
	String message() default "{Email }";
}
