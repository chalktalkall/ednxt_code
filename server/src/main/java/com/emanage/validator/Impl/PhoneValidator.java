package com.emanage.validator.Impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.emanage.utility.DataValidator;
import com.emanage.validator.Phone;

public class PhoneValidator implements ConstraintValidator<Phone, String> {
	 
    @Override
    public void initialize(Phone phone) { }
 
    @Override
    public boolean isValid(String phoneNo, ConstraintValidatorContext cxt) {
    	if(phoneNo == null) {
            return false;
        }
        return DataValidator.isValidMobileNo(phoneNo);
    }
 
}
