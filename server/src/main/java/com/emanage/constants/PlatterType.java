package com.emanage.constants;

public enum PlatterType {
	MAIN_PLATTER, ADD_ON, COURSE, WEBINAR, ON_DEMAND
}
