package com.emanage.analyticsAndReport;
/*
package com.path.report;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

*/
/**
 * Created by rahul on 2/14/2017.
 * <p>
 * generate list of reports
 *
 * @return Generate report thumbnail
 * @param response
 * @param request
 * @param name
 * @throws EngineException
 * @throws IOException
 * <p>
 * Generate full report
 * @param response
 * @param request
 * @param name
 * @throws EngineException
 * @throws IOException
 *//*


@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    */
/**
 * generate list of reports
 *
 * @return
 *//*

    @RequestMapping(produces = "application/json", method = RequestMethod.GET, value = "/report")
    @ResponseBody
    public List<Report> listReports() {
        return reportService.getReports();
    }

    */
/**
 * Generate report thumbnail
 *
 * @param response
 * @param request
 * @param name
 * @throws EngineException
 * @throws IOException
 *//*

    @RequestMapping(method = RequestMethod.GET, value = "/report/thumb/{name}")
    @ResponseBody
    public void generateReportThumb(HttpServletResponse response, HttpServletRequest request,
                                    @PathVariable("name") String name) throws Exception {
        reportService.generateReportThumb(name, response, request);
    }

    */
/**
 * Generate full report
 *
 * @param response
 * @param request
 * @param name
 * @throws EngineException
 * @throws IOException
 *//*

    @RequestMapping(method = RequestMethod.POST, value = "/report/main/{name}")
    @ResponseBody
    public void generateFullReport(HttpServletResponse response, HttpServletRequest request,
                                   @PathVariable("name") String name) throws Exception {
        reportService.generateMainReport(name, response, request);
    }

}
*/
