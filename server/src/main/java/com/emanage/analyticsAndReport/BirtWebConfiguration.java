
package com.emanage.analyticsAndReport;

public class BirtWebConfiguration {
}
/*
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.BeanNameViewResolver;

*/
/**
 * Created by rahul on 2/19/2017.
 *//*

@EnableWebMvc
@ComponentScan( {"com.emanage.analyticsAndReport"})
@Configuration
public class BirtWebConfiguration  extends WebMvcConfigurerAdapter {

    final static Logger logger = Logger.getLogger(BirtWebConfiguration.class);


    //@Autowired private CarService carService ;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/reports").setViewName("birtView");
        logger.info("addViewControllers..");

    }

    @Bean
    public BirtView birtView(){
        BirtView bv = new BirtView();
        //bv.setReportFormatRequestParameter("ReportFormat");
        //bv.setReportNameRequestParameter("ReportName");
        bv.setBirtEngine( this.engine().getObject() );
        logger.info("Set BirtViewBean..");
        return bv;
    }


    @Bean
    public BeanNameViewResolver beanNameResolver(){
        BeanNameViewResolver br = new BeanNameViewResolver() ;
        logger.info("Set BeanNameViewResolver..");
        return br;
    }

    @Bean
    protected BirtEngineFactory engine(){
        BirtEngineFactory factory = new BirtEngineFactory() ;
        //factory.setLogLevel( Level.FINEST);
        //factory.setLogDirectory ( new File ("c:/logs"));
        //factory.setLogDirectory( new FileSystemResource("/logs"));
        logger.info("get BirtEngineFactory..");
        return factory ;
    }


}
*/
