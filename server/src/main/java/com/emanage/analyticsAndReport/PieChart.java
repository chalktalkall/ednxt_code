package com.emanage.analyticsAndReport;

import java.io.IOException;

import org.eclipse.birt.chart.examples.radar.model.type.RadarSeries;
import org.eclipse.birt.chart.examples.radar.model.type.impl.RadarSeriesImpl;
import org.eclipse.birt.chart.model.ChartWithoutAxes;
import org.eclipse.birt.chart.model.attribute.ChartDimension;
import org.eclipse.birt.chart.model.attribute.LineAttributes;
import org.eclipse.birt.chart.model.attribute.LineStyle;
import org.eclipse.birt.chart.model.attribute.impl.ColorDefinitionImpl;
import org.eclipse.birt.chart.model.attribute.impl.LineAttributesImpl;
import org.eclipse.birt.chart.model.component.Series;
import org.eclipse.birt.chart.model.component.impl.SeriesImpl;
import org.eclipse.birt.chart.model.data.SeriesDefinition;
import org.eclipse.birt.chart.model.data.impl.SeriesDefinitionImpl;
import org.eclipse.birt.chart.model.impl.ChartWithoutAxesImpl;
import org.eclipse.birt.chart.model.layout.Legend;
import org.eclipse.birt.chart.model.layout.Plot;
import org.eclipse.birt.chart.reportitem.ChartReportItemImpl;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.DesignEngine;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.ExtendedItemHandle;
import org.eclipse.birt.report.model.api.PropertyHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.ScriptDataSetHandle;
import org.eclipse.birt.report.model.api.ScriptDataSourceHandle;
import org.eclipse.birt.report.model.api.SessionHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.command.ContentException;
import org.eclipse.birt.report.model.api.command.NameException;
import org.eclipse.birt.report.model.api.elements.structures.ColumnHint;
import org.eclipse.birt.report.model.api.elements.structures.ResultSetColumn;
import org.eclipse.birt.report.model.api.extension.ExtendedElementException;
import org.eclipse.birt.report.model.metadata.MetaDataDictionary;

/**
 * Created by rahul on 2/24/2017.
 */
public class PieChart {

    ReportDesignHandle reportDesignHandle = null;
    ElementFactory elementFactory = null;
    StructureFactory structFactory1 = null;
    MetaDataDictionary dict1 = null;

    public static void main(String[] args) throws SemanticException,
            IOException {
        new PieChart().createReport();
    }

    void createReport() throws SemanticException, IOException {
        //A session handle for all open reports
        SessionHandle session = DesignEngine.newSession(null);

        //Create a new report
        reportDesignHandle = session.createDesign();

        //Element factory is used to create instances of BIRT elements
        elementFactory = reportDesignHandle.getElementFactory();

        //Structure factory is used to create instances of BIRT structures
        structFactory1 = new StructureFactory();

        dict1 = MetaDataDictionary.getInstance();

        createMasterPages();
        //createDataSources( );
        //createDataSets( );
        createChart();

        reportDesignHandle.saveAs("PieChartExample.rptdesign");
    }

    //Data Source
    void createDataSources() throws SemanticException {
        ScriptDataSourceHandle dataSourceHandle = elementFactory.newScriptDataSource(
                "Data Source");

        /*UserPropertyDefn userPropDefn = new UserPropertyDefn( );
        userPropDefn.setName( "valid" );
        userPropDefn.setType( dict1.getPropertyType( PropertyType.STRING_TYPE ) );
        dataSourceHandle.addUserPropertyDefn( userPropDefn );
        dataSourceHandle.setProperty( "valid", "true" );*/

        reportDesignHandle.getDataSources().add(dataSourceHandle);
    }

    //Data Set
    void createDataSets() throws SemanticException {
        ScriptDataSetHandle dataSetHandle = elementFactory.newScriptDataSet("Data Set");
        dataSetHandle.setDataSource("Data Source");

        //Set open( ) in code
        dataSetHandle.setOpen("i=0;"
                + "sourcedata = new Array( new Array(2), new Array(2), new Array(2), new Array(2));"
                + "sourcedata[0][0] = \"Chris Kwai\"; "
                + "sourcedata[0][1] = 232;"
                + "sourcedata[1][0] = \"Ice Bella\";"
                + "sourcedata[1][1] = 291;"
                + "sourcedata[2][0] = \"Nola Dicci\";"
                + "sourcedata[2][1] = 567;"
                + "sourcedata[3][0] = \"Chris Kwai\";"
                + "sourcedata[3][1] = 312;");

        //Set fetch( ) in code
        dataSetHandle.setFetch("if ( i < 4 ){"
                + "row[\"Category\"] = sourcedata[i][0];"
                + "row[\"SalesBalance\"] = sourcedata[i][1];"
                + "i++;"
                + "return true;}"
                + "return false;");

        //Set Output Columns in Data Set
        ColumnHint ch1 = StructureFactory.createColumnHint();
        ch1.setProperty("columnName", "Category");

        ColumnHint ch2 = StructureFactory.createColumnHint();
        ch2.setProperty("columnName", "SalesBalance");

        PropertyHandle columnHint = dataSetHandle
                .getPropertyHandle(ScriptDataSetHandle.COLUMN_HINTS_PROP);
        columnHint.addItem(ch1);
        columnHint.addItem(ch2);

        //Set Preview Results columns in Data Set
        ResultSetColumn rs1 = StructureFactory.createResultSetColumn();
        rs1.setColumnName("Category");
        rs1.setPosition(new Integer(1));
        rs1.setDataType("string");

        ResultSetColumn rs2 = StructureFactory.createResultSetColumn();
        rs2.setColumnName("SalesBalance");
        rs2.setPosition(new Integer(2));
        rs2.setDataType("integer");

        PropertyHandle resultSet = dataSetHandle
                .getPropertyHandle(ScriptDataSetHandle.RESULT_SET_PROP);
        resultSet.addItem(rs1);
        resultSet.addItem(rs2);

        reportDesignHandle.getDataSets().add(dataSetHandle);
    }

    //Master Page
    void createMasterPages() throws ContentException, NameException {
        DesignElementHandle simpleMasterPage = elementFactory
                .newSimpleMasterPage("Page Master");
        reportDesignHandle.getMasterPages().add(simpleMasterPage);
    }

    //Chart
    void createChart() {

        ElementFactory ef = reportDesignHandle.getElementFactory();

        //Create an ExtendedItemHandle used as an communicant between report and chart
        ExtendedItemHandle eih = ef.newExtendedItem(null, "Chart");

        //The data info from data set
        ScriptDataSetHandle dataset = (ScriptDataSetHandle) reportDesignHandle.getDataSets().get(0);

        try {
            eih.setHeight("250pt");
            eih.setWidth("400pt");
            eih.setDataSet(dataset);
        } catch (SemanticException e) {
            e.printStackTrace();
        }

        ChartWithoutAxes cwoaPie = ChartWithoutAxesImpl.create();
        cwoaPie.setSeriesThickness(20);
        //cwoaPie.setGridColumnCount( 2);
        cwoaPie.getBlock().setBackground(ColorDefinitionImpl.WHITE());

        //2D dimensional with DEPTH (birt DOESN'T SUPPORT 3D for Pie Chart)
        cwoaPie.setDimension(ChartDimension.TWO_DIMENSIONAL_WITH_DEPTH_LITERAL);

        // Plot
        Plot p = cwoaPie.getPlot();
        p.getClientArea().setBackground(null);
        p.getClientArea().getOutline().setVisible(true);
        p.getOutline().setVisible(true);

        // Legend
        Legend lg = cwoaPie.getLegend();
        lg.getText().getFont().setSize(16);
        lg.setBackground(null);
        lg.getOutline().setVisible(true);

        // Title
        cwoaPie.getTitle().getLabel().getCaption().setValue("Pie Chart");
        cwoaPie.getTitle().getOutline().setVisible(true);


        SeriesDefinition sdX = SeriesDefinitionImpl.create();
        sdX.getSeriesPalette().shift(0);
        Series categorySeries = SeriesImpl.create();
        sdX.getSeries().add(categorySeries);
        sdX.getQuery().setDefinition("Base Series");
        SeriesDefinition sdY = SeriesDefinitionImpl.create();
        sdY.setZOrder(1);
        sdY.getSeriesPalette().shift(0);
        RadarSeries valueSeries = RadarSeriesImpl.create();
        LineAttributes lia =
                LineAttributesImpl.create(ColorDefinitionImpl.GREY(),
                        LineStyle.SOLID_LITERAL,
                        1);
        valueSeries.setWebLineAttributes(lia);
        valueSeries.getLabel().setVisible(true);
        valueSeries.setSeriesIdentifier("Series 1");
        sdY.getSeries().add(valueSeries);
        sdX.getSeriesDefinitions().add(sdY);

        cwoaPie.getSeriesDefinitions().add(sdX);
        /*PieSeries sePie = ( PieSeries ) PieSeriesImpl.create( );
        sePie.setDataSet( seriesOneValues );
        sePie.setSeriesIdentifier( "Cities" );
        sd.getSeriesDefinitions( ).add( sdCity );
        xAxisPrimary.getSeriesDefinitions( ).add( sdX );
        yAxisPrimary.getSeriesDefinitions( ).add( sdY1 );
        yAxisPrimary.getSeriesDefinitions( ).add( sdY2 );*/


       /* // Data Set
        TextDataSet categoryValues = TextDataSetImpl.create( new String[]{
                "New York", "Boston", "Chicago", "San Francisco", "Dallas"} );//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        NumberDataSet seriesOneValues = NumberDataSetImpl.create( new double[]{
                54.65, 21, 75.95, 91.28, 37.43
        } );*/

        //SampleData sdata = DataFactory.eINSTANCE.createSampleData( );
        //BaseSampleData sdBase = DataFactory.eINSTANCE.createBaseSampleData( );
        //sdBase.setDataSetRepresentation( "" );//$NON-NLS-1$
        //sdata.getBaseSampleData( ).add( sdBase );

//        OrthogonalSampleData sdOrthogonal = DataFactory.eINSTANCE.createOrthogonalSampleData( );
//        sdOrthogonal.setDataSetRepresentation( "" );//$NON-NLS-1$
//        sdOrthogonal.setSeriesDefinitionIndex( 0 );
//        sdata.getOrthogonalSampleData( ).add( sdOrthogonal );
//
//        cwoaPie.setSampleData( sdata );
        /*SampleData sd = DataFactory.eINSTANCE.createSampleData();
        BaseSampleData sdBase = DataFactory.eINSTANCE.createBaseSampleData();
        sdBase.setDataSetRepresentation("A, B, C, D");
        sd.getBaseSampleData().add(sdBase);

        OrthogonalSampleData sdOrthogonal = DataFactory.eINSTANCE
                .createOrthogonalSampleData();
        sdOrthogonal.setDataSetRepresentation("1,2,3,4");
        sdOrthogonal.setSeriesDefinitionIndex(0);
        sd.getOrthogonalSampleData().add(sdOrthogonal);

        cwoaPie.setSampleData(sd);

        Series seCategory = SeriesImpl.create();
        Query query = QueryImpl.create("row[\"Category\"]");
        seCategory.getDataDefinition().add(query);

//// Base Series
//        Series seCategory = SeriesImpl.create( );
//        seCategory.setDataSet( categoryValues );

*//*        SeriesDefinition sdd = SeriesDefinitionImpl.create( );
        cwoaPie.getSeriesDefinitions( ).add( sd );
        sd.getSeriesPalette( ).shift( 0 );
        sd.getSeries( ).add( seCategory );*//*

        SeriesDefinition sdX = SeriesDefinitionImpl.create();
        sdX.getSeries().add(seCategory);
        cwoaPie.getSeriesDefinitions().add(sdX);

// Orthogonal Series
        PieSeries sePie = (PieSeries) PieSeriesImpl.create( );
        //sePie.setDataSet( seriesOneValues );
        Query query2 = QueryImpl.create("row[\"SalesBalance\"]");
        sePie.getDataDefinition().add(query2);
        //sePie.setSeriesIdentifier( "Cities" );//$NON-NLS-1$
        sePie.setExplosion( 5 );

        SeriesDefinition sdCity = SeriesDefinitionImpl.create( );
        sdCity.getSeries( ).add( sePie );
        //sdCity.getSeries( ).add( sePie );*/


        ChartReportItemImpl crii;
        try {
            //Add ChartReportItemImpl to ExtendedItemHandle
            crii = (ChartReportItemImpl) eih.getReportItem();
            //Add chart instance to ChartReportItemImpl
            crii.setProperty("chart.instance", cwoaPie);
        } catch (ExtendedElementException e) {
            e.printStackTrace();
        }

        try {
            //Add the ExtendedItemHandle to the report
            reportDesignHandle.getBody().add(eih);
        } catch (ContentException e) {
            e.printStackTrace();
        } catch (NameException e) {
            e.printStackTrace();
        }

    }

}
