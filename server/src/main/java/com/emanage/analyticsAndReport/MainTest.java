package com.emanage.analyticsAndReport;

import com.ibm.icu.util.ULocale;
import org.eclipse.birt.chart.model.Chart;
import org.eclipse.birt.chart.model.ChartWithoutAxes;
import org.eclipse.birt.chart.model.attribute.ChartDimension;
import org.eclipse.birt.chart.model.impl.ChartWithoutAxesImpl;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.model.api.*;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.elements.structures.ComputedColumn;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul on 2/22/2017.
 */
public class MainTest {
    private List<Double> data;


    public static void main(String[] args) {
        try {
            MainTest.buildReport();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SemanticException e) {
            e.printStackTrace();
        }
    }

    // This method shows how to build a very simple BIRT report with a
    // minimal set of content: a simple grid with an image and a label.

    static void buildReport() throws IOException, SemanticException {
        // Create a session handle. This is used to manage all open designs.
        // Your app need create the session only once.

        //Configure the Engine and start the Platform
        DesignConfig config = new DesignConfig();

        //config.setProperty("BIRT_HOME", "C:/birt-runtime-2_1_1/birt-runtime-2_1_1/ReportEngine");
        IDesignEngine engine = null;
        try {
            Platform.startup(config);
            IDesignEngineFactory factory = (IDesignEngineFactory) Platform.createFactoryObject(IDesignEngineFactory.EXTENSION_DESIGN_ENGINE_FACTORY);
            engine = factory.createDesignEngine(config);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        SessionHandle session = engine.newSessionHandle(ULocale.ENGLISH);

        // Create a new report design.
        ReportDesignHandle design = session.createDesign();

        // The element factory creates instances of the various BIRT elements.
        ElementFactory efactory = design.getElementFactory();

        // Create a simple master page that describes how the report will appear when printed.
        //
        // Note: The report will fail to load in the BIRT designer unless you create a master page.
        DesignElementHandle element = efactory.newSimpleMasterPage("Page Master");
        design.getMasterPages().add(element);

        // Create a grid and add it to the "body" slot of the report design.
        GridHandle grid = efactory.newGridItem(null, 2 /* cols */, 1 /* row */);
        design.getBody().add(grid);

        ExtendedItemHandle eih = efactory.newExtendedItem(null, "Chart");
        ComputedColumn cs1 = null, cs2 = null;
        try {
            eih.setHeight("175pt");//$NON-NLS-1$
            eih.setWidth("450pt");//$NON-NLS-1$
            eih.setProperty(ExtendedItemHandle.DATA_SET_PROP,
                    "ChartData");//$NON-NLS-1$
            eih.setProperty("outputFormat", "PNG");

            /*PropertyHandle cs = eih.getColumnBindings();
            cs1 = StructureFactory.createComputedColumn();
            cs2 = StructureFactory.createComputedColumn();
            cs1.setName("xaxis");
            cs1.setDataType("string");
            cs2.setName("yaxis");
            cs2.setDataType("float");

            cs1.setExpression("dataSetRow[\"xaxis\"]");
            cs2.setExpression("dataSetRow[\"yaxis\"]");
            cs.addItem(cs1);
            cs.addItem(cs2);*/
            eih.getReportItem().setProperty("chart.instance", createPie());

            design.getBody().add(eih);

            design.getBody().add(efactory.newGridItem("Test", 4, 6));

            GridHandle grid1 = efactory.newGridItem("Test2", 3, 2);
            grid1.setCaption("Caption");
            grid1.setSummary("Summery");
            grid1.setWidth("50%");

            for (int i = 0; i < grid1.getRows().getCount(); i++) {
                RowHandle rowHandler = (RowHandle) grid1.getRows().get(i);
                for (int j = 0; j < rowHandler.getCells().getCount(); j++) {
                    CellHandle cellHandler = (CellHandle) rowHandler.getCells().get(j);
                    LabelHandle label = efactory.newLabel(null);
                    cellHandler.getContent().add(label);
                    label.setText("Hello, world! " + i + " : " + j);

                }

            }
            design.getBody().add(grid1);

            // Note: Set the table width to 100% to prevent the label
            // from appearing too narrow in the layout view.
            grid.setWidth("100%");

            // Get the first row.
            RowHandle row = (RowHandle) grid.getRows().get(0);

            // Create an image and add it to the first cell.
            ImageHandle image = efactory.newImage(null);
            CellHandle cell = (CellHandle) row.getCells().get(0);
            cell.getContent().add(image);
            image.setURL("\"urlofimage\""); 

            // Create a label and add it to the second cell.
            LabelHandle label = efactory.newLabel(null);
            cell = (CellHandle) row.getCells().get(1);
            cell.getContent().add(label);
            label.setText("Hello, world!");

            // Save the design and close it.
            design.saveAs("sample.rptdesign");
            design.close();
            System.out.println("Finished");

            // We're done!
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static final Chart createPie() {
        ChartWithoutAxes cwoaPie = ChartWithoutAxesImpl.create();
        cwoaPie.setDimension(ChartDimension.TWO_DIMENSIONAL_WITH_DEPTH_LITERAL);
        cwoaPie.setType("Pie Chart"); //$NON-NLS-1$
        cwoaPie.setSubType("Standard Pie Chart"); //$NON-NLS-1$
/*
        // Plot
        cwoaPie.setSeriesThickness( 10 );

        // Legend
        Legend lg = cwoaPie.getLegend( );
        lg.getOutline( ).setVisible( true );

        // Title
        cwoaPie.getTitle( ).getLabel( ).getCaption( ).setValue( "Pie Chart" );//$NON-NLS-1$

        // Data Set
        TextDataSet categoryValues = TextDataSetImpl.create( new String[]{
                "New York", "Boston", "Chicago", "San Francisco", "Dallas"} );//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        NumberDataSet seriesOneValues = NumberDataSetImpl.create( new double[]{
                54.65, 21, 75.95, 91.28, 37.43
        } );

        SampleData sdata = DataFactory.eINSTANCE.createSampleData( );
        BaseSampleData sdBase = DataFactory.eINSTANCE.createBaseSampleData( );
        sdBase.setDataSetRepresentation( "" );//$NON-NLS-1$
        sdata.getBaseSampleData( ).add( sdBase );

        OrthogonalSampleData sdOrthogonal = DataFactory.eINSTANCE.createOrthogonalSampleData( );
        sdOrthogonal.setDataSetRepresentation( "" );//$NON-NLS-1$
        sdOrthogonal.setSeriesDefinitionIndex( 0 );
        sdata.getOrthogonalSampleData( ).add( sdOrthogonal );

        cwoaPie.setSampleData( sdata );

// Base Series
        Series seCategory = SeriesImpl.create( );
        seCategory.setDataSet( categoryValues );

        SeriesDefinition sd = SeriesDefinitionImpl.create( );
        cwoaPie.getSeriesDefinitions( ).add( sd );
        sd.getSeriesPalette( ).shift( 0 );
        sd.getSeries( ).add( seCategory );

// Orthogonal Series
        PieSeries sePie = (PieSeries) PieSeriesImpl.create( );
        sePie.setDataSet( seriesOneValues );
        sePie.setSeriesIdentifier( "Cities" );//$NON-NLS-1$
        sePie.setExplosion( 5 );

        SeriesDefinition sdCity = SeriesDefinitionImpl.create( );
        sd.getSeriesDefinitions( ).add( sdCity );
        sdCity.getSeries( ).add( sePie );*/

        return cwoaPie;
    }
}
