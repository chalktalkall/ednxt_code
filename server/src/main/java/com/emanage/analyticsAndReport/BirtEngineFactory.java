
package com.emanage.analyticsAndReport;

public class BirtEngineFactory {
}
/*
import com.emanage.controller.CashRegisterController;
import org.apache.log4j.Logger;
import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;

*
 * Created by Abhishek on 2/19/2017.


public class BirtEngineFactory implements FactoryBean, ApplicationContextAware, DisposableBean {

    final static Logger logger = Logger.getLogger(BirtEngineFactory.class);


    public boolean isSingleton(){ return true ; }

    private ApplicationContext context ;
    private IReportEngine birtEngine ;
    private Resource logDirectory ;
    private File _resolvedDirectory ;
    private java.util.logging.Level logLevel ;

    public void setApplicationContext(ApplicationContext ctx){
        logger.info("Set Application Context to  BIRT");
        this.context = ctx;
    }

    public void destroy() throws Exception {
        logger.info("Destroy BIRT Engine and ShutDown Platform");
        birtEngine.destroy();
        Platform.shutdown() ;
    }

    public void setLogLevel(  java.util.logging.Level  ll){
        logger.info("Set Logging level to  BIRT to level : "+ll.getName());
        this.logLevel = ll ;
    }

    public void setLogDirectory( org.springframework.core.io.Resource resource ){
        File f=null;
        try {
            f = resource.getFile();
            logger.info("Set Logging file to  BIRT to file : "+f.getName());
            validateLogDirectory(f);
            this._resolvedDirectory = f ;
        } catch (IOException e) {
            throw new RuntimeException( "couldn't set the log directory");
        }


    }

    private void validateLogDirectory (File f) {
        Assert.notNull ( f ,  " the directory must not be null");
        Assert.isTrue(f.isDirectory() , " the path given must be a directory");
        Assert.isTrue(f.exists() , "the path specified must exist!");
    }

    public void setLogDirectory ( java.io.File f ){
        validateLogDirectory(f) ;
        this._resolvedDirectory = f;
    }

    public IReportEngine getObject(){

        EngineConfig config = new EngineConfig();
        logger.info("get IReportEngine");
        //This line injects the Spring Context into the BIRT Context
        config.getAppContext().put("spring", this.context );
        config.setLogConfig( null != this._resolvedDirectory ? this._resolvedDirectory.getAbsolutePath() : null  , this.logLevel);
        try {
            Platform.startup( config );
            logger.info("BIRT Platform is up and running");
        }
        catch ( BirtException e ) {
            throw new RuntimeException ( "Could not start the Birt engine!", e) ;
        }

        IReportEngineFactory factory = (IReportEngineFactory) Platform.createFactoryObject( IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY );
        IReportEngine be = factory.createReportEngine( config );
        logger.info("BIRT factory ="+factory);
        this.birtEngine = be ;
        return be ;
    }

    @Override
    public Class getObjectType() {
        return IReportEngine.class;
    }
}
*/
