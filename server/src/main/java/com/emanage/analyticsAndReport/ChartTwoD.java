package com.emanage.analyticsAndReport;
//package com.emanage.analyticsAndReport;
//
//import org.eclipse.birt.chart.model.ChartWithAxes;
//import org.eclipse.birt.chart.model.attribute.Orientation;
//import org.eclipse.birt.chart.model.attribute.impl.BoundsImpl;
//import org.eclipse.birt.chart.model.component.Axis;
//import org.eclipse.birt.chart.model.component.Series;
//import org.eclipse.birt.chart.model.component.impl.SeriesImpl;
//import org.eclipse.birt.chart.model.data.*;
//import org.eclipse.birt.chart.model.data.impl.QueryImpl;
//import org.eclipse.birt.chart.model.data.impl.SeriesDefinitionImpl;
//import org.eclipse.birt.chart.model.impl.ChartWithAxesImpl;
//import org.eclipse.birt.chart.model.type.BarSeries;
//import org.eclipse.birt.chart.model.type.impl.BarSeriesImpl;
//import org.eclipse.birt.chart.reportitem.ChartReportItemImpl;
//import org.eclipse.birt.report.model.api.*;
//import org.eclipse.birt.report.model.api.activity.SemanticException;
//import org.eclipse.birt.report.model.api.command.ContentException;
//import org.eclipse.birt.report.model.api.command.NameException;
//import org.eclipse.birt.report.model.api.elements.structures.ColumnHint;
//import org.eclipse.birt.report.model.api.elements.structures.ResultSetColumn;
//import org.eclipse.birt.report.model.api.extension.ExtendedElementException;
//import org.eclipse.birt.report.model.metadata.MetaDataDictionary;
//
//import java.io.IOException;
//
///**
// * Created by Abhishek on 2/23/2017.
// */
//public class ChartTwoD {
//
//    ReportDesignHandle reportDesignHandle = null;
//    ElementFactory elementFactory = null;
//    StructureFactory structFactory1 = null;
//    MetaDataDictionary dict1 = null;
//
//    public static void main(String[] args) throws SemanticException,
//            IOException {
//        new ChartTwoD().createReport();
//    }
//
//    void createReport() throws SemanticException, IOException {
//        //A session handle for all open reports
//        SessionHandle session = DesignEngine.newSession(null);
//
//        //Create a new report
//        reportDesignHandle = session.createDesign();
//
//        //Element factory is used to create instances of BIRT elements
//        elementFactory = reportDesignHandle.getElementFactory();
//
//        //Structure factory is used to create instances of BIRT structures
//        structFactory1 = new StructureFactory();
//
//        dict1 = MetaDataDictionary.getInstance();
//
//        createMasterPages();
//        createDataSources();
//        createDataSets();
//        createChart();
//
//        //Save the output report to a designated directory
//        reportDesignHandle.saveAs("ChartExample.rptdesign");
//    }
//
//    //Data Source
//    void createDataSources() throws SemanticException {
//        ScriptDataSourceHandle dataSourceHandle = elementFactory.newScriptDataSource(
//                "Data Source");
//
//        /*UserPropertyDefn userPropDefn = new UserPropertyDefn( );
//        userPropDefn.setName( "valid" );
//        userPropDefn.setType( dict1.getPropertyType( PropertyType.STRING_TYPE ) );
//        dataSourceHandle.addUserPropertyDefn( userPropDefn );
//        dataSourceHandle.setProperty( "valid", "true" );*/
//
//        reportDesignHandle.getDataSources().add(dataSourceHandle);
//    }
//
//    //Data Set
//    void createDataSets() throws SemanticException {
//        ScriptDataSetHandle dataSetHandle = elementFactory.newScriptDataSet("Data Set");
//        dataSetHandle.setDataSource("Data Source");
//
//        //Set open( ) in code
//        dataSetHandle.setOpen("i=0;"
//                + "sourcedata = new Array( new Array(2), new Array(2), new Array(2), new Array(2));"
//                + "sourcedata[0][0] = \"Chris Kwai\"; "
//                + "sourcedata[0][1] = 232;"
//                + "sourcedata[1][0] = \"Ice Bella\";"
//                + "sourcedata[1][1] = 291;"
//                + "sourcedata[2][0] = \"Nola Dicci\";"
//                + "sourcedata[2][1] = 567;"
//                + "sourcedata[3][0] = \"Chris Kwai\";"
//                + "sourcedata[3][1] = 312;");
//
//        //Set fetch( ) in code
//        dataSetHandle.setFetch("if ( i < 4 ){"
//                + "row[\"Category\"] = sourcedata[i][0];"
//                + "row[\"SalesBalance\"] = sourcedata[i][1];"
//                + "i++;"
//                + "return true;}"
//                + "return false;");
//
//        //Set Output Columns in Data Set
//        ColumnHint ch1 = StructureFactory.createColumnHint();
//        ch1.setProperty("columnName", "Category");
//
//        ColumnHint ch2 = StructureFactory.createColumnHint();
//        ch2.setProperty("columnName", "SalesBalance");
//
//        PropertyHandle columnHint = dataSetHandle
//                .getPropertyHandle(ScriptDataSetHandle.COLUMN_HINTS_PROP);
//        columnHint.addItem(ch1);
//        columnHint.addItem(ch2);
//
//        //Set Preview Results columns in Data Set
//        ResultSetColumn rs1 = StructureFactory.createResultSetColumn();
//        rs1.setColumnName("Category");
//        rs1.setPosition(new Integer(1));
//        rs1.setDataType("string");
//
//        ResultSetColumn rs2 = StructureFactory.createResultSetColumn();
//        rs2.setColumnName("SalesBalance");
//        rs2.setPosition(new Integer(2));
//        rs2.setDataType("integer");
//
//        PropertyHandle resultSet = dataSetHandle
//                .getPropertyHandle(ScriptDataSetHandle.RESULT_SET_PROP);
//        resultSet.addItem(rs1);
//        resultSet.addItem(rs2);
//
//        reportDesignHandle.getDataSets().add(dataSetHandle);
//    }
//
//    //Master Page
//    void createMasterPages() throws ContentException, NameException {
//        DesignElementHandle simpleMasterPage = elementFactory
//                .newSimpleMasterPage("Master Page");
//        reportDesignHandle.getMasterPages().add(simpleMasterPage);
//    }
//
//    //Chart
//    void createChart() {
//
//        ElementFactory ef = reportDesignHandle.getElementFactory();
//
//        //Create an ExtendedItemHandle used as an communicant between report and chart
//        ExtendedItemHandle eih = ef.newExtendedItem(null, "Chart");
//
//        //The data info from data set
//        ScriptDataSetHandle dataset = (ScriptDataSetHandle) reportDesignHandle.getDataSets().get(0);
//
//        try {
//            eih.setHeight("250pt");
//            eih.setWidth("400pt");
//            eih.setDataSet(dataset);
//        } catch (SemanticException e) {
//            e.printStackTrace();
//        }
//
//        //Chart-related properties
//        ChartWithAxes cwaBar = ChartWithAxesImpl.create();
//        cwaBar.setType("Bar Chart");
//
//        //cwaBar.setSubType("Side-by-side");
//        cwaBar.getTitle().getLabel().getCaption().setValue("Bar Chart Title");
//
//        Axis xAxisPrimary = cwaBar.getPrimaryBaseAxes()[0];
//        xAxisPrimary.setCategoryAxis(true);
//        Axis yAxisPrimary = cwaBar.getPrimaryOrthogonalAxis(xAxisPrimary);
//        cwaBar.setOrientation(Orientation.VERTICAL_LITERAL);
//        cwaBar.getBlock().setBounds(BoundsImpl.create(0, 0, 250, 400));
//
//        SampleData sd = DataFactory.eINSTANCE.createSampleData();
//        BaseSampleData sdBase = DataFactory.eINSTANCE.createBaseSampleData();
//        sdBase.setDataSetRepresentation("A");
//        sd.getBaseSampleData().add(sdBase);
//
//        OrthogonalSampleData sdOrthogonal = DataFactory.eINSTANCE
//                .createOrthogonalSampleData();
//        sdOrthogonal.setDataSetRepresentation("1");
//        sdOrthogonal.setSeriesDefinitionIndex(0);
//        sd.getOrthogonalSampleData().add(sdOrthogonal);
//
//        cwaBar.setSampleData(sd);
//
//        Series seCategory = SeriesImpl.create();
//        Query query = QueryImpl.create("row[\"Category\"]");
//        seCategory.getDataDefinition().add(query);
//
//        SeriesDefinition sdX = SeriesDefinitionImpl.create();
//        sdX.getSeries().add(seCategory);
//        xAxisPrimary.getSeriesDefinitions().add(sdX);
//
//        BarSeries bs = (BarSeries) BarSeriesImpl.create();
//        Query query2 = QueryImpl.create("row[\"SalesBalance\"]");
//        bs.getDataDefinition().add(query2);
//
//        SeriesDefinition sdY = SeriesDefinitionImpl.create();
//        sdY.getSeries().add(bs);
//        yAxisPrimary.getSeriesDefinitions().add(sdY);
//
//        ChartReportItemImpl crii;
//        try {
//            //Add ChartReportItemImpl to ExtendedItemHandle
//            crii = (ChartReportItemImpl) eih.getReportItem();
//            //Add chart instance to ChartReportItemImpl
//            crii.setProperty("chart.instance", cwaBar);
//        } catch (ExtendedElementException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            //Add the ExtendedItemHandle to the report
//            reportDesignHandle.getBody().add(eih);
//        } catch (ContentException e) {
//            e.printStackTrace();
//        } catch (NameException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//}
