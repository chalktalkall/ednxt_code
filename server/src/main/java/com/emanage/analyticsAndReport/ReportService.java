package com.emanage.analyticsAndReport;
/*
package com.path.report;

import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.*;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

*/
/**
 * Created by rahul on 2/14/2017.
 * <p>
 * Load report files to memory
 *
 * @throws EngineException
 * <p>
 * Generate a report as html
 * @param report
 * @param response
 * @param request
 *//*

@Service
public class ReportService implements ApplicationContextAware {
    @Autowired
    private ServletContext servletContext;

    private IReportEngine birtEngine;

    private ApplicationContext context;

    private Map<String, IReportRunnable> thumbnails = new HashMap<>();
    private Map<String, IReportRunnable> reports = new HashMap<>();

    private static final String IMAGE_FOLDER = "/images";

    @SuppressWarnings("unchecked")
    @PostConstruct
    protected void initialize() throws BirtException {
        EngineConfig config = new EngineConfig();
        config.getAppContext().put("spring", this.context);
        Platform.startup(config);
        IReportEngineFactory factory = (IReportEngineFactory) Platform
                .createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
        birtEngine = factory.createReportEngine(config);
        loadReports();
    }

    @Override
    public void setApplicationContext(ApplicationContext context) {
        this.context = context;
    }

    */
/**
 * Load report files to memory
 *
 * @throws EngineException
 *//*

    public void loadReports() throws EngineException {
        File folder = new File("webapp/WEB-INF/reports");
        for (String file : folder.list()) {
            if (!file.endsWith(".rptdesign")) {
                continue;
            }
            if (file.contains("thumb")) {
                thumbnails.put(file.replace("-thumb.rptdesign", ""),
                        birtEngine.openReportDesign(folder.getAbsolutePath() + File.separator + file));
            } else {
                reports.put(file.replace(".rptdesign", ""),
                        birtEngine.openReportDesign(folder.getAbsolutePath() + File.separator + file));
            }
        }
    }

    public List<Report> getReports() {
        List<Report> response = new ArrayList<>();
        for (Map.Entry<String, IReportRunnable> entry : thumbnails.entrySet()) {
            IReportRunnable report = reports.get(entry.getKey());
            IGetParameterDefinitionTask task = birtEngine.createGetParameterDefinitionTask(report);
            Report reportItem = new Report(report.getDesignHandle().getProperty("title").toString(), entry.getKey());
            for (Object h : task.getParameterDefns(false)) {
                IParameterDefn def = (IParameterDefn) h;
                reportItem.getParameters()
                        .add(new Report.Parameter(def.getPromptText(), def.getName(), getParameterType(def)));
            }
            response.add(reportItem);
        }
        return response;
    }

    private Report.ParameterType getParameterType(IParameterDefn param) {
        if (IParameterDefn.TYPE_INTEGER == param.getDataType()) {
            return Report.ParameterType.INT;
        }
        return Report.ParameterType.STRING;
    }

    public void generateReportThumb(String reportName, HttpServletResponse response, HttpServletRequest request) throws Exception {
        generateReport(thumbnails.get(reportName), response, request);
    }

    public void generateMainReport(String reportName, HttpServletResponse response, HttpServletRequest request) throws Exception {
        generateReport(reports.get(reportName), response, request);
    }

    */
/**
 * Generate a report as html
 *
 * @param report
 * @param response
 * @param request
 *//*

    @SuppressWarnings("unchecked")
    public void generateReport(IReportRunnable report, HttpServletResponse response, HttpServletRequest request) throws Exception {
        IRunAndRenderTask runAndRenderTask = birtEngine.createRunAndRenderTask(report);
        response.setContentType(birtEngine.getMIMEType("html"));
        IRenderOption options = new RenderOption();
        HTMLRenderOption htmlOptions = new HTMLRenderOption(options);
        htmlOptions.setOutputFormat("html");
        htmlOptions.setImageHandler(new HTMLServerImageHandler());
        htmlOptions.setBaseImageURL(request.getContextPath() + IMAGE_FOLDER);
        htmlOptions.setImageDirectory(servletContext.getRealPath(IMAGE_FOLDER));
        runAndRenderTask.setRenderOption(htmlOptions);
        runAndRenderTask.getAppContext().put(EngineConstants.APPCONTEXT_BIRT_VIEWER_HTTPSERVET_REQUEST, request);
        IGetParameterDefinitionTask task = birtEngine.createGetParameterDefinitionTask(report);
        Map<String, Object> params = new HashMap<>();
        for (Object h : task.getParameterDefns(false)) {
            IParameterDefn def = (IParameterDefn) h;
            if (def.getDataType() == IParameterDefn.TYPE_INTEGER) {
                params.put(def.getName(), Integer.parseInt(request.getParameter(def.getName())));
            } else {
                params.put(def.getName(), request.getParameter(def.getName()));
            }
        }
        try {
            htmlOptions.setOutputStream(response.getOutputStream());
            runAndRenderTask.run();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            runAndRenderTask.close();
        }
    }


    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        //get report name and launch the engine
        resp.setContentType("text/html");
        //resp.setContentType( "application/pdf" );
        //resp.setHeader ("Content-Disposition","inline; filename=test.pdf");
        String reportName = req.getParameter("ReportName");
        String[] cols = (String[])req.getParameterMap().get("dyna1");
        ServletContext sc = req.getSession().getServletContext();
        birtEngine = BirtEngine.getBirtEngine(sc);

        IReportRunnable design;
        try
        {
            //Open report design
            design = birtReportEngine.openReportDesign( sc.getRealPath("/Reports")+"/"+reportName );
            ReportDesignHandle report = (ReportDesignHandle) design.getDesignHandle( );
            buildReport( cols,  report );

            //create task to run and render report
            IRunAndRenderTask task = birtReportEngine.createRunAndRenderTask( design );
            task.setAppContext( contextMap );

            //set output options
            HTMLRenderOption options = new HTMLRenderOption();
            options.setImageHandler( new HTMLServerImageHandler() );
            options.setImageDirectory( sc.getRealPath("/images");
            options.setBaseImageURL( req.getContextPath() + "/images" );
            options.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_HTML);
            //options.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_PDF);
            options.setOutputStream(resp.getOutputStream());
            task.setRenderOption(options);

            //run report
            task.run();
            task.close();
        }catch (Exception e){

            e.printStackTrace();
            throw new ServletException( e );
        }
    }

    public void buildReport(String[]  cols, ReportDesignHandle designHandle){


        try{
            ElementFactory designFactory = designHandle.getElementFactory( );

            buildDataSource(designFactory, designHandle);


            //ArrayList cols = new ArrayList();
            //cols.add("OFFICECODE");
            //cols.add("CITY");
            //cols.add("COUNTRY");

            buildDataSet(cols, "From Offices", designFactory, designHandle);

            TableHandle table = designFactory.newTableItem( "table", cols.length );
            table.setWidth( "100%" );
            table.setDataSet( designHandle.findDataSet( "ds" ) );


            PropertyHandle computedSet = table.getColumnBindings( );
            ComputedColumn  cs1 = null;

            for( int i=0; i < cols.length; i++){
                cs1 = StructureFactory.createComputedColumn();
                cs1.setName((String)cols[i]);
                cs1.setExpression("dataSetRow[\"" + (String)cols[i] + "\"]");
                computedSet.addItem(cs1);
            }


            // table header
            RowHandle tableheader = (RowHandle) table.getHeader( ).get( 0 );


            for( int i=0; i < cols.length; i++){
                LabelHandle label1 = designFactory.newLabel( (String)cols[i] );
                label1.setText((String)cols[i]);
                CellHandle cell = (CellHandle) tableheader.getCells( ).get( i );
                cell.getContent( ).add( label1 );
            }

            // table detail
            RowHandle tabledetail = (RowHandle) table.getDetail( ).get( 0 );
            for( int i=0; i < cols.length; i++){
                CellHandle cell = (CellHandle) tabledetail.getCells( ).get( i );
                DataItemHandle data = designFactory.newDataItem( "data_"+(String)cols[i] );
                data.setResultSetColumn( (String)cols[i]);
                cell.getContent( ).add( data );
            }



            designHandle.getBody( ).add( table );
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    void buildDataSource( ElementFactory designFactory, ReportDesignHandle designHandle ) throws SemanticException
    {

        OdaDataSourceHandle dsHandle = designFactory.newOdaDataSource(
                "Data Source", "org.eclipse.birt.report.data.oda.jdbc" );
        dsHandle.setProperty( "odaDriverClass",
                "org.eclipse.birt.report.data.oda.sampledb.Driver" );
        dsHandle.setProperty( "odaURL", "jdbc:classicmodels:sampledb" );
        dsHandle.setProperty( "odaUser", "ClassicModels" );
        dsHandle.setProperty( "odaPassword", "" );

        designHandle.getDataSources( ).add( dsHandle );

    }

    void buildDataSet(String[] cols, String fromClause, ElementFactory designFactory, ReportDesignHandle designHandle ) throws SemanticException
    {

        OdaDataSetHandle dsHandle = designFactory.newOdaDataSet( "ds",
                "org.eclipse.birt.report.data.oda.jdbc.JdbcSelectDataSet" );
        dsHandle.setDataSource( "Data Source" );
        String qry = "Select ";
        for( int i=0; i < cols.length; i++){
            qry += " " + cols[i];
            if( i != (cols.length -1) ){
                qry += ",";
            }

        }
        qry += " " + fromClause;

        dsHandle.setQueryText( qry );

        designHandle.getDataSets( ).add( dsHandle );


    }
}
*/
