<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Section Type Manager</title>
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		.data, .data td {
			border-collapse: collapse;
			width: 100%;
			border: 1px solid #aaa;
			margin: 2px;
			padding: 2px;
		}
		.data th {
			font-weight: bold;
			background-color: #5C82FF;
			color: white;
		}
	</style>
	<base href="${pageContext.request.contextPath}/"/>
	<link rel="stylesheet" href="css/style.css" />
	
</head>
<body>
<a href="manageRestaurant.jsp" style="float: right;">Return to Home</a>
<hr/>
<h3>Add Section Type</h3>

<form:form method="post" action="cuisineDishType/addSectionType.html" commandName="sectionType">
	
	<form:hidden path="sectionTypeId" />
	<form:hidden path="countryId" value='<%=request.getSession().getAttribute("countryId")%>'/>
	<table>
	<tr>
		<td><form:label path="name"><spring:message code="label.name"/></form:label></td>
		<td><form:input path="name"  maxlength="45" required="true"/></td> 
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="Add Section Type"/>
		</td>
	</tr>
</table>	
</form:form>

<hr/>	
<h3>Section Types</h3>
<c:if  test="${!empty sectionTypeList}">
<table class="data">
<tr>
	<th>Name</th>
	<th>&nbsp;</th>
	<th>&nbsp;</th>
</tr>
<c:forEach items="${sectionTypeList}" var="sectionType">
	<tr>
		<td>${sectionType.name}</td>
		<td><a href="cuisineDishType/deleteSectionType/${sectionType.sectionTypeId}">delete</a></td>
		<td><a href="cuisineDishType/editSectionType/${sectionType.sectionTypeId}">edit</a></td>
	</tr>
</c:forEach>
</table>
</c:if>


</body>
</html>
