<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
</head>

<body style="padding: 50px !important;">


<h1  style="cursor:poiter" align="center"> ${restaurant.bussinessName} </h1>
<h5 align="center"></h5>
<div align="center"><a href="" align="center"></a></div>
<div align="center"><a href="" align="center"></a></div>
<div align="center"></div>

<div align="center">${restaurant.address1}<!-- <span id='date-time'></span>  --></div>
<div align="center">${checkDate}<!-- <span id='date-time'></span>  --></div>
<table align="center" width="100%">
<c:if  test="${!empty customer}">
<tr>
<td width="50%"  style="text-align:left">Name: ${checkRespone.name}</td>
<%-- <td width="50%" style="text-align: right;">Order Source: <b> ${checkRespone.orderSource}</b></td> --%>
</tr>
<tr>
	
<td width="50%" style="text-align:left">Payment Mode: <b> ${checkRespone.paymentMode}</b></td>

</tr>
<tr>
	<td width="50%" style="text-align:left" >Email Id: ${customer.email}</td>
</tr>
<tr>
<td width="50%" style="text-align:left">College: <b> ${vendor.businessName}</b></td>
</tr>
<tr>
<%-- <td width="50%"  style="text-align:left" >Phone No.: ${checkRespone.phone}</td> --%>
<td width="50%" style="text-align:left">Class Mode: <b> ${checkRespone.classMode}</b></td>

</tr>
 <tr></tr>
 <tr></tr> 
 <tr></tr> 
</c:if>
<c:if test="${!empty tableId }">
<tr><td>Table ID: ${tableId}</td></tr> 
</c:if>
</table>
<table align="center" width="100%">
<tr>
<th width="34%" align="left">Item</th>
<th width="33%" align="center" style="text-align:center !important"  >Enrollment</th>
<th width="33%"  align="right" style="text-align:right !important" >Amount</th>
</tr>
<c:if test="${!empty itemsMap}">
<c:forEach items="${itemsMap}" var="item">
	<tr>
	<td width="34%" align="left">${item.value.name}   </td>
		<c:if test="${item.value.price>0}" >
		<td width="33%" align="center" >X ${item.value.quantity}</td>
		<td width="33%" align="right" >
		<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${item.value.price}" />
		</td>
		</c:if>
	</tr>
	 <c:forEach items="${item.value.section}" var="section"> 
		<c:if test="${section !=null && section.name != '' && section.platterSectionType=='MAIN_PLATTER'}" >
		<tr>	 
		 <td style="font-size: 10px;" ><b>${section.name}: &nbsp;</b> 
			 <c:forEach items="${section.items}" var="itm">
				<c:if test="${itm.isDishReplaceable==false}">
				 ${itm.name} &nbsp; |  &nbsp; 
				 </c:if>
			</c:forEach>
		 <td>
		 </td>
		 </tr>
		 </c:if>
		</c:forEach>									 
   <c:forEach items="${item.value.addOns}" var="itemAdd"> 
	<c:if test="${item.value.addOns !=null && itemAdd.dishId == item.value.id}" >
	<tr>
	<%-- <c:if test="${itemAdd.dishSize =='Default'}"> --%>
	<td width="34%" align="left" >${itemAdd.name}</td>
	<%-- </c:if> --%>
	<%-- <c:if test="${itemAdd.dishSize!='Default'}">
	<td width="34%" align="left"><b>Add-On :</b> ${itemAdd.name}  ${itemAdd.dishSize !=''? itemAdd.dishSize:''}</td>
	</c:if> --%>
		<td width="33%" align="center" >X ${itemAdd.quantity}</td>
		<td width="33%" align="right" ><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${itemAdd.price * itemAdd.quantity}" /></td>
	</tr>
	</c:if>
	</c:forEach>
	<c:if test="${!empty checkRespone.couponCal}"> 
		<c:forEach items="${checkRespone.couponCal}" var="coupon">
				<c:if test="${coupon.key == item.value.id}">
					<tr>
						<td width="33%">${coupon.value.couponName}</td>
						<td width="33%" align="center" ></td>
			            <td width="33%" align="right" ><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${- coupon.value.calcAmount}" /></td>
					</tr>
				</c:if>
		</c:forEach>
	</c:if>
</c:forEach>


<%-- <c:if test = "${checkRespone.outCircleDeliveryCharges > 0}">
	<tr><td width="33%">Delivery Charges ${checkRespone.additionalChargeName1}</td> 
	<td width="33%" align="center" >-</td>
	<td width="33%" align="right">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.outCircleDeliveryCharges}" />
</td></tr>
</c:if>
<c:if test="${checkRespone.waiveOffCharges > 0}">
	<tr><td width="33%">Waived off Delivery Charges </td> 
	<td width="33%" align="center" >-</td>
	<td width="33%" align="right">
	-<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.waiveOffCharges}" />
	</td>
	</tr>
</c:if> --%>
</c:if>

</table>
<hr/>
<table id="checkData" align="center" width="100%">
<tr>
	<td width="33%">Sub Total</td>
	<td width="33%" align="center"></td> 
	<td width="33%" align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.amount}" /></td>
	</tr>

<c:if test="${!empty checkRespone.chargeDetails}">	
	<c:forEach items="${checkRespone.chargeDetails}" var="list">
    <tr>
	<td width="33%">${list.key}</td>
	<td width="33%" align="center"></td> 
	<td width="33%" align="right">
	+<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${list.value}" /></td>
	</tr> 
	</c:forEach>	
</c:if>
<c:if test="${!empty checkRespone.chargeDetails }">
	<tr>
		<td width="33%">Total after discount</td>  
		<td width="33%" align="center"></td> 
	    <td width="33%" align="right"><fmt:formatNumber type="number" 
            maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.amountAfterCharge}" /></td>
	</tr>
</c:if>

<c:if test="${!empty checkRespone.discountDetails}"> 
<c:forEach items="${checkRespone.discountDetails}" var="list">
<tr>
	<td width="33%">${list.key}</td>
	<td width="33%" align="center"></td> 
	<td width="33%" align="right">
	-<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${list.value}" /></td>
	</tr> 
	</c:forEach>
</c:if>	
	
<c:if test="${!empty checkRespone.discountDetails }">
	<tr>
		<td  width="33%" >Total after discount</td> 
		<td width="33%" align="center"></td> 
		<td width="33%" align="right"><fmt:formatNumber type="number" 
            maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.amountAfterDiscountCharges}" /></td>
	</tr>
</c:if>

	
<c:if test="${checkRespone.discountAmount > 0}">
	<tr>
		<td width="33%">Discount</td>
		<td width="33%" align="center"></td> 
		<td width="33%" align="right">${checkRespone.discountAmount}</td></tr>
	<tr>
		<td width="33%">Total(-disc.)</td>
		<td width="33%" align="center"></td> 
		<td width="33%" align="right">${checkRespone.amountAfterDiscount}</td>
	</tr>
</c:if>

<c:if test = "${checkRespone.outCircleDeliveryCharges > 0}">
<tr>
<td width="34%">Pickup Charges</td> 
<td width="33%" align="center" ></td>
<td width="33%"  align="right" style="float:right;text-align:right">
<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.outCircleDeliveryCharges}" />
</td></tr>
</c:if>

<c:if test="${checkRespone.waiveOffCharges > 0}">
	<tr>
		<td width="34%">Waived off Pickup Charges</td>
		<td width="33%" align="center" ></td>
		<td width="33%"  align="right" style="float:right;text-align:right">-<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.waiveOffCharges}" /></td>
		</tr>
</c:if>

<tr>
<td>
<c:forEach items="${checkRespone.taxDetails}" var="item">
 <c:if test="${item.value !=null}">
<tr>
<td width="33%" >${item.key}</td>
<td width="33%" align="center"></td> 
<td width="33%" align="right"><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${item.value}" /></td></tr>
</c:if></c:forEach> 
</td>
</tr>
<tr>
<tr>
<td  width="33%" >Order Total</td> 
<td width="33%" align="center"></td> 
<td width="33%" align="right">
<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.total}" />
</td>
</tr>
	<%-- <tr><td width="33%" ><b>Token Amount Paid</b></td> 
	<td width="33%" align="center"></td> 
	<td id="roundOff"  width="33%" align="right"><b>- ${checkRespone.tokenAmount} </b></td>
	</tr>
	<tr><td width="33%" ><b>Remaining Amount to be paid at Institute</b></td> 
	<td width="33%" align="center"></td> 
	<td id="roundOff"  width="33%" align="right"><b>${checkRespone.total - checkRespone.tokenAmount} </b></td>
	</tr> --%>
</table>
<table id="checkData" width="100%">
<c:if  test="${checkRespone.checkCreditBalance>0}">
<hr/>
<tr>
<td width="33%">Previous Credit Balance</td>
<td width="33%" align="center"></td> 
<td width="33%" align="right"><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.checkCreditBalance}"/></td></tr>
<c:set var="ccAmount" value="${checkRespone.checkCreditBalance}"></c:set>
</c:if>
<c:if  test="${checkRespone.checkCreditBalance<0}">
<tr>
<td width="33%" >Credit amount</td>
<td width="33%" align="center"></td> 
<td width="33%" align="right"><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.checkCreditBalance}"/></td>
</tr>
<c:set var="ccAmount" value="${checkRespone.checkCreditBalance}"></c:set>
</c:if>
<c:if  test="${checkRespone.paymentMode=='CUSTOMER CREDIT' && checkRespone.checkCreditBalance>0}">
<tr>
<td width="33%" >Current Order Total</td>
<td width="33%" align="center"></td> 
<td width="33%" align="right"><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.total}"/></td><tr>
</c:if>

<c:if  test="${checkRespone.paymentMode!='CUSTOMER CREDIT' && checkRespone.checkCreditBalance>0}">
<tr><td width="33%" >Current Order Total</td>
<td width="33%" align="center"></td> 
<td width="33%" align="right"><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.total}"/></td><tr>
</c:if>

<c:if test="${checkRespone.paymentMode=='CUSTOMER CREDIT' && checkRespone.checkCreditBalance>0}">
	<tr><td width="33%">Round Off</td> 
	<td width="33%" align="center"></td> 
	<td width="33%" align="right">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${ccAmount + checkRespone.total}" />
	</td>
	</tr>
	<tr><td width="33%" >Payable Amount</td> 
	<td width="33%" align="center"></td> 
	<td width="33%" align="right">0.00</td>
	</tr>
</c:if>

<%-- <c:if test="${checkRespone.paymentMode!='CUSTOMER CREDIT'}">
<tr><td width="33%" >Payable Amount</td> 
	<td width="33%" align="center"></td> 
	<td  id="roundOff" width="33%" align="right">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.roundedOffTotal + ccAmount}" />
	</td>
	</tr>
</c:if> --%>

<c:if test="${checkRespone.paymentMode=='CUSTOMER CREDIT'}">
<tr><td width="33%">Round Off</td> 
	<td width="33%" align="center"></td> 
	<td id="roundOff"  width="33%" align="right">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${checkRespone.roundedOffTotal}" />
	</td>
	</tr>
<!-- <tr><td width="33%" >Payable Amount</td> 
	<td width="33%" align="center"></td> 
	<td id="roundOff"  width="33%" align="right">0.00</td>
	</tr> -->
</c:if>
<%-- <c:if test="${customer.credit.creditBalance<0}">
<tr><td>Remaining Credit balance</td> 
	<td id="roundOff" style="float:right;text-align:right">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2" value="${customer.credit.creditBalance}" />
	</td>
	</tr>
</c:if> --%>
<c:if test = "${checkRespone.amountSaved > 0}">
<tr>
<td  width="33%">Amount you saved</td>
<td width="33%" align="center"></td> 
<td width="33%" align="right">${checkRespone.amountSaved}</td>
</tr>
</c:if>	
</table>

<hr/>
<c:if test = "${restaurant.tinNo !=null && restaurant.tinNo!='' && !checkRespone.toShowOldVat_ServiceTax}">
<div align="center">GSTIN No.: ${restaurant.tinNo}</div>
</c:if>
<c:if test = "${checkRespone.toShowOldVat_ServiceTax}">
	<c:if test = "${restaurant.restaurantId==21}">
			<div align="center">TIN No.: 06551835911</div>
	</c:if>
	<c:if test = "${restaurant.restaurantId==33}">
			<div align="center">TIN No.: 07406994308</div>
	</c:if>
<div align="center">Service Tax No.: ${restaurant.serviceTaxNo}</div>
</c:if>
<div align="center">Invoice No.: ${checkRespone.invoiceId}</div>
<div align="center">Order No.: ${checkRespone.orderId}</div>
<br/>
<br/>
<div align="center">You're Awesome</div>
<div align="center">-------</div>
</body>
</html>
