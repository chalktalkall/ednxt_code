<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="com.emanage.constants.PlatterType" %>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Events </title>
	<base href="${pageContext.request.contextPath}/"/>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="themes/base/jquery.ui.all.css">
	<link rel="stylesheet" type="text/css" href="themes/base/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css" href="themes/base/jquery.multiselect.filter.css" />
	<script src="js/jquery-1.9.0.js"></script>
	<script src="js/ui/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.filter.js"></script>	

	<!--  <link rel="stylesheet" href="../demos.css">  -->
	<style>
	#section { list-style-type: none; margin: 1 px; padding: 1 px; width: 79%; border-style:solid; border-width:1px; }
	#section li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em;  border-style:solid; border-width:1px}
	.dish { list-style-type: none; margin: 1 px; padding: 1 px;  border-style:solid; border-width:1px;  }
	.dish li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; border-style:solid; border-width:1px;}
	.ui-multiselect.dishDialog {font-size:1.0em;}
	.ui-multiselect-menu.dishDialog {font-size:1.0em;}
	</style>
	<script type="text/javascript" src="js/nicEdit.js"></script>
	<script type="text/javascript">
	/*  bkLib.onDomLoaded(function() { 
			var nicEditorInstance =  new nicEditor({fullPanel : false, iconsPath: 'images/nicEditorIcons.gif', buttonList : ['bold','italic','underline','left','center','right', 'justify', 'ol', 'ul', 'subscript', 'superscript', 'strikethrough', 'removeformat', 'indent', 'outdent', 'hr', 'forecolor', 'bgcolor', 'fontSize', 'fontFamily', 'fontFormat']});
			nicEditorInstance.panelInstance('menuDescription');
    	   $(".nicEdit-main").attr('id', 'smallDescription');
            $(".nicEdit-main").attr('tabindex', '1');
            $(".nicEdit-main").attr('oninput', 'smallDescriptionLimitText(this,900);'); 
		});  */
	</script>
	<script>

	function smallDescriptionLimitText(textarea,charCount){
		var data = textarea.innerHTML;
		var textLength = data.toString();
       if(textLength.length>charCount){
			alert("Characters  limit exceeded. You can only enter "+charCount+" characters ");
			textarea.innerHTML=textLength.substr(0,charCount);
            }
		}
	
	function removeDish(dishEL) {
		if (confirm('Do you want to remove this Test?')) {
			dishEL.parent().remove();
		}
	}
	
	function editSection(sectionEditBtnEL) {
		var sectionEL = sectionEditBtnEL.parent();
		var sectionELId = sectionEL.attr('id');
		$('#addSection').hide();
		$('#saveSection').attr("data-sectionCount", sectionELId);
		$('#saveSection').show();
		$('#sectionId').val(sectionEL.find('.sectionSectionId')[0].value);
		$('#name').val(sectionEL.find('.sectionName')[0].value);
		$('#price').val(sectionEL.find('.sectionPrice')[0].value);
		$('#shortName').val(sectionEL.find('.sectionShortName')[0].value);
		$('#description').val(sectionEL.find('.sectionDescription')[0].value);
		//$('#header').val(sectionEL.find('.sectionHeader')[0].value);
		$('#sectionTypeId').val(sectionEL.find('.sectionSectionTypeId')[0].value);
		$('#platterSectionType').val(sectionEL.find('.sectionPlatterSectionType')[0].value);
		//$('#footer').val(sectionEL.find('.sectionFooter')[0].value);
		$('#sectionForm').dialog({
			width: 500,
			modal: true
			});
	}
	function removeSection(sectionEL) {
		if (confirm('Do you really want to delete this section?')) {
			$(sectionEL.parent()).find(".validSection")[0].value = false;
			$(sectionEL.parent()).hide();
		}
		
	}
	function addDishes(dishesELId) {
		
		var el = $('#' + dishesELId); 
		var dishELs = el.find("li");
		var countDishes = dishELs.size();
		var dishes = {};
		var dishIdListOptions = $("#dishIdList").find("option");
		for (var i = 0; i < countDishes; i++) {
			var dishId = dishELs[i].attributes["data-dishid"].value;
			var dishName = dishELs[i].attributes["data-dishname"].value;
			dishes[dishId] = dishName;
		}
		console.log(dishes);
		
		for (var i =0; i < dishIdListOptions.size(); i++) {
			optionDishId = dishIdListOptions[i].value;
			if (optionDishId in dishes) {
				if(!(dishIdListOptions[i].selected)) {
					$("#ui-multiselect-dishIdList-option-" + i).click();
				}
			} else {
				if(dishIdListOptions[i].selected) {
					$("#ui-multiselect-dishIdList-option-" + i).click();
				}
			}
		}
		$("#addDish").attr("data-disheselid", dishesELId);
		$("#dishSection").dialog({width:500});
		$("select#dishIdList").multiselect({classes: "dishDialog", autoOpen: true}).multiselectfilter();
	}
	
	function addReplacableDishes(dishIdOnly, dish) {
		var dishesELIdRaw=dish.parent().parent();
		var dishesELId = dishesELIdRaw[0].id;
		var el = $('#' + dishesELId); 
		var dishELs = el.find("li");
		var countDishes = dishELs.size();
		var dishes = {};
		
		var alreadyAddedDishIds  = $("#replaceableId"+dishesELId+dishIdOnly).val();
		 var arDishIdList = [];
		if(alreadyAddedDishIds!=undefined || alreadyAddedDishIds!=''){
			 arDishIdList = alreadyAddedDishIds.split("_");
		}  
		
		var dishIdListOptions = $("#replace"+dishesELId).find("option");
		
		 for (var i =0; i < dishIdListOptions.size(); i++) {
			if(dishIdListOptions[i].selected) {
				 $("#ui-multiselect-replace"+dishesELId+"-option-" + i).click();
			}
		} 
		
		for (var i =0; i < dishIdListOptions.size(); i++) {
			optionDishId = dishIdListOptions[i].value;
			var selected=false;
			for(var j=0;j<arDishIdList.length;j++){
				if(arDishIdList[j]==optionDishId){
					selected= true;
				}
			}
			if (selected) {
			    $("#ui-multiselect-replace"+dishesELId+"-option-" + i).click();
			}
		}
		$("#addReplacable"+dishesELId).attr("data-disheselid", dishesELId);
		$("#addDishIdRep"+dishesELId).attr("data-disheselid", dishIdOnly);
		$("#dishReplacableSection"+dishesELId).dialog({width:500});
		$("select#replace"+dishesELId).multiselect({classes: "dishDialog", autoOpen: true}).multiselectfilter();
	}
	
	function addSelectedReplacableToDish(count) {
		var dishesELId = $("#addReplacabledish"+count).attr("data-disheselid");
		var dishIdOnly = $("#addDishIdRepdish"+count).attr("data-disheselid");
		var dishIdListOptions = $("#replacedish"+count).find("option");
		var dishNames="";
		var dishIds="";
		
		for (var i =0; i < dishIdListOptions.size(); i++) {
				if(dishIdListOptions[i].selected) {
					 dishIds += dishIdListOptions[i].value +"_";
					 dishNames += dishIdListOptions[i].text+" | ";
				}
		}
		console.log(dishIds);
		$("#replaceableId"+dishesELId+dishIdOnly).val(dishIds);
		$("#replaceable"+dishesELId+dishIdOnly).val(dishNames);

	}
	
	function addSelectedDish() {
		var dishesELId = $("#addDish").attr("data-disheselid");
		var platterType = $('#'+dishesELId+"platterSectionTypeName").val();
		var el = $('#' + dishesELId);
		var dishELs = el.find("li");
		var dishes = {};
		for (var i = 0; i < dishELs.size(); i++) {
			var dishId = dishELs[i].attributes["data-dishid"].value;
			var dishName = dishELs[i].attributes["data-dishname"].value;
			dishes[dishId] = dishName;
		}
		if (dishELs.size() < 1) {
			el.sortable();	
			el.disableSelection();
		}
		var dishIdListOptions = $("#dishIdList").find("option");
		for (var i =0; i < dishIdListOptions.size(); i++) {
			optionDishId = dishIdListOptions[i].value;
			if (!(optionDishId in dishes)) {
				if(dishIdListOptions[i].selected) {
					var dishId = dishIdListOptions[i].value;
					var dishName = dishIdListOptions[i].text;
					var appendDishName= '<li class="ui-state-default" style="font-size:0.8em;" data-dishid="'+ dishId + '" data-dishname="' + dishName + '">' + dishName;
					var removeButton='<button type="button" class="removeDish" onclick="removeDish($(this));" style="float:right">-</button>';
					var platterTypeColoum='';
					if(platterType=='MAIN_PLATTER'){
						platterTypeColoum=' <a title="Mark as replacement"> <input type="checkbox" id="checkbox'+dishesELId+dishId + '" style="float:right" "></a></li>';
					}else if(platterType=='ADD_ON'){
						platterTypeColoum='<input type="number" id="price'+dishesELId+dishId + '" placeholder="CT price" style="float:right" " ><input type="number" id="displayPrice'+dishesELId+dishId + '" placeholder="Display price" style="float:right" " >  </li>';
					}
					
					el.append(appendDishName+removeButton+platterTypeColoum);
					//addedDishIds[j++] = dishId;  <span> Replacement</span> 
				}
			}
		}
		//el.parent().find(".addedDishIds")[0].value = addedDishIds.toString();
		$("#dishSection").dialog("close");
	}
	
	function submitMenu() {
		
		var sectionELs = $("#section").find("li.section");
		for (var i = 0;  i < sectionELs.size(); i++) {
			$(sectionELs[i]).find(".position")[0].value = i;
			var dishELs = $($(sectionELs[i]).find(".dish")[0]).find("li");

			var addedDishIds = new Array();
			var dynamicPrice="NA";
			var displayPricedish="NA";
			var isReplaceable=false;
			var replacements="NA";
			for (var j = 0 ; j < dishELs.size(); j++) {
				addedDishIds[j] = dishELs[j].attributes["data-dishid"].value;
				if(document.getElementById("pricedish"+i+addedDishIds[j]) != null && document.getElementById("pricedish"+i+addedDishIds[j]).value != ""){
					dynamicPrice=document.getElementById("pricedish"+i+addedDishIds[j]).value;
				}
				if(document.getElementById("displayPricedish"+i+addedDishIds[j]) != null && document.getElementById("displayPricedish"+i+addedDishIds[j]).value != ""){
					displayPricedish=document.getElementById("displayPricedish"+i+addedDishIds[j]).value;
				}
				if(document.getElementById("checkboxdish"+i+addedDishIds[j]) != null && document.getElementById("checkboxdish"+i+addedDishIds[j]).value != ""){
					isReplaceable=document.getElementById("checkboxdish"+i+addedDishIds[j]).checked;
				}
				if(document.getElementById("replaceableIddish"+i+addedDishIds[j]) != null && document.getElementById("replaceableIddish"+i+addedDishIds[j]).value != ""){
					
					replacements=document.getElementById("replaceableIddish"+i+addedDishIds[j]).value;
				}
				addedDishIds[j] = dishELs[j].attributes["data-dishid"].value+"-"+isReplaceable+"-"+dynamicPrice+"-"+replacements+"-"+displayPricedish;
				//alert(addedDishIds[j]);
				dynamicPrice="NA";
				isReplaceable=false;
				replacements="NA";
				displayPricedish="NA";
			}
			$(sectionELs[i]).find(".addedDishIds")[0].value = addedDishIds.toString();
		}
		
		//return false;
		//$("#menuForm").find('[name="description"]')[0].value = $("menuDescription").val();
		//$('#menuDescription').val($($('#menuDescription').prev()).find('div.nicEdit-main').html());
		$("#menuForm").submit();
	}
	
	function openSectionForm() {
		
		$('#sectionId').val("");
		$('#name').val("");
		//$('#price').val("");
		$('#description').val("");
		$('#shortName').val("");
		//$('#header').val("");
		//$('#footer').val("");
		$('#platterSectionType select').val("MAIN_PLATTER");
		$('#addSection').show();
		$('#saveSection').hide();
		$('#sectionForm').dialog({
			width: 500,
			modal: true
			});
	}
	
	
	$(function() {
		$("select#dishIdList").multiselect({classes: "dishDialog"}).multiselectfilter();
		$( "#section" ).sortable();
		$( "#section" ).disableSelection();
		$( ".dish" ).sortable();
		$( ".dish" ).disableSelection();
		$("#saveSection").click(function( event ) {
			var name = $( "#name" ).val(),
			 price = $( "#price" ).val(),
			 description = $( "#description" ).val(),
			 shortName = $( "#shortName" ).val(),
			// header = $("#header").val(),
			// footer = $("#footer").val(),
			 sectionId = $("#sectionId").val();
			 sectionTypeId= $("#sectionTypeId").val(),
			 sectionTypeName=$("#sectionTypeId").find("option:selected").text();
			 platterSectionType = $("#platterSectionType").val();
			 
			$('#sectionForm').dialog('close');
			var sectionELId = $(this).attr("data-sectionCount");
			var sectionEL = $('#' + sectionELId);
			$(sectionEL.find('.sectionNameText')[0]).text(name);
			sectionEL.find('.sectionName')[0].value = name;
			sectionEL.find('.sectionPrice')[0].value = price;
			sectionEL.find('.sectionShortName')[0].value = shortName;
			sectionEL.find('.sectionDescription')[0].value = description;
			//sectionEL.find('.sectionHeader')[0].value = header;
			//sectionEL.find('.sectionFooter')[0].value = footer;
			sectionEL.find('.sectionSectionTypeId')[0].value = sectionTypeId;
			sectionEL.find('.sectionSectionTypeName')[0].value =sectionTypeName;
			sectionEL.find('.sectionSectionTypeName')[0].text =sectionTypeName;
			sectionEL.find('.sectionPlatterSectionType')[0].value =platterSectionType;
			sectionEL.find('.sectionPlatterSectionTypeName')[0].value =platterSectionType;
		});
		$("#addSection").click(function( event ) {
			 var name = $( "#name" ).val(),
			 price = $( "#price" ).val(),
			 shortName = $( "#shortName" ).val(),
			 description = $( "#description" ).val(),
			 sectionId = $("#sectionId").val();
			 sectionTypeId = $("#sectionTypeId").val();
			 sectionTypeName=$("#sectionTypeId").find("option:selected").text();
			 platterSectionType=$("#platterSectionType").val();
			 
			 
			$('#sectionForm').dialog("close");
			var count = $("#section").children().size();			
			
			
			$("#section").append('<li id="section' + count + '" class="section ui-state-default"><span class="sectionNameText">' + name + '</span>'
					+ '<input type="hidden" class="addedDishIds" name="sections[' + count +'].dishIds" value=""/>'
					+ '<input type="hidden" class="sectionSectionId" name="sections[' + count +'].sectionId" value="' + sectionId + '"/>'
					+ '<input type="hidden" class="sectionName" name="sections[' + count +'].name" value="' + name + '"/>'
					+ '<input type="hidden" class="sectionSectionTypeId" name="sections[' + count +'].sectionTypeId" value="' + sectionTypeId + '"/>'
					+ '<input type="hidden" class="sectionPlatterSectionType" name="sections[' + count +'].platterSectionType" value="' + platterSectionType + '"/>'
					+ '<input type="hidden" class="sectionPrice" name="sections[' + count +'].price" value="' + price + '"/>'
					+ '<input type="hidden" class="sectionShortName" name="sections[' + count +'].shortName" value="' + shortName + '"/>'
					+ '<input type="hidden" class="sectionDescription" name="sections[' + count +'].description" value="' + description + '"/>'
					+ '<input type="hidden" name="sections[' + count +'].valid" class="validSection" value="true"/>'
					+ '<input type="hidden" name="sections[' + count +'].position" class="position" value="'+count+'"/>'
					+ '<button type="button" onclick="removeSection($(this));" style="float:right">x</button>'
					+ '<button type="button" onclick="editSection($(this));" style="float:right">E</button>'
					+ '<button type="button" class="addDish" onclick="addDishes(\'dish'+count +'\')" style="float:right">+</button>'
					+ '<input type="button"  class="sectionSectionTypeName" style="float:right" value="'+sectionTypeName+'">'
					+ '<input type="button" class="sectionPlatterSectionTypeName" id="dish' + count +'platterSectionTypeName" name="sections[' + count +'].platterSectionType" value="' + platterSectionType + '"/>'
					+ '<ul id="dish' + count + '" class="dish" ></ul> </li>');
			
			$( "#dish" + count ).sortable();
			$( "#dish" + count ).disableSelection();
			
      });		
	});

	$(function(){
	/* 	$("#validity").datepicker({
		    dateFormat: 'yy-mm-dd hh:mm',
		    maxDate:"100Y",
			minDate:"0Y",
			changeYear:true,
			changeMonth:true
		}) */
		
	
	})

	 window.onload = function() {
		
		var datetime ='${menu.validity}';
		var dateAndTime = datetime.split(' ');
		var date = dateAndTime[0].split('.').reverse();
		dateAndTime = date.join('-') + 'T' + dateAndTime[1] ;
		document.getElementById("validity").value=dateAndTime;
	}
		
	function copyFunction(id) {
		  /* Get the text field */
		  var copyText = document.getElementById("myInput"+id);

		  /* Select the text field */
		  copyText.select();
		  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

		  /* Copy the text inside the text field */
		  document.execCommand("copy");

		  /* Alert the copied text */
		  alert("Copied the link: " + copyText.value);
		  return false;
		}

	
	</script>
</head>
<body>
<div id="timeMenu" style="display:none">

<%-- <c:choose>
	<c:when test="${!empty menu.startTime}">
		Start Time<input type="Time" name="startTime" value="${menu.startTime}" id="startTime">
		End Time <input type="time" name="endTime"  value="${menu.endTime}" id="endTime"><br/><br/>
	</c:when>
	<c:otherwise>
		Start Time<input type="Time" name="startTime" id="startTime">
		End Time <input type="time" name="endTime" id="endTime"><br/><br/>
	</c:otherwise>
</c:choose> --%>
</div>
<a href="vendorPlatterMenu/" style="float: right;">Return to Events</a>
<div id="menu">
<form id="menuForm" action="vendorPlatterMenu/addNew" method="post"  enctype="multipart/form-data">
<input type="hidden" name="userId" id="userId" value='<%=request.getSession().getAttribute("userId")%>'/>
<input type="hidden" name="vendorId" id="vendorId" value='<%=request.getSession().getAttribute("vendorId")%>'/>
<input type="hidden" name="countryId" id="countryId" value='<%=request.getSession().getAttribute("countryId")%>'/>
<input type="hidden" name="platterId" id="platterId" value="${menu.platterId}"/>
<input type="hidden" name="imageUrl" id="imageUrl" value="${menu.imageUrl}"/>

<table>
<tr>
<td>Name:</td>
<td> <input type="text" name="name"  placeholder="Name" maxlength="100" value="${menu.name}"/>
</td>
</tr>
<tr>
<td>
Description:
</td>
<td> 
<textarea id="menuDescription" name="description" placeholder="Description" maxlength="900" style="width: 640px">${menu.description}</textarea>
</td>
</tr>
<tr>
<td>
Menu Image </td>
<td> (${menu.imageUrl}): <input type="file" name="file"/>
</td> 
</tr>
<tr>
<td>
CT Price:
</td>
<td>
  <input type="number" name="price" id="platterPrice" value="${menu.price}"/> 
</td>
</tr>

<tr>
<td>
Display Price :
</td>
<td>  <input type="number" name="displayPrice" id="platterPrice" value="${menu.displayPrice}"/> 
</td>
</tr>

<tr>
<td>
Min. Participant :
</td>
<td>  <input type="number" name="minAvail" value="${menu.minAvail}"/> 
</td>
</tr>

<tr>
<td>
Max Participant :
</td>
<td>  <input type="number" name="maxAvail" value="${menu.maxAvail}"/> 
</td>
</tr>


<tr>
<td>Valid till/Event date </td>
<td>
<input  type="datetime-local"  name="validity" id="validity" style="width:250px" value="${menu.validity}"   /> 
</td>
</tr>

<tr><td>
Demo video URL:
</td>
<td> 
<input type="text" name="demoUrl"  placeholder="Demo Url" maxlength="100" value="${menu.demoUrl}"/>
</td>
</tr>

<tr><td>
Redirect URL(to third party application): 
</td>
<td> 
<input type="text" name="redirectURL"  placeholder="Redirect Url" maxlength="100" value="${menu.redirectURL}"/>
</td>
</tr>

 <tr>
		<td>Feature this Course</td>
		<td>
		<c:choose>
			<c:when test="${menu.featureCourse}"><input type="checkbox" id="featureCourse" name="featureCourse" checked/></c:when>
			<c:otherwise><input type="checkbox" id="featureCourse" name="featureCourse" /></c:otherwise>
		</c:choose>
		
		</td>
	</tr> 

<%-- <tr>
<td>	
Margin %age</td>
<td> 
<input  name="vendorMargin" style="width:173px"  id="vendorMargin" placeholder="percentage" value="${menu.vendorMargin}"  /> 
</td>
</tr> --%>

<tr>
<td>
Status :
</td>
<td> 
<select name="status">
<c:forEach items="${statusTypes}" var="statusType">
	<c:choose>
		<c:when test="${statusType == menu.status }">
			<option value="${statusType}" selected="selected">${statusType}</option>
		</c:when>
		<c:otherwise>
			<option value="${statusType}">${statusType}</option>
		</c:otherwise>
	</c:choose>
</c:forEach>
</select>
</td>
</tr>

<tr>
<td>Type:</td>
<td>
<select name="platterType" id="platterType">
<c:forEach items="${platterTypeList}" var="platterType">
	<c:choose>
		<c:when test="${platterType == menu.platterType }">
			<option value="${platterType}" selected="selected">${platterType}</option>
		</c:when>
		<c:otherwise>
			<option value="${platterType}">${platterType}</option>
		</c:otherwise>
	</c:choose>
</c:forEach>
</select>
</td>

</tr>

<tr>

<c:if test="${menu.platterId!=null}">
<td>Marketing link</td>
<td> 

<c:if test="${menu.platterType=='COURSE'}">
<input style="width:100%" type="text" id="myInput${menu.platterId}"  value="https://letschalktalk.com/#/menu?id=${menu.platterId}">
</c:if>

<c:if test="${menu.platterType=='WEBINAR'}">
<input style="width:100%" type="text" id="myInput${menu.platterId}"  value="https://letschalktalk.com/#/webprofile?id=${menu.platterId}">
</c:if>
</td>
<td><input type="button"  onclick="copyFunction(${menu.platterId})" value="Copy Link">  </td>
</c:if>
</tr>

<tr>
<c:if test="${menu.tagList !=null}">
<td>Selected Tags: </td>
<td> <input style="width:100%" type="textarea" disabled value="${menu.tagList}">
</td>
</c:if>
</tr>

<%-- <tr>
<td>
Add/override Tags </td>
<td>
<input type="email" list="emails" name="tagList" placeholder="Add comma (,) seperated tags. eg java EE, Java SE, Python" multiple style="width:100%">
<datalist id="emails">
<c:forEach items="${cuisineTypeList}" var="cuisineType">
			<option value="${cuisineType.name}"></option>
</c:forEach>
</datalist>
</td>
</tr> --%>

<tr><td>
Class Mode : 
</td>
<td>
<select name=classMode>
				<c:forEach items="${classMode}" var="cM">
					<c:choose>
						<c:when test="${cM == menu.classMode}">
							<option value="${cM}" selected="selected">${cM}</option>
						</c:when>
						<c:otherwise>
							<option value="${cM}">${cM}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
</select>
</td>
</tr>
 
<%-- <tr><td>
POS Visible Only :</td>
<td>
<c:choose>
		<c:when test="${menu.posVisible}"><input type="checkbox" id="posVisible" name="posVisible" checked /></c:when>
		<c:otherwise><input type="checkbox" id="posVisible" name="posVisible" /></c:otherwise>
</c:choose> </td>
</tr> --%>

</table>
<ul id="section">
<c:if test="${!empty menu.sections}">
<c:set var="sectionCount" value='0'/>
<c:forEach items="${menu.sections}" var="section">
<c:if test="${!empty menu.sections}">
<li id="section${sectionCount}" class="section ui-state-default"> 
	<span class="sectionNameText">${section.name} </span> 
	<input type="hidden" class="addedDishIds" name="sections[${sectionCount}].dishIds" value=""/>
	<input type="hidden" class="sectionSectionId" name="sections[${sectionCount}].sectionId" value="${section.sectionId}"/>
	<input type="hidden" class="sectionName" maxlength="30" name="sections[${sectionCount}].name" value="${section.name}"/>
	<input type="hidden" class="sectionShortName" maxlength="30" name="sections[${sectionCount}].shortName" value="${section.shortName}"/>
	<input type="hidden" class="sectionPlatterSectionType" maxlength="5" name="sections[${sectionCount}].platterSectionType" value="${section.platterSectionType}"/>
	<input type="hidden" class="sectionPrice" maxlength="5" name="sections[${sectionCount}].price" value="${section.price}"/>
	<input type="hidden" class="sectionSectionTypeId" maxlength="5" name="sections[${sectionCount}].sectionTypeId" value="${section.sectionTypeId}"/>	
	<input type="hidden" class="sectionDescription" maxlength="900" name="sections[${sectionCount}].description" value="${section.description}"/>
	<%-- <input type="hidden" class="sectionHeader" name="sections[${sectionCount}].header" value='${section.header}'/>
	<input type="hidden" class="sectionFooter" maxlength="50" name="sections[${sectionCount}].footer" value="${section.footer}"/> --%>
	<input type="hidden" name="sections[${sectionCount}].valid" class="validSection" value="true"/>
	<input type="hidden" name="sections[${sectionCount}].position" class="position" value="${sectionCount}"/>
	<button type="button" onclick="removeSection($(this));" style="float:right">x</button>
	<button type="button" onclick="editSection($(this));" style="float:right">E</button>
	<button type="button" class="addDish" onclick="addDishes('dish${sectionCount}')" style="float:right">+</button>
	<input type="button" class="sectionSectionTypeName"  style="float:right" value="${section.sectionType.name}">
	<input type="button" class="sectionPlatterSectionTypeName" maxlength="5" id="dish${sectionCount}platterSectionTypeName" name="sections[${sectionCount}].platterSectionType" value="${section.platterSectionType}"/>
	
	<ul id="dish${sectionCount}" class="dish" >
		<c:if test="${!empty section.dishes}">
			<c:forEach items="${section.dishes}" var="dish">
				<li class="ui-state-default" style="font-size:0.8em;" data-dishid="${dish.dishId}"  data-dishname="${dish.name}">${dish.name} 
					<button type="button" class="removeDish" onclick="removeDish($(this));" style="float:right">-</button>
					
					<c:if test="${ section.platterSectionType == 'MAIN_PLATTER'}">
							<c:choose>
								<c:when test="${dish.replaceable}"><a title="unMark as replacement"><input type="checkbox" id="checkboxdish${sectionCount}${dish.dishId}" checked  style="float:right"/></a></c:when>
								<c:otherwise>
								<a title="Mark as replacement"><input type="checkbox" id="checkboxdish${sectionCount}${dish.dishId}" style="float:right"></a>
								<input type="button" id="replacabale${sectionCount}${dish.dishId}" onclick="addReplacableDishes(${dish.dishId} , $(this));" value="Rep" style="float:right">
							 	<font style="font-size:10px;color:red;"> Rep: </font>
							 	<input id="replaceabledish${sectionCount}${dish.dishId}" size="120" disabled type="text" value="${dish.replacementNames}">
							 	<input id="replaceableIddish${sectionCount}${dish.dishId}" disabled type="text" value="${dish.replacements}" hidden="true">
							 
								</c:otherwise>
							</c:choose>
					</c:if>
					<c:if test="${ section.platterSectionType == 'ADD_ON'}">
					 <input type="number" id="pricedish${sectionCount}${dish.dishId}" value="${dish.price}" style="float:right" placeholder="Actual Price">
					 <input type="number" id="displayPricedish${sectionCount}${dish.dishId}" value="${dish.displayPrice}" style="float:right" placeholder="Display Price">
					</c:if>
				</li>	
			</c:forEach>
		</c:if>	
	</ul>
</li>
</c:if>



<c:set var="sectionCount" value='${sectionCount + 1}'/>
</c:forEach>
</c:if>
</ul>
<button type="button" id="addSomeSection" onclick="openSectionForm();">Add Section</button> <br/>

<c:choose>
	<c:when test="${!empty menu.platterId}"><input type="button" onclick="submitMenu();" value="Update"/></c:when>
	<c:otherwise><input type="button" onclick="submitMenu();" value="Save"/></c:otherwise>
</c:choose>
</form>
</div>


<form id="sectionForm" hidden="true" title="Create Section" style="width:400px;">
<input type="hidden" id="sectionId" name="sectionId" />
Name: <input type="text" id="name" name="name" placeholder="Name" />
Price: <input type="text" id="price" name="price" style="float: right;" placeholder="Price"/><br/>
<!-- <input type="text" id="shortName" name="shortName" placeholder="Short Name" /><br/> -->
Description <input type="text" id="description" name="description" placeholder="Description"/> <br/>
<!-- <input type="text" id="header" name="header" placeholder="Header"/> <br> -->
Section Type: 
<select name="sectionTypeId" id="sectionTypeId">
<c:forEach items="${sectionTypeList}" var="sectionType">
	<c:choose>
		<c:when test="${sectionType.sectionTypeId == section.sectionTypeId }">
			<option value="${sectionType.sectionTypeId}" selected="selected">${sectionType.name}</option>
		</c:when>
		<c:otherwise>
			<option value="${sectionType.sectionTypeId}">${sectionType.name}</option>
		</c:otherwise>
	</c:choose>
</c:forEach>
</select><br/>
Platter Type: 
<select name="platterSectionType" id="platterSectionType">
<c:forEach items="${platterTypeList}" var="platterType">
	<c:choose>
		<c:when test="${platterType == section.platterSectionType }">
			<option value="${platterType}" selected="selected">${platterType}</option>
		</c:when>
		<c:otherwise>
			<option value="${platterType}">${platterType}</option>
		</c:otherwise>
	</c:choose>
</c:forEach>
</select><br/>

<!-- <input type="text" id="footer" name="footer" placeholder="Footer" /><br> -->
<input type="button" id="addSection" value="Add Section" />
<input type="button" id="saveSection" value="Save Section" data-sectionCount="" hidden="true"/>
</form>

<form id="dishSection" hidden="true" title="Select Dishes">
<select id="dishIdList" name="selectedDishIds" multiple="multiple" style="width: 400px">
	<c:if test="${!empty dishList}">
		<c:forEach items="${dishList}" var="dishVar">
			<option value="${dishVar.dishId}">${dishVar.name}</option>
		</c:forEach>
	</c:if>
</select>
<input type="button" id="addDish" onclick="addSelectedDish();" value="Add Selected Dishes" />
</form>



<c:set var="replacableCount" value='0'/>
<c:forEach items="${menu.sections}" var="section">
<c:if test="${!empty menu.sections}">
<c:if test="${section.platterSectionType=='MAIN_PLATTER'}">
<form id="dishReplacableSectiondish${replacableCount}" hidden="true" title="Select Replaceable">
		<select id="replacedish${replacableCount}" hidden="true" name="selectedDishIds" multiple="multiple" style="width: 400px">
			<c:if test="${!empty section.dishes}">
					<c:forEach items="${section.dishes}" var="dishVar">
					   <c:if  test="${dishVar.replaceable==true}" >
						<option value="${dishVar.dishId}">${dishVar.name}</option>
						</c:if>
					</c:forEach>
				</c:if>
			</select>
			
	<input type="text" hidden="true" id="addDishIdRepdish${replacableCount}"/>
	<input type="button" id="addReplacabledish${replacableCount}" onclick="addSelectedReplacableToDish(${replacableCount});" value="Add Selected Replacable Dishes" />
</form>
</c:if>
</c:if>
<c:set var="replacableCount" value='${replacableCount + 1}'/>
</c:forEach>



</body>
</html>
