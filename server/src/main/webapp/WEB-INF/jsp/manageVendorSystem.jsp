<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<title>Manage <%
	if (request.getSession().getAttribute("vendorName") != null) {
%> <%=request.getSession().getAttribute("vendorName")%> <%
 	}
 %>
</title>
<base href="${pageContext.request.contextPath}/" />
<link rel="stylesheet" href="css/style.css" />

<link rel="stylesheet" href="themes/base/jquery.ui.all.css" />
<script type="text/javascript" src="js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript" src="js/ui/jquery-ui.js"></script>
<script src="js/jquery.validationEngine-en.js" type="text/javascript"
	charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript"
	charset="utf-8"></script>

<link rel="stylesheet" href="css/validationEngine.jquery.css"
	type="text/css" />
<style type="text/css">
body {
	font-family: sans-serif;
}

.data, .data td {
	border-collapse: collapse;
/* 	width: 100%; */
	border: 1px solid #aaa;
	margin: 2px;
	padding: 2px;
}

.data th {
	font-weight: bold;
	background-color: #808080;
	color: white;
}

.leftPad {
	padding-left: 8px;
}

h3 {
	color: #FFBB43
}

h2 {
	display: block;
	font-size: 1.2em;
	margin-top: 0.83em;
	margin-bottom: 0.83em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
}

nav {
	float: left;
	max-width: 160px;
	margin: 0;
	padding: 1em;
}

nav ul {
	list-style-type: none;
	padding: 0;
}

nav ul a {
	text-decoration: none;
}

#mainDiv {
	margin-left: 170px;
	border-left: 1px solid gray;
	padding: 1em;
	overflow: hidden;
}
</style>
<script type="text/javascript">
		function deleteMenu(platterId) {
			if (confirm('Do you really want to delete this Menu')) {
				window.location.href = 'vendorPlatterMenu/delete/' + platterId;
			}
		}
</script>
<script type="text/javascript">

	bkLib.onDomLoaded(function() { 
		//nicEditors.allTextAreas()
		var nicEditorInstance = new nicEditor({fullPanel : false, iconsPath: 'images/nicEditorIcons.gif', buttonList : ['bold','italic','underline','left','center','right', 'justify', 'ol', 'ul', 'subscript', 'superscript', 'strikethrough', 'removeformat', 'indent', 'outdent', 'hr', 'forecolor', 'bgcolor', 'fontSize', 'fontFamily', 'fontFormat']});
		nicEditorInstance.panelInstance('closedText');
		//nicEditorInstance.panelInstance('description');
	}); 
	



	</script>
</head>
<body>
	<a href="vendor/listVendor" style="float: right;">Back</a>
<br/>
	<nav id="myNav">
		<ul>


			<c:forEach items="${vendorPops}" var="entry">
				<li class="item"><a href="${entry.key}">${entry.value}</a></li>
			</c:forEach>

		</ul>
	</nav>


	<div id="mainDiv">

		<c:set var="sessionUserId"
			value='<%=request.getSession().getAttribute("userId")%>' />
		<c:if test='${!empty sessionUserId}'>
				Logged in as <%=request.getSession().getAttribute("username")%>
			<a href="user/logout" style="float: right;">Logout</a>
		</c:if>
<br/>
		<a href="vendorPlatterMenu/create">Create menu</a><br>
		<hr />

		<h3>Menu List</h3>
		<c:if test="${!empty plattersList}">
			<table class="data">
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Type</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
				<c:forEach items="${plattersList}" var="platter">
					<tr>
						<td width=20%>${platter.name}</td>
						<td  width=40%>${platter.description}</td>
						<td  width=10%>${platter.platterType}</td>
						<td><button type="button"
								onclick="deleteMenu(${platter.platterId})">Delete</button></td>
						<td><button type="button"
								onclick="window.location.href='vendorPlatterMenu/edit/${platter.platterId}';">Edit</button></td>
						<td><button type="button"
								onclick="window.location.href='vendorPlatterMenu/show/${platter.platterId}';">Show</button></td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</div>

</body>
<script>
	 $(function() {
		var v = document.getElementById('chkBox_maxCount');
		enable_text(v);	
		
		var dur = document.getElementById('chkBox_duration');
		enable_duration(dur);	
		
		
		
		
		$("#coupon").validationEngine();
	}); 
  $(function(){
		$("#endDate").datepicker({
		    dateFormat: 'yy-mm-dd',
		    maxDate:"100Y",
			minDate:"0Y",
			changeYear:true,
			changeMonth:true
		})
	})
	$(function(){
		$("#startDate").datepicker({
		    dateFormat: 'yy-mm-dd',
		    maxDate:"100Y",
			minDate:"0Y",
			changeYear:true,
			changeMonth:true
		})
	})
		
</script>

</html>
