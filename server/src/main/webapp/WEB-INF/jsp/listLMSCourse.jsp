<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
	<title>Course</title> 
	
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		.data, .data td {
			border-collapse: collapse;
			width: 100%;
			border: 1px solid #aaa;
			margin: 2px;
			padding: 2px;
			text-align:center;
			vertical-align:middle;
		}
		.data th {
			font-weight: bold;
			background-color: #5C82FF;
			color: white;
		}
	

/* .data td {
	border: none;
	margin-left: 50 px;
} */

.leftPad {
	padding-left: 8px;
}

h3 {
	color: #FFBB43
}

h2 {
	display: block;
	font-size: 1.2em;
	margin-top: 0.83em;
	margin-bottom: 0.83em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
}

nav {
	float: left;
	max-width: 160px;
	margin: 0;
	padding: 1em;
}

nav ul {
	list-style-type: none;
	padding: 0;
}

nav ul a {
	text-decoration: none;
}
img{
	height: 150px;
    width: 250px;
}
#mainDiv {
	margin-left: 170px;
	border-left: 1px solid gray;
	padding: 1em;
	overflow: scroll;
}
</style>
	<base href="${pageContext.request.contextPath}/"/>
	<link rel="stylesheet" href="css/style.css" />
	<script type="text/javascript" src="js/nicEdit.js"></script>

	<script type="text/javascript" src="js/jquery-1.9.0.js"></script>
	<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="themes/base/jquery.ui.all.css">
	<link rel="stylesheet" type="text/css" href="themes/base/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css" href="themes/base/jquery.multiselect.filter.css" />
	<script src="js/jquery-1.9.0.js"></script>
	<script src="js/ui/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.filter.js"></script>	
	
	<script type="text/javascript">
		bkLib.onDomLoaded(function() {
			//nicEditors.allTextAreas()
			var nicEditorInstance = new nicEditor({fullPanel : false, iconsPath: 'images/nicEditorIcons.gif', buttonList : ['bold','italic','underline','left','center','right', 'justify', 'ol', 'ul', 'subscript', 'superscript', 'strikethrough', 'removeformat', 'indent', 'outdent', 'hr', 'forecolor', 'bgcolor', 'fontSize', 'fontFamily', 'fontFormat']});
		
			nicEditorInstance.panelInstance('shortDescription');
    	   $(".nicEdit-main").attr('id', 'smallDescription');
            $(".nicEdit-main").attr('tabindex', '1');
            $(".nicEdit-main").attr('oninput', 'smallDescriptionLimitText(this,4950);'); 
		}); 

		function deleteCourse(courseId) {
			if (confirm('Do you really want to delete this course')) {
				window.location.href = 'vendorDish/deleteLMSCourse/' + courseId;
			}
		}
			
	</script>
</head>
<body>
<a href="vendor/listVendor" style="float: right;">Return to Home</a>
<%-- 	<nav id="myNav">
		<ul>

			
				<c:forEach items="${vendorPops}" var="entry">
					<li class="item"><a href="${entry.key}">${entry.value}</a></li>
				</c:forEach>
		
		</ul>
	</nav> --%>
<div id="">
<%-- <form:form method="post" action="vendorDish/addLMSCourse.html" modelAttribute="courseLMS" commandName="courseLMS" enctype="multipart/form-data">
	<form:hidden path="courseId"/>
	<form:hidden path="countryId" value='<%=request.getSession().getAttribute("countryId")%>'/>
	<form:hidden path="vendorId" value='<%=request.getSession().getAttribute("vendorId")%>'/>
	<table>
	<tr>
		<td><form:label path="name"><spring:message code="label.name"/>* </form:label></td>
		<td><form:input path="name" maxlength="100" class="validate[required]" /></td> 
	</tr>
		<tr>
		<td><form:label path="description"><spring:message code="label.description"/></form:label></td>
		<td><textarea id="description" name="description"  placeholder="Description" style="width:680px;" >${vendorDish.description}</textarea></td>
	</tr>
	<tr>
		<td><form:label path="shortDescription">Bullet Points</form:label></td>
		<td><textarea id="shortDescription" name="shortDescription"  placeholder="Short Description" style="width:680px;">${vendorDish.shortDescription}</textarea></td>
	</tr>

	<tr class="price">
		<td><form:label path="price">CT Price* </form:label></td>
		<td><form:input path="price" maxlength="5" class="validate[required]" /></td>
	</tr>
	<tr class="displayPrice">
		<td><form:label path="displayPrice">Display Price </form:label></td>
		<td><form:input path="displayPrice"  maxlength="5"  /></td>
	</tr>
	
	<tr class="displayPrice">
		<td><form:label path="courseDuration"> Duration in Days </form:label></td>
		<td><form:input path="courseDuration" placeholder="in days"  maxlength="5"  /></td>
	</tr>
	<tr class="displayPrice">
		<td><form:label path="courseDurationInHr"> Duration in Hrs </form:label></td>
		<td><form:input path="courseDurationInHr" placeholder="in hrs"  maxlength="5"  /></td>
	</tr>
	
	 <tr>
		<td><form:label path="dishTypeId" class="validate[required]"> Category *</form:label></td>
		
		<td>
		<select  name="dishTypeId" id="dishType">
		
			<c:forEach items="${dishTypes}" var="dishType">
				<c:choose>
					<c:when test="${dishType.dishTypeId == vendorDish.dishTypeId }">
						<option value="${dishType.dishTypeId}" selected="selected">${dishType.name}</option>
					</c:when>
					<c:otherwise>
						<option value="${dishType.dishTypeId}">${dishType.name}</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			
		</select>
		</td>
	</tr> 
	<tr>
		<td><form:label path="online">is Online</form:label></td>
		<td>
		<c:choose>
			<c:when test="${vendorDish.online}"><input type="checkbox" id="online" name="online" checked/></c:when>
			<c:otherwise><input type="checkbox" id="online" name="online" /></c:otherwise>
		</c:choose>
		
		</td>
	</tr>
	<tr>
		<td><form:label path="disabled">Disabled</form:label></td>
		<td>
		<c:choose>
			<c:when test="${vendorDish.disabled}"><input type="checkbox" id="disabled" name="disabled" checked /></c:when>
			<c:otherwise><input type="checkbox" id="disabled" name="disabled" /></c:otherwise>
		</c:choose>
		
		
		</td>
	</tr>
	
	<tr>
		<td width="30"><form:label path="imageUrl">
		<spring:message code="label.imageUrl"/>
		<c:choose>
			<c:when test="${fn:startsWith(vendorDish.imageUrl, 'http://')}">(Uploaded)</c:when>
			<c:when test="${fn:startsWith(vendorDish.imageUrl, '/')}">(Uploaded)</c:when>
		</c:choose> 
		</form:label></td>
		<td><input type="file" name="file"/>
		<form:errors path="imageUrl" style="color:red;"/> </td>
	</tr>
	
	<tr>
		<td><form:label path="rectangularImageUrl">
		Rectagular  Image
		<c:choose>
			<c:when test="${fn:startsWith(vendorDish.rectangularImageUrl, 'http://')}">(Uploaded)</c:when>
			<c:when test="${fn:startsWith(vendorDish.rectangularImageUrl, '/')}">(Uploaded)</c:when>
		</c:choose> 
		</form:label></td>
		<td><input type="file" name="file[1]"/>
		<form:errors path="rectangularImageUrl" style="color:red;"/> </td>
	</tr>
	<tr>
		<td><form:label path="courseDetailsURL">
		 Details pdf
		<c:choose>
			<c:when test="${fn:startsWith(vendorDish.courseDetailsURL, 'http://')}">(Uploaded)</c:when>
			<c:when test="${fn:startsWith(vendorDish.courseDetailsURL, '/')}">(Uploaded)</c:when>
		</c:choose> 
		</form:label></td>
		<td><input type="file" name="file[2]"/>
		<form:errors path="courseDetailsURL" style="color:red;"/> </td>
	</tr>
	<tr>
		<td colspan="2">
				<c:choose>
					<c:when test="${!empty dish.dishId}"><input type="submit" value="Save Dish"/><button type="button" onclick="document.location.href='#'">Cancel</button></c:when>
					<c:otherwise><input type="submit" value="Done"/> &nbsp;
					<input type="reset" value="Cancel"></c:otherwise>
				</c:choose>
		</td>
	</tr>
</table>
</form:form> --%>
<hr/>
<h3>Course List</h3>
<c:if  test="${!empty courses}">
<table class="data">
<tr>
	<th>Name</th>
	<th>Start - End Date</th>
	<th>Image</th>
	<th>Price</th>
	<th> Marketing Link</th>
	<th>&nbsp;</th>
	<th>&nbsp;</th>
</tr>
<c:forEach items="${courses}" var="course">
	<tr>
		<td style="width:30%;">${course.name}</td>
		<%-- <td style="width:10%;">${course.course_code}</td> --%>
		<td style="width:30%;">${course.start_at} to  ${course.end_at}</td>
		<td><img height="150" width="150" src="${course.image_download_url}" /></td>
		<td>${course.price}</td>
		<td>www.coursedge.org/#/menu?id=${course.courseId}</td>
		 <td>
		    <c:choose>
				<c:when test="${course.status == 'INACTIVE'}">
	 				<button type="button" onclick="window.location.href='vendorDish/deactivateLMSCourse/${course.courseId}';">Activate</button>
				</c:when>
				<c:otherwise>
			 		<button type="button" onclick="window.location.href='vendorDish/deactivateLMSCourse/${course.courseId}';">Deactivate</button>
				</c:otherwise>
			</c:choose>
	    </td>
		<td><button type="button" onclick="deleteCourse(${course.courseId});">delete</button></td> 
	</tr>
</c:forEach>
</table>
</c:if>
</div>
</body>
<script>
	$(function() {
		 var azimuth = 0;
		$("#dish").validationEngine();
	});

	function smallDescriptionLimitText(textarea,charCount){
		var data = textarea.innerHTML;
		var textLength = data.toString();
       if(textLength.length>charCount){
			alert("Characters  limit exceeded. You can only enter "+charCount+" characters ");
			textarea.innerHTML=textLength.substr(0,charCount);
            }
		}

</script>
</html>
