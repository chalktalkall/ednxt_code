<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
	<title>Login Manager</title>
	<base href="${pageContext.request.contextPath}/"/>
	<link rel="stylesheet" href="css/style.css" />
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		.data, .data td {
			border-collapse: collapse;
			width: 100%;
			border: 1px solid #aaa;
			margin: 2px;
			padding: 2px;
		}
		.data th {
			font-weight: bold;
			background-color: #5C82FF;
			color: white;
		}
		.formCss{
			justify-content: center;
    		display: flex;
    		padding-top: 100px;
		}
		
		form {
	     background: #ffffff26;
    border: 3px solid hsl(0deg 0% 100% / 7%);
    border-radius: 10px;
		}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
     background-color: #0268b3;
    color: white;
    padding: 9px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    font-size: 17px;
    width: 100%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
      width: 34%;
    height: 60px;
 /*  border-radius: 50%; */
}

.container {
  padding: 16px;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
	</style>
</head>
<body class="homepage">
<!-- <div data-role="header" data-id="globalHeader" data-theme="d" data-position="fixed" style="background: #ffffff !important; box-shadow:1px 1px 0px 1px #e4e9eb8c !important">
			<div class="header-content" style="    max-width: 100% !important;padding-right: 20px;">
			 <div class="restaurantName"><span class="restaurantLink"></span> 
			<span class="poweredBy">
			<img alt="Powered by CoursEdge" style="width: 130px !important; height: 32px;padding-left: 10px; " src="images/ce.png"/>
			</span></div> 
			</div>
</div> -->
<div  class="formCss">

<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
      <font color="red">
        <c:out value="Invalid Username and Password!"/>
      </font>
    </c:if>
  <form name='form' action='j_spring_security_check' method='POST'>
  
 <div class="imgcontainer">
    <img src="images/ce.png" alt="Avatar" class="avatar">
  </div> 

  <div class="container">
    <label for="uname"><b>Username</b></label>
    <input type='text' name='j_username' value='' placeholder="Enter Username" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name='j_password' required>
        
    <button name="submit" type="submit" value="Login">Login</button>
    <label>
      <input type="checkbox" checked="checked" name="remember-me" > Remember me
    </label>
   <!--   <label>
      <a href="user/forgotPassword"> Forgot Password</a>
    </label> -->
  </div>

<!--   <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn">Cancel</button>
  </div> -->
  
<!-- 	 <table>
	    <tr>
	      <td>User Name:</td>
	      <td><input type='text' name='j_username' value=''></td>
	    </tr>
	    <tr>
	      <td>Password:</td>
	      <td><input type='password' name='j_password'/></td>
	    </tr>
	    <tr>
		  <td>Remember Me: </td>
			 <td><input type="checkbox" name="remember-me" /></td>
		  </tr>
	    <tr>
	      <td colspan='2'>
	        <input name="submit" type="submit" value="Login"/></td>
	    </tr>
	  </table> -->
  </form>
<!-- <a href="user/signup">Signup</a>
<a href="user/forgotPassword"> Forgot Password</a><br>
<a href="socialauth?id=facebook"><img src="images/images.jpg" alt="Facebook" title="Facebook" border="0"></img></a>
<a href="socialauth?id=googleplus"><img src="images/sign-in-with-google.png" alt="Gmail" title="Gmail" border="0"></img></a>
 --></div>