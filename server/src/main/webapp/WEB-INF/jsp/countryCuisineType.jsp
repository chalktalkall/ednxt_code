<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Cuisine Type Manager</title>
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		.data, .data td {
			border-collapse: collapse;
			width: 100%;
			border: 1px solid #aaa;
			margin: 2px;
			padding: 2px;
		}
		.data th {
			font-weight: bold;
			background-color: #5C82FF;
			color: white;
		}
	</style>
	<base href="${pageContext.request.contextPath}/"/>
	<link rel="stylesheet" href="css/style.css" />
	<script src="js/jquery-1.9.0.js"></script>
	<script src="js/ui/jquery-ui.js"></script>

</head>
<body>
<a href="manageRestaurant.jsp" style="float: right;">Return to Home</a>
<hr/>
<h3>Add Cuisine Type</h3>
<form:form method="post" action="cuisineDishType/addCuisineType" commandName="cuisineType">

	<form:hidden path="cuisineTypeId" />
	<form:hidden path="countryId" value='<%=request.getSession().getAttribute("countryId")%>'/>
	<table>
	
	<tr>
		<td><form:label path="name"><spring:message code="label.name"/></form:label></td>
		<td><form:input path="name"  maxlength="100" required="true"/></td> 
	</tr>
	<tr>
	<td><form:label path=""></form:label></td>
	
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="Save"/>
		</td>
	</tr>
</table>	
</form:form>
<hr/>	
<h3>Cuisine Types</h3>
<c:if  test="${!empty cuisineTypeList}">
<table class="data">
<tr>
	<th>Name</th>
	<th>&nbsp;</th>
	<th>&nbsp;</th>
</tr>
<c:forEach items="${cuisineTypeList}" var="cuisineType">
	<tr>
		<td>${cuisineType.name}</td>
		<td><a href="cuisineDishType/deleteCuisine/${cuisineType.cuisineTypeId}">delete</a></td>
		<td><a href="cuisineDishType/editCuisineType/${cuisineType.cuisineTypeId}">edit</a></td>
	</tr>
</c:forEach>
</table>
</c:if>


</body>
</html>
