	<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
	<title>List Campus</title>
	<base href="${pageContext.request.contextPath}/"/>
	<link rel="stylesheet" href="css/style.css" />
	
	<link rel="stylesheet" href="themes/base/jquery.ui.all.css" />	
	<script type="text/javascript" src="js/jquery-1.9.0.js"></script>
	<script type="text/javascript" src="js/nicEdit.js"></script> 
	<script type="text/javascript" src="js/ui/jquery-ui.js"></script>
	<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
	
		
	
	
	</script>
	<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		.data, .data td {
			border-collapse: collapse;
			width: 100%;
			border: 1px solid #aaa;
			margin: 2px;
			padding: 2px;
		}
		.data th {
			font-weight: bold;
			background-color: #5C82FF;
			color: white;
		}
		#user td{
		padding-bottom:10px;
		}
		h3{color:#FFBB43}
		.underline {
    text-decoration: underline;
}
		
		
nav {
    float: left;
    max-width: 160px;
    margin: 0;
    padding: 1em;
}

nav ul {
    list-style-type: none;
    padding: 0;
}
   
nav ul a {
    text-decoration: none;
}

#mainDiv {
    margin-left: 170px;
    border-left: 1px solid gray;
    padding: 1em;
    overflow: hidden;
}
	</style>
	
	<script type="text/javascript">

	bkLib.onDomLoaded(function() { 
		//nicEditors.allTextAreas()
		var nicEditorInstance = new nicEditor({fullPanel : false, iconsPath: 'images/nicEditorIcons.gif', buttonList : ['bold','italic','underline','left','center','right', 'justify', 'ol', 'ul', 'subscript', 'superscript', 'strikethrough', 'removeformat', 'indent', 'outdent', 'hr', 'forecolor', 'bgcolor', 'fontSize', 'fontFamily', 'fontFormat']});
		nicEditorInstance.panelInstance('closedText');
		//nicEditorInstance.panelInstance('description');
	}); 
	
	function copyFunction(id) {
		  /* Get the text field */
		  var copyText = document.getElementById("myInput"+id);

		  /* Select the text field */
		  copyText.select();
		  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

		  /* Copy the text inside the text field */
		  document.execCommand("copy");

		  /* Alert the copied text */
		  alert("Copied the text: " + copyText.value);
		}



	</script>
</head>
<body>
<a href="manageRestaurant.jsp" style="float: right;">Return to Home</a>

<nav id="myNav">
  <ul>
    <li><a href="vendor/createVendor">Create  Campus</a></li>
	<li><a href="vendor/listVendor">List Campus</a></li>  
  </ul>
</nav>


<div id="mainDiv">

<div style="color:red;">${errorMsg}</div>

<h3>Campus</h3>



<c:if  test="${!empty vendorList}">
<table class="data">
<tr>
	<th>Buisness Name</th>
	<th>City</th>
	<th>State</th>
		<th>Source</th>
	<th>Link</th>

	<th>&nbsp;</th>	
	<th>&nbsp;</th>
	<th>&nbsp;</th>
	<th>&nbsp;</th>
</tr>
<c:forEach items="${vendorList}" var="vendor">
<%-- <c:if test="${coupon.state != 'NonActive'}"> --%>
	<tr>
		<td class="underline" style="width:20%;"><a href="vendor/view/${vendor.vendorId}">${vendor.businessName}</a></td>
		<td style="width:10%;">${vendor.city}</td>
		<td style="width:10%;">${vendor.state}</td>
			<td style="width:10%;">${vendor.lms_name}</td>
		<td style="width:30%;">
		<input type="text"  value="https://coursedge.org/#/institute?id=${vendor.vendorId}" id="myInput${vendor.vendorId}" >
			<button  onclick="copyFunction(${vendor.vendorId})"> Copy Link </button></td>
	<td  style="width:10%;"><button type="button" onclick="window.location.href='vendor/disableEnable/${vendor.vendorId}';">
		<c:choose>
			<c:when test="${vendor.status == 'INACTIVE'}">
 				Enable 
			</c:when>
			<c:otherwise>
		 		Disable 
			</c:otherwise>
		</c:choose>
	</button></td>
	
		<td  style="width:10%;"><button  type="button" onclick="window.location.href='vendor/edit/${vendor.vendorId}';">Edit</button></td>
		<td style="width:20%;"><button onclick="submitPoll(${vendor.vendorId})" type="button"id="lmsData">Fetch Data</button></td>
		<td  style="width:20%;"><button  type="button" onclick="window.location.href='vendorDish/getLMSCourses/${vendor.vendorId}';">Fetched Courses</button></td>
	<%-- 	<td><button type="button" onclick="deleteVendor(${vendor.vendorId});">Delete</button></td> --%>
	</tr>

</c:forEach>
</table>
</c:if>

</div>

</body>
<script>
	/* $(function() {
		$("#coupon").validationEngine();
	}); */

	function submitPoll(vendorId){
		if(confirm("Thank you for the request. Your LMS Data will be updated in sometime. Please wait for few mins.")){
	      document.getElementById("lmsData").disabled = true;
	      //setTimeout(function(){document.getElementById("lmsData").disabled = false;},100000);
	      window.location.href = 'vendor/fetchLMSData/'+vendorId;
		}
  }
		
</script>

</html>
