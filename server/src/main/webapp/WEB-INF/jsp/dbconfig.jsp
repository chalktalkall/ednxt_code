<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>DB Config Manager</title>
<style type="text/css">
body {
	font-family: sans-serif;
}

.data, .data td {
	border-collapse: collapse;
	width: auto;
	border: 1px solid #aaa;
	margin: 2px;
	padding: 2px;
}

.data th {
	font-weight: bold;
	background-color: #5C82FF;
	color: white;
}
</style>
<base href="${pageContext.request.contextPath}/" />
<link rel="stylesheet" href="css/style.css" />
<script src="js/jquery-1.9.0.js"></script>
<script src="js/ui/jquery-ui.js"></script>
<script type="text/javascript">
		function deleteTax(Id) {
			if (confirm('Do you really want to delete this Configuration')) {
				window.location.href = 'vendorDBConfig/delete/' + Id;
			} 
			
		}
		function showTaxDiv(){
			if($('#override').is(':checked')){
				 $("#taxDiv").html($("#postTaxList").html());
				 $("#selectText").show();
			}
			else {
				$("#selectText").hide();
				$("#taxDiv").html("");
				}
			}

		  window.onload = function() {
	               var azimuth = 0;
	            	var value= "${taxType.overridden}";
	            	if(value!="" && value!=undefined){
	            		$('#override').prop('checked', true);
	            		showTaxDiv();
		            	}
	               
	      }
	</script>

</head>
<body>
	<a href="manageRestaurant.jsp" style="float: right;">Return to Home</a>
	<hr />
	<h3>Add Charges</h3>
	<form:form method="post" action="vendorDBConfig/add" commandName="dbConfig">

		<form:hidden path="id" />
		<form:hidden path="univId"
			value='<%=request.getSession().getAttribute("countryId")%>' />
		<table>
			<tr>
				<td>Select Campus*</td>
				<td><select name="campusId">
						<c:forEach items="${campusList}" var="campus">
							<c:if test="${campus.lms_name == 'DB_VIEW'}">
								<c:choose>
									<c:when test="${campus.vendorId == dbConfig.campusId}">
										<option value="${campus.vendorId}" selected="selected">${campus.vendorName}</option>
									</c:when>
									<c:otherwise>

										<option value="${campus.vendorId}">${campus.vendorName}</option>

									</c:otherwise>
								</c:choose>
							</c:if>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td><form:label path="name">name*</form:label></td>
				<td><form:input path="name" maxlength="45" required="True" /></td>
			</tr>
			<tr>
				<td>DB Type*</td>
				<td><select name="dbType">
						<c:forEach items="${dbType}" var="dbType">
							<c:choose>
								<c:when test="${dbType == dbConfig.dbType}">
									<option value="${dbType}" selected="selected">${dbType}</option>
								</c:when>
								<c:otherwise>
									<option value="${dbType}">${dbType}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td><form:label path="dbUrl">DB URL</form:label></td>
				<td><form:input type="text" path="dbUrl" required="true" /></td>
			</tr>
			<tr>
				<td><form:label path="username">DB User Name</form:label></td>
				<td><form:input type="text" path="username" required="true" /></td>
			</tr>
			<tr>
				<td><form:label path="password">DB Password</form:label></td>
				<td><form:input type="password" path="password" required="true" /></td>
			</tr>
			<tr>
				<td><form:label path="dbPort">DB Port</form:label></td>
				<td><form:input type="number" path="dbPort" required="true" /></td>
			</tr>
			<tr>
				<td><form:label path="dbView">DB View</form:label></td>
				<td><form:input type="text" path="dbView" required="true" /></td>
			</tr>
			<tr>
				<td><form:label path="dbInvoiceTable">DB Invoice Table</form:label></td>
				<td><form:input type="text" path="dbInvoiceTable"
						required="true" /></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit" value="Add/Update" /></td>
			</tr>
		</table>
	</form:form>

	<hr />
	<h3>DB Instances</h3>
	<c:if test="${!empty dbConfigList}">
		<table class="data">
			<tr>
				<th style="width: 15%">Name</th>
				<th style="width: 15%">URL</th>
				<th style="width: 15%">User Name</th>
				<th style="width: 15%">View</th>
				<th style="width: 15%">Invoice Table</th>
				<th style="width: 15%">&nbsp;</th>
				<th style="width: 15%">&nbsp;</th>
			</tr>
			<c:forEach items="${dbConfigList}" var="dbConfig">
				<tr>
					<td width="15%">${dbConfig.name}</td>
					<td width="15%">${dbConfig.dbUrl}</td>
					<td width="15%">${dbConfig.username}</td>
					<td width="15%">${dbConfig.dbView}</td>
					<td width="15%">${dbConfig.dbInvoiceTable}</td>
					<td width="15%"><button type="Button"
							onclick="deleteTax(${dbConfig.id});">delete</button></td>
					<td width="15%"><button type="Button"
							onclick="window.location.href='vendorDBConfig/edit/${dbConfig.id}';">edit</button></td>
				</tr>

			</c:forEach>
		</table>
	</c:if>
</body>
<script>
</script>
</html>
