<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:th='http://www.thymeleaf.org'>
    <head>
        <title>Checkout</title>
        <style>
            body {
                font-family: 'arial';
            }
            #checkout-form input,
            #checkout-form button {
                display: block;
                margin: 12px;
            }
            .stripe-button-el span{
            position: relative;
		    padding: 0 12px;
		    height: 30px;
		    line-height: 30px;
		    background: #1275ff;
		    background-image: -moz-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
		    background-image: -ms-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
		    background-image: -o-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
		    background-image: -moz-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
		    background-image: -ms-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
		    background-image: -o-linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
		    background-image: linear-gradient(#7dc5ee,#008cdd 85%,#30a2e4);
		    font-size: 14px;
		    color: white;
		    font-weight: bold;
		    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		    text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
		    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
		    -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
		    -ms-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
		    -o-box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
		    box-shadow: inset 0 1px 0 rgba(255,255,255,0.25);
		    -webkit-border-radius: 4px;
		    -moz-border-radius: 4px;
		    -ms-border-radius: 4px;
		    -o-border-radius: 4px;
		    border-radius: 4px;
		    -webkit-column-rule-color: rgb(255 255 255);
		    -webkit-text-emphasis-color: rgb(255 255 255);
		    -webkit-text-fill-color: #ffffff;
		    -webkit-text-stroke-color: rgb(0, 0, 0);
		    border-bottom-color: rgb(12 11 11);
		    border-left-color: rgb(0, 0, 0);
            }
    html { background-color: light-gray; }
	@media only screen and (min-width: 1024px){
		.header-content, #restaurants-content, #closed-content, #order-content, #check-content, #menu-content, .footer-content {
			max-width: 1024px !important;
			margin: 0 auto !important;
		}
	}
		#popupContact form, #popupNameAddress form, #popupEmail form{
		padding:10px 20px;
	}
	
	p.Charges_message{
		width:20em; 
		text-wrap:none;
		text-align:center;
     }
	#check-content {
		font-family: courier;
		font-size: smaller;
	}
	.controlSeparator {
		cursor: pointer;
		font-weight: bold;
		float: right;
	}
	#email-check {
		background: url("shared/css/images/at.png") no-repeat scroll center 0 rgba(0, 0, 0, 0);
		height: auto;
		padding: 25px;
		text-align: center;
		z-index: 100;
	}
	.header-content .right-controlgroup .ui-controlgroup-controls a:last-child {
		padding-right:0;
	}
	body {
	  font-family: "Lucida Sans Unicode", "Lucida Grande", "sans-serif";
	  font-size: 14px;
	  line-height: 2em;
	  letter-spacing: 0px;
	  text-align: left;
	  font-weight: normal;
	  font-style: normal;
	  background-color: light-gray;
	}

	#email-check a {
		text-decoration: none;
	}

	h3.sectionTitle, .itemContent .title {
		font-size: 18px;
		font-weight: bold;
	}
	h3.sectionTitle {
		text-transform:uppercase;
	}
	.ui-footer-fixed {
		height: 46px;
		clear:both;
	}
	.fb-like, .g-plus-container, .pin-it, .tweet {
		float:left;
		padding-right:6px;
		display:none;
	}
	.g-plus-container, .pin-it, .tweet {
		padding-top:8px;
	}
	.poweredBy {
		vertical-align: bottom;
	}
	.restaurantName img, .poweredBy img {
		height:20px;
		width:20px;
		vertical-align: text-bottom;
	}
	.restaurantName 
	{
		    padding: 8px;
	    padding-left: 0px;
	    line-height: 2em;
	    float: left;
	    font-weight: 800;
	    cursor: pointer;
	
	}
	/* JQM theme overrides */
	.ui-page .ui-content {
		padding-left:1em;
		padding-right:1em;
	}
	.ui-page .ui-collapsible-content {
		padding-left:0;
		padding-right:0;
	}
	.ui-header, .ui-footer, .ui-popup-container {
		background:white;
	}
	.ui-header, .ui-footer, .ui-header-fixed, .ui-footer-fixed {
		border: none;
		border-style: none;
		border-width: 0;
	}
	
	* {
		-webkit-column-rule-color: rgb(0, 0, 0);
		-webkit-text-emphasis-color: rgb(0, 0, 0);
		-webkit-text-fill-color: rgb(0, 0, 0);
		-webkit-text-stroke-color: rgb(0, 0, 0);
		border-bottom-color: rgb(0, 0, 0);
		border-left-color: rgb(0, 0, 0);
		border-right-color: rgb(0, 0, 0);
		border-top-color: rgb(0, 0, 0);
		color: rgb(0, 0, 0);
		outline-color: rgb(0, 0, 0);
		}
	.ui-page .ui-corner-all {
		-webkit-border-radius: 0em;
		border-radius: 0em;
		border: none
	}
	.ui-popup .ui-btn-icon-notext.ui-btn-corner-all, .ui-popup .ui-btn-icon-notext.ui-corner-all {
		-webkit-border-radius: 1em;
		border-radius: 1em
	}
	.ui-popup .ui-corner-all {
		-webkit-border-radius: .3125em;
		border-radius: .3125em
	}
	.restaurantName img, .poweredBy img {
		height:20px;
		width:20px;
		vertical-align: text-bottom;
	}
	
	.header-content .right-controlgroup .ui-controlgroup-controls a:last-child {
		padding-right:0;
	}
	
	.right-controlgroup {
		float:right;
	}
	
	#email-check {
		background: url("shared/css/images/at.png") no-repeat scroll center 0 rgba(0, 0, 0, 0);
		height: auto;
		padding: 25px;
		text-align: center;
		z-index: 100;
	}
	.ui-shadow {
		-webkit-box-shadow: 0 1px 0 rgba(0, 0, 0, .15);
		-moz-box-shadow: 0 1px 0 rgba(0, 0, 0, .15);
		box-shadow: 0 1px 0px rgba(0, 0, 0, .15);
	}
    
    /* #### Mobile Phones Portrait #### */
	@media screen and (max-device-width: 480px) and (orientation: portrait){
		.restaurantName, #delivery-btn {
			display:block;
		}
		#splash {
			background-image: url('images/SD_IphoneSplash-01.jpg');
		}
	}

	/* #### Mobile Phones Landscape #### */
	@media screen and (max-device-width: 640px) and (orientation: landscape) {
		#splash {
			background-image: url('images/SD_IphoneSplash-02.jpg');
		}
	}

	/* #### iPhone 4+ Portrait #### */
	@media screen and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait) {
	.restaurantName, #delivery-btn {
			display:block;
		}
		#splash {
			background-image: url('images/SD_IphoneSplash-01.jpg');
		}
	}

	/* #### iPhone 4+ Landscape #### */
	@media screen and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: landscape) {
		#splash {
			background-image: url('images/SD_IphoneSplash-02.jpg');
		}
	}

	/* #### Tablets Portrait #### */
	@media screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {
		#splash {
			background-image: url('images/SD_TabletSplash-01.jpg');
		}
	}

	/* #### Tablets Landscape #### */
	@media screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) {
		#splash {
			background-image: url('images/SD_TabletSplash-02.jpg');
		}
	}

	 /* #### Desktops ####  */
	@media screen and (min-width: 1024px){
		#splash {
			background-image: url('images/SD_TabletSplash-02.jpg');
		}
	}
	/* #### Tablets and Desktops #### */
	@media screen and (max-device-width: 768px) {
		.shortDescription { 
			display: block;
		}
		.longDescription { 
			display: none;
		}
	}
	/* #### Desktops #### */
	/* The large images don't display properly on a Galaxy Note with a resolution of 1280x800 */
	@media screen and (min-device-width: 1024px) {
		.shortDescription { 
			display: none;
		}
		.longDescription { 
			display: block;
		}
		li {
			margin-top: 16px;
			margin-bottom: 16px;
		}
		li img {
			height: 200px;
			min-height: 200px;
			width:200px;
			min-width: 200px;
		}
		.itemContent {
			padding-left: 120px;
			min-height: 200px;
		}
		.itemDetailContent {
			width: 200px;
		}
		.ui-listview img, .ui-li-static img {
			width: 200px;
			height: 200px;
		}
		
	}
	.print {display:none}
	@media print {  
		.section {
			float:left;
			display:inline;
			width:600px;
			margin-right:10px;
		}
		br {display:none}
		.shortDescription { 
			display: none;
		}
		.longDescription { 
			display: block;
		}
		li {
			margin-top: 16px;
			margin-bottom: 16px;
		}
		li img {
			height: 200px;
			min-height: 200px;
			width:200px;
			min-width: 200px;
		}
		.itemContent {
			padding-left: 120px;
			min-height: 200px;
		}
		.itemDetailContent {
			width: 200px;
		}
		.ui-listview img, .ui-li-static img {
			width: 200px;
			height: 200px;
		}
		#order-button {display:none}
		.print {display:inline} 
		.ui-listview, .ui-li-static {-webkit-region-break-inside: avoid;page-break-inside: avoid;page-break-after: always;}
		h3.sectionTitle {page-break-before: always;}
		.addItemToOrder {display:none}
		.ui-footer {display:none}
	}  
	[contenteditable="plaintext-only"] { outline: 1px dashed #CCC; }
	[contenteditable="plaintext-only"]:hover { outline: 1px dashed #0090D2; }

        </style>
    </head>
    <body style="    justify-content: center;
    display: grid;
    transform-origin: center;
    width: 100%;">
    
    <div data-role="header" data-id="globalHeader" data-theme="d" data-position="fixed" style=" box-shadow: 1px 1px 1px 1px #d5d5d59e;   position: absolute;background: #ffffff !important; width:100%">
			<div class="header-content" style="    max-width: 100% !important;padding-right: 20px;">
			 <div class="restaurantName"><span class="restaurantLink"></span> 
			<span class="poweredBy">
			<a target="cs" href="http://www.coursedge.org">
			<img alt="Powered by CoursEdge" style="width: 130px !important; height: 32px;padding-left: 10px; " src="../images/ce.png"/></a>
			</span></div> 
			</div>
	</div>
	 <div data-role="header" data-id="globalHeader" data-theme="d" data-position="fixed" style="background: #ffffff !important;     margin-top: 150px;width:100%">
        <form action='/ednxt/restaurant/charge'  th:object="${chargeRequest}" method='POST' id='checkout-form'>
           
            <input type="hidden" value='${checkId}' name='checkId'/>	
            <!-- NOTE: data-key/data-amount/data-currency will be rendered by Thymeleaf -->
            <script
                src='https://checkout.stripe.com/checkout.js' 
                class='stripe-button'
                data-key='${stripePublicKey}'
                data-amount='${amount}'
                data-currency='${currency}'     
                data-name='${univName}'
                data-description='Payment checkout'
                data-image='../images/favicon.png'
                data-locale='auto'
                data-zip-code='false'
                data-email='${email}'
                th:attr='data-key=${stripePublicKey}, 
		        data-amount=${amount}, 
		        data-currency=${currency}'
                >
            </script>
            <script  type= text/javascript >
			document.querySelector('.stripe-button-el').click();
			</script>
            <style type="text/css">

			.stripe-button-el{
				/* color: white;
			    width: 200px;
			    font-size: 16px;
			    cursor:pointer;
			    border-radius: 10px;
			    height: 50px; */
			    /* background:#F37254; */
			}

			</style>
        </form>
        
        </div>
    </body>
</html>