var currency_map = {
    'Australia Dollar':'&#36',
    'Canada Dollars':'&#36',
    'Denars':'&#1076;&#1077;&#1085',
    'Euro':'&#8364',
    'Francs':'&#8355',
    'HongKong Dollars':'&#22291',
    'Indian Rupee': '&#8377',
    'Lire':'&#8356',
    'Nairas':'&#8358',
    'Pounds':'&#163',
    'Rials':'&#65020',
    'Ringgits':'&#82;&#77',
    'Rupiahs':'&#82;&#112',
    'Shillings':'&#83',
    'Switzerland Francs':'&#67;&#72;&#70',
    'Taiwan Dollars':'&#78;&#84;&#36',
    'US Dollar':'&#36',
    'Yen':'&#165'
};


